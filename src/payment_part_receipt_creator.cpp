
#include <qrinvoice/payment_part_receipt_creator.hpp>
#include <qrinvoice/output/payment_part_receipt.hpp>
#include <qrinvoice/model/qr_invoice.hpp>
#include <qrinvoice/page_size.hpp>
#include <qrinvoice/font_family.hpp>

#include <paymentpart/write_payment_part_receipt_internal.hpp>
#include <paymentpart/writer_options_internal.hpp>
#include <paymentpart/layout_helper_internal.hpp>


namespace qrinvoice {

output::payment_part_receipt payment_part_receipt_creator::create_payment_part_receipt() const
{
	payment_part_receipt::writer_options w_options {
		page_size_,
		font_family_,
		output_format_,
		output_resolution_,
		locale_,
		boundary_lines_,
		boundary_line_scissors_,
		boundary_line_separation_text_,
		additional_print_margin_,
		payment_part_receipt::choose_layout(qr_invoice_)
	};

	return payment_part_receipt::write(w_options, qr_invoice_);
}

} // namespace qrinvoice

