
#ifndef QR_INVOICE_PAYMENT_PART_RECEIPT_LAYOUT_DEFINITIONS_INTERNAL_HPP
#define QR_INVOICE_PAYMENT_PART_RECEIPT_LAYOUT_DEFINITIONS_INTERNAL_HPP

#include <array>
#include <set>
#include <qrinvoice/currency.hpp>

#include "../qr_invoice_swiss_qr_code_internal.hpp"
#include "../layout/dimension_internal.hpp"
#include "../system/libqrencode/error_correction_level.hpp"


/**
 * Dimensions are in millimeters, font-sizes in points.
 */
namespace qrinvoice {
namespace payment_part_receipt {
namespace layout_definitions {

    /**
     * Decimal separator according to the spec 4.3.3 - Amount section - v2.0
     */
    constexpr const char amount_decimal_separator = '.';

    /**
     * Thousands separator according to the spec 3.5.3 - Amount section - v2.0
     */
    constexpr const char* amount_thousands_separator = " ";

    // ----------------------------------------------------------------------------------------------------------
    // Font sizes
    // ----------------------------------------------------------------------------------------------------------
    /**
     * Fixed font size (in bold) for the title section of both receipt and payment part according to the spec 3.5.1 and 3.6.1 - v2.0
     */
    constexpr unsigned int font_size_title = 11;
    /**
     * Fixed font size for the separation label obove the payment part - size not specified in the spec v2.0
     */
    constexpr unsigned int font_size_separation_label = 7;
    /**
     * Smallest possible font size according to the spec 3.4 - Fonts and font sizes - v2.0
     */
    constexpr unsigned int font_size_min = 6;
    /**
     * Smallest possible font size for values according to the spec 3.4 - Fonts and font sizes - v2.0 (smallest font size is 6 and heading must be 2pt smaller than value -&gt; 6 + 2 = 8)
     */
    constexpr unsigned int font_size_difference_values_heading = 2;

    constexpr unsigned int receipt_font_size_heading_min = font_size_min;
    constexpr unsigned int receipt_font_size_value_min = receipt_font_size_heading_min + font_size_difference_values_heading;

    constexpr unsigned int payment_part_font_size_heading_recommended = 8;
    constexpr unsigned int payment_part_font_size_value_recommended = payment_part_font_size_heading_recommended + font_size_difference_values_heading;

    /**
     * Fixed font size for the further information section 3.5.5 - v2.0
     */
    constexpr unsigned int payment_part_font_size_further_info = 7;

    // ----------------------------------------------------------------------------------------------------------
    // Paragraph & line spacing
    // ----------------------------------------------------------------------------------------------------------

    /** Line spacing is 9 according to the qr bill style guide - page 15 (version 16.01.2019) - we take 9 - 6 (font-size) as leading 3 */
    constexpr unsigned int receipt_heading_line_spacing_pts = 3;
    /** Line spacing is 9 according to the qr bill style guide - page 15 (version 16.01.2019) - we take 9 - 8 (font-size) as leading 1 */
    constexpr unsigned int receipt_value_line_spacing_pts = 1;
    /** Line spacing is 9 according to the qr bill style guide - page 15 (version 16.01.2019) */
    constexpr unsigned int receipt_paragraph_spacing_pts = 9;
    /** Line spacing is 11 according to the qr bill style guide - page 15 (version 16.01.2019) - we take 11 - 8 (font-size) as leading 3 */
    constexpr unsigned int receipt_amount_line_spacing_pts = 3;

    /** Line spacing is 11 according to the qr bill style guide - page 15 (version 16.01.2019) - we take 11 - 8 (font-size) as leading 3 */
    constexpr unsigned int payment_part_heading_line_spacing_pts = 3;
    /** Line spacing is 11 according to the qr bill style guide - page 15 (version 16.01.2019) - we take 11 - 10 (font-size) as leading 1 */
    constexpr unsigned int payment_part_value_line_spacing_pts = 1;
    /** Line spacing is 11 according to the qr bill style guide - page 15 (version 16.01.2019) */
    constexpr unsigned int payment_part_paragraph_spacing_pts = 11;
    /** Line spacing is 13 according to the qr bill style guide - page 15 (version 16.01.2019) - we take 13 - 10 (font-size) as leading 3 */
    constexpr unsigned int payment_part_amount_line_spacing_pts = 3;
    /** Line spacing is 8 according to the qr bill style guide - page 15 (version 16.01.2019) - we take 8 - 7 (font-size) as leading 3 */
    constexpr unsigned int payment_part_further_info_line_spacing_pts = 1;

    // ----------------------------------------------------------------------------------------------------------
    // Layout
    // ----------------------------------------------------------------------------------------------------------
    /**
     * Min 5mm quiet space according to the spec 3.5 - Sections of the payment part - v2.0
     */
    constexpr unsigned int quiet_space = 5u;
    constexpr unsigned int quiet_space_doubled = quiet_space * 2;
    /**
     * Basically we fully adhere to the style guide, which state 5mm (quiet space) as a print margin.
     * However tests with printers and print services have shown that 5mm might just not be enough, adding an extra mm solves most of these issues.
     */
    constexpr unsigned int additional_print_margin = 1u;

    constexpr unsigned int box_corner_line_length = 3;
    /**
     * line thickness 0.75 pt according to the spec v2.0 (equals ~0.2646mm)
     */
    constexpr double box_corner_line_width = 0.2646;

    constexpr unsigned int scissor_length = 5;

    constexpr qrinvoice::layout::dimension<unsigned int> receipt{62u, 105u};
    constexpr qrinvoice::layout::dimension<unsigned int> payment_part{148u, 105u};
    constexpr qrinvoice::layout::dimension<unsigned int> receipt_payment_part{210u, 105u};

    constexpr unsigned int approx_max_line_length_payment_part_info_section = 40;

    // ----------------------------------------------------------------------------------------------------------
    // Receipt
    // ----------------------------------------------------------------------------------------------------------

    /**
     * Size according to the spec 3.6.2 - Information section - v2.0
     */
    constexpr qrinvoice::layout::dimension<unsigned int> receipt_debtor_field {52u, 20u};
    /**
     * Size according to the spec 3.6.3 - Amount section - v2.0
     */
    constexpr qrinvoice::layout::dimension<unsigned int> receipt_amount_field {30u, 10u};
    /**
     * Height is 14mm according to the qr bill style guide - page 6 (version 16.01.2019)
     */
    constexpr qrinvoice::layout::dimension<unsigned int> receipt_amount_section {receipt.get_width() - quiet_space_doubled, 14u};
    /**
     * Size of the acceptance section is not explicitly specified in the spec 3.6.4 - Acceptance point section - v2.0<br>
     * Width is obviously the width of the receipt - two times the quiet space.<br>
     * Height is 18mm according to the qr bill style guide - page 6 (version 16.01.2019)
     */
    constexpr qrinvoice::layout::dimension<unsigned int> receipt_acceptance_section {receipt.get_width() - quiet_space_doubled, 18u};
    /**
     * Size of the title section is not explicitly specified in the spec 3.6.1 - Title section - v2.0<br>
     * Width is obviously the width of the receipt - two times the quiet space.<br>
     * Height is 7mm according to the qr bill style guide - page 6 (version 16.01.2019)
     */
    constexpr qrinvoice::layout::dimension<unsigned int> receipt_title_section {receipt.get_width() - quiet_space_doubled, 7u};
    /**
     * Height is 56mm according to the qr bill style guide - page 6 (version 16.01.2019)
     */
    constexpr qrinvoice::layout::dimension<unsigned int> receipt_information_section {receipt.get_width() - quiet_space_doubled, 56u};

    // ----------------------------------------------------------------------------------------------------------
    // PaymentPart
    // ----------------------------------------------------------------------------------------------------------

    /**
     * Size according to the spec 3.5.4 - Information section - v2.0
     */
    constexpr qrinvoice::layout::dimension<unsigned int> payment_part_debtor_field {65u, 25u};
    /**
     * Size according to the spec 3.5.3 - Amount section - v2.0
     */
    constexpr qrinvoice::layout::dimension<unsigned int> payment_part_amount_field {40u, 15u};

    /**
     * Left column in the payment part consist of the title, the swiss qr code and the amount section.<br>
     * Due to the swiss qr code and the quiet space, we need the column to be at least the size of the qr_code (46mm) plus the quiet space (5mm).<br>
     * Width is 51mm according to the qr bill style guide - page 6 (version 16.01.2019)
     */
    constexpr unsigned int payment_part_left_col = swiss_qr_code::qr_code_size.get_width() + quiet_space;
    /**
     * In the right column of the payment part, there is only the information section.<br>
     * It takes the space that is left from the payment part minus left column minus quiet space on both sides of the payment part.<br>
     * Width is 87mm according to the qr bill style guide - page 6 (version 16.01.2019)
     */
    constexpr unsigned int payment_part_right_col = payment_part.get_width() - quiet_space_doubled - payment_part_left_col;
    /**
     * Size of the title section is not explicitly specified in the spec 3.5.1 - Title section - v2.0<br>
     * Width is about the qr code + 2 times the quiet space we added as distance from the qr code to the information section<br>
     * Height is 7mm according to the qr bill style guide - page 6 (version 16.01.2019)
     */
    constexpr qrinvoice::layout::dimension<unsigned int> payment_part_title_section {payment_part_left_col, 7u};
    /**
     * Height is 22mm according to the qr bill style guide - page 6 (version 16.01.2019)
     */
    constexpr qrinvoice::layout::dimension<unsigned int> payment_part_amount_section {payment_part_left_col, 22u};
    /**
     * Height is 56mm according to the qr bill style guide - page 6 (version 16.01.2019)
     */
    constexpr qrinvoice::layout::dimension<unsigned int> payment_part_qr_section {payment_part_left_col, swiss_qr_code::qr_code_size.get_width() + quiet_space_doubled};

    /**
     * Size of the further information section is not explicitly specified in the spec 3.5.5 - Title section - v2.0<br>
     * Width is the payment part width minus 2 times the quiet space (left and right)<br>
     * Height is 10mm according to the qr bill style guide - page 6 (version 16.01.2019)
     */
    constexpr qrinvoice::layout::dimension<unsigned int> payment_part_further_info_section {payment_part.get_width() - quiet_space_doubled, 10u};

    /**
     * Width is the payment part width minus 2 times the quiet space (left and right) minus swiss qr code section<br>
     * Height is the payment part height minus 2 times the quiet space minus the info section, or 85mm according to the qr bill style guide - page 6 (version 16.01.2019)
     */
    constexpr qrinvoice::layout::dimension<unsigned int> payment_part_information_section {
                payment_part_right_col,
                        payment_part.get_height() //
                        - quiet_space_doubled
                        - payment_part_further_info_section.get_height()
            };

    constexpr const char* more_text_indicator = "...";

} // namespace layout_definitions
} // namespace payment_part_receipt
} // namespace qrinvoice


#endif // QR_INVOICE_PAYMENT_PART_RECEIPT_LAYOUT_DEFINITIONS_INTERNAL_HPP

