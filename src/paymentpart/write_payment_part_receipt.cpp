
#include <memory>
#include <qrinvoice/not_yet_implemented_exception.hpp>
#include <qrinvoice/model/mapper/model_to_swiss_payments_code.hpp>

#include <qrcode/swiss_qr_code_writer_internal.hpp>
#include <paymentpart/writer_internal.hpp>
#include <paymentpart/write_payment_part_receipt_internal.hpp>
#include <paymentpart/writer_options_internal.hpp>
#include <paymentpart/graphics_writer_internal.hpp>
#include <pdf_writer/hummus_writer_internal.hpp>


namespace qrinvoice {
namespace payment_part_receipt {

output::payment_part_receipt write(const writer_options& writer_options, const model::qr_invoice& qr_invoice)
{
	auto writer = select_writer(writer_options);

	const std::string spc = map_model_to_swiss_payments_code(qr_invoice).to_swiss_payments_code_string();
	const qrinvoice::qrcode::qr_code_bitmap qr_code_image = qrcode::swiss_qr_code_writer{}.write_bitmap(spc);

	return writer->write(qr_invoice, qr_code_image);
}

auto select_writer(const writer_options& writer_options) -> std::unique_ptr<writer>
{
	switch (writer_options.output_format) {
	case output_format::pdf:
		return std::make_unique<hummus_writer>(writer_options);
	case output_format::png:
	case output_format::jpg:
	case output_format::bmp:
		return std::make_unique<graphics_writer>(writer_options);
	}

	throw not_yet_implemented_exception{
		std::string{"Output Format \""} + output_format_ops::get_mime_type(writer_options.output_format) +
		"\" has not yet been implemented"
	};
}


} // namespace payment_part_receipt
} // namespace qrinvoice

