
#ifndef QR_INVOICE_WRITER_OPTIONS_INTERNAL_HPP
#define QR_INVOICE_WRITER_OPTIONS_INTERNAL_HPP

#include <qrinvoice/page_size.hpp>
#include <qrinvoice/font_family.hpp>
#include <qrinvoice/output_format.hpp>
#include <qrinvoice/output_resolution.hpp>
#include <qrinvoice/locale.hpp>
#include <qrinvoice/boundary_lines.hpp>

#include <paymentpart/layout_internal.hpp>


namespace qrinvoice {
namespace payment_part_receipt {

struct writer_options {
	page_size page_size;
	font_family font_family;
	output_format output_format;
	output_resolution output_resolution;
	qrinvoice::locale locale;
	boundary_lines boundary_lines;
    bool boundary_line_scissors;
 	bool boundary_line_separation_text;
 	bool additional_print_margin;
	layout layout;
};


} // namespace payment_part_receipt
} // namespace qrinvoice


#endif // QR_INVOICE_WRITER_OPTIONS_INTERNAL_HPP

