
#ifndef QR_INVOICE_PAYMENT_PART_LAYOUT_HELPER_INTERNAL_HPP
#define QR_INVOICE_PAYMENT_PART_LAYOUT_HELPER_INTERNAL_HPP

#include <vector>
#include <string>
#include <qrinvoice/model/qr_invoice.hpp>
#include <qrinvoice/model/address.hpp>

#include <paymentpart/layout_internal.hpp>


namespace qrinvoice {
namespace payment_part_receipt {


layout choose_layout(const model::qr_invoice& qr_invoice);
int calculate_layout_points(const model::qr_invoice& qr_invoice);
int calculate_layout_points(const model::address& address);
int calculate_layout_points(int threshold, const std::vector<std::string>& strs);

bool compressed_layout(const layout& lo);
bool uncompressed_layout(const layout& lo);

std::string layout_as_string(const layout& layout);

} // namespace payment_part_receipt
} // namespace qrinvoice


#endif // QR_INVOICE_PAYMENT_PART_LAYOUT_HELPER_INTERNAL_HPP

