
#ifndef QR_INVOICE_GRAPHICS_WRITER_INTERNAL_HPP
#define QR_INVOICE_GRAPHICS_WRITER_INTERNAL_HPP

#include <string>
#include <qrinvoice/output/payment_part_receipt.hpp>
#include <qrinvoice/output_resolution.hpp>
#include <qrinvoice/model/qr_invoice.hpp>
#include <qrinvoice/model/payment_amount_info.hpp>

#include <qr_invoice_swiss_qr_code_internal.hpp>
#include <paymentpart/payment_part_bitmap.hpp>
#include <paymentpart/writer_internal.hpp>
#include <paymentpart/writer_options_internal.hpp>
#include <layout/point_internal.hpp>
#include <layout/dimension_internal.hpp>
#include <layout/dimension_unit_utils_internal.hpp>
#include <layout/rect_internal.hpp>
#include <system/i18n/translator.hpp>
#include <system/freetype/freetype.hpp>

#include <spdlog/spdlog.h>

namespace qrinvoice {
    namespace payment_part_receipt {


        class graphics_writer : public writer {
        public:
            explicit graphics_writer(const writer_options& options);
            ~graphics_writer() override;

            output::payment_part_receipt write(const model::qr_invoice& qr_invoice, const qrinvoice::qrcode::qr_code_bitmap& qr_code_bitmap) override;

        private:
            void set_font_size(FT_Face face, const unsigned int font_size) const;
            const qrinvoice::layout::dimension<unsigned> init_qr_code() const;

            const qrinvoice::layout::rect<unsigned> init_receipt_title_sect_rect() const;
            const qrinvoice::layout::rect<unsigned> init_receipt_info_sect_rect() const;
            const qrinvoice::layout::rect<unsigned> init_receipt_amount_sect_rect() const;
            const qrinvoice::layout::rect<unsigned> init_receipt_acceptance_sect_rect() const;

            const qrinvoice::layout::rect<unsigned> init_payment_part_title_sect_rect() const;
            const qrinvoice::layout::rect<unsigned> init_payment_part_qr_sect_rect() const;
            const qrinvoice::layout::rect<unsigned> init_payment_part_info_sect_rect() const;
            const qrinvoice::layout::rect<unsigned> init_payment_part_amount_sect_rect() const;
            const qrinvoice::layout::rect<unsigned> init_payment_part_further_info_sect_rect() const;

            void add_receipt_title_section(payment_part_bitmap &img) const;
            void add_receipt_info_section(payment_part_bitmap &img, const model::qr_invoice &) const;
            void add_receipt_amount(payment_part_bitmap &img, const model::payment_amount_info &) const;
            void add_receipt_acceptance_section(payment_part_bitmap &img) const;

            void write_receipt_info_section_title(payment_part_bitmap &img, float * y_pos, const std::string &text) const;
            void write_receipt_info_section_title(payment_part_bitmap &img, float * y_pos, const std::string &text, bool first_heading) const;
            void write_receipt_info_section_value(payment_part_bitmap &img, float * y_pos, const std::string &text) const;

            void add_payment_part_title_section(payment_part_bitmap &img) const;
            void add_payment_part_qr_code_image(payment_part_bitmap &img, const qrinvoice::qrcode::qr_code_bitmap& qr_code_bitmap) const;
            void add_payment_part_amount(payment_part_bitmap &img, const model::payment_amount_info &) const;
            void add_payment_part_info_section(payment_part_bitmap &img, const model::qr_invoice &) const;
            void add_payment_part_further_info_section(payment_part_bitmap &img, const model::qr_invoice &) const;

            void write_payment_part_info_section_title(payment_part_bitmap &img, float * y_pos, const std::string &text) const;
            void write_payment_part_info_section_title(payment_part_bitmap &img, float * y_pos, const std::string &text, bool first_heading) const;
            void write_payment_part_info_section_value(payment_part_bitmap &img, float * y_pos, const std::string &text) const;

            std::string graphics_writer::trim_if_required(const std::string& input, float max_width, FT_Face font) const;

            void write_free_text_box(payment_part_bitmap &img, qrinvoice::layout::rect<unsigned>) const;
            void debug_layout(payment_part_bitmap &img, qrinvoice::layout::rect<unsigned>) const;

            void write_separation_label(payment_part_bitmap &img) const;
            void draw_boundary_lines(payment_part_bitmap &img) const;
            void draw_horizontal_boundary_line(payment_part_bitmap &img) const;
            void draw_vertical_boundary_line(payment_part_bitmap &img) const;

        private:
            unsigned get_page_canvas_width() const;
            unsigned get_page_canvas_height() const;

            unsigned get_receipt_height() const;

            unsigned get_payment_part_width() const;
            unsigned get_payment_part_height() const;

            qrinvoice::layout::dimension<unsigned> mm_to_px(qrinvoice::layout::dimension<unsigned> mm_dim) const;

            static const unsigned calculate_additional_print_margin(bool additional_margin, unsigned dpi);
            static qrinvoice::layout::dimension<unsigned> calculate_page_canvas_dimensions(page_size, unsigned dpi);

            const unsigned int translate_y(const unsigned int y) const;
            float translate_y(float y) const;

            std::string label(const std::string& id) const;

            array2d crop_image(array2d &canvas) const;
            void invert_colors(array2d &canvas) const;

        private:
            const payment_part_receipt::writer_options		opts_;
            const unsigned					dpi_;
            const qrinvoice::layout::dimension<unsigned>	page_canvas_;

            const std::string				font_path_title_;
            const std::string				font_path_heading_;
            const std::string				font_path_value_;

            FT_Library 						ft_library_;
            FT_Face 						font_title_;
            FT_Face 						font_heading_;
            FT_Face 						font_value_;

            const qrinvoice::layout::dimension<unsigned>	din_lang_landscape_px_;
            const qrinvoice::layout::point<unsigned>	root_lower_left_;
            const qrinvoice::layout::point<unsigned>	payment_part_lower_left_;

            const unsigned					boundary_line_width_;
            const unsigned					box_corner_line_width_;
            const unsigned					box_corner_line_length_;

            const unsigned					quiet_space_px_;
            const unsigned					additional_print_margin_px_;
            const unsigned					print_margin_px_;

            const qrinvoice::layout::dimension<unsigned>	receipt_amount_field_;
            const qrinvoice::layout::dimension<unsigned>	receipt_debtor_field_;

            const qrinvoice::layout::dimension<unsigned>	payment_part_amount_field_;
            const qrinvoice::layout::dimension<unsigned>	payment_part_debtor_field_;

            const qrinvoice::layout::dimension<unsigned>		qr_code_;

            const qrinvoice::layout::rect<unsigned>		receipt_title_section_rect_;
            const qrinvoice::layout::rect<unsigned>		receipt_info_section_rect_;
            const qrinvoice::layout::rect<unsigned>		receipt_amount_section_rect_;
            const qrinvoice::layout::rect<unsigned>		receipt_acceptance_section_rect_;

            const qrinvoice::layout::rect<unsigned>		payment_part_title_section_rect_;
            const qrinvoice::layout::rect<unsigned>		payment_part_qr_section_rect_;
            const qrinvoice::layout::rect<unsigned>		payment_part_info_section_rect_;
            const qrinvoice::layout::rect<unsigned>		payment_part_amount_section_rect_;
            const qrinvoice::layout::rect<unsigned>		payment_part_further_info_section_rect_;

            const system::i18n::translator			tr_;

        };

        inline unsigned graphics_writer::get_page_canvas_width() const
        {
            return page_canvas_.get_width();
        }

        inline unsigned graphics_writer::get_page_canvas_height() const
        {
            return page_canvas_.get_height();
        }

        inline unsigned graphics_writer::get_payment_part_width() const
        {
            return din_lang_landscape_px_.get_width();
        }

        inline unsigned graphics_writer::get_payment_part_height() const {
            return din_lang_landscape_px_.get_height();
        }

        inline unsigned graphics_writer::get_receipt_height() const {
            return din_lang_landscape_px_.get_height();
        }

        inline qrinvoice::layout::dimension<unsigned> graphics_writer::mm_to_px(qrinvoice::layout::dimension<unsigned> mm_dim) const
        {
            using namespace qrinvoice::layout::dimension_unit_utils;
            return {
                    millimeters_to_points_rounded(mm_dim.get_width(), dpi_),
                    millimeters_to_points_rounded(mm_dim.get_height(), dpi_)
            };
        }

// this function is to be called in the constructor, it assumes that
// quiet_space_px_ & amount_section_ are already initialized
// TODO unused? translate?
        inline const qrinvoice::layout::dimension<unsigned> graphics_writer::init_qr_code() const
        {
            return mm_to_px(swiss_qr_code::qr_code_size);
        }

        inline const unsigned int graphics_writer::translate_y(const unsigned int y) const
        {
            return get_page_canvas_height() - y;
        }

        inline float graphics_writer::translate_y(const float y) const
        {
            return static_cast<float>(get_page_canvas_height()) - y;
        }

        inline int half_floored(int input) {
            return static_cast<int>(std::floor(input / 2.0f));
        }

        inline int half_rounded(int input) {
            return static_cast<int>(std::round(input / 2.0f));
        }

    } // namespace payment_part_receipt
} // namespace qrinvoice


#endif // QR_INVOICE_GRAPHICS_WRITER_INTERNAL_HPP

