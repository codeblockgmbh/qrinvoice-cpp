#include <string>
#include <algorithm>
#include <codecvt>

#include <resource/scissor.hpp>
#include <qrinvoice/font_family.hpp>
#include <qrinvoice/font_style.hpp>
#include <qrinvoice/boundary_lines.hpp>
#include <qrinvoice/output/payment_part_receipt.hpp>
#include <qrinvoice/model/qr_invoice.hpp>
#include <qrinvoice/layout/layout_exception.hpp>

#include <qrcode/swiss_qr_code_writer_internal.hpp>

#include <paymentpart/graphics_writer_internal.hpp>
#include <paymentpart/layout_definitions_internal.hpp>
#include <fonts/font_manager_internal.hpp>
#include <util/text_util_internal.hpp>
#include <model/util/address_utils_internal.hpp>
#include <model/util/alternative_schemes_utils_internal.hpp>
#include <qrinvoice/util/reference_util.hpp>
#include <util/payment_reference_util_internal.hpp>
#include <util/iban_util_internal.hpp>
#include <util/amount_util_internal.hpp>
#include <config/env_vars_internal.hpp>
#include "layout_helper_internal.hpp"

#include <system/stb/stbi_helper.hpp>

namespace qrinvoice {
    namespace payment_part_receipt {

        namespace du = qrinvoice::layout::dimension_unit_utils;

        graphics_writer::graphics_writer(const writer_options& w_options)
                :opts_{ w_options }
                ,dpi_{ qrinvoice::get_resolution_dpi(opts_.output_resolution) }
                ,page_canvas_{ calculate_page_canvas_dimensions(opts_.page_size, dpi_) }
                ,font_path_title_{ font_manager::get_font_file_path(opts_.font_family, qrinvoice::font_style::bold) }
                ,font_path_heading_{ font_manager::get_font_file_path(opts_.font_family, qrinvoice::font_style::bold)}
                ,font_path_value_{ font_manager::get_font_file_path(opts_.font_family, qrinvoice::font_style::regular) }
                // init all dimensions
                ,din_lang_landscape_px_{ mm_to_px(layout_definitions::receipt_payment_part) }
                // determine the lower left position of the payment part on the page canvas
                ,root_lower_left_{ page_canvas_.get_width() - din_lang_landscape_px_.get_width(), 0 }
                ,payment_part_lower_left_{ page_canvas_.get_width() - mm_to_px(layout_definitions::payment_part).get_width(), 0 }
                ,boundary_line_width_{ du::points_to_pixels(0.25, dpi_) }
                ,box_corner_line_width_{ du::millimeters_to_points_rounded(layout_definitions::box_corner_line_width, dpi_) }
                ,box_corner_line_length_{ du::millimeters_to_points_rounded(layout_definitions::box_corner_line_length, dpi_) }

                ,quiet_space_px_{ du::millimeters_to_points_rounded(layout_definitions::quiet_space, dpi_) }
                ,additional_print_margin_px_{ calculate_additional_print_margin(opts_.additional_print_margin, dpi_) }
                ,print_margin_px_{ du::millimeters_to_points_rounded(layout_definitions::quiet_space, dpi_) + calculate_additional_print_margin(opts_.additional_print_margin, dpi_) }

                ,receipt_debtor_field_{ mm_to_px(layout_definitions::receipt_debtor_field) }
                ,receipt_amount_field_{ mm_to_px(layout_definitions::receipt_amount_field) }
                ,payment_part_amount_field_{ mm_to_px(layout_definitions::payment_part_amount_field) }
                ,payment_part_debtor_field_{ mm_to_px(layout_definitions::payment_part_debtor_field) }

                ,qr_code_{ init_qr_code() }

                ,receipt_title_section_rect_{ init_receipt_title_sect_rect() }
                ,receipt_info_section_rect_{ init_receipt_info_sect_rect() }
                ,receipt_amount_section_rect_{ init_receipt_amount_sect_rect() }
                ,receipt_acceptance_section_rect_{ init_receipt_acceptance_sect_rect() }

                ,payment_part_title_section_rect_{ init_payment_part_title_sect_rect() }
                ,payment_part_qr_section_rect_{ init_payment_part_qr_sect_rect() }
                ,payment_part_info_section_rect_{ init_payment_part_info_sect_rect() }
                ,payment_part_amount_section_rect_{ init_payment_part_amount_sect_rect() }
                ,payment_part_further_info_section_rect_{ init_payment_part_further_info_sect_rect() }
                ,tr_{ opts_.locale } {
            spdlog::trace("graphics_writer::graphics_writer - before loading fonts");

            FT_Error error;
            error = FT_Init_FreeType(&ft_library_);
            if (error) {
                throw qrinvoice::technical_exception{"Unable to initialize freetype"};
            }

            font_title_ = font_value_;
            font_heading_ = font_value_;
            // TITLE
            // furthermore, font might be cached longer than on a per execution basis
            error = FT_New_Face(ft_library_, font_path_title_.c_str(), 0, &font_title_);
            if (error) {
               throw qrinvoice::technical_exception{"Unable to load font from path " + font_path_title_};
            }

            // HEADING
            if(font_path_heading_ == font_path_title_) {
                // same font as title
                font_heading_ = font_title_;
            } else  {
                error = FT_New_Face(ft_library_, font_path_heading_.c_str(), 0, &font_heading_);
                if (error) {
                    throw qrinvoice::technical_exception{"Unable to load font from path " + font_path_heading_};
                }
            }

            // VALUE
            if(font_path_value_ == font_path_title_) {
                // same font as title
                font_value_ = font_title_;
            } else if (font_path_value_ == font_path_heading_) {
                // same font as title
                font_value_ = font_heading_;
            } else  {
                error = FT_New_Face(ft_library_, font_path_value_.c_str(), 0, &font_value_);
                if (error) {
                    throw qrinvoice::technical_exception{"Unable to load font from path " + font_path_value_};
                }
            }

            FT_Select_Charmap(font_title_,  FT_ENCODING_UNICODE);
            FT_Select_Charmap(font_heading_,  FT_ENCODING_UNICODE);
            FT_Select_Charmap(font_value_,  FT_ENCODING_UNICODE);
            spdlog::trace("graphics_writer::graphics_writer - after loading fonts");
        }

        std::string graphics_writer::trim_if_required(const std::string& input, const float max_width, FT_Face font) const
        {
            auto width = freetype_helper::get_text_width(font, input);
            if(width <= max_width) {
                return input;
            }

            std::string newString = input;
            while (width > max_width) {
                int len = newString.length();
                float ratio_too_wide = width / max_width;
                // make sure string gets shorter in any circumstances
                int new_len = std::min((int) floor(len / ratio_too_wide), (int) newString.length() - 1);

                newString = newString.substr(0, new_len);
                // TODO use regular get width?
                width = freetype_helper::get_text_width(font, newString + layout_definitions::more_text_indicator);
            }

            return newString + layout_definitions::more_text_indicator;
        }

        static const unsigned char WHITE = 255;

        output::payment_part_receipt graphics_writer::write(const model::qr_invoice& qr_invoice, const qrinvoice::qrcode::qr_code_bitmap& qr_code_bitmap)
        {
            spdlog::trace("graphics_writer::write - start");
            payment_part_bitmap img(opts_.page_size, opts_.output_resolution);
            array2d &canvas = img.canvas();
            spdlog::trace("graphics_writer::write - canvas created");

            add_receipt_title_section(img);
            add_receipt_info_section(img, qr_invoice);
            add_receipt_amount(img, qr_invoice.get_payment_amount_info());
            add_receipt_acceptance_section(img);

            add_payment_part_title_section(img);
            add_payment_part_qr_code_image(img, qr_code_bitmap);
            add_payment_part_amount(img, qr_invoice.get_payment_amount_info());
            add_payment_part_info_section(img, qr_invoice);
            add_payment_part_further_info_section(img, qr_invoice);
            draw_boundary_lines(img);

            invert_colors(canvas);

            std::vector<unsigned char> result = {};
            if (opts_.page_size == page_size::din_lang_cropped) {
                array2d result_canvas = crop_image(canvas);
                if(!stbi_helper::saveImageToVector(&result, result_canvas, opts_.output_format)) {
                    throw technical_exception("Error while trying to write image into memory");
                }
            } else {
                if(!stbi_helper::saveImageToVector(&result, canvas, opts_.output_format)) {
                    throw technical_exception("Error while trying to write image into memory");
                }
            }

            spdlog::trace("graphics_writer::write - image written successfully");

            auto bytes = static_cast<const unsigned char*>(result.data());
            return {opts_.output_format, bytes, result.size()};
        }

         array2d graphics_writer::crop_image(array2d &src_canvas) const {
             spdlog::trace("graphics_writer::crop_image - start");
             /*
              * Basically the following cropping is done. Remove print margin on the left, the right and at the bottom.
              * +--------------------------------------+
              * |  |                                |  |
              * |  |        Cropped DIN LANG        |  |
              * |  |                                |  |
              * |  +--------------------------------+  |
              * |                                      |
              * +--------------------------------------+
              */
             const int source_height = src_canvas.get_height();
             const int target_height = source_height - print_margin_px_;
             const int target_width = src_canvas.get_width() - 2 * print_margin_px_;
             array2d target_canvas = array2d(target_height, target_width);
             for (int j = 0; j < source_height; j++) {
                 unsigned char *const rowArrSrc = src_canvas[j];
                 unsigned char *const rowArrTarget = target_canvas[j];
                 if (j < target_height) {
                     for (int i = 0; i < target_width; i++) {
                         rowArrTarget[i] = rowArrSrc[i + print_margin_px_];
                     }
                 }
             }
             spdlog::trace("graphics_writer::crop_image - end");
             return target_canvas;
        }

        void graphics_writer::invert_colors(array2d &canvas) const {
            spdlog::trace("graphics_writer::invert_colors - start");
            for (int j = 0; j < canvas.get_height(); j++) {
                unsigned char *const rowArr = canvas[j];
                for (int i = 0; i < canvas.get_width(); i++) {
                    rowArr[i] = WHITE - rowArr[i];
                }
            }
            spdlog::trace("graphics_writer::invert_colors - end");
        }

// this function is to be called in the constructor, it assumes that following members are already initialized:
// quiet_space_px_, qr_code_, din_lang_landscape_px_, root_lower_left_, page_canvas_

        const qrinvoice::layout::rect<unsigned> graphics_writer::init_receipt_title_sect_rect() const
        {
            const unsigned info_sect_x = quiet_space_px_;
            const unsigned info_sect_y = get_receipt_height() - quiet_space_px_ - mm_to_px(layout_definitions::receipt_title_section).get_height();
            return qrinvoice::layout::rect<unsigned> {info_sect_x, info_sect_y, mm_to_px(layout_definitions::receipt_title_section)
            }.translate(root_lower_left_);
        }

        const qrinvoice::layout::rect<unsigned> graphics_writer::init_receipt_info_sect_rect() const
        {
            const unsigned info_sect_x = quiet_space_px_;
            const unsigned info_sect_y = quiet_space_px_ + mm_to_px(layout_definitions::receipt_acceptance_section).get_height() + mm_to_px(layout_definitions::receipt_amount_section).get_height();
            return qrinvoice::layout::rect<unsigned> {info_sect_x, info_sect_y, mm_to_px(layout_definitions::receipt_information_section)}.translate(root_lower_left_);
        }

        const qrinvoice::layout::rect<unsigned> graphics_writer::init_receipt_amount_sect_rect() const
        {
            const unsigned info_sect_x = quiet_space_px_;
            const unsigned info_sect_y = quiet_space_px_ + mm_to_px(layout_definitions::receipt_acceptance_section).get_height();
            return qrinvoice::layout::rect<unsigned> {info_sect_x, info_sect_y, mm_to_px(layout_definitions::receipt_amount_section)}.translate(root_lower_left_);
        }

        const qrinvoice::layout::rect<unsigned> graphics_writer::init_receipt_acceptance_sect_rect() const
        {
            const unsigned info_sect_x = quiet_space_px_;
            const unsigned info_sect_y = quiet_space_px_;
            return qrinvoice::layout::rect<unsigned> {info_sect_x, info_sect_y, mm_to_px(layout_definitions::receipt_acceptance_section)}.translate(root_lower_left_);
        }

        const qrinvoice::layout::rect<unsigned> graphics_writer::init_payment_part_title_sect_rect() const
        {
            const unsigned info_sect_x = quiet_space_px_;
            const unsigned info_sect_y = get_payment_part_height() - quiet_space_px_ - mm_to_px(layout_definitions::payment_part_title_section).get_height();
            return qrinvoice::layout::rect<unsigned> {info_sect_x, info_sect_y, mm_to_px(layout_definitions::payment_part_title_section)}.translate(payment_part_lower_left_);
        }

        const qrinvoice::layout::rect<unsigned> graphics_writer::init_payment_part_qr_sect_rect() const
        {
            const unsigned info_sect_x = quiet_space_px_;
            const unsigned info_sect_y = get_payment_part_height() - quiet_space_px_ - mm_to_px(layout_definitions::payment_part_title_section).get_height() - mm_to_px(layout_definitions::payment_part_qr_section).get_height();
            return qrinvoice::layout::rect<unsigned> {info_sect_x, info_sect_y, mm_to_px(layout_definitions::payment_part_qr_section)}.translate(payment_part_lower_left_);
        }

        const qrinvoice::layout::rect<unsigned> graphics_writer::init_payment_part_info_sect_rect() const
        {
            const unsigned info_sect_x = quiet_space_px_ + mm_to_px(layout_definitions::payment_part_qr_section).get_width();
            const unsigned info_sect_y = get_payment_part_height() - quiet_space_px_ - mm_to_px(layout_definitions::payment_part_information_section).get_height();
            return qrinvoice::layout::rect<unsigned> {info_sect_x, info_sect_y, mm_to_px(layout_definitions::payment_part_information_section)}.translate(payment_part_lower_left_);
        }

        const qrinvoice::layout::rect<unsigned> graphics_writer::init_payment_part_amount_sect_rect() const
        {
            const unsigned info_sect_x = quiet_space_px_;
            const unsigned info_sect_y = quiet_space_px_ + mm_to_px(layout_definitions::payment_part_further_info_section).get_height();
            return qrinvoice::layout::rect<unsigned> {info_sect_x, info_sect_y, mm_to_px(layout_definitions::payment_part_amount_section)}.translate(payment_part_lower_left_);
        }

        const qrinvoice::layout::rect<unsigned> graphics_writer::init_payment_part_further_info_sect_rect() const
        {
            const unsigned info_sect_x = quiet_space_px_;
            const unsigned info_sect_y = quiet_space_px_;
            return qrinvoice::layout::rect<unsigned> {info_sect_x, info_sect_y, mm_to_px(layout_definitions::payment_part_further_info_section)}.translate(payment_part_lower_left_);
        }

        void graphics_writer::set_font_size(FT_Face face, const unsigned int font_size_pts) const {
            FT_Error error = FT_Set_Char_Size(face, freetype_helper::freetype_char_height(font_size_pts), 0, dpi_, 0);
            if (error) {
                throw qrinvoice::technical_exception{"Unable to set font size to " + std::to_string(font_size_pts)};
            }
        }
        void graphics_writer::add_receipt_title_section(payment_part_bitmap &img) const
        {
            spdlog::trace("graphics_writer::add_receipt_title_section");
            debug_layout(img, receipt_title_section_rect_);

            const auto x = static_cast<float>(receipt_title_section_rect_.get_left_x()) + additional_print_margin_px_;
            const auto y_title = translate_y(static_cast<float>(receipt_title_section_rect_.get_top_y()));

            set_font_size(font_title_, layout_definitions::font_size_title);
            freetype_helper::draw_text(img, font_title_, x, y_title, label("TitleReceipt"));
        }

        void graphics_writer::add_receipt_info_section(payment_part_bitmap &img, const model::qr_invoice& qr_invoice) const
        {
            spdlog::trace("graphics_writer::add_receipt_info_section");
            debug_layout(img, receipt_info_section_rect_);

            // According to the spec 3.6.2 - Information section - v2.0
            // "Because of the limited space, it is permitted to omit the street name and building number from the addresses of the creditor (Payable to) and the debtor (Payable by)"
            const bool omitStreetNameHouseNumberOrAddressLine1 = opts_.layout ==  payment_part_receipt::layout::compressed;

            const unsigned bottom_y = receipt_info_section_rect_.get_bottom_y();
            const unsigned left_x = receipt_info_section_rect_.get_left_x() + additional_print_margin_px_;
            const auto top_y = static_cast<float>(translate_y(receipt_info_section_rect_.get_top_y()));

            // Account
            auto y_pos = top_y;
            write_receipt_info_section_title(img, &y_pos, label("CdtrInf.IBANCreditor"), true);
            write_receipt_info_section_value(img, &y_pos, iban_util::format_iban(qr_invoice.get_creditor_info().get_iban()));

            const auto addr = qr_invoice.get_creditor_info().get_creditor().get_address();
            for (const auto& addr_line : model::address_utils::to_address_lines(addr, omitStreetNameHouseNumberOrAddressLine1)) {
                write_receipt_info_section_value(img, &y_pos, addr_line);
            }

            const model::payment_reference &pr = qr_invoice.get_payment_reference();
            if (!pr.empty()) {
                switch (pr.get_reference_type().get()) {
                    case model::reference_type::qr_reference:
                    case model::reference_type::creditor_reference:
                        if(uncompressed_layout(opts_.layout)) {
                            y_pos += du::points_to_pixels(layout_definitions::receipt_paragraph_spacing_pts, dpi_);
                        }
                        write_receipt_info_section_title(img, &y_pos, label("RmtInf.Ref"));
                        write_receipt_info_section_value(img, &y_pos, payment_reference_util::format_reference(pr));
                        break;
                    case model::reference_type::without_reference:
                    default:
                        break;
                }
            }
            if(uncompressed_layout(opts_.layout)) {
                y_pos += du::points_to_pixels(layout_definitions::receipt_paragraph_spacing_pts, dpi_);
            }
            // Debtor
            if (qr_invoice.get_ultimate_debtor().empty()) {
                write_receipt_info_section_title(img, &y_pos, label("UltmtDbtr.Empty"));
                y_pos += receipt_debtor_field_.get_height() + du::millimeters_to_points_rounded(0.75, dpi_);
                const auto lower_y = static_cast<const unsigned int>(translate_y(y_pos));
                const qrinvoice::layout::rect<unsigned> payment_part_debtor_field_rect{ left_x, lower_y, receipt_debtor_field_ };
                write_free_text_box(img, payment_part_debtor_field_rect);

                // some spacing needed
                y_pos += payment_part_debtor_field_rect.get_height();
            } else {
                write_receipt_info_section_title(img, &y_pos, label("UltmtDbtr"));
                for (const auto& addr_line : model::address_utils::to_address_lines(qr_invoice.get_ultimate_debtor().get_address()))
                    write_receipt_info_section_value(img, &y_pos, addr_line);
            }

            if (y_pos > bottom_y && env::is_true(env::dont_ignore_layout_errors))
                throw qrinvoice::layout::layout_exception{"Not all content could be printed to the receipt information section"};

        }

        void graphics_writer::add_receipt_amount(payment_part_bitmap &img, const model::payment_amount_info &amount_info) const
        {
            spdlog::trace("graphics_writer::add_receipt_amount");
            debug_layout(img, receipt_amount_section_rect_);

            namespace du = qrinvoice::layout::dimension_unit_utils;

            const auto upper_y = static_cast<float>(receipt_amount_section_rect_.get_top_y());

            // header
            const auto currency_x = static_cast<float>(receipt_amount_section_rect_.get_left_x() + additional_print_margin_px_);
            const float amount_x = currency_x + du::millimeters_to_points_rounded(12, dpi_);
            const float header_y = translate_y(upper_y);


            set_font_size(font_heading_, layout_definitions::receipt_font_size_heading_min);
            freetype_helper::draw_text(img, font_heading_, currency_x, header_y, label("Currency"));

            set_font_size(font_heading_, layout_definitions::receipt_font_size_heading_min);
            freetype_helper::draw_text(img, font_heading_, amount_x, header_y, label("Amount"));

            // body
            const float values_y = translate_y(upper_y
                    - du::points_to_pixels(layout_definitions::receipt_font_size_heading_min, dpi_)
                    - du::points_to_pixels(layout_definitions::receipt_amount_line_spacing_pts, dpi_));

            set_font_size(font_value_, layout_definitions::receipt_font_size_value_min);
            freetype_helper::draw_text(img, font_value_, currency_x, values_y, currency_ops::get_currency_code(amount_info.get_currency()));

            if (amount_info.is_amount_set()) {
                 std::string amount{ amount_util::format_amount_for_print(amount_info.get_amount()) };
                freetype_helper::draw_text(img, font_value_, amount_x, values_y, amount);
            } else {
                unsigned int amount_field_x = receipt_amount_section_rect_.get_right_x() - receipt_amount_field_.get_width() + additional_print_margin_px_;
                unsigned int amount_field_y = static_cast<unsigned int>(upper_y) - receipt_amount_field_.get_height() -  du::millimeters_to_points_rounded(0.75, dpi_);
                qrinvoice::layout::rect<unsigned> box(amount_field_x, amount_field_y, receipt_amount_field_);
                write_free_text_box(img, box);
            }
        }

        void graphics_writer::add_receipt_acceptance_section(payment_part_bitmap &img) const
        {
            spdlog::trace("graphics_writer::add_receipt_acceptance_section");
            namespace du = qrinvoice::layout::dimension_unit_utils;

            debug_layout(img, receipt_acceptance_section_rect_);

            const std::string &lbl = label("AcceptancePoint");
            set_font_size(font_heading_, layout_definitions::receipt_font_size_heading_min);

            float text_width = freetype_helper::get_text_width(font_heading_, lbl);
            const float x = receipt_acceptance_section_rect_.get_right_x() - text_width;
            const auto y_title = static_cast<float>(translate_y(receipt_acceptance_section_rect_.get_top_y()));
            freetype_helper::draw_text(img, font_heading_, x, y_title, lbl);
        }

        void graphics_writer::add_payment_part_title_section(payment_part_bitmap &img) const
        {
            spdlog::trace("graphics_writer::add_payment_part_title_section");
            debug_layout(img, payment_part_title_section_rect_);

            const auto x = payment_part_title_section_rect_.get_left_x();
            const auto y_title = translate_y(payment_part_title_section_rect_.get_top_y());

            set_font_size(font_title_, layout_definitions::font_size_title);
            freetype_helper::draw_text(img, font_title_, x, y_title, label("Title"));
        }

        void graphics_writer::add_payment_part_qr_code_image(payment_part_bitmap &img, const qrinvoice::qrcode::qr_code_bitmap& qr_code_bitmap) const
        {
            spdlog::trace("graphics_writer::add_payment_part_qr_code_image");
            array2d &canvas = img.canvas();

            debug_layout(img, payment_part_qr_section_rect_);
            const auto absolute_pos = payment_part_qr_section_rect_;

            const unsigned int modules_count = qr_code_bitmap.get_modules();
            auto optimal_qr_code_size = qrinvoice::qrcode::swiss_qr_code_writer::get_optimal_qr_code_render_size(qr_code_.get_width(), modules_count);

            const auto scale = optimal_qr_code_size / modules_count ;
            const unsigned int unscaled_qr_code_size = modules_count * scale;
            array2d qrcode_unscaled(unscaled_qr_code_size,unscaled_qr_code_size);

            for (unsigned int module_row = 0; module_row < modules_count; module_row++) {
                const auto module_row_offset = module_row * scale;
                for (unsigned int module_col = 0; module_col < modules_count; module_col++) {
                    const auto module_col_offset = module_col * scale;

                    auto is_set = qr_code_bitmap.map_[module_row][module_col];

                    const unsigned char val = is_set ? WHITE : static_cast<unsigned char>(0);
                    for (unsigned int i = 0; i < scale; i++) {
                        unsigned char *const qrCodeUnscaledRowArr = qrcode_unscaled[module_row_offset + i];
                        for (unsigned int j = 0; j < scale; j++) {
                            qrCodeUnscaledRowArr[module_col_offset + j] = val;
                        }
                    }
                }
            }

            const int scaled_qr_code_size = qr_code_.get_width();
            array2d qrcode_scaled(scaled_qr_code_size,scaled_qr_code_size);

            stbi_helper::resizeUint8(qrcode_unscaled.data(), unscaled_qr_code_size, unscaled_qr_code_size, 0,
                               qrcode_scaled.data(), scaled_qr_code_size, scaled_qr_code_size, 0, 1);

            int offset_x = absolute_pos.get_left_x();
            int offset_y = translate_y(absolute_pos.get_top_y()- quiet_space_px_);

            for (int row = 0; row < scaled_qr_code_size; row++) {
                unsigned char *const rowArr = canvas[offset_y + row];
                for (int col = 0; col < scaled_qr_code_size; col++) {
                    const unsigned char i = qrcode_scaled[row][col];
                    rowArr[offset_x + col] = i;
                }
            }

           // overlay with swiss cross
            const int half_cross_size = half_rounded(static_cast<int>(du::millimeters_to_points(swiss_qr_code::qr_code_logo_size.get_width(), dpi_)));
            const int half_qr_code_size = scaled_qr_code_size / 2;
            const int swiss_cross_y = offset_y + half_qr_code_size - half_cross_size;
            const int swiss_cross_x = payment_part_qr_section_rect_.get_left_x() + half_qr_code_size - half_cross_size;

            if(opts_.output_resolution == output_resolution::low_150_dpi) {
                for (int row = 0; row < 41; row++) {
                    unsigned char *const rowArr = canvas[swiss_cross_y + row];
                    for (int col = 0; col < 41; col++) {
                        const unsigned char i = qrinvoice::resource::swiss_cross_41x41[row][col];
                        rowArr[swiss_cross_x + col] = WHITE - i;
                    }
                }
            } else if(opts_.output_resolution == output_resolution::medium_300_dpi) {
                for (int row = 0; row < 83; row++) {
                    unsigned char *const rowArr = canvas[swiss_cross_y + row];
                    for (int col = 0; col < 83; col++) {
                        const unsigned char i = qrinvoice::resource::swiss_cross_83x83[row][col];
                        rowArr[swiss_cross_x + col] = WHITE - i;
                    }
                }
            } else if(opts_.output_resolution == output_resolution::high_600_dpi) {
                for (int row = 0; row < 166; row++) {
                    unsigned char *const rowArr = canvas[swiss_cross_y + row];
                    for (int col = 0; col < 166; col++) {
                        const unsigned char i = qrinvoice::resource::swiss_cross_166x166[row][col];
                        rowArr[swiss_cross_x + col] = WHITE - i;
                    }
                }
            }
        }

        void graphics_writer::add_payment_part_amount(payment_part_bitmap &img, const model::payment_amount_info &amount_info) const
        {
            spdlog::trace("graphics_writer::add_payment_part_amount");
            debug_layout(img, payment_part_amount_section_rect_);
            namespace du = qrinvoice::layout::dimension_unit_utils;

            const auto lower_y = payment_part_amount_section_rect_.get_bottom_y();
            const auto upper_y = payment_part_amount_section_rect_.get_top_y();

            // header
            const auto currency_x = payment_part_amount_section_rect_.get_left_x();
            const auto amount_x = currency_x + du::millimeters_to_points_rounded(14, dpi_);
            const auto header_y = translate_y(upper_y);

            set_font_size(font_heading_, layout_definitions::payment_part_font_size_heading_recommended);
            freetype_helper::draw_text(img, font_heading_, currency_x, header_y, label("Currency"));

            set_font_size(font_heading_, layout_definitions::payment_part_font_size_heading_recommended);
            freetype_helper::draw_text(img, font_heading_, amount_x, header_y, label("Amount"));

            //body
            const auto values_y = translate_y(upper_y
                    - du::points_to_pixels(layout_definitions::payment_part_font_size_heading_recommended, dpi_)
                    - du::points_to_pixels(layout_definitions::payment_part_amount_line_spacing_pts, dpi_));

            set_font_size(font_value_, layout_definitions::payment_part_font_size_value_recommended);
            freetype_helper::draw_text(img, font_value_, currency_x, values_y, currency_ops::get_currency_code(amount_info.get_currency()));

            if (amount_info.is_amount_set()) {
                std::string amount{ amount_util::format_amount_for_print(amount_info.get_amount()) };

                set_font_size(font_value_, layout_definitions::payment_part_font_size_value_recommended);
                freetype_helper::draw_text(img, font_value_, amount_x, values_y, amount);
            } else {
                unsigned int amount_field_x = payment_part_amount_section_rect_.get_right_x() - payment_part_amount_field_.get_width() - du::millimeters_to_points_rounded(1, dpi_);
                unsigned int amount_field_y = lower_y + du::millimeters_to_points_rounded(2, dpi_);
                qrinvoice::layout::rect<unsigned> box(amount_field_x, amount_field_y, payment_part_amount_field_);
                write_free_text_box(img, box);
            }
        }

        void graphics_writer::add_payment_part_info_section(payment_part_bitmap &img, const model::qr_invoice &qr_invoice) const
        {
            spdlog::trace("graphics_writer::add_payment_part_info_section");
            const unsigned bottom_y = payment_part_info_section_rect_.get_bottom_y();
            const unsigned left_x = payment_part_info_section_rect_.get_left_x();
            const auto top_y = static_cast<float>(translate_y(payment_part_info_section_rect_.get_top_y()));

            debug_layout(img, payment_part_info_section_rect_);

            // Account
            float y_pos = top_y;
            write_payment_part_info_section_title(img, &y_pos, label("CdtrInf.IBANCreditor"), true);
            write_payment_part_info_section_value(img, &y_pos, iban_util::format_iban(qr_invoice.get_creditor_info().get_iban()));

            const auto addr = qr_invoice.get_creditor_info().get_creditor().get_address();
            for (const auto& addr_line : model::address_utils::to_address_lines(addr))
                write_payment_part_info_section_value(img, &y_pos, addr_line);

            model::payment_reference pr = qr_invoice.get_payment_reference();
            if (!pr.empty()) {
                switch (pr.get_reference_type().get()) {
                    case model::reference_type::qr_reference:
                    case model::reference_type::creditor_reference:
                        if(uncompressed_layout(opts_.layout)) {
                            y_pos += du::points_to_pixels(layout_definitions::payment_part_paragraph_spacing_pts, dpi_);
                        }
                        write_payment_part_info_section_title(img, &y_pos, label("RmtInf.Ref"));
                        write_payment_part_info_section_value(img, &y_pos, payment_reference_util::format_reference(pr));
                        break;
                    case model::reference_type::without_reference:
                    default:
                        break;
                }

                // Additional information
                const std::string &unstructured_message = pr.get_additional_information().get_unstructured_message();
                const bool unstructured_msg_do_not_use_for_payment = unstructured_message == "NICHT ZUR ZAHLUNG VERWENDEN"
                                                                     || unstructured_message == "NE PAS UTILISER POUR LE PAIEMENT"
                                                                     || unstructured_message == "DO NOT USE FOR PAYMENT"
                                                                     || unstructured_message == "NON UTILIZZARE PER IL PAGAMENTO";

                const std::string final_unstructured_msg = unstructured_msg_do_not_use_for_payment ? tr_.translate("DoNotUseForPayment") : unstructured_message;

                const std::string &bill_information = pr.get_additional_information().get_bill_information();
                if (!final_unstructured_msg.empty() || !bill_information.empty()) {
                    if(uncompressed_layout(opts_.layout)) {
                        y_pos += du::points_to_pixels(layout_definitions::payment_part_paragraph_spacing_pts, dpi_);
                    }
                    write_payment_part_info_section_title(img, &y_pos, label("RmtInf.Ustrd"));

                    const auto unstructured_msg_length = string_util::utf8_length(final_unstructured_msg);
                    const auto bill_information_length = string_util::utf8_length(bill_information);
                    const auto total_length = unstructured_msg_length + bill_information_length;
                    if (!final_unstructured_msg.empty() && !bill_information.empty() && opts_.layout == payment_part_receipt::layout::compressed && total_length > (2 * layout_definitions::approx_max_line_length_payment_part_info_section)) {
                        // according to the spec version 2.0 - 3.5.4:
                        // If both elements are filled in, then a line break can be introduced after the information in the first element "Ustrd" (Unstructured message).
                        // If there is insufficient space, the line break can be omitted (but this makes it more difficult to read).
                        // If not all the details contained in the QR code can be displayed, the shortened content must be marked with "..." at the end. It must be ensured
                        // that all personal data is displayed.
                        auto joined = final_unstructured_msg + " " + bill_information;
                        write_payment_part_info_section_value(img, &y_pos, joined);
                    } else {
                        if (!final_unstructured_msg.empty()) {
                            write_payment_part_info_section_value(img, &y_pos, final_unstructured_msg);
                        }
                        if (!bill_information.empty()) {
                            write_payment_part_info_section_value(img, &y_pos, bill_information);
                        }
                    }
                }
            }

            // Debtor
            if(uncompressed_layout(opts_.layout)) {
                y_pos += du::points_to_pixels(layout_definitions::payment_part_paragraph_spacing_pts, dpi_);
            }
            if (qr_invoice.get_ultimate_debtor().empty()) {
                write_payment_part_info_section_title(img, &y_pos, label("UltmtDbtr.Empty"));
                y_pos += payment_part_debtor_field_.get_height() + du::millimeters_to_points_rounded(0.75, dpi_);
                const auto lower_y = static_cast<unsigned>(std::round(translate_y(y_pos)));
                const qrinvoice::layout::rect<unsigned> payment_part_debtor_field_rect{ left_x, lower_y, payment_part_debtor_field_ };
                write_free_text_box(img, payment_part_debtor_field_rect);

                // some spacing needed
                y_pos += payment_part_debtor_field_rect.get_height();
            } else {
                write_payment_part_info_section_title(img, &y_pos, label("UltmtDbtr"));
                for (const auto& addr_line : model::address_utils::to_address_lines(qr_invoice.get_ultimate_debtor().get_address()))
                    write_payment_part_info_section_value(img, &y_pos, addr_line);
            }

            if (y_pos > bottom_y && env::is_true(env::dont_ignore_layout_errors))
                throw qrinvoice::layout::layout_exception{"Not all content could be printed to the payment part information section"};

        }


        void graphics_writer::add_payment_part_further_info_section(payment_part_bitmap &img, const model::qr_invoice &qr_invoice) const
        {
            spdlog::trace("graphics_writer::add_payment_part_further_info_section");
            set_font_size(font_heading_, layout_definitions::payment_part_font_size_further_info);
            set_font_size(font_value_, layout_definitions::payment_part_font_size_further_info);
            debug_layout(img, payment_part_further_info_section_rect_);

            auto x = static_cast<float>(payment_part_further_info_section_rect_.get_left_x());
            auto lowerY = static_cast<float>(translate_y(payment_part_further_info_section_rect_.get_bottom_y())) - additional_print_margin_px_;

            auto params = qr_invoice.get_alternative_schemes().get_alternative_scheme_params();
            for (unsigned i = params.size(); i-- > 0; )
            {
                auto alternative_scheme = params[i];
                lowerY -= freetype_helper::freetype_dimension_to_pixels(font_heading_->size->metrics.ascender);
                lowerY -= du::points_to_pixels(layout_definitions::payment_part_further_info_line_spacing_pts, dpi_);

                const model::alternative_schemes_utils::alternative_scheme_pair &pair = model::alternative_schemes_utils::parse_for_output(alternative_scheme);
                auto remaining_space_value = static_cast<float>(payment_part_further_info_section_rect_.get_width());
                float value_x = x;
                if(!pair.name.empty()) {
                    freetype_helper::draw_text(img, font_heading_, x, lowerY, pair.name);

                    auto name_width = freetype_helper::get_text_width(font_heading_, pair.name);
                    remaining_space_value -= name_width;
                    value_x += name_width;
                }

                if(!pair.value.empty()) {
                    const std::string &value = trim_if_required(pair.value, remaining_space_value, font_value_);
                    freetype_helper::draw_text(img, font_value_, std::round(value_x), std::round(lowerY), value);
                }
            }


            // Ultimate Creditor / FINAL Creditor - according to the spec 3.5.5 - further information section:
            // "This section is where the "Final creditor" field, if available and approved for use, is displayed.
            // Instead of the designation "Final creditor", the relevant values in the Swiss QR Code are preceded by the words "In favour of" (bold).
            // Just one line is available, so it is possible that not all the information in the QR-bill can be printed there.
            // If that is the case, the shortened entry must be marked by "..." at the end. The data is printed in font size 7 pt, in the same order as in the Swiss QR Code."
            if (!qr_invoice.get_ultimate_creditor().empty()) {
                lowerY -= du::points_to_pixels(layout_definitions::payment_part_further_info_line_spacing_pts, dpi_);
                float ultimate_creditor_y = lowerY - freetype_helper::freetype_dimension_to_pixels(font_heading_->size->metrics.ascender);
                const std::string &ultmt_cdtr_label = label("UltmtCdtr");
                freetype_helper::draw_text(img, font_heading_, x, ultimate_creditor_y, ultmt_cdtr_label);

                auto label_width = freetype_helper::get_text_width(font_heading_, ultmt_cdtr_label);
                auto remaining_space_value = payment_part_further_info_section_rect_.get_width() - label_width;

                auto address_line = model::address_utils::to_single_line_address(qr_invoice.get_ultimate_creditor().get_address());
                auto address_line_trimmed = trim_if_required(address_line, remaining_space_value, font_value_);

                freetype_helper::draw_text(img, font_value_, x + label_width, ultimate_creditor_y, address_line_trimmed);
            }
        }

        void graphics_writer::write_receipt_info_section_title(payment_part_bitmap &img, float *const y_pos, const std::string &text) const
        {
            write_receipt_info_section_title(img,y_pos,text, false);
        }
        void graphics_writer::write_receipt_info_section_title(payment_part_bitmap &img, float *const y_pos, const std::string &text, const bool first_heading) const
        {
            // don't add spacing if it is the first header
            if(!first_heading) {
                if(uncompressed_layout(opts_.layout)) {
                    *y_pos += du::points_to_pixels(layout_definitions::receipt_heading_line_spacing_pts, dpi_);
                }
            }
            const auto left_x = static_cast<float>(receipt_info_section_rect_.get_left_x() + additional_print_margin_px_);

            set_font_size(font_heading_, layout_definitions::receipt_font_size_heading_min);
            freetype_helper::draw_text(img, font_heading_, left_x, *y_pos, text);
            *y_pos += freetype_helper::font_size_to_pixels(layout_definitions::receipt_font_size_value_min, dpi_);
        }

        void graphics_writer::write_receipt_info_section_value(payment_part_bitmap &img, float *const y_pos, const std::string &text) const
        {
            set_font_size(font_value_, layout_definitions::receipt_font_size_value_min);

            const auto left_x = static_cast<float>(receipt_info_section_rect_.get_left_x() + additional_print_margin_px_);

            auto font = font_value_;
            text_util::text_splitter text_splitter { [&font](const std::string& text) {
                return freetype_helper::get_text_widths(font, text);
            }};

            auto break_width = static_cast<float>(receipt_info_section_rect_.get_width() - additional_print_margin_px_);
            auto lines = text_splitter.split_text_by_boundary(text, break_width);
            for (const auto& line : lines) {
                if(uncompressed_layout(opts_.layout)) {
                    *y_pos += du::points_to_pixels(layout_definitions::receipt_value_line_spacing_pts, dpi_);
                }

                freetype_helper::draw_text(img, font_value_, left_x, *y_pos, line);
                *y_pos += freetype_helper::font_size_to_pixels(layout_definitions::receipt_font_size_value_min, dpi_);
            }

        }

        void graphics_writer::write_payment_part_info_section_title(payment_part_bitmap &img, float *const y_pos, const std::string &text) const
        {
            write_payment_part_info_section_title(img,y_pos,text, false);
        }
        void graphics_writer::write_payment_part_info_section_title(payment_part_bitmap &img, float *const y_pos, const std::string &text, const bool first_heading) const {
            // don't add spacing if it is the first header
            if (!first_heading) {
                if(uncompressed_layout(opts_.layout)) {
                    *y_pos += du::points_to_pixels(layout_definitions::payment_part_heading_line_spacing_pts, dpi_);
                }
            }
            const auto left_x = static_cast<float>(payment_part_info_section_rect_.get_left_x());

            set_font_size(font_heading_, layout_definitions::payment_part_font_size_heading_recommended);
            freetype_helper::draw_text(img, font_heading_, left_x, *y_pos, text);
            *y_pos += freetype_helper::font_size_to_pixels(layout_definitions::payment_part_font_size_value_recommended, dpi_);
        }

        void graphics_writer::write_payment_part_info_section_value(payment_part_bitmap &img, float *const y_pos, const std::string &text) const
        {
            const auto left_x = static_cast<float>(payment_part_info_section_rect_.get_left_x());

            auto font = font_value_;
            set_font_size(font_value_, layout_definitions::payment_part_font_size_value_recommended);
            text_util::text_splitter text_splitter { [&font](const std::string& text) {
                return freetype_helper::get_text_widths(font, text);
            }};

            auto break_width = static_cast<float>(payment_part_info_section_rect_.get_width() - additional_print_margin_px_);
            auto lines = text_splitter.split_text_by_boundary(text, break_width);
            for (const auto& line : lines) {
                freetype_helper::draw_text(img, font_value_, left_x, *y_pos, line);
                *y_pos += freetype_helper::font_size_to_pixels(layout_definitions::payment_part_font_size_value_recommended, dpi_);

                if(uncompressed_layout(opts_.layout)) {
                   *y_pos += du::points_to_pixels(layout_definitions::payment_part_value_line_spacing_pts, dpi_);
                }
            }

        }

        void graphics_writer::write_free_text_box(payment_part_bitmap &img, qrinvoice::layout::rect<unsigned> rect) const
        {
            spdlog::trace("graphics_writer::write_free_text_box - start");
            const auto corner_len = box_corner_line_length_;
            const auto left_x = rect.get_lower_left_x();
            const auto lower_y = translate_y(rect.get_lower_left_y());
            const auto right_x = rect.get_upper_right_x();
            const auto upper_y = translate_y(rect.get_upper_right_y());

            // upper horizontal lines
            array2d &canvas = img.canvas();
            for(unsigned int line_y = 0; line_y < box_corner_line_width_; line_y++) {
                unsigned int y = upper_y + line_y;
                unsigned char *const rowArray = canvas[y];
               // upper left horizontal line
                for(unsigned int x = left_x; x < (left_x + corner_len); x++) {
                    rowArray[x] = WHITE;
                }
                // upper right horizontal line
                for(unsigned int x = right_x - corner_len; x < right_x; x++) {
                    rowArray[x] = WHITE;
                }
            }

            // lower horizontal lines
            for(unsigned int line_y = 0; line_y < box_corner_line_width_; line_y++) {
                unsigned int y = lower_y - box_corner_line_width_ + line_y;
                unsigned char *const rowArray = canvas[y];
                // lower left horizontal line
                for(unsigned int x = left_x; x < (left_x + corner_len); x++) {
                    rowArray[x] = WHITE;
                }
                // lower right horizontal line
                for(unsigned int x = right_x - corner_len; x < right_x; x++) {
                    rowArray[x] = WHITE;
                }
            }

            // upper vertical lines
            for(unsigned int line_y = 0; line_y < corner_len; line_y++) {
                unsigned int y = upper_y + line_y;
                unsigned char *const rowArray = canvas[y];
                // upper left vertical line
                for(unsigned int x = left_x; x < (left_x + box_corner_line_width_); x++) {
                    rowArray[x] = WHITE;
                }
                // upper right vertical line
                for(unsigned int x = right_x - box_corner_line_width_; x < right_x; x++) {
                    rowArray[x] = WHITE;
                }
            }

            // lower vertical lines
            for(unsigned int line_y = 0; line_y < corner_len; line_y++) {
                unsigned int y = lower_y - corner_len + line_y;
                unsigned char *const rowArray = canvas[y];
                // lower left vertical line
                for(unsigned int x = left_x; x < (left_x + box_corner_line_width_); x++) {
                    rowArray[x] = WHITE;
                }
                // lower right vertical line
                for(unsigned int x = right_x - box_corner_line_width_; x < right_x; x++) {
                    rowArray[x] = WHITE;
                }

            }
            spdlog::trace("graphics_writer::write_free_text_box - end");
        }


        void graphics_writer::debug_layout(payment_part_bitmap &img, qrinvoice::layout::rect<unsigned> rect) const
        {
            if (env::is_true(env::debug_layout)) {
                array2d &canvas = img.canvas();
                const unsigned int upper_y = translate_y(rect.get_top_y());
                const unsigned int lower_y = translate_y(rect.get_bottom_y());

                unsigned char *const upperArr = canvas[upper_y];
                unsigned char *const lowerArr = canvas[lower_y];
                for(unsigned int x = rect.get_left_x(); x <= rect.get_right_x(); x++) {
                    upperArr[x] = WHITE;
                    lowerArr[x] = WHITE;
                }

                for(unsigned int y = lower_y; y >= upper_y; y--) {
                    unsigned char *const verticalArr = canvas[y];
                    verticalArr[rect.get_left_x()] = WHITE;
                    verticalArr[rect.get_right_x()] = WHITE;
                }
            }
        }

        void graphics_writer::draw_boundary_lines(payment_part_bitmap &img) const
        {
            if (opts_.boundary_lines == boundary_lines::none)
                return;

            write_separation_label(img);
            draw_horizontal_boundary_line(img);
            draw_vertical_boundary_line(img);
        }

        void graphics_writer::write_separation_label(payment_part_bitmap &img) const
        {
            spdlog::trace("graphics_writer::write_separation_label");
            if(opts_.boundary_line_separation_text && opts_.page_size != page_size::din_lang && opts_.page_size != page_size::din_lang_cropped) {
                const auto x = static_cast<float>(payment_part_lower_left_.get_x() + quiet_space_px_);
                const auto y = translate_y(
                        static_cast<float>(payment_part_lower_left_.get_y())
                        + static_cast<float>(get_payment_part_height())
                        + du::points_to_pixels(layout_definitions::font_size_separation_label, dpi_) * 1.5f
                        );

                set_font_size(font_value_, layout_definitions::font_size_separation_label);
                freetype_helper::draw_text(img, font_value_, x, y, label("SeparationLabel"));
            } else if(opts_.boundary_line_separation_text) {
                spdlog::info("Separation label above payment part was not printed as PageSize is too small.");
            }
        }


        void graphics_writer::draw_horizontal_boundary_line(payment_part_bitmap &img) const
        {
            spdlog::trace("graphics_writer::draw_horizontal_boundary_line");
            const auto line_width = boundary_line_width_;
            const auto line_start_x = opts_.boundary_lines == qrinvoice::boundary_lines::enabled_with_margins ? root_lower_left_.get_x() + print_margin_px_ : root_lower_left_.get_x();
            const auto line_end_x = opts_.boundary_lines == qrinvoice::boundary_lines::enabled_with_margins ? get_page_canvas_width() - print_margin_px_ : get_page_canvas_width();

            const auto y_top = translate_y(payment_part_lower_left_.get_y() + get_payment_part_height());

            array2d &canvas = img.canvas();
            for(unsigned int y = y_top; y < (y_top + line_width); y++) {
                unsigned char *const rowArr = canvas[y];
                for(unsigned int x = line_start_x; x < line_end_x; x++) {
                    rowArr[x] = WHITE;
                }
            }
            if(opts_.boundary_line_scissors && opts_.page_size != page_size::din_lang && opts_.page_size != page_size::din_lang_cropped) {
                const auto scissor_left_x = receipt_title_section_rect_.get_left_x() + (opts_.boundary_lines == qrinvoice::boundary_lines::enabled_with_margins ? print_margin_px_ : additional_print_margin_px_);

                if(opts_.output_resolution == output_resolution::low_150_dpi) {
                    const int scissor_horizontal_y = translate_y(payment_part_lower_left_.get_y() + get_payment_part_height() + half_floored(18) - line_width);
                    for (int row = 0; row < 18; row++) {
                        const int y = scissor_horizontal_y + row;
                        unsigned char *const rowArr = canvas[y];
                        for (int col = 0; col < 30; col++) {
                            const unsigned char i = qrinvoice::resource::scissor_30x18_horizontal[row][col];
                            const unsigned int x = scissor_left_x + col;
                            rowArr[x] = WHITE - i;
                        }
                    }
                } else if(opts_.output_resolution == output_resolution::medium_300_dpi) {
                    const int scissor_horizontal_y = translate_y(payment_part_lower_left_.get_y() + get_payment_part_height() + half_floored(35)- line_width);
                    for (int row = 0; row < 35; row++) {
                        const int y = scissor_horizontal_y + row;
                        unsigned char *const rowArr = canvas[y];
                        for (int col = 0; col < 58; col++) {
                            const unsigned int x = scissor_left_x + col;
                            const unsigned char i = qrinvoice::resource::scissor_58x35_horizontal[row][col];
                            rowArr[x] = WHITE - i;
                        }
                    }
                } else if(opts_.output_resolution == output_resolution::high_600_dpi) {
                    const int scissor_horizontal_y = translate_y(payment_part_lower_left_.get_y() + get_payment_part_height() + half_floored(71)- line_width);
                    for (int row = 0; row < 71; row++) {
                        const int y = scissor_horizontal_y + row;
                        unsigned char *const rowArr = canvas[y];
                        for (int col = 0; col < 118; col++) {
                            const unsigned int x = scissor_left_x + col;
                            const unsigned char i = qrinvoice::resource::scissor_118x71_horizontal[row][col];
                            rowArr[x] = WHITE - i;
                        }
                    }
                }

            } else {
               if(opts_.boundary_line_scissors) {
                    spdlog::debug("Scissor on horizontal line is only printed when using PageSize A4 or A5");
                }
            }

        }

       void graphics_writer::draw_vertical_boundary_line(payment_part_bitmap &img) const
       {
           spdlog::trace("graphics_writer::draw_vertical_boundary_line");
           array2d &canvas = img.canvas();
           const auto line_width = boundary_line_width_;
           const auto half_line_width = half_rounded(line_width);
           const auto x = payment_part_lower_left_.get_x();
           const auto x_left = x - half_line_width;

           const auto line_start_y = translate_y(payment_part_lower_left_.get_y() + get_payment_part_height());
           const auto line_end_y = opts_.boundary_lines == qrinvoice::boundary_lines::enabled_with_margins ? canvas.get_height() - print_margin_px_ : canvas.get_height();
           for(unsigned int y = line_start_y; y < line_end_y; y++) {
               unsigned char *const rowArr = canvas[y];
               for(unsigned int line_x = x_left; line_x < (x_left + line_width); line_x++) {
                   rowArr[line_x] = WHITE;
               }
           }

           if(opts_.boundary_line_scissors) {
               spdlog::trace("graphics_writer::draw_vertical_boundary_line - draw scissors");
                const int scissor_upper_y = translate_y(receipt_title_section_rect_.get_top_y());

               if(opts_.output_resolution == output_resolution::low_150_dpi) {
                   const int scissor_x = static_cast<int>(std::round(x - (18 / 2)));
                   for (int row = 0; row < 30; row++) {
                       unsigned char *const rowArr = canvas[scissor_upper_y + row];
                       for (int col = 0; col < 18; col++) {
                           const unsigned char i = qrinvoice::resource::scissor_30x18_vertical[row][col];
                           rowArr[scissor_x + col] = WHITE - i;
                       }
                   }
               } else if(opts_.output_resolution == output_resolution::medium_300_dpi) {
                   const int scissor_x = static_cast<int>(std::round(x - (35 / 2)));
                   for (int row = 0; row < 58; row++) {
                       unsigned char *const rowArr = canvas[scissor_upper_y + row];
                       for (int col = 0; col < 35; col++) {
                           const unsigned char i = qrinvoice::resource::scissor_58x35_vertical[row][col];
                           rowArr[scissor_x + col] = WHITE - i;
                       }
                   }
               } else if(opts_.output_resolution == output_resolution::high_600_dpi) {
                   const int scissor_x = static_cast<int>(std::round(x - (71 / 2)));
                   for (int row = 0; row < 118; row++) {
                       unsigned char *const rowArr = canvas[scissor_upper_y + row];
                       for (int col = 0; col < 71; col++) {
                           const unsigned char i = qrinvoice::resource::scissor_118x71_vertical[row][col];
                           rowArr[scissor_x + col] = WHITE - i;
                       }
                   }
               }
           }
        }

        qrinvoice::layout::dimension<unsigned> graphics_writer::calculate_page_canvas_dimensions(page_size sz, unsigned dpi)
        {
            switch (sz) {
                case page_size::a4:
                    return {du::millimeters_to_points_rounded(210, dpi), du::millimeters_to_points_rounded(297, dpi)};
                case page_size::a5:
                    return {du::millimeters_to_points_rounded(210, dpi), du::millimeters_to_points_rounded(148, dpi)};
                case page_size::din_lang:
                case page_size::din_lang_cropped:
                    return {du::millimeters_to_points_rounded(210, dpi), du::millimeters_to_points_rounded(105, dpi)};
                default:
                    throw technical_exception("unsupported page_size");
            }
        }

        const unsigned graphics_writer::calculate_additional_print_margin(bool additional_margin, unsigned dpi)
        {
            return additional_margin ? du::millimeters_to_points_rounded(layout_definitions::additional_print_margin, dpi) : 0;
        }

        std::string graphics_writer::label(const std::string& id) const
        {
            return tr_.translate_label(id);
        }

        graphics_writer::~graphics_writer() {
            spdlog::trace("destruct freetype");
            // TITLE
            // furthermore, font might be cached longer than on a per execution basis
            FT_Done_Face(font_title_);

            // HEADING
            if(font_path_heading_ == font_path_title_) {
                // same font as title - ignore
            } else  {
                FT_Done_Face(font_heading_);
            }

            // VALUE
            if(font_path_value_ == font_path_title_) {
                // same font as title - ignore
            } else if (font_path_value_ == font_path_heading_) {
                // same font as title - ignore
            } else  {
                FT_Done_Face(font_value_);
            }
            FT_Done_FreeType( ft_library_ );
        }


    } // namespace payment_part_receipt
} // namespace qrinvoice

