
#ifndef QR_INVOICE_PAYMENT_PART_WRITER_INTERNAL_HPP
#define QR_INVOICE_PAYMENT_PART_WRITER_INTERNAL_HPP


#include <qrinvoice/output/payment_part_receipt.hpp>
#include <qrinvoice/model/qr_invoice.hpp>
#include <qrcode/qr_code_bitmap_internal.hpp>


namespace qrinvoice {
namespace payment_part_receipt {

class writer {
public:
	virtual ~writer() = default;

	virtual output::payment_part_receipt write(const model::qr_invoice& qr_invoice, const qrinvoice::qrcode::qr_code_bitmap& qr_code_bitmap) = 0;
};


} // namespace payment_part_receipt
} // namespace qrinvoice


#endif	// QR_INVOICE_PAYMENT_PART_WRITER_INTERNAL_HPP

