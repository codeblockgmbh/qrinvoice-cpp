
#include <vector>
#include <string>
#include <numeric>

#include <model/mapper/swiss_payments_code_to_model_internal.hpp>
#include <paymentpart/layout_internal.hpp>
#include <paymentpart/layout_helper_internal.hpp>
#include <util/string_util_internal.hpp>
#include <spdlog/spdlog.h>

namespace qrinvoice {
namespace payment_part_receipt {

layout choose_layout(const model::qr_invoice& qr_invoice)
{
	const int layout_points = calculate_layout_points(qr_invoice);

	layout lo;
	if (layout_points >= 19)
		lo = layout::compressed;
	else if (layout_points >= 17)
		lo = layout::narrowed;
	else
		lo = layout::relaxed;

	spdlog::debug("LayoutPoints={} Layout={}", layout_points, layout_as_string(lo));
	return lo;
}

// could be improved (ostream)
std::string layout_as_string(const layout& layout)
{
    switch( layout )
    {
        case layout::relaxed:
            return "relaxed";
        case layout::narrowed:
            return "narrowed";
        case layout::compressed:
            return "compressed";
		default:
			throw technical_exception("unsupported layout");
    }
}

int calculate_layout_points(const model::qr_invoice& qr_invoice)
{
	int points = 0;
	points += calculate_layout_points(qr_invoice.get_creditor_info().get_creditor().get_address());
	points += calculate_layout_points(qr_invoice.get_ultimate_debtor().get_address());

	//if no debtor is provided, free text field is written which takes a log of space
	points += (qr_invoice.get_ultimate_debtor().empty()) ? 7 : 0;

	const model::payment_reference& pay_ref = qr_invoice.get_payment_reference();
	const model::additional_information& add_info = pay_ref.get_additional_information();
	points += calculate_layout_points(50, {add_info.get_unstructured_message()});
	points += calculate_layout_points(50, {add_info.get_bill_information()});

	// +2 points if reference is given
	points += (pay_ref.get_reference_type().get() == model::reference_type::without_reference) ? 0 : 2;

	return points;
}

int calculate_layout_points(const model::address& address)
{
	if (address.empty())
		return 0;

	int points = 1;
	points += calculate_layout_points(35, {address.get_name()});
	// either structured
	points += calculate_layout_points(35, {address.get_street_name(), address.get_house_number()});
	points += calculate_layout_points(35, {address.get_postal_code(), address.get_city()});
	// or combined
	points += calculate_layout_points(35, {address.get_address_line1()});
	points += calculate_layout_points(35, {address.get_address_line2()});
	return points;
}

int calculate_layout_points(int threshold, const std::vector<std::string>& strs)
{
	using namespace std;

	int sum = accumulate(strs.begin(), strs.end(), 0, [](int acc, const string& b) {
		return acc + string_util::utf8_length(b);
	});

	if (sum == 0)
		return 0;
	if (sum < threshold)
		return 1;
	else
		return 2;
}

bool compressed_layout(const layout & lo) {
	return lo == layout::compressed;
}
bool uncompressed_layout(const layout & lo) {
	return !compressed_layout(lo);
}

} // namespace payment_part_receipt
} // namespace qrinvoice

