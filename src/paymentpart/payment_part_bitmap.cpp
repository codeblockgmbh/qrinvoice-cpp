#include <qrinvoice/page_size.hpp>
#include <qrinvoice/technical_exception.hpp>
#include "payment_part_bitmap.hpp"

qrinvoice::array2d qrinvoice::payment_part_receipt::payment_part_bitmap::init_canvas(qrinvoice::page_size page_size, qrinvoice::output_resolution output_resolution) {
    int w, h;
    if(page_size == page_size::din_lang || page_size == page_size::din_lang_cropped){
        if(output_resolution == output_resolution::low_150_dpi) {
            w = DIN_LANG_WIDTH_150;
            h = DIN_LANG_HEIGHT_150;
        } else if (output_resolution == output_resolution::medium_300_dpi) {
            w = DIN_LANG_WIDTH_300;
            h = DIN_LANG_HEIGHT_300;
        } else if (output_resolution == output_resolution::high_600_dpi) {
            w = DIN_LANG_WIDTH_600;
            h = DIN_LANG_HEIGHT_600;
        } else{
            throw technical_exception("unsupported output_resolution");
        }
    } else if(page_size == page_size::a5){
        if(output_resolution == output_resolution::low_150_dpi) {
            w = A5_WIDTH_150;
            h = A5_HEIGHT_150;
        } else if (output_resolution == output_resolution::medium_300_dpi) {
            w = A5_WIDTH_300;
            h = A5_HEIGHT_300;
        } else if (output_resolution == output_resolution::high_600_dpi) {
            w = A5_WIDTH_600;
            h = A5_HEIGHT_600;
        } else{
            throw technical_exception("unsupported output_resolution");
        }
    }else if(page_size == page_size::a4){
        if(output_resolution == output_resolution::low_150_dpi) {
            w = A4_WIDTH_150;
            h = A4_HEIGHT_150;
        } else if (output_resolution == output_resolution::medium_300_dpi) {
            w = A4_WIDTH_300;
            h = A4_HEIGHT_300;
        } else if (output_resolution == output_resolution::high_600_dpi) {
            w = A4_WIDTH_600;
            h = A4_HEIGHT_600;
        } else{
            throw technical_exception("unsupported output_resolution");
        }
    } else {
        throw technical_exception("unsupported page_size");
    }
    return array2d(h,w);
}

qrinvoice::payment_part_receipt::payment_part_bitmap::payment_part_bitmap(qrinvoice::page_size page_size, qrinvoice::output_resolution output_resolution) : canvas_ { init_canvas(page_size, output_resolution) } {
    width = canvas_.get_width();
    height = canvas_.get_height();
}

qrinvoice::array2d& qrinvoice::payment_part_receipt::payment_part_bitmap::canvas() {
    return canvas_;
}