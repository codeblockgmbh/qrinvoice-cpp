
#ifndef QR_INVOICE_WRITE_PAYMENT_PART_INTERNAL_HPP
#define QR_INVOICE_WRITE_PAYMENT_PART_INTERNAL_HPP

#include <memory>
#include <qrinvoice/output/payment_part_receipt.hpp>
#include <qrinvoice/model/qr_invoice.hpp>

#include <paymentpart/writer_internal.hpp>
#include <paymentpart/writer_options_internal.hpp>


namespace qrinvoice {
namespace payment_part_receipt {

auto write(const writer_options& writer_options, const model::qr_invoice& qr_invoice) -> output::payment_part_receipt;
auto select_writer(const writer_options& writer_options) -> std::unique_ptr<writer>;


} // namespace payment_part_receipt
} // namespace qrinvoice


#endif // QR_INVOICE_WRITE_PAYMENT_PART_INTERNAL_HPP

