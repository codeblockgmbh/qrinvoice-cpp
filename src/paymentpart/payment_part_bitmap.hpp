#ifndef QRINVOICE_CPP_PAYMENT_PART_BITMAP_HPP
#define QRINVOICE_CPP_PAYMENT_PART_BITMAP_HPP

#include <qrinvoice/output_resolution.hpp>
#include <qrinvoice/page_size.hpp>
#include "util/array2d.hpp"

namespace qrinvoice {
namespace payment_part_receipt {

class payment_part_bitmap {

private:
    array2d canvas_;

    array2d init_canvas(qrinvoice::page_size page_size, output_resolution resolution);

public:
    int width;
    int height;

    explicit payment_part_bitmap(qrinvoice::page_size page_size, qrinvoice::output_resolution output_resolution);

    ~payment_part_bitmap (){

    }

    array2d& canvas();

    constexpr static unsigned int DIN_LANG_HEIGHT_150 = 620;
    constexpr static unsigned int DIN_LANG_WIDTH_150 = 1240;

    constexpr static unsigned int DIN_LANG_HEIGHT_300 = 1240;
    constexpr static unsigned int DIN_LANG_WIDTH_300 = 2480;

    constexpr static unsigned int DIN_LANG_HEIGHT_600 = 2480;
    constexpr static unsigned int DIN_LANG_WIDTH_600 = 4960;

    constexpr static unsigned int A5_HEIGHT_150 = 874;
    constexpr static unsigned int A5_WIDTH_150 = 1240;

    constexpr static unsigned int A5_HEIGHT_300 = 1748;
    constexpr static unsigned int A5_WIDTH_300 = 2480;

    constexpr static unsigned int A5_HEIGHT_600 = 3496;
    constexpr static unsigned int A5_WIDTH_600 = 4960;

    constexpr static unsigned int A4_HEIGHT_150 = 1754;
    constexpr static unsigned int A4_WIDTH_150 = 1240;

    constexpr static unsigned int A4_HEIGHT_300 = 3508;
    constexpr static unsigned int A4_WIDTH_300 = 2480;

    constexpr static unsigned int A4_HEIGHT_600 = 7016;
    constexpr static unsigned int A4_WIDTH_600 = 4960;

};

}
}

#endif //QRINVOICE_CPP_PAYMENT_PART_BITMAP_HPP
