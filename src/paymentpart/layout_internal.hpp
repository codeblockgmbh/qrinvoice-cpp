
#ifndef QR_INVOICE_PAYMENT_PART_LAYOUT_INTERNAL_HPP
#define QR_INVOICE_PAYMENT_PART_LAYOUT_INTERNAL_HPP

namespace qrinvoice {
namespace payment_part_receipt {


enum class layout {
	relaxed,
	narrowed,
	compressed
};

} // namespace payment_part_receipt
} // namespace qrinvoice


#endif // QR_INVOICE_PAYMENT_PART_LAYOUT_INTERNAL_HPP

