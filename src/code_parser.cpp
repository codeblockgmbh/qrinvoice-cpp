
#include <string>

#include <qrinvoice/code_parser.hpp>
#include <qrinvoice/model/qr_invoice.hpp>
#include <qrinvoice/model/swiss_payments_code.hpp>
#include <qrinvoice/model/parse_exception.hpp>
#include <qrinvoice/model/validation.hpp>
#include <qrinvoice/model/parse/parse_swiss_payments_code.hpp>
#include <qrinvoice/model/mapper/swiss_payments_code_to_model.hpp>

#include <model/validation/qr_invoice_validator_internal.hpp>


namespace qrinvoice {
namespace code_parser {


model::qr_invoice parse(const std::string& spc_str)
{
	try {
		const model::swiss_payments_code spc = model::parse_swiss_payments_code(spc_str);
		return model::map_swiss_payments_code_to_model(spc);
	} catch (const base_exception&) {
		throw;
	} catch (...) {
		throw model::parse_exception{"Unexpected exception occured during parsing of a Swiss Payment Code as qr_invoice"};
	}
}

void validate(const model::qr_invoice& qi)
{
	model::validation::validate(qi).throw_exception_on_errors();
}

model::qr_invoice parse_and_validate(const std::string& spc)
{
	auto qi = parse(spc);
	code_parser::validate(qi);
	return qi;
}


} // namespace code_parser
} // namespace qrinvoice

