
#ifndef QR_INVOICE_DIMENSION_UNIT_UTILS_INTERNAL_HPP
#define QR_INVOICE_DIMENSION_UNIT_UTILS_INTERNAL_HPP

#include <cmath>


namespace qrinvoice {
namespace layout {
namespace dimension_unit_utils {

constexpr unsigned default_dpi = 72;
constexpr double millimeters_per_inch = 25.4;

constexpr double inches_to_points(double val, unsigned dpi) noexcept
{
	return val * static_cast<double>(dpi);
}

// [[deprecated]]
constexpr double inches_to_points(double val) noexcept
{
	return inches_to_points(val, default_dpi);
}

constexpr double millimeters_to_inches(double val) noexcept
{
	return val / millimeters_per_inch;
}

constexpr double millimeters_to_points(double val, unsigned dpi) noexcept
{
	return inches_to_points(millimeters_to_inches(val), dpi);
}

// [[deprecated]]
constexpr double millimeters_to_points(double val) noexcept
{
	return inches_to_points(millimeters_to_inches(val));
}

inline unsigned millimeters_to_points_rounded(double val, unsigned dpi) noexcept
{
	return static_cast<unsigned int>(std::round(millimeters_to_points(val, dpi)));
}

inline unsigned points_to_pixels(unsigned points, unsigned dpi) noexcept
{
	const double result = static_cast<double>(points) * static_cast<double>(dpi) / static_cast<double>(default_dpi);
	return static_cast<unsigned int>(std::round(result));
}

inline unsigned points_to_pixels(double points, unsigned dpi) noexcept
{
	return static_cast<unsigned int>(std::round(points * static_cast<double>(dpi) / static_cast<double>(default_dpi)));
}

} // dimension_unit_utils
} // namespace layout
} // namespace qrinvoice


#endif // QR_INVOICE_DIMENSION_UNIT_UTILS_INTERNAL_HPP

