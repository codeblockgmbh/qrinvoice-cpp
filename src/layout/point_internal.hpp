
#ifndef QR_INVOICE_POINT_INTERNAL_HPP
#define QR_INVOICE_POINT_INTERNAL_HPP


namespace qrinvoice {
namespace layout {


template <typename Num>
	// requires: operator + (Num, Num) should be defined
class point {
public:
	constexpr point(Num x, Num y)
		:x_{ x }
		,y_{ y } {}

	constexpr point(const point&) = default;

public:
	constexpr Num get_x() const {
		return x_;
	}

	constexpr Num get_y() const {
		return y_;
	}

	constexpr point translate(const point offset) const {
		return translate(offset.x_, offset.y_);
	}

	constexpr point translate(const Num offset_x, const Num offset_y) const {
		return point{x_ + offset_x, y_ + offset_y};
	}

private:
	const Num x_;
	const Num y_;
};


} // namespace layout
} // namespace qrinvoice


#endif // QR_INVOICE_POINT_INTERNAL_HPP

