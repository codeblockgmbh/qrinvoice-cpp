
#ifndef QR_INVOICE_RECT_INTERNAL_HPP
#define QR_INVOICE_RECT_INTERNAL_HPP


#include <algorithm>

#include <layout/point_internal.hpp>
#include <layout/dimension_internal.hpp>


namespace qrinvoice {
namespace layout {

/**
 * A rect represents a rectangle with a specific width and height, including a specific location in a bottom left oriented coordinate system.
 * <p>
 * <pre>
 * ll = lower left corner
 * ul = upper left corner
 * ur = upper right corner
 * lr = lower right corner
 *
 * width  = 5
 * height = 3
 *
 * ul (0,3)              (5,3) ur
 *         +------------+
 *         |            |
 *         |            |
 *         |            |
 *         +------------+
 * ll (0,0)              (5,0) lr
 * </pre>
 *
 * @param <T> The concrete number type such as int, double...
 */

template <typename T>
	// requires: operator + (T, T) should be defined
class rect {
public:
	constexpr rect(T llx, T lly, T urx, T ury)
		:llx_{ llx } ,lly_{ lly }
		,urx_{ urx } ,ury_{ ury } {}

	constexpr rect(T llx, T lly, dimension<T> dim)
		:llx_{ llx } ,lly_{ lly }
		,urx_{ llx + dim.get_width() }
		,ury_{ lly + dim.get_height() } {}

	constexpr rect(const rect&) = default;

public:
	constexpr T get_lower_left_x() const {
		return llx_;
	}

	constexpr T get_lower_left_y() const {
		return lly_;
	}

	constexpr T get_upper_right_x() const {
		return urx_;
	}

	constexpr T get_upper_right_y() const {
		return ury_;
	}

	constexpr T get_left_x() const {
		return get_lower_left_x();
	}

	constexpr T get_right_x() const {
		return get_upper_right_x();
	}

	constexpr T get_bottom_y() const {
		return get_lower_left_y();
	}

	constexpr T get_top_y() const {
		return get_upper_right_y();
	}

	constexpr T get_width() const {
		return std::max(urx_, llx_) - std::min(urx_, llx_);
	}

	constexpr T get_height() const {
		return std::max(ury_, lly_) - std::min(ury_, lly_);
	}

	constexpr rect translate(const point<T> pt) const {
		return translate(pt.get_x(), pt.get_y());
	}

	constexpr rect translate(T offset_x, T offset_y) const {
		return rect{llx_ + offset_x, lly_ + offset_y, {get_width(), get_height()}};
	}

	// this function should be made constexpr in C++14
	inline rect translate_y_to_left_top(const T coordinate_sys_height) const {
		return rect {
			llx_, std::max(coordinate_sys_height, lly_) - std::min(coordinate_sys_height, lly_),
			urx_, std::max(coordinate_sys_height, ury_) - std::min(coordinate_sys_height, ury_)
		};
	}

private:
	/**
	 * the lower left x-coordinate.
	 */
	const T llx_;

	/**
	 * the lower left y-coordinate.
	 */
	const T lly_;

	/**
	 * the upper right x-coordinate.
	 */
	const T urx_;

	/**
	 * the upper right y-coordinate.
	 */
	const T ury_;
};


} // namespace layout
} // namespace qrinvoice


#endif // QR_INVOICE_RECT_INTERNAL_HPP

