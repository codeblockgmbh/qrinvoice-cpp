
#ifndef QR_INVOICE_DIMENSION_INTERNAL_HPP
#define QR_INVOICE_DIMENSION_INTERNAL_HPP


namespace qrinvoice {
namespace layout {

/**
 * Dimension of a geometric shape such as a rectangle.
 * <p>
 * <pre>
 *   width  = 5
 * +------------+
 * |            |
 * |            | height = 3
 * |            |
 * +------------+
 * </pre>
 *
 * @param <T> The concrete number type such as int, double...
 */

template <typename T>
class dimension {
public:
	constexpr dimension(T width, T height) noexcept
		:width_{ width }
		,height_{ height } {}

	constexpr dimension(const dimension&) noexcept = default;

public:

	constexpr T get_width() const noexcept {
		return width_;
	}

	constexpr T get_height() const noexcept {
		return height_;
	}

private:
	const T width_;
	const T height_;
};


} // namespace layout
} // namespace qrinvoice


#endif // QR_INVOICE_DIMENSION_INTERNAL_HPP

