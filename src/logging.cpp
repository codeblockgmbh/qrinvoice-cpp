#include <qrinvoice/logging.hpp>
#include <string>
#include <spdlog/spdlog.h>
#include <spdlog/sinks/basic_file_sink.h>
#include "spdlog/sinks/basic_lazy_file_sink.h"

namespace qrinvoice {
namespace logging {
    static const std::string logger_name = "QrInvoiceCpp";
    
    QRINVOICE_API const void set_log_level(const char* log_level)
    {
        spdlog::level::level_enum level = spdlog::level::from_str(log_level);
        spdlog::set_level(level);
        auto logger = spdlog::get(logger_name);
        if(logger)
        {
            logger->set_level(level);
        }
    }

    QRINVOICE_API const void set_log_file(const char* log_file)
    {
        auto file_logger = spdlog::basic_logger_mt(logger_name, log_file);
        file_logger->flush_on(spdlog::level::level_enum::warn);
        spdlog::set_default_logger(file_logger);
    }

    QRINVOICE_API const void set_lazy_log_file(const char* log_file)
    {
        auto file_logger = spdlog::basic_lazy_logger_mt(logger_name, log_file);
        file_logger->flush_on(spdlog::level::level_enum::warn);
        spdlog::set_default_logger(file_logger);
    }

    QRINVOICE_API const void set_flush_level(const char* flush_level)
    {
        spdlog::level::level_enum level = spdlog::level::from_str(flush_level);
        spdlog::flush_on(level);
        auto logger = spdlog::get(logger_name);
        if(logger)
        {
            logger->flush_on(level);
        }
    }

    QRINVOICE_API const void flush()
    {
        auto logger = spdlog::get(logger_name);
        if(logger)
        {
            logger->flush();
        }
    }

    QRINVOICE_API const void shutdown()
    {
        spdlog::shutdown();
    }
} // namespace logging
} // namespace qrinvoice


