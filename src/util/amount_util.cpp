
#include <string>
#include <sstream>
#include <iomanip>

#include <paymentpart/layout_definitions_internal.hpp>
#include <util/amount_util_internal.hpp>


namespace qrinvoice {
namespace amount_util {


std::string format_amount_for_print(double amount)
{
	std::ostringstream oss;
	oss << std::fixed << std::setprecision(2) << amount;
	std::string amt{ oss.str() };

	const auto dot_pos = amt.find(qrinvoice::payment_part_receipt::layout_definitions::amount_decimal_separator);
	if (dot_pos == std::string::npos)
	{
		return amt;
	}

	for (int separator_pos = dot_pos - 3; separator_pos > 0; separator_pos -= 3)
	{
		amt.insert(separator_pos, qrinvoice::payment_part_receipt::layout_definitions::amount_thousands_separator);
	}

	return amt;
}

std::string format_amount_for_qrcode(double amount)
{
	std::ostringstream oss;
	oss << std::fixed << std::setprecision(2) << amount;
	return oss.str();
}


} // namespace amount_util
} // namespace qrinvoice



