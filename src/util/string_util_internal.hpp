
#ifndef QR_INVOICE_STRING_UTIL_INTERNAL_HPP
#define QR_INVOICE_STRING_UTIL_INTERNAL_HPP

#include <vector>
#include <string>
#include <boost/regex.hpp>

#include <qrinvoice/util/string_util.hpp>

namespace qrinvoice {
namespace string_util {

std::vector<std::string> split_tokens(const std::string& str, const boost::regex& separator_pattern);
std::vector<std::string> split_words(const std::string& str);
std::vector<std::string> split_utf8_characters(const std::string& str);

std::string join(const std::vector<std::string>& v, const std::string& delimiter);

int utf8_length(const std::string& str);

inline int get_numeric_value(char c)
{
	return c - 55;		// subtracting 55 from ASCII value
}

long modulo_97(const std::string& number_string);

} // namespace string_util
} // namespace qrinvoice


#endif // QR_INVOICE_STRING_UTIL_INTERNAL_HPP

