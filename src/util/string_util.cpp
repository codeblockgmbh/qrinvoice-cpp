
#include <vector>
#include <string>
#include <boost/regex.hpp>
#include <sstream>

#include <util/string_util_internal.hpp>
#include "qrinvoice/technical_exception.hpp"

namespace qrinvoice {
namespace string_util {

namespace details {
std::string remove_white_spaces(const std::string& str)
{
    return boost::regex_replace(str, boost::regex{ R"(\s)" }, "");
}

bool contains_white_space(const std::string& str)
{
    boost::smatch sm;
    return boost::regex_search(str, sm, boost::regex{ R"(\s)" });
}
}

std::vector<std::string> split_tokens(const std::string& str, const boost::regex& separator_pattern)
{
	std::vector<std::string> strs;
	boost::sregex_token_iterator it{ str.begin(), str.end(), separator_pattern, -1 };
	while (it != boost::sregex_token_iterator{})
		strs.push_back(*it++);
	return strs;
}

std::vector<std::string> split_words(const std::string& str)
{
	std::istringstream iss{str};
	std::vector<std::string> words;
	for (std::string word; iss >> word;)
		words.push_back(word);

	return words;
}


bool is_valid_utf8_continuation(std::string::const_iterator it, int count)
{
    for (int i = 0; i < count; ++i, ++it) {
        if ((*it & 0xC0u) != 0x80u) {
            return false;
        }
    }
    return true;
}

std::vector<std::string> split_utf8_characters(const std::string& str)
{
    std::vector<std::string> chars;
    for (auto it = str.begin(); it < str.end();) {
        auto ch = static_cast<unsigned char>(*it);

        if (ch <= 0x7F) {
            // 1-byte ASCII character
            chars.emplace_back(it, it + 1);
            ++it;
        } else if ((ch & 0xE0u) == 0xC0u && (ch >= 0xC2 && ch <= 0xDF)) {
            // 2-byte character
            // (ch >= 0xC2 && ch <= 0xDF) --> special restriction for 2 byte utf-8 chars
            if (std::distance(it, str.end()) < 2) {
                throw technical_exception("invalid utf8 sequence");
            }
            chars.emplace_back(it, it + 2);
            it += 2;
        } else if ((ch & 0xF0u) == 0xE0u) {
            // 3-byte character
            if (std::distance(it, str.end()) < 3 || !is_valid_utf8_continuation(it + 1, 2)) {
                throw technical_exception("invalid utf8 sequence");
            }
            chars.emplace_back(it, it + 3);
            it += 3;
        } else if ((ch & 0xF8u) == 0xF0u) {
            // 4-byte character
            if (std::distance(it, str.end()) < 4 || !is_valid_utf8_continuation(it + 1, 3)) {
                throw technical_exception("invalid utf8 sequence");
            }
            chars.emplace_back(it, it + 4);
            it += 4;
        } else {
            // Invalid UTF-8 byte
            throw technical_exception("invalid utf8 byte");
        }
    }

    return chars;
}

int utf8_length(const std::string& str)
{
	std::vector<std::string> chars = split_utf8_characters(str);
	return chars.size();
}

// stepwise check for mod 97 in chunks of 9 at the first time, then
// in chunks of seven prepended by the last mod 97 operation converted
// to a string
long modulo_97(const std::string& number_string)
{
	constexpr int mod_97 = 97;
	int segstart = 0;
	int step = 9;
	std::string prepended;
	long number = 0;
	while (segstart  < static_cast<int>(number_string.length( )) - step) {
		number = std::stol(prepended + number_string.substr(segstart , step));
		const int remainder = number % mod_97;
		if (remainder < 10) {
			prepended = "0";
			prepended.append(std::to_string(remainder));
		} else {
			prepended = std::to_string(remainder);
		}
		segstart = segstart + step;
		step = 7;
	}
	number = std::stol(prepended + number_string.substr(segstart));
	return number % mod_97;
}

std::string join(const std::vector<std::string>& v, const std::string& delimiter) {
	std::string out;
	auto i = v.begin(), e = v.end();
	if (i != e) {
		out += *i++;
		for (; i != e; ++i) out.append(delimiter).append(*i);
	}
	return out;
}


} // namespace string_util
} // namespace qrinvoice

