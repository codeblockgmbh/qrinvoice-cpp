
#ifndef QR_INVOICE_AMOUNT_UTIL_INTERNAL_HPP
#define QR_INVOICE_AMOUNT_UTIL_INTERNAL_HPP

#include <string>


namespace qrinvoice {
namespace amount_util {


std::string format_amount_for_print(double amount);
std::string format_amount_for_qrcode(double amount);


} // namespace amount_util
} // namespace qrinvoice


#endif // QR_INVOICE_AMOUNT_UTIL_INTERNAL_HPP

