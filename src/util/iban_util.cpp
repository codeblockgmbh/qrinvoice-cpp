
#include <string>
#include <cctype>

#include <qrinvoice/util/iban_util.hpp>

#include <util/iban_util_internal.hpp>
#include <util/string_util_internal.hpp>


namespace qrinvoice {
namespace iban_util {


std::string format_iban(const std::string& iban_input)
{
	std::string iban = normalize_iban(iban_input);
	constexpr int chars_per_block = 4;
	const auto number_of_blocks = static_cast<int>(ceil(static_cast<float>(iban.length()) / static_cast<float>(chars_per_block)));
	for (int block_nr = 1; block_nr < number_of_blocks; block_nr++) {
		auto it = iban.begin() + ((block_nr * chars_per_block) + block_nr - 1);
		iban.insert(it, ' ');
	}
	return iban;
}

bool is_valid_iban(const std::string& iban_input, bool should_validate_country_code)
{
	constexpr int min_length = 15;
	constexpr int max_length = 34;
	constexpr int mod_97_remainder = 1;

	// first, remove white spaces
	const std::string iban = normalize_iban(iban_input);

        // 1. Check that the total IBAN length is correct as per the country. If not, the IBAN is invalid
        // --> simple, non-country based check of the iban length
	if (iban.length() < min_length || iban.length() > max_length)
		return false;

        // 2. Move the four initial characters to the end of the string
        // --> CH3908704016075473007 -> 08704016075473007CH39
	std::string iban_rearranged{ iban };
	std::rotate(iban_rearranged.begin(), iban_rearranged.begin() + 4, iban_rearranged.end());

        // 3. Replace each letter in the string with two digits, thereby
	// expanding the string, where A = 10, B = 11, ..., Z = 35.
	std::string numeric_iban;
	for (const char c : iban_rearranged)
		if (std::isupper(c))
			numeric_iban += std::to_string(string_util::get_numeric_value(c));
		else
			numeric_iban += c;

        // 4. Interpret the string as a decimal integer and compute the remainder of that number on division by 97
	bool iban_valid = string_util::modulo_97(numeric_iban) == mod_97_remainder;

	// 5. validate country code
	if (iban_valid && should_validate_country_code)	// only if IBAN is valid, perform country code validation
		return validate_country_code(iban);
	else						// just return the result
		return iban_valid;
}

bool is_qr_iban(const std::string& iban_input)
{
	// Start of the QR IID (Instituts-Identifikation) range - see https://www.iso-20022.ch/lexikon/qr-iban/ for further details
	constexpr int qr_iid_range_start = 30000;

	// End of the QR IID (Instituts-Identifikation) range - see https://www.iso-20022.ch/lexikon/qr-iban/ for further details
	constexpr int qr_iid_range_end = 31999;

	// first, remove white spaces
	std::string iban = normalize_iban(iban_input);
	if (!is_valid_iban(iban, false))
		return false;

        // IBAN: CH3181239000001245689
	//
        // CH		2-stelliger Ländercode / CH für die Schweiz
        // 31		2-stellige Prüfziffer, pro Konto und Bank individuell
        // 81239	Bank-Clearing-Nummer bzw. IID oder QR-IID, zur eindeutigen
	//		Identifizierung der kontoführenden Bank des Zahlungsempfängers (pro Bank individuell)
        // 000001245689	12-stellige Kontonummer des Zahlungsempfängers, wo nötig mit führenden Nullen auf 12 Stellen ergänzt
	const int iid = std::stoi(iban.substr(4, 5));

        // check that IID is within the special QR-IID range
	return qr_iid_range_start <= iid && iid <= qr_iid_range_end;
}


} // namepace iban_util
} // namespace qrinvoice

