#ifndef QRINVOICE_CPP_ARRAY2D_HPP
#define QRINVOICE_CPP_ARRAY2D_HPP

#include <algorithm>

namespace qrinvoice {
    class array2d {
    private:
        unsigned char *data_;
        int height_, width_;
    public:
        array2d(int height, int width) {
            height_ = height;
            width_ = width;
            data_ = new unsigned char[height * width];
            std::fill(data_, data_ + height * width, 0);
        }

        ~array2d() {
            delete[] data_;
        }

        unsigned char *operator[](int x) {
            return (data_ + x * width_);
        }

        unsigned char *data() const {
            return data_;
        }

        int get_width() const {
            return width_;
        }

        int get_height()const {
            return height_;
        }
    };
}

#endif //QRINVOICE_CPP_ARRAY2D_HPP
