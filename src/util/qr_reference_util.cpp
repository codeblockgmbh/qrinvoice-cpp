
#include <string>
#include <array>

#include <qrinvoice/util/string_util.hpp>

#include <qrinvoice/util/qr_reference_util.hpp>
#include <util/string_util_internal.hpp>
#include <qrinvoice/model/validation/validation_exception.hpp>
#include <qrinvoice/technical_exception.hpp>


namespace qrinvoice {
namespace qr_reference_util {


/**
 * The QR reference must have length of 27 characters
 */
constexpr int length = 27;
constexpr std::array<int, 10> digit_table = {{0, 9, 4, 6, 8, 2, 7, 1, 3, 5}};

constexpr int chars_first_block = 2;
constexpr int chars_per_block = 5;

bool is_valid_qr_reference(const std::string& qr_reference_input)
{
	// first, remove all whitespaces
	// --> 11 00012 34560 00000 00008 13457 -> 110001234560000000000813457
	std::string qr_reference = string_util::details::remove_white_spaces(qr_reference_input);

	// 1. perform basic length and prefix checks
	if (qr_reference.length() != length)
		return false;

	// 2. split ESR number to verify checksum digit
        // 110001234560000000000813457 -> 11000123456000000000081345
	const std::string qr_reference_without_checksum_digit{ qr_reference.substr(0, 26) };
        // 110001234560000000000813457 -> 7
	const int checksum_digit = std::stoi(std::string{ qr_reference[26] });

        // 3. calculate checksum digit and verify it
	return modulo_10_recursive(qr_reference_without_checksum_digit) == checksum_digit;
}

int modulo_10_recursive(const std::string& number)
{
	int transfer = 0;
	for (const auto c : number)
		transfer = digit_table[(transfer + std::stoi(std::string{c})) % 10];

	return (10 - transfer) % 10;
}

std::string format_qr_reference(const std::string& qr_reference_input)
{
    if(qr_reference_input.length() < chars_first_block) {
        return qr_reference_input;
    }
    std::string qr_reference = normalize_qr_reference(qr_reference_input);
    const auto number_of_blocks = static_cast<int>(ceil(static_cast<float>((qr_reference.length() - chars_first_block)) / static_cast<float>(chars_per_block)) + 1.0f);
    for (int block_nr = 1; block_nr < number_of_blocks; block_nr++) {
        if(block_nr == 1) {
            qr_reference.insert(qr_reference.begin() + chars_first_block, ' ');
        } else {
            auto it = qr_reference.begin() + (chars_first_block + ((block_nr - 1) * chars_per_block)) + block_nr - 1;
            qr_reference.insert(it, ' ');
        };
    }
    return qr_reference;
}


std::string create_qr_reference(const std::string& qr_reference_input)
{
    if(string_util::details::remove_white_spaces(qr_reference_input).empty())
    {
        throw technical_exception("Input must not be empty");
    }

    const std::string &normalized_qr_reference = normalize_qr_reference(qr_reference_input);
    if(is_valid_qr_reference(normalized_qr_reference))
    {
        return normalized_qr_reference;
    }
    else if (normalized_qr_reference.length() == length)
    {
        throw technical_exception("Input reference length equals QR reference number total length of '" + std::to_string(length) + "' but is not a valid QR reference");
    }

    if(normalized_qr_reference.length() > length)
    {
        throw technical_exception("Invalid QR reference number - total length of '" + std::to_string(length) + "' exceeded");
    }
    else
    {
        const std::string qr_reference_filled_up = string_util::details::left_pad(qr_reference_input, length -1, '0');
        // Merging the first 26 digits of the QR reference with the correct calculated checksum
        const std::string new_qr_reference_number = qr_reference_filled_up + std::to_string(modulo_10_recursive(qr_reference_filled_up));
        if (is_valid_qr_reference(new_qr_reference_number))
        {
            return new_qr_reference_number;
        }
        else
        {
            throw technical_exception("Invalid QR reference number");
        }
    }

}

std::string create_qr_reference(const std::string& customer_id, const std::string& qr_reference_input)
{

    const std::string clean_customer_id = string_util::details::remove_white_spaces(customer_id);
    const std::string clean_qr_reference_input = string_util::details::remove_white_spaces(qr_reference_input);

    // If no customerId is given, return a conventional qrReferenceNumber
    if (clean_customer_id.empty()) {
        return create_qr_reference(clean_qr_reference_input);
    }

    // Check validity of the qrReferenceInput
    if (clean_qr_reference_input.empty()) {
        throw technical_exception("Input must not be empty");
    }

    // Check total length
    if (clean_customer_id.length() + clean_qr_reference_input.length() > length - 1) {
        throw technical_exception("Combined length of customer Id and QR reference number cannot exceed " + std::to_string(length - 1) + ".");
    }

    // Otherwise use the customerId as prefix, the qrReferenceInput as suffix and fill the rest with 0's.
    const std::string combined_input = clean_customer_id + string_util::details::left_pad(clean_qr_reference_input, length - 1 - clean_customer_id.length(), '0');

    return create_qr_reference(combined_input);
}

} // namespace qr_reference_util
} // namespace qrinvoice

