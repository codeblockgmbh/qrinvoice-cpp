
#ifndef QR_INVOICE_PAYMENT_REFERENCE_UTIL_INTERNAL_HPP
#define QR_INVOICE_PAYMENT_REFERENCE_UTIL_INTERNAL_HPP

#include <string>

#include <qrinvoice/model/payment_reference.hpp>

namespace qrinvoice {

namespace payment_reference_util {

std::string format_reference(const model::payment_reference& pr);

} // namespace payment_reference_util
} // namespace qrinvoice


#endif // QR_INVOICE_PAYMENT_REFERENCE_UTIL_INTERNAL_HPP

