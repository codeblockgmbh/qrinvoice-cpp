#include <utility>

#include <vector>
#include <string>
#include <algorithm>
#include <numeric>
#include <iterator>

#include <util/string_util_internal.hpp>
#include <util/text_util_internal.hpp>

#include <spdlog/spdlog.h>
#include <iostream>

namespace qrinvoice {
namespace text_util {

std::vector<std::string> text_splitter::split_text_by_boundary(const std::string& text, double width) const
{
	spdlog::trace("text_splitter::split_text_by_boundary - start - string: {}", text);
	double total_length;
	if(widths_fn_) {
		const std::vector<float> result = widths_fn_(text);
		sizes_ = std::vector<float>(result);
		total_length = static_cast<double>(sizes_.back());
	} else {
		total_length = width_fn_(text);
	}

	if (total_length <= width) {
		spdlog::trace("text_splitter::split_text_by_boundary - end - no line splitting required");
		return {text};
	}

	// try first with word-based wrapping
	auto lines = split_text_by_words(text, width);
	if (lines.empty() || lines.size() > 2) {
		// either even a single word isn't fitting in a line or there are more than 2 lines,
		// thus applying aggressive splitting without further checks
		spdlog::debug("applying aggressive splitting");

		lines = split_text_aggressively(text, width);
	}
	
	spdlog::trace("text_splitter::split_text_by_boundary - end - line count: {}", lines.size());
	return lines;
}

std::vector<std::string> text_splitter::split_text_by_words(const std::string& text, const double width) const
{
	std::vector<std::string> words{string_util::split_words(text)};
	return split_text(std::move(words), width);
}

std::vector<std::string> text_splitter::split_text_aggressively(const std::string& text, const double width) const
{
	spdlog::trace("text_splitter::split_text_aggressively - start");
	std::vector<std::string> chars{string_util::split_utf8_characters(text)};
	const auto result = split_text_aggressively(std::move(chars), width);
	spdlog::trace("text_splitter::split_text_aggressively - end");
	return result;
}

std::vector<std::string> text_splitter::split_text_aggressively(std::vector<std::string>&& words, const double width) const
{
    if (words.empty())
        return{};

    // calculate sizes from starting of the text to end of each word
    std::vector<double> sizes = calculate_break_pos_sizes(words);

    std::vector<std::string> lines;
    while (sizes.begin() != sizes.end()) {
        const auto break_pos = std::upper_bound(sizes.begin(), sizes.end(), width);

        auto words_break_pos = words.begin() + (break_pos - sizes.begin());
        auto line = std::accumulate(words.begin() + 1, words_break_pos, *words.begin(),
                                    [&](const std::string& a, const std::string& b) { return a + b; });
        lines.push_back(line);

        const auto delta = *(break_pos - 1);

        words.erase(words.begin(), words_break_pos);
        sizes.erase(sizes.begin(), break_pos);

        std::for_each(sizes.begin(), sizes.end(), [delta](double& sz) {
            sz -= delta;
        });
    }

    return lines;
}
std::vector<std::string> text_splitter::split_text(std::vector<std::string>&& words, const double width) const
{
	if (words.empty()) {
		return{};
	}

	const char* space = " ";
	const auto space_size = width_fn_ ? width_fn_(space) : widths_fn_(space).front();

	// calculate sizes from starting of the text to end of each word
	std::vector<double> sizes = calculate_break_pos_sizes(words, space_size);

	std::vector<std::string> lines;
	while (sizes.begin() != sizes.end()) {
		const auto break_pos = std::upper_bound(sizes.begin(), sizes.end(), width);
		if (break_pos == sizes.begin()) { // not even the first word is fitting, return empty in this case
			spdlog::trace("not even the first word is fitting, going for aggressive splitting");
			return {};
		}

		auto words_break_pos = words.begin() + (break_pos - sizes.begin());
		auto line = std::accumulate(words.begin() + 1, words_break_pos, *words.begin(),
				[&space](const std::string& a, const std::string& b) {
			std::string result = a;
			result.append(space).append(b);
			return result;
		});
		lines.push_back(line);

		const auto delta = *(break_pos - 1) + space_size;

		words.erase(words.begin(), words_break_pos);
		sizes.erase(sizes.begin(), break_pos);

		std::for_each(sizes.begin(), sizes.end(), [delta](double& sz) {
			sz -= delta;
		});
	}

	return lines;
}

// same purpose as calculate_break_pos_sizes(words, space_size), however it does calculate width by building intermediate strings - needed due to glyph kerning
std::vector<double> text_splitter::calculate_break_pos_sizes(const std::vector<std::string>& characters) const
{
	// TODO horrible speed - measure 60-70ms - see trace log - calculates all intermediate text widths...
	std::vector<double> sizes;
	sizes.reserve(characters.size());

	int pos = 0;

	spdlog::trace("text_splitter::calculate_break_pos_sizes - start");
	if(widths_fn_) {
		std::transform(characters.begin(), characters.end(), std::back_inserter(sizes), [this, &characters, &pos](const std::string& word) {
			const double width = static_cast<double>(sizes_.at(pos));
			pos++;
			return width;
		});
	} else {
		std::transform(characters.begin(), characters.end(), std::back_inserter(sizes), [this, &characters, &pos](const std::string& word) {
			auto end = characters.begin() + (pos + 1);
			auto line = std::accumulate(characters.begin() + 1, end, *characters.begin(), [](const std::string& a, const std::string& b) { return a + b; });
			const double width = width_fn_(line);

			spdlog::trace("text_splitter::calculate_break_pos_sizes - line {} ({})", line, width);

			pos++;

			return width;
		});
	}
	spdlog::trace("text_splitter::calculate_break_pos_sizes - end");
	return sizes;
}

//
// This function calculates sizes from starting of the text to end of each word
// For example, given the words {"some", "sample", "text"} this will produce
// "some"		-> 4
// "some sample"	-> 11
// "some sample text"	-> 16
// => {4, 11, 16}
//
// Above example assumes that each character is of size 1, only for understanding.
// Sizes are calculated according to the font.
//
std::vector<double> text_splitter::calculate_break_pos_sizes(const std::vector<std::string>& words, double space_size) const
{
	std::vector<double> sizes;
	sizes.reserve(words.size());

	if(widths_fn_) {
		int num_chars = 0;
		for(std::string const &word : words) {
			if(num_chars > 0) {
				num_chars += 1; // space
			}
			num_chars += string_util::utf8_length(word);
			const int idx = num_chars - 1;
			double width = sizes_.at(idx);
			spdlog::trace("text_splitter::calculate_break_pos_sizes idx={} width={}", idx, width);
			sizes.push_back(width);
		}
	} else {
		std::transform(words.begin(), words.end(), std::back_inserter(sizes), [this](const std::string& word) {
			return width_fn_(word);
		});

		if (space_size > 0) {
			std::for_each(sizes.begin(), sizes.end() - 1, [&](double& size) { size += space_size; });
		}

		std::partial_sum(sizes.begin(), sizes.end(), sizes.begin());

		if (space_size > 0) {
			std::for_each(sizes.begin(), sizes.end() - 1, [&](double& size) { size -= space_size; });
		}
	}

	return sizes;
}

    text_splitter::text_splitter(text_splitter::width_fn_t width_fn)
            :width_fn_{std::move(width_fn)} {}

	text_splitter::text_splitter(text_splitter::widths_fn_t widths_fn)
			:widths_fn_{std::move(widths_fn)} {}

} // namespace text_util
} // namespace qrinvoice

