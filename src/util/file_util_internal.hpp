
#ifndef QR_INVOICE_FILE_UTIL_INTERNAL_HPP
#define QR_INVOICE_FILE_UTIL_INTERNAL_HPP

#include <string>
#include <fstream>

namespace qrinvoice {
namespace file_util {


constexpr char path_separator = PATH_SEPARATOR;


inline void ensure_trailing_path_separator(std::string& dir_path)
{
	if (dir_path.back() != file_util::path_separator)
		dir_path += file_util::path_separator;
}

/*
 * This function actually checks for accessibility of the file rather than its existence
 */
inline bool file_exists(const std::string& file_name)
{
	std::ifstream file{ file_name };
	return file.good();
}


} // namespace file_util
} // namespace qrinvoice


#endif // QR_INVOICE_FILE_UTIL_INTERNAL_HPP


