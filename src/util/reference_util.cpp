
#include <string>
#include <array>

#include <qrinvoice/util/reference_util.hpp>
#include <qrinvoice/util/creditor_reference_util.hpp>
#include <qrinvoice/util/qr_reference_util.hpp>

namespace qrinvoice {
namespace reference_util {

std::string format_reference(const model::reference_type::ref_type& reference_type , const std::string& reference)
{
    switch (reference_type) {
        case model::reference_type::qr_reference:
            return qr_reference_util::format_qr_reference(reference);
        case model::reference_type::creditor_reference:
            return creditor_reference_util::format_creditor_reference(reference);
        case model::reference_type::without_reference:
        default:
            // for the sake of completeness
            return "";
    }
}

std::string normalize_reference(const model::reference_type::ref_type& reference_type , const std::string& reference)
{
    switch (reference_type) {
        case model::reference_type::qr_reference:
            return qr_reference_util::normalize_qr_reference(reference);
        case model::reference_type::creditor_reference:
            return creditor_reference_util::normalize_creditor_reference(reference);
        case model::reference_type::without_reference:
        default:
            return reference;
    }
}

} // namespace reference_util
} // namespace qrinvoice

