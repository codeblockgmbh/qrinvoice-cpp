
#ifndef QR_INVOICE_TEXT_UTIL_INTERNAL_HPP
#define QR_INVOICE_TEXT_UTIL_INTERNAL_HPP

#include <vector>
#include <string>
#include <functional>


namespace qrinvoice {
namespace text_util {

/**
* This class is used to split the text to by boundaries.
*/

class text_splitter {
public:
	using width_fn_t = std::function<double(const std::string&)>;
	using widths_fn_t = std::function<std::vector<float>(const std::string&)>;

public:
	explicit text_splitter(width_fn_t width_fn);
	explicit text_splitter(widths_fn_t widths_fn);

	std::vector<std::string> split_text_by_boundary(const std::string& text, double width) const;

private:
	std::vector<std::string> split_text_by_words(const std::string& text, double width) const;
	std::vector<std::string> split_text(std::vector<std::string>&& words, double width) const;
	std::vector<std::string> split_text_aggressively(const std::string& text, double width) const;
	std::vector<std::string> split_text_aggressively(std::vector<std::string>&& words, double width) const;
	std::vector<double> calculate_break_pos_sizes(const std::vector<std::string>& characters) const;
	std::vector<double> calculate_break_pos_sizes(const std::vector<std::string>& words, double space_size) const;

private:
	const width_fn_t width_fn_;
	const widths_fn_t widths_fn_;

	std::vector<float>& sizes_ = std::vector<float>();
};


} // namespace text_util
} // namespace qrinvoice

#endif // QR_INVOICE_TEXT_UTIL_INTERNAL_HPP

