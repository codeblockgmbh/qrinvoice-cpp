#include <vector>
#include <string>
#include <sstream>

#include <qrinvoice/util/charset_encoding_util.hpp>

#include <boost/locale.hpp>

namespace qrinvoice {
namespace charset_encoding_util {

std::string to_utf8(const std::string &input, const std::string &input_charset) {
    std::string utf8_string = boost::locale::conv::to_utf<char>(input, input_charset);
    return utf8_string;
}

} // namespace charset_encoding_util
} // namespace qrinvoice

