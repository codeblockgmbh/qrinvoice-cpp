
#ifndef QR_INVOICE_IBAN_UTIL_INTERNAL_HPP
#define QR_INVOICE_IBAN_UTIL_INTERNAL_HPP

#include <string>

#include <util/string_util_internal.hpp>
#include <qrinvoice/util/country_util.hpp>
#include <qrinvoice/model/swiss_payments_code.hpp>


namespace qrinvoice {
namespace iban_util {

inline bool validate_country_code(const std::string& iban)
{
	const std::string country_code = iban.substr(0, 2);
	return qrinvoice::model::spc::supported_iban_countries.find(country_code) != qrinvoice::model::spc::supported_iban_countries.end();
}

} // namespace iban_util
} // namespace qrinvoice


#endif // QR_INVOICE_IBAN_UTIL_INTERNAL_HPP

