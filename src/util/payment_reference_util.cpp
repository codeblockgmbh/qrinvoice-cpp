
#include <string>
#include <array>

#include <qrinvoice/util/reference_util.hpp>
#include <util/payment_reference_util_internal.hpp>

namespace qrinvoice {
namespace payment_reference_util {

    std::string format_reference(const model::payment_reference& pr)
    {
        if (!pr.empty()) {
            return qrinvoice::reference_util::format_reference(pr.get_reference_type().get(), pr.get_reference());
        }
        return {};
    }

} // namespace payment_reference_util
} // namespace qrinvoice

