
#include <string>
#include <cctype>

#include <boost/regex.hpp>

#include <qrinvoice/util/string_util.hpp>

#include <qrinvoice/util/creditor_reference_util.hpp>
#include <qrinvoice/technical_exception.hpp>
#include <util/string_util_internal.hpp>
#include <qrinvoice/technical_exception.hpp>

namespace qrinvoice {
namespace creditor_reference_util {


/**
 * In the beginning there are two letters 'RF'
 */
const std::string prefix{ "RF" };

constexpr int chars_per_block = 4;

/**
 * At least four chars expected (RF + 2 Digits Checksum)
 */
constexpr int min_length = 4;

/**
 * The Creditor Reference is 25 characters long and alphanumeric
 */
constexpr int max_length = 25;

constexpr int mod_97_remainder = 1;

const boost::regex valid_character_pattern{"^[0-9a-zA-Z]*$"};

bool is_valid_creditor_reference(const std::string& creditor_reference_input)
{

	// first, remove all white spaces
	// --> RF45 1234 5123 45 -> RF451234512345
	const std::string creditor_reference{ normalize_creditor_reference(creditor_reference_input) };

	// 1. perform basic length and prefix check
	if (creditor_reference.length() < min_length || max_length < creditor_reference.length())
		return false;

	if (creditor_reference.compare(0, prefix.length(), prefix) != 0)
		return false;

	// 2. move the 4 initial characters to the end of the string
	// RF451234512345 -> 1234512345RF45
	auto creditor_reference_rearranged{ creditor_reference };
	std::rotate(creditor_reference_rearranged.begin(), creditor_reference_rearranged.begin() + 4,
			creditor_reference_rearranged.end());

	// 3. replace each letter in the string with two digits, thereby
	// expanding the string, where A = 10, B = 11, ..., Z = 35.
	// 1234512345RF45 -> 1234512345271545
	const std::string numeric_creditor_reference = convert_to_numeric_representation(creditor_reference_rearranged);

	// 4. interpret the string as a decimal integer and compute the remainder of that number on division by 97
	// true and valid, if remainder equals 1
	return string_util::modulo_97(numeric_creditor_reference) == mod_97_remainder;
}

std::string convert_to_numeric_representation(std::string str)
{
    if (!regex_match(str, valid_character_pattern)) {
        throw technical_exception("Encountered illegal characters");
    }
    std::string numeric_representation;
    string_util::details::to_upper(str);
    for (const char c : str)
    {
        if (std::isupper(c))
        {
            numeric_representation += std::to_string(string_util::get_numeric_value(c));
        }
        else
        {
            numeric_representation += c;
        }
    }
    return numeric_representation;
}

std::string format_creditor_reference(const std::string& creditor_reference_input)
{
	std::string creditor_reference = normalize_creditor_reference(creditor_reference_input);
	const auto number_of_blocks = static_cast<int>(ceil(static_cast<float>(creditor_reference.length()) / static_cast<float>(chars_per_block)));
	for (int block_nr = 1; block_nr < number_of_blocks; block_nr++) {
		auto it = creditor_reference.begin() + ((block_nr * chars_per_block) + block_nr - 1);
		creditor_reference.insert(it, ' ');
	}
	return creditor_reference;
}

std::string create_creditor_reference(const std::string& creditor_reference_input)
{
    if (string_util::details::remove_white_spaces(creditor_reference_input).empty()) {
        throw technical_exception("Input must not be empty");
    }

    const std::string normalized_creditor_reference = normalize_creditor_reference(creditor_reference_input);
    if (is_valid_creditor_reference(normalized_creditor_reference)) {
        return normalized_creditor_reference;
    } else if (normalized_creditor_reference.length() == max_length) {
        throw technical_exception("Input reference length equals Creditor reference number max length of '" + std::to_string(max_length) + "' but is not a valid Creditor reference");
    }

    if (normalized_creditor_reference.length() > max_length) {
        throw technical_exception("Invalid Creditor reference number - max length of '" + std::to_string(max_length) + "' exceeded");
    } else {
        // we build the creditor reference in the format used to validate the checksum (e.g. <referenc><RF><checkdigits>)
        // e.g. input is 42 -> 42RF00 whereas checkdigits are left to 00 which makes it easy to determine the correct checkdigits
        const std::string check_digits = calculate_check_digits_for_reference_number(normalized_creditor_reference);
        const std::string new_creditor_reference_number = prefix + check_digits + normalized_creditor_reference;

        if (is_valid_creditor_reference(new_creditor_reference_number)) {
            return new_creditor_reference_number;
        } else {
            throw technical_exception("Invalid Creditor reference number");
        }
    }
}

std::string calculate_check_digits_for_reference_number(const std::string& input)
{
    return calculate_check_digits(convert_to_numeric_representation(input + prefix + "00"));
}

std::string calculate_check_digits(const std::string& input)
{
    if (input.empty() || input.length() < 3) {
        throw technical_exception("Input must not be empty and be at least 3 digits long");
    }
    if (input.substr(input.length() - 2, input.length()) != "00") {
        throw technical_exception("Input must end with two zeros '00'");
    }

    const int current_mod_97_remainder = string_util::modulo_97(input);
    // e.g. 97 - 53 + 1 (whereas 53 is determined by input)
    const int checksum = 97 - current_mod_97_remainder + mod_97_remainder;
    if (checksum >= 10) {
        return std::to_string(checksum);
    } else {
        return "0" + std::to_string(checksum);
    }
}

} // namespace creditor_reference_util
} // namespace qrinvoice

