
#ifndef QR_INVOICE_SWISS_QR_CODE_INTERNAL_HPP
#define QR_INVOICE_SWISS_QR_CODE_INTERNAL_HPP

#include <layout/dimension_internal.hpp>
#include <system/libqrencode/error_correction_level.hpp>


/**
 * Dimensions are in millimeters, font-sizes in points.
 */
namespace qrinvoice {
namespace swiss_qr_code {

/**
 * Error correction level M must be used according to the spec 5.1 - Error correction level - v2.0
 */
constexpr auto ec_level = system::qrencode::error_correction_level::m;

/*
 * Max permitted QR Code version with error correction level M and binary level M must be used according to the spec 5.2  - v2.0
 * It implicitly verifies that max content and modules are not exceeded
 */
constexpr int qr_code_max_version = 25;

/**
 * Size in mm according to the spec 5.4 - Measurements of the Swiss QR Code for printing - v2.0
 */
constexpr layout::dimension<unsigned int> qr_code_size{46u, 46u};

/**
 * Size in mm according to the spec 5.4.2 - Recognition characters - v2.0
 */
constexpr layout::dimension<unsigned int> qr_code_logo_size{7u, 7u};
} // namespace swiss_qr_code
} // namespace qrinvoice


#endif // QR_INVOICE_SWISS_QR_CODE_INTERNAL_HPP

