
#include <string>
#include <cstdlib>

#include <config/env_vars_internal.hpp>

namespace qrinvoice {
namespace env {


constexpr const char* const boolean_env_var_truth_value = "true";

const char* const get_string(const char* env_var)
{
    return std::getenv(env_var);
}

bool is_true(const char* env_var)
{
	const char* val = std::getenv(env_var);
	return val && std::string{boolean_env_var_truth_value} == val;
}

void set_env(const char* env_var)
{
	_putenv_s(env_var, boolean_env_var_truth_value);
}


} // namespace env
} // namespace qrinvoice
