
#ifndef QR_INVOICE_ENV_VARS_INTERNAL_HPP
#define QR_INVOICE_ENV_VARS_INTERNAL_HPP


/*
 * Following environment variables can be set by the user to alter the behavior
 * of the library.
 */

namespace qrinvoice {
namespace env {


/*
 * A boolean environment variable. It can either be set to any value or unset.
 */
constexpr const char* const debug_layout = "QRINVOICE_DEBUG_LAYOUT";

/*
 * A boolean environment variable. It can either be set to any value or unset.
 */
constexpr const char* const dont_ignore_layout_errors = "QR_INVOICE_DONT_IGNORE_LAYOUT_ERRORS";

/*
 * A boolean environment variable. It can either be set to any value or unset.
 */
constexpr const char* const strict_validation = "QR_INVOICE_STRICT_VALIDATION";

/*
 * A boolean environment variable. It can either be set to any value or unset.
 */
constexpr const char* const disable_bill_information_validation = "QR_INVOICE_DISABLE_BILL_INFORMATION_VALIDATION";

/*
 * A boolean environment variable. It can either be set to any value or unset.
 */
constexpr const char* const disable_do_not_use_for_payment_validation = "QR_INVOICE_DISABLE_DO_NOT_USE_FOR_PAYMENT_VALIDATION";

/*
 * A boolean environment variable. It can either be set to any value or unset.
 */
constexpr const char* const ignore_system_fonts = "QR_INVOICE_IGNORE_SYSTEM_FONTS";	// currently unused

/*
 * User should set this environment variable to directory path where font files
 * reside.
 */
constexpr const char* const font_path = "QR_INVOICE_FONT_PATH";


/* In v2.0 of the specification in november 2018, the ultimate creditor is not allowed for use, but prepared for future use
   this environment variable unlocks the usage in this version */
constexpr const char* const  unlock_ultimate_creditor = "QRINVOICE_UNLOCK_ULTIMATE_CREDITOR";

const char* const get_string(const char* env_var);

bool is_true(const char* env_var);

void set_env(const char* env_var);


} // namespace env
} // namespace qrinvoice


#endif // QR_INVOICE_ENV_VARS_INTERNAL_HPP

