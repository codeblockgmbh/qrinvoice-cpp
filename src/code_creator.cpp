
#include <string>

#include <qrinvoice/code_creator.hpp>
#include <qrinvoice/model/mapper/model_to_swiss_payments_code.hpp>

#include <qrcode/swiss_qr_code_writer_internal.hpp>


namespace qrinvoice {


std::string code_creator::create_swiss_payments_code() const {
	return model::map_model_to_swiss_payments_code(qr_invoice_).to_swiss_payments_code_string();
}

qrinvoice::output::qr_code code_creator::create_qr_code() const {
	return qrcode::swiss_qr_code_writer{}.write(
			output_format_,
			create_swiss_payments_code(),
			desired_qr_code_size_);
}


} // namespace qrinvoice

