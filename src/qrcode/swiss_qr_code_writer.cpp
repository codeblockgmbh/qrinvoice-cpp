
#include <vector>
#include <string>
#include <qrinvoice/output/qr_code.hpp>
#include <qrinvoice/output_format.hpp>
#include <qrinvoice/model/validation/validation_exception.hpp>
#include <qrinvoice/not_yet_implemented_exception.hpp>

#include <resource/swiss_cross.hpp>

#include <system/libqrencode/encode_string.hpp>
#include <system/libqrencode/qr_code_modules.hpp>
#include <qrcode/swiss_qr_code_writer_internal.hpp>
#include <util/string_util_internal.hpp>
#include <qr_invoice_swiss_qr_code_internal.hpp>
#include <qrinvoice/model/swiss_payments_code.hpp>
#include "swiss_qr_code_writer_internal.hpp"
#include "qr_code_bitmap_internal.hpp"

#include <spdlog/spdlog.h>
#include <system/stb/stbi_helper.hpp>

namespace qrinvoice {
namespace qrcode {


output::qr_code swiss_qr_code_writer::write(output_format output_fmt,
    const std::string& qr_code_string, const unsigned desired_qr_code_size) const
{
	validate_qr_code_string_argument(qr_code_string);
	validate_desired_qr_code_size(desired_qr_code_size);

	try {
		auto bytes = render_swiss_qr_code(output_fmt, qr_code_string, desired_qr_code_size);
		return {output_fmt, bytes};
	} catch (const base_exception&) {
		throw;
	} catch (const std::exception&) {
		throw technical_exception {"Unexpected exception encountered during swiss_qr_code creation"};
	}
}

const qrinvoice::qrcode::qr_code_bitmap swiss_qr_code_writer::write_bitmap(const std::string& qr_code_string) const
{
	validate_qr_code_string_argument(qr_code_string);

	try {
		const auto qr_modules = system::qrencode::encode_string(qr_code_string, ec_level_);
		spdlog::trace("swiss_qr_code_writer::write_bitmap - modules: {}", qr_modules.width());
		spdlog::trace("swiss_qr_code_writer::write_bitmap - version: {}", qr_modules.version());

		// No margin. It gets added later on in the image/rendering. It's much
		// easier because the spec says it has to be 5 mm (2.5.3 - Swiss QR Code section - v1.0)

		return qr_code_modules_to_bitmap(qr_modules);
	} catch (const base_exception&) {
		throw;
	} catch (const std::exception&) {
		throw technical_exception {"Unexpected exception encountered during swiss_qr_code creation"};
	}
}

std::vector<unsigned char> swiss_qr_code_writer::render_swiss_qr_code(output_format output_fmt, const std::string& qr_code_string,
		const unsigned desired_qr_code_size) const
{
	switch (output_fmt) {
	case output_format::png:
	case output_format::jpg:
	case output_format::bmp:
	{
		auto qr_image = render_int_scaled_qr_code(qr_code_string, desired_qr_code_size);

        // now overlay with swiss cross - scaled
        spdlog::trace("swiss_qr_code_writer::render_swiss_qr_code - before swiss cross overlay");
		array2d swiss_cross_unscaled = swiss_cross();
		// swiss cross is 7mm whereas qr code is 46mm - use this ratio and apply it to the int-scaled qr image size
		const auto swiss_cross_size = static_cast<int>(std::floor(static_cast<float>(swiss_qr_code::qr_code_logo_size.get_width()) / static_cast<float>(swiss_qr_code::qr_code_size.get_width()) * static_cast<float>(qr_image.get_width())));
		array2d swiss_cross_scaled(swiss_cross_size,swiss_cross_size);
		const auto offset = static_cast<int>(std::round(static_cast<float>((qr_image.get_width() - swiss_cross_size)) / 2.0f));

		stbi_helper::resizeUint8(swiss_cross_unscaled.data(), swiss_cross_unscaled.get_width(), swiss_cross_unscaled.get_height(), 0,
								 swiss_cross_scaled.data(), swiss_cross_size, swiss_cross_size, 0, 1);

		for(int y = 0; y < swiss_cross_scaled.get_height(); y++) {
			unsigned char *const rowArr = qr_image[y + offset];
			for(int x = 0; x < swiss_cross_scaled.get_width(); x++) {
				rowArr[x + offset] = swiss_cross_scaled[y][x];
			}
		}
        spdlog::trace("swiss_qr_code_writer::render_swiss_qr_code - after swiss cross overlay");

		std::vector<unsigned char> result = {};
		if(!stbi_helper::saveImageToVector(&result, qr_image, output_fmt)) {
			throw technical_exception("Error while trying to write image into memory");
		}
        spdlog::trace("swiss_qr_code_writer::render_swiss_qr_code - after save image to vector");

		return result;
	}
	default:
		throw not_yet_implemented_exception {std::string{"Output format \""} + output_format_ops::get_mime_type(output_fmt) +
			"\" has not yet been implemented" };
	}
}

qrinvoice::array2d swiss_qr_code_writer::render_int_scaled_qr_code(const std::string &qr_code_string, unsigned desired_qr_code_size) const {
	spdlog::trace("swiss_qr_code_writer::render_int_scaled_qr_code - before encode_string");
	const auto qr_code_bitmap = write_bitmap(qr_code_string);
	spdlog::trace("swiss_qr_code_writer::render_int_scaled_qr_code - after encode_string");
	return render_int_scaled_qr_code(qr_code_bitmap, desired_qr_code_size);
}
qrinvoice::array2d swiss_qr_code_writer::render_int_scaled_qr_code(const qrinvoice::qrcode::qr_code_bitmap& qr_code_bitmap, unsigned desired_qr_code_size) const {
	const auto optimal_qr_code_size = get_optimal_qr_code_render_size(desired_qr_code_size, qr_code_bitmap.get_modules());

	spdlog::trace("swiss_qr_code_writer::render_int_scaled_qr_code - desired_qr_code_size: {}", desired_qr_code_size);
	spdlog::trace("swiss_qr_code_writer::render_int_scaled_qr_code - optimal_qr_code_render_size: {}", optimal_qr_code_size);
	spdlog::trace("swiss_qr_code_writer::render_int_scaled_qr_code - qr_modules: {}", qr_code_bitmap.get_modules());

	// No margin. It gets added later on in the image/rendering. It's much
	// easier because the spec says it has to be 5 mm (2.5.3 - Swiss QR Code section - v1.0)
	const qrinvoice::array2d image = qr_code_modules_to_image(qr_code_bitmap, optimal_qr_code_size);
	spdlog::trace("swiss_qr_code_writer::render_int_scaled_qr_code - qr_image_width: {}", image.get_width());
	spdlog::trace("swiss_qr_code_writer::render_int_scaled_qr_code - qr_image_height: {}", image.get_height());
	return image;
}

qrinvoice::array2d swiss_qr_code_writer::qr_code_modules_to_image(const qrinvoice::qrcode::qr_code_bitmap& qr_code_bitmap, unsigned width) const {
	unsigned char BLACK = 0;
	unsigned char WHITE = 255;
	const unsigned int modules_count = qr_code_bitmap.get_modules();

	const auto scale = width / modules_count ;
	const unsigned int unscaled_qr_code_size = modules_count * scale;
	array2d qrcode_int_scaled(unscaled_qr_code_size,unscaled_qr_code_size);

	for (unsigned int module_row = 0; module_row < modules_count; module_row++) {
		const auto module_row_offset = module_row * scale;
		for (unsigned int module_col = 0; module_col < modules_count; module_col++) {
			const auto module_col_offset = module_col * scale;

			auto is_set = qr_code_bitmap.map_[module_row][module_col];

			const unsigned char val = is_set ? BLACK : WHITE;
			for (unsigned int i = 0; i < scale; i++) {
				unsigned char *const rowArr = qrcode_int_scaled[module_row_offset + i];
				for (unsigned int j = 0; j < scale; j++) {
					rowArr[module_col_offset + j] = val;
				}
			}
		}
	}

	return qrcode_int_scaled;
}

const array2d swiss_qr_code_writer::swiss_cross() const
{
	    
	unsigned char BLACK = 0;
	unsigned char WHITE = 255;
	spdlog::trace("swiss_qr_code_writer::swiss_cross - start");
	auto swiss_cross = qrinvoice::resource::swiss_cross;
	array2d swiss_cross_array(swiss_cross.size(), swiss_cross.size());

    for (unsigned row = 0; row < swiss_cross.size(); row++) {
        const std::vector<int> &vectorRow = swiss_cross[row];
        for (unsigned col = 0; col < vectorRow.size(); col++) {
			if(vectorRow[col] == 1) {
				swiss_cross_array[row][col] = BLACK;
			} else {
				swiss_cross_array[row][col] = WHITE;
			}
		}
    }

    spdlog::trace("swiss_qr_code_writer::swiss_cross - end");

	return swiss_cross_array;
}

void swiss_qr_code_writer::validate_qr_code_string_argument(const std::string& qr_code_string)
{
	const auto size = string_util::utf8_length(qr_code_string);
	if (size > qrinvoice::model::spc::swiss_payments_code_max_length)
		throw model::validation::validation_exception {
			"The maximum Swiss QR Code data content permitted is " +
			std::to_string(qrinvoice::model::spc::swiss_payments_code_max_length) +
			" characters (including the element separators). But encountered " +
			std::to_string(size) + " characters"
		};
}

unsigned swiss_qr_code_writer::get_optimal_qr_code_render_size(unsigned desired_size, unsigned minimal_size)
{
	if (minimal_size >= desired_size)
		return minimal_size;
	else
		return minimal_size * (desired_size / minimal_size);
}

} // namespace qrcode
} // namespace qrinvoice

