
#ifndef QR_INVOICE_SWISS_QR_CODE_WRITER_INTERNAL_HPP
#define QR_INVOICE_SWISS_QR_CODE_WRITER_INTERNAL_HPP

#include <vector>
#include <string>
#include <qrinvoice/output/qr_code.hpp>
#include <qrinvoice/output_format.hpp>
#include <qrinvoice/technical_exception.hpp>

#include <qr_invoice_swiss_qr_code_internal.hpp>
#include <system/libqrencode/qr_code_modules.hpp>
#include <system/libqrencode/error_correction_level.hpp>
#include <resource/swiss_cross.hpp>
#include <util/array2d.hpp>
#include "qr_code_bitmap_internal.hpp"

namespace qrinvoice {
namespace qrcode {

/**
 * The following information comes from the specification v2.0
 * <p>"The measurements of the Swiss QR Code for printing must always be 46 x 46 mm (without surrounding quiet space) regardless of the Swiss QR Code version."</p>
 * <p>"To increase the recognizability and differentiation for users, the Swiss QR Code created for printout is to be overlaid with a Swiss cross logo measuring 7 x 7 mm."</p>
 */
class swiss_qr_code_writer {
public:
	output::qr_code write(output_format output_fmt, const std::string& qr_code_string, unsigned desired_qr_code_size) const;

    const qrinvoice::qrcode::qr_code_bitmap write_bitmap(const std::string& qr_code_string) const;

	const array2d swiss_cross() const;

	static unsigned get_optimal_qr_code_render_size(unsigned desired_size, unsigned minimal_size);
    qrinvoice::array2d render_int_scaled_qr_code(const qrinvoice::qrcode::qr_code_bitmap& qr_code_bitmap, unsigned desired_qr_code_size) const;
private:

    qrinvoice::array2d qr_code_modules_to_image(const qrinvoice::qrcode::qr_code_bitmap& qr_code_bitmap, unsigned width) const;

	std::vector<unsigned char> render_swiss_qr_code(output_format output_fmt, const std::string& qr_code_string, unsigned desired_qr_code_size) const;

    qrinvoice::array2d render_int_scaled_qr_code(const std::string &qr_code_string, unsigned desired_qr_code_size) const;

	static void validate_qr_code_string_argument(const std::string& qr_code_string);
	static void validate_desired_qr_code_size(unsigned desired_qr_code_size);

private:
	system::qrencode::error_correction_level	ec_level_ = qrinvoice::swiss_qr_code::ec_level;
};


inline void swiss_qr_code_writer::validate_desired_qr_code_size(const unsigned desired_qr_code_size)
{
	if (desired_qr_code_size > 5000)
		throw technical_exception {
			"QR Code size is limited to 5000 pixels. Please consider memory usage and "
			"file sizes when creating high resolution qr codes."
		};
}

} // namespace qrcode
} // namespace qrinvoice


#endif // QR_INVOICE_SWISS_QR_CODE_WRITER_INTERNAL_HPP

