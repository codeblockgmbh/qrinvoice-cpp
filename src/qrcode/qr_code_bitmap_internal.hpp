#ifndef QRINVOICE_CPP_QR_CODE_BITMAP_HPP
#define QRINVOICE_CPP_QR_CODE_BITMAP_HPP

#include <vector>

namespace qrinvoice {
namespace qrcode {

class qr_code_bitmap {
private:
    unsigned int modules_;

public:
    std::vector<std::vector<bool>> map_;

    void init(unsigned int modules);

    const unsigned int get_modules() const;
};

}
}

#endif //QRINVOICE_CPP_QR_CODE_BITMAP_HPP
