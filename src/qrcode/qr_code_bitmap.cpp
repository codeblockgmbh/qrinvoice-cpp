#include "qr_code_bitmap_internal.hpp"

namespace qrinvoice {
    namespace qrcode {

        void qr_code_bitmap::init(unsigned int modules) {
            modules_ = modules;
            map_.resize(modules);
            for ( int i = 0 ; i < modules ; i++ ) {
                map_[i].resize(modules);
            }
        }

        const unsigned int qr_code_bitmap::get_modules() const {
            return modules_;
        }
    }
}
