#include <util/array2d.hpp>
#include <spdlog/spdlog.h>
#include <qrinvoice/layout/layout_exception.hpp>
#include <codecvt>
#include <qrinvoice/technical_exception.hpp>
#include "freetype.hpp"

namespace qrinvoice {
    namespace freetype_helper {
        void draw_bitmap(array2d &canvas, FT_Bitmap *bitmap, FT_Int x, FT_Int y) {
            FT_Int i, j, p, q;
            FT_Int x_max = x + bitmap->width;
            FT_Int y_max = y + bitmap->rows;

            for (j = y, q = 0; j < y_max; j++, q++) {
                unsigned char *const rowArr = canvas[j];
                const bool yOutsideCanvas = j < 0 || j >= canvas.get_height();
                for (i = x, p = 0; i < x_max; i++, p++) {
                    const bool xOutsideCanvas = i < 0 || i >= canvas.get_width();
                    if (xOutsideCanvas || yOutsideCanvas) {
                        spdlog::warn("attempt to draw outside of the canvas i={} j={} width={} height={}", i, j, canvas.get_width(), canvas.get_height());
                        auto msg = "attempt to draw outside of the canvas i="+std::to_string(i)+" j="+std::to_string(j)+" width="+std::to_string(canvas.get_width())+" height="+std::to_string(canvas.get_height());
                        throw qrinvoice::layout::layout_exception(msg);
                    }

                    rowArr[i] |= bitmap->buffer[q * bitmap->width + p];
                }
            }
        }

        void draw_text(array2d &img, FT_Face face, FT_Vector &pen, const std::string &text) {
            FT_Error error;
            FT_GlyphSlot slot = face->glyph;

            const FT_Pos i = face->size->metrics.ascender; // needed, otherwise y is taken as baseline position
            const auto y_offset = static_cast<unsigned int>(std::round(freetype_dimension_to_pixels(i)));

            std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>, wchar_t> convert;
            const std::wstring wideText = convert.from_bytes(text);
            for (wchar_t ch: wideText) {
                //load glyph image into the slot (erase previous one)
                error = FT_Load_Char(face, static_cast<FT_ULong>(ch), FT_LOAD_RENDER);
                if (error) {
                    spdlog::error("error loading char");
                    throw qrinvoice::technical_exception{"error loading char"};
                }
                // now, draw to our target surface
                draw_bitmap(img, &slot->bitmap,
                            pen.x + slot->bitmap_left,
                            pen.y - slot->bitmap_top + y_offset);

                // increment pen position
                pen.x += slot->advance.x >> 6;
            }
        }

        void draw_text(qrinvoice::payment_part_receipt::payment_part_bitmap &img, FT_Face face, const unsigned int x, const unsigned int y, const std::string &text) {
            FT_Vector pen;
            pen.x = static_cast<signed long>(x);
            pen.y = static_cast<signed long>(y);
            draw_text(img.canvas(), face, pen, text);
        }

        void draw_text(qrinvoice::payment_part_receipt::payment_part_bitmap &img, FT_Face face, const float x, const float y, const std::string &text) {
            FT_Vector pen;
            pen.x = static_cast<signed long>(std::round(x));
            pen.y = static_cast<signed long>(std::round(y));
            draw_text(img.canvas(), face, pen, text);
        }

        std::vector<float> get_text_widths(FT_Face font, const std::string& text)
        {

            std::vector<float> widths;

            spdlog::trace("freetype::get_text_width - text {}", text);
            // not that cheap for longer texts
            std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>, wchar_t> convert;
            const std::wstring wideText = convert.from_bytes(text);

            FT_Error error;
            FT_GlyphSlot  slot = font->glyph;

            FT_F26Dot6 w = 0;
            FT_UInt previous = 0;
            for(wchar_t ch : wideText) {
                auto ulong_char = static_cast<FT_ULong>(ch);
                FT_UInt glyph_index = FT_Get_Char_Index( font, ulong_char );
                error = FT_Load_Glyph(font, glyph_index, FT_LOAD_RENDER);
                if (error) {
                    spdlog::error("FT_Load_Glyph error char");
                }

                w += slot->advance.x;
                if(previous && glyph_index )
                {
                    FT_Vector delta;
                    FT_Get_Kerning( font, previous, glyph_index, FT_KERNING_DEFAULT, &delta );
                    w += delta.x ;
                }
                previous = glyph_index;

                float width = freetype_helper::freetype_dimension_to_pixels(w);
                widths.push_back(width);
                //spdlog::trace("freetype_helper::get_text_widths idx: {} width: {}", widths.size(), width);
            }
            return widths;
        }
        float get_text_width(FT_Face font, const std::string& text)
        {
            const float &width = get_text_widths(font, text).back();
            spdlog::trace("freetype_helper::get_text_width - text {} ({})", text, width);
            return width;
        }
    }
}