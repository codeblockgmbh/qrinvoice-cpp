#ifndef QRINVOICE_CPP_FREETYPE_HPP
#define QRINVOICE_CPP_FREETYPE_HPP

#include <ft2build.h>
#include <paymentpart/payment_part_bitmap.hpp>
#include FT_FREETYPE_H
#include FT_OUTLINE_H
#include FT_STROKER_H
#include FT_GLYPH_H
#include FT_TRUETYPE_IDS_H

namespace qrinvoice {
    namespace freetype_helper {
        inline FT_F26Dot6 freetype_char_height(const unsigned int font_size_pts) {
            // The character widths and heights are specified in 1/64th of points. A point is a physical distance, equaling 1/72th of an inch. Normally, it is not equivalent to a pixel.
            return static_cast<FT_F26Dot6>(static_cast<signed long>(font_size_pts) * 64);
        }

        inline float freetype_dimension_to_pixels(FT_F26Dot6 dim) {
            // The character widths and heights are specified in 1/64th of points. A point is a physical distance, equaling 1/72th of an inch. Normally, it is not equivalent to a pixel.
            return (static_cast<float>(dim) / 64.0f);
        }

        inline float font_size_to_pixels(const unsigned int font_size_pts, const unsigned int dpi) {
            return freetype_helper::freetype_dimension_to_pixels((freetype_helper::freetype_char_height(font_size_pts) * static_cast<FT_F26Dot6>(dpi / 72)));
        }

        void draw_bitmap(array2d &canvas, FT_Bitmap *bitmap, FT_Int x, FT_Int y);

        void draw_text(array2d &img, FT_Face face, FT_Vector &pen, const std::string &text);

        void draw_text(qrinvoice::payment_part_receipt::payment_part_bitmap &img, FT_Face face, unsigned int x, unsigned int y, const std::string &text);

        void draw_text(qrinvoice::payment_part_receipt::payment_part_bitmap &img, FT_Face face, float x, float y, const std::string &text);

        float get_text_width(FT_Face font, const std::string& text);

        std::vector<float> get_text_widths(FT_Face font, const std::string& text);
    }
}
#endif // QRINVOICE_CPP_FREETYPE_HPP