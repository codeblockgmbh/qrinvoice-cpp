
#ifndef QR_INVOICE_SYSTEM_COMMON_HPP
#define QR_INVOICE_SYSTEM_COMMON_HPP


namespace qrinvoice {
namespace system {


constexpr const char* const new_line = NEW_LINE;


} // namespace system
} // namespace qrinvoice

#endif // QR_INVOICE_SYSTEM_COMMON_HPP

