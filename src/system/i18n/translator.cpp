
#include <string>
#include <unordered_map>
#include <system/i18n/translator.hpp>


namespace qrinvoice {
namespace system {
namespace i18n {

translator::map_t translator::init_translator_map(qrinvoice::locale lcl)
{
	switch (lcl) {
	case qrinvoice::locale::french:
		return init_translator_map_fr();
	case qrinvoice::locale::english:
		return init_translator_map_en();
	case qrinvoice::locale::italian:
		return init_translator_map_it();
	case qrinvoice::locale::german:
		return init_translator_map_de();
	}
}

translator::map_t translator::init_translator_map_de()
{
	return {
	{u8"Title", u8"Zahlteil"},
	{u8"TitleReceipt", u8"Empfangsschein"},
	{u8"CdtrInf.IBANCreditor", u8"Konto / Zahlbar an"},
	{u8"UltmtCdtr", u8"Endg\u00FCltiger Zahlungsempf\u00E4nger"},
	{u8"UltmtDbtr", u8"Zahlbar durch"},
	{u8"UltmtDbtr.Empty", u8"Zahlbar durch (Name/Adresse)"},
	{u8"RmtInf.Ref", u8"Referenz"},
	{u8"RmtInf.Ustrd", u8"Zus\u00E4tzliche Informationen"},
	{u8"Currency", u8"W\u00E4hrung"},
	{u8"Amount", u8"Betrag"},
	{u8"AcceptancePoint", u8"Annahmestelle"},
	{u8"SeparationLabel", u8"Vor der Einzahlung abzutrennen"},
	{u8"DoNotUseForPayment", u8"NICHT ZUR ZAHLUNG VERWENDEN"}};
}

translator::map_t translator::init_translator_map_fr()
{
	return {
	{u8"Title", u8"Section paiement"},
	{u8"TitleReceipt", u8"R\u00E9c\u00E9piss\u00E9"},
	{u8"CdtrInf.IBANCreditor", u8"Compte / Payable \u00E0"},
	{u8"UltmtCdtr", u8"En faveur de"},
	{u8"UltmtDbtr", u8"Payable par"},
	{u8"UltmtDbtr.Empty", u8"Payable par (nom/adresse)"},
	{u8"RmtInf.Ref", u8"R\u00E9f\u00E9rence"},
	{u8"RmtInf.Ustrd", u8"Informations suppl\u00E9mentaires"},
	{u8"Currency", u8"Monnaie"},
	{u8"Amount", u8"Montant"},
	{u8"AcceptancePoint", u8"Point de d\u00E9p\u00F4t"},
	{u8"SeparationLabel", u8"A d\u00E9tacher avant le versement"},
	{u8"DoNotUseForPayment", u8"NE PAS UTILISER POUR LE PAIEMENT"}};
}

translator::map_t translator::init_translator_map_en()
{
	return {
	{u8"Title", u8"Payment part"},
	{u8"TitleReceipt", u8"Receipt"},
	{u8"CdtrInf.IBANCreditor", u8"Account / Payable to"},
	{u8"UltmtCdtr", u8"In favour of"},
	{u8"UltmtDbtr", u8"Payable by"},
	{u8"UltmtDbtr.Empty", u8"Payable by (name/address)"},
	{u8"RmtInf.Ref", u8"Reference"},
	{u8"RmtInf.Ustrd", u8"Additional information"},
	{u8"Currency", u8"Currency"},
	{u8"Amount", u8"Amount"},
	{u8"AcceptancePoint", u8"Acceptance point"},
	{u8"SeparationLabel", u8"Separate before paying in"},
	{u8"DoNotUseForPayment", u8"DO NOT USE FOR PAYMENT"}};
}

translator::map_t translator::init_translator_map_it()
{
	return {
	{u8"Title", u8"Sezione pagamento"},
	{u8"TitleReceipt", u8"Ricevuta"},
	{u8"CdtrInf.IBANCreditor", u8"Conto / Pagabile a"},
	{u8"UltmtCdtr", u8"A favore di"},
	{u8"UltmtDbtr", u8"Pagabile da"},
	{u8"UltmtDbtr.Empty", u8"Pagabile da (nome/indirizzo)"},
	{u8"RmtInf.Ref", u8"Riferimento"},
	{u8"RmtInf.Ustrd", u8"Informazioni supplementari"},
	{u8"Currency", u8"Valuta"},
	{u8"Amount", u8"Importo"},
	{u8"AcceptancePoint", u8"Punto di accettazione"},
	{u8"SeparationLabel", u8"Da staccare prima del versamento"},
	{u8"DoNotUseForPayment", u8"NON UTILIZZARE PER IL PAGAMENTO"}};
}

const char* translator::translate(const std::string& id) const
{
	auto it = translator_map_.find(id);
	if (it == translator_map_.end())
		return {};
	return it->second.c_str();
}

const std::string translator::translate_label(const std::string& id) const
{
	auto it = translator_map_.find(id);
	if (it == translator_map_.end())
		return "";
	return it->second;
}


} // namespace i18n
} // namespace system
} // namespace qrinvoice

