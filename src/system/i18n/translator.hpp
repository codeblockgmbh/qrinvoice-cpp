
#ifndef QR_INVOICE_SYSTEM_TRANSLATOR_HPP
#define QR_INVOICE_SYSTEM_TRANSLATOR_HPP

#include <string>
#include <unordered_map>
#include <qrinvoice/locale.hpp>


namespace qrinvoice {
namespace system {
namespace i18n {

// This is hand-crafted implementation for text translation
// according to internationalization, for now.
//
// translator::translate() should return string mapped with the id,
// according to which language code is set.
// For ex. "Title" will give "QR-bill payment part" for English.

class translator {
public:
	explicit translator(qrinvoice::locale lcl)
		:translator_map_{ init_translator_map(lcl) } {}

	const char* translate(const std::string& id) const;

	// TOO check naming -> do we even need char*?
	const std::string translate_label(const std::string& id) const;

private:
	using map_t = const std::unordered_map<std::string, std::string>;

	static map_t init_translator_map(qrinvoice::locale);
	static map_t init_translator_map_de();
	static map_t init_translator_map_fr();
	static map_t init_translator_map_en();
	static map_t init_translator_map_it();

private:
	map_t	translator_map_;
};


} // namespace i18n
} // namespace system
} // namespace qrinvoice

#endif // QR_INVOICE_SYSTEM_TRANSLATOR_HPP

