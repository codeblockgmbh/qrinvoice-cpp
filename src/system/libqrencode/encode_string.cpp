
#include <system/libqrencode/encode_string.hpp>
#include <spdlog/spdlog.h>

namespace qrinvoice {
namespace system {
namespace qrencode {

const qrinvoice::qrcode::qr_code_bitmap qr_code_modules_to_bitmap(const qr_code_modules& modules)
{
	spdlog::trace("qr_code_modules_to_bitmap - start");
	qrinvoice::qrcode::qr_code_bitmap qr_code_bitmap;
	qr_code_bitmap.init(modules.width());

	const auto* p2 = modules.data();
	const auto modules_count = modules.width();
	for (unsigned int module_row = 0; module_row < modules_count; module_row++) {
		std::vector<bool> &x = qr_code_bitmap.map_[module_row];
		for (unsigned int module_col = 0; module_col < modules_count; module_col++) {
			const unsigned char i = *p2++;
			const unsigned char is_set = i & static_cast<unsigned char>(1); // = black
			// is_set = 0 => white / is_set = 1 => black
			x[module_col] = is_set != 0;
		}
	}

	spdlog::trace("qr_code_modules_to_bitmap - end");

	return qr_code_bitmap;
}

} // namespace qrencode
} // namespace system
} // namespace qrinvoice

