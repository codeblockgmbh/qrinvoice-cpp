
#ifndef QR_INVOICE_SYSTEM_ENCODE_STRING_HPP
#define QR_INVOICE_SYSTEM_ENCODE_STRING_HPP

#include <string>
#include <qrencode.h>
#include <system/libqrencode/qr_code_modules.hpp>
#include <system/libqrencode/error_correction_level.hpp>
#include <qrcode/qr_code_bitmap_internal.hpp>


namespace qrinvoice {
namespace system {
namespace qrencode {

inline qr_code_modules encode_string(const std::string& str, error_correction_level ec_level)
{
	constexpr int version = 0;
	constexpr int case_sensitive = 1;

	return qr_code_modules { QRcode_encodeString(str.c_str(), version,
		static_cast<QRecLevel>(ec_level), QR_MODE_8, case_sensitive) };
}

const qrinvoice::qrcode::qr_code_bitmap qr_code_modules_to_bitmap(const qr_code_modules& modules);

} // namespace qrencode
} // namespace system
} // namespace qrinvoice


#endif // QR_INVOICE_SYSTEM_ENCODE_STRING_HPP

