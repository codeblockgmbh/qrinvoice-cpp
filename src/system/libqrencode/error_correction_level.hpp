
#ifndef QR_INVOICE_SYSTEM_ERROR_CORRECTION_LEVEL_HPP
#define QR_INVOICE_SYSTEM_ERROR_CORRECTION_LEVEL_HPP

#include <qrencode.h>


namespace qrinvoice {
namespace system {
namespace qrencode {

enum class error_correction_level {
	l = QR_ECLEVEL_L,
	m = QR_ECLEVEL_M,
	q = QR_ECLEVEL_Q,
	h = QR_ECLEVEL_H
};


} // namespace qrencode
} // namespace system
} // namespace qrinvoice


#endif // QR_INVOICE_SYSTEM_ERROR_CORRECTION_LEVEL_HPP

