
#ifndef QR_INVOICE_SYSTEM_QR_CODE_MODULES_HPP
#define QR_INVOICE_SYSTEM_QR_CODE_MODULES_HPP

#include <memory>
#include <stdexcept>
#include <qrencode.h>

namespace qrinvoice {
namespace system {
namespace qrencode {

class qr_code_modules {
public:
	explicit qr_code_modules(QRcode* qr_code)
		:qr_code_{ qr_code, &QRcode_free}
	{
		if (!qr_code_)
			throw std::runtime_error{"Failed to create qr code matrix"};
	}

	int version() const {
		return qr_code_->version;
	}

	unsigned int width() const {
		return static_cast<unsigned int>(qr_code_->width);
	}

	const unsigned char* data() const {
		return qr_code_->data;
	}

private:
	std::unique_ptr<QRcode, decltype(&QRcode_free)>	qr_code_;
};


} // namespace qrencode
} // namespace system
} // namespace qrinvoice


#endif // QR_INVOICE_SYSTEM_QR_CODE_MODULES_HPP

