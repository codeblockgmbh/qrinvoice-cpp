#include "stbi_helper.hpp"

#include <spdlog/spdlog.h>
#include <qrinvoice/technical_exception.hpp>

#define STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_WRITE_IMPLEMENTATION

#include <stb_image_write.h>

#define STB_IMAGE_RESIZE_IMPLEMENTATION

#include <stb_image_resize.h>

namespace qrinvoice {
    namespace stbi_helper {

        void writeCallback(void *context, void *data, int size) {
            auto vec = static_cast<std::vector<unsigned char> *>(context);
            auto charData = static_cast<unsigned char *>(data);
            vec->insert(vec->end(), charData, charData + size);
        }

        bool saveImageToVectorPng(std::vector<unsigned char> *targetData, array2d &sourceData) {
            spdlog::trace("stbi_helper::saveImageToVectorPng");
            targetData->clear();
            targetData->reserve(sourceData.get_width() * sourceData.get_height() * 1);
            if (stbi_write_png_to_func(stbi_helper::writeCallback, targetData, sourceData.get_width(), sourceData.get_height(), 1, sourceData.data(), 0)) {
                return true;
            }
            spdlog::error("Failed to save PNG image to vector");
            return false;
        }

        bool saveImageToVectorBmp(std::vector<unsigned char> *targetData, array2d &sourceData) {
            spdlog::trace("stbi_helper::saveImageToVectorBmp");
            targetData->clear();
            targetData->reserve(sourceData.get_width() * sourceData.get_height() * 1 + 54);
            if (stbi_write_bmp_to_func(stbi_helper::writeCallback, targetData, sourceData.get_width(), sourceData.get_height(), 1, sourceData.data())) {
                return true;
            }
            spdlog::error("Failed to save BMP image to vector");
            return false;
        }

        bool saveImageToVectorJpg(std::vector<unsigned char> *targetData, array2d &sourceData, int quality) {
            spdlog::trace("stbi_helper::saveImageToVectorJpg");
            targetData->clear();
            targetData->reserve(sourceData.get_width() * sourceData.get_height() * 1);
            if (stbi_write_jpg_to_func(stbi_helper::writeCallback, targetData, sourceData.get_width(), sourceData.get_height(), 1, sourceData.data(),
                                       quality)) {
                return true;
            }
            spdlog::error("Failed to save JPG image to vector");
            return false;
        }

        bool saveImageToVector(std::vector<unsigned char> *targetData, array2d &sourceData, const output_format &output_format) {
            spdlog::trace("stbi_helper::saveImageToVector");
            switch (output_format) {
                case output_format::bmp:
                    return saveImageToVectorBmp(targetData, sourceData);
                case output_format::png:
                    return saveImageToVectorPng(targetData, sourceData);
                case output_format::jpg:
                    return saveImageToVectorJpg(targetData, sourceData, 100);
                default:
                    throw technical_exception("Unsupported image format");
            }
        }

        int resizeUint8(const unsigned char *input_pixels, int input_w, int input_h, int input_stride_in_bytes,
                        unsigned char *output_pixels, int output_w, int output_h, int output_stride_in_bytes,
                        int num_channels) {
            return stbir_resize_uint8(input_pixels, input_w, input_h, input_stride_in_bytes, output_pixels, output_w, output_h, output_stride_in_bytes,
                                      num_channels);
        }
    }
}

