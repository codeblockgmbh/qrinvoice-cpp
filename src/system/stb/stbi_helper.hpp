
#ifndef QRINVOICE_CPP_STBI_HELPER_HPP
#define QRINVOICE_CPP_STBI_HELPER_HPP

#include <vector>
#include <util/array2d.hpp>
#include <qrinvoice/output_format.hpp>

namespace qrinvoice {
    namespace stbi_helper {
        static void writeCallback(void *context, void *data, int size);

        bool saveImageToVector(std::vector<unsigned char> *targetData, array2d &sourceData, const qrinvoice::output_format &output_format);

        bool saveImageToVectorPng(std::vector<unsigned char> *targetData, array2d &sourceData);

        bool saveImageToVectorBmp(std::vector<unsigned char> *targetData, array2d &sourceData);

        bool saveImageToVectorJpg(std::vector<unsigned char> *targetData, array2d &sourceData, int quality);

        int resizeUint8(const unsigned char *input_pixels, int input_w, int input_h, int input_stride_in_bytes,
                        unsigned char *output_pixels, int output_w, int output_h, int output_stride_in_bytes,
                        int num_channels);
    }
}

#endif //QRINVOICE_CPP_STBI_HELPER_HPP