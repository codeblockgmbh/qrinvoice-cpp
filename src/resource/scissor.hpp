
#ifndef QR_INVOICE_RESOURCE_SCISSOR_HPP
#define QR_INVOICE_RESOURCE_SCISSOR_HPP

#include <array>
#include <vector>
#include <qrinvoice/output_resolution.hpp>

namespace qrinvoice {
namespace resource {

extern const unsigned char scissor_30x18_horizontal[18][30];
extern const unsigned char scissor_30x18_vertical[30][18];
extern const unsigned char scissor_58x35_horizontal[35][58];
extern const unsigned char scissor_58x35_vertical[58][35];
extern const unsigned char scissor_118x71_horizontal[71][118];
extern const unsigned char scissor_118x71_vertical[118][71];

} // namespace resource
} // namespace qrinvoice


#endif // QR_INVOICE_RESOURCE_SCISSOR_HPP

