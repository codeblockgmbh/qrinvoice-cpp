
#ifndef QR_INVOICE_RESOURCE_SWISS_CROSS_HPP
#define QR_INVOICE_RESOURCE_SWISS_CROSS_HPP

#include <array>
#include <vector>

namespace qrinvoice {
namespace resource {

//
// The Swiss Cross logo image in as pixel map.
//
    extern const std::vector<std::vector<int>> swiss_cross;

    // 0.7/2.54*150=41.33
    extern const unsigned char swiss_cross_41x41[41][41];
    // 0.7/2.54*300=82.67
    extern const unsigned char swiss_cross_83x83[83][83];
    // 0.7/2.54*600=165.35 -> round up, make it a even number
    extern const unsigned char swiss_cross_166x166[166][166];

} // namespace resource
} // namespace qrinvoice


#endif // QR_INVOICE_RESOURCE_SWISS_CROSS_HPP

