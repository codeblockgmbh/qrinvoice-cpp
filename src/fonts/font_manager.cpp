
#include <string>

#include <util/file_util_internal.hpp>
#include <config/env_vars_internal.hpp>
#include <qrinvoice/fonts/font_manager.hpp>
#include <fonts/font_manager_internal.hpp>

#include <qrinvoice/font_family.hpp>
#include <qrinvoice/font_style.hpp>
#include <qrinvoice/not_yet_implemented_exception.hpp>

namespace qrinvoice {
namespace font_manager {
std::string font_path_directory;

QRINVOICE_API void set_font_path(const std::string& font_path) {
    qrinvoice::font_manager::font_path_directory = font_path;
}

std::string get_font_file_path(const std::string& font_file_name)
{
	std::string dir_path;
	const char* const directory_path_env = qrinvoice::env::get_string(env::font_path);
	if(!qrinvoice::font_manager::font_path_directory.empty()) // path explicitly set
	{
		dir_path = qrinvoice::font_manager::font_path_directory;
	}
	else if (directory_path_env) // environment variable set
	{
		dir_path = std::string {directory_path_env};
	}

	if(!dir_path.empty()) {
		file_util::ensure_trailing_path_separator(dir_path);

		dir_path += font_file_name;
		if (file_util::file_exists(dir_path)) // file exists
			return dir_path;
	}

	return font_file_name;
}

std::string get_font_file_name(const qrinvoice::font_family& font_family, const qrinvoice::font_style font_style)
{
	switch(font_family) {
		case qrinvoice::font_family::arial:
			return qrinvoice::font_style::bold == font_style ? "ArialBd.ttf" : "Arial.ttf";
		case qrinvoice::font_family::helvetica:
			return qrinvoice::font_style::bold == font_style ? "HelveticaBd.ttf" : "Helvetica.ttf";
		case qrinvoice::font_family::liberation_sans:
			return qrinvoice::font_style::bold == font_style ? "LiberationSans-Bold.ttf" : "LiberationSans-Regular.ttf";
		default:
			throw not_yet_implemented_exception{"no support for given font family"};
	}
}


std::string get_font_file_path(const qrinvoice::font_family& font_family, const qrinvoice::font_style font_style)
{
	const auto font_file_name = get_font_file_name(font_family, font_style);
	return get_font_file_path(font_file_name);
}

} // namespace font
} // namespace qrinvoice

