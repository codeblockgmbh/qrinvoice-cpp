
#ifndef QR_INVOICE_FONT_MANAGER_INTERNAL_HPP
#define QR_INVOICE_FONT_MANAGER_INTERNAL_HPP

#include <string>
#include <qrinvoice/font_family.hpp>
#include <qrinvoice/font_style.hpp>

namespace qrinvoice {

/*
 * This namespace can be converted to a class as the "font manager" will evolve.
 * Currently, keeping it as a namespace as it's too simple.
 */
namespace font_manager {
extern std::string font_path_directory;

std::string get_font_file_name(const qrinvoice::font_family& font_family, qrinvoice::font_style font_style);

std::string get_font_file_path(const qrinvoice::font_family& font_family, const qrinvoice::font_style font_style);

std::string get_font_file_path(const std::string& font_file_name);

} // namespace font
} // namespace qrinvoice


#endif // QR_INVOICE_FONT_MANAGER_INTERNAL_HPP

