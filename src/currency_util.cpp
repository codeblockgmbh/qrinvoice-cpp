
#include <string>
#include <qrinvoice/currency.hpp>
#include <qrinvoice/model/parse_exception.hpp>


namespace qrinvoice {
namespace currency_ops {

std::string get_currency_code(currency curr)
{
	switch (curr) {
	case currency::chf:
		return "CHF";
	case currency::eur:
		return "EUR";
	case currency::invalid:
		return "";
	}
}

currency parse_currency(const std::string& str)
{
	if (str == "CHF")
		return currency::chf;
	if (str == "EUR")
		return currency::eur;

	return currency::invalid;
}


} // namespace currency_ops
} // namespace qrinvoice

