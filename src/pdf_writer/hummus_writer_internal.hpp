
#ifndef QR_INVOICE_HUMMUS_WRITER_INTERNAL_HPP
#define QR_INVOICE_HUMMUS_WRITER_INTERNAL_HPP

#include <string>
#include <vector>

#include <PDFWriter/PDFWriter.h>
#include <qrinvoice/output/payment_part_receipt.hpp>
#include <qrinvoice/page_size.hpp>
#include <qrinvoice/model/qr_invoice.hpp>

#include <qr_invoice_swiss_qr_code_internal.hpp>
#include <paymentpart/writer_internal.hpp>
#include <paymentpart/writer_options_internal.hpp>
#include <layout/rect_internal.hpp>
#include <layout/dimension_internal.hpp>
#include <layout/dimension_unit_utils_internal.hpp>
#include <system/i18n/translator.hpp>


namespace qrinvoice {
namespace payment_part_receipt {


class hummus_writer : public writer {
public:
	explicit hummus_writer(const writer_options& w_options);

	output::payment_part_receipt write(const model::qr_invoice& qr_invoice, const qrinvoice::qrcode::qr_code_bitmap& qr_code_bitmap) override;

private:
	constexpr const double hummus_writer::get_payment_part_width() const
	{
		return din_lang_landscape_.get_width();
	}

	constexpr const double hummus_writer::get_payment_part_height() const
	{
		return din_lang_landscape_.get_height();
	}

	constexpr const double hummus_writer::get_receipt_height() const
	{
		return din_lang_landscape_.get_height();
	}

	const payment_part_receipt::writer_options			opts_;

	static constexpr qrinvoice::layout::dimension<double> din_lang_landscape_{ 595, 297 };
	static const qrinvoice::layout::dimension<double> init_page_canvas(qrinvoice::page_size page_sz);

	void setup_page(PDFPage& page);


	void add_receipt_title_section(PageContentContext* ctx, PDFUsedFont* font_title);
	void add_receipt_info_section(PageContentContext* ctx, PDFUsedFont* font_heading, PDFUsedFont* font_value, const model::qr_invoice &);
	void add_receipt_amount(PageContentContext* ctx, PDFUsedFont* font_heading, PDFUsedFont* font_value, const model::payment_amount_info &);
	void add_receipt_acceptance_section(PageContentContext* ctx, PDFUsedFont* font_heading);

	void add_payment_part_title_section(PageContentContext* ctx, PDFUsedFont* font_title);
	void add_payment_part_qr_code_image(PDFWriter& pdf_writer, PDFPage& page, PageContentContext* ctx, const qrinvoice::qrcode::qr_code_bitmap& qr_code_bitmap);
	void add_payment_part_amount(PageContentContext* ctx, PDFUsedFont* font_heading, PDFUsedFont* font_value, const model::payment_amount_info& amount_info);
	void add_payment_part_info_section(PageContentContext* ctx, PDFUsedFont* font_heading, PDFUsedFont* font_value, const model::qr_invoice& qr_invoice);
	void add_payment_part_further_info_section(PageContentContext* ctx, PDFUsedFont* font_heading, PDFUsedFont* font_value, const model::qr_invoice &);

	void write_free_text_box(PageContentContext* ctx, qrinvoice::layout::rect<double> rect);

	void draw_boundary_lines(PDFWriter& pdf_writer, PDFPage& page, PageContentContext* ctx, PDFUsedFont* font);
	void write_separation_label(PageContentContext* ctx, PDFUsedFont* font);
	void draw_horizontal_boundary_line(PDFWriter& pdf_writer, PDFPage& page, PageContentContext* ctx, ObjectIDType& scissor_object_id);
	void draw_vertical_boundary_line(PDFWriter& pdf_writer, PDFPage& page, PageContentContext* ctx, ObjectIDType& scissor_object_id);

	void debug_layout(PageContentContext* ctx, qrinvoice::layout::rect<double> rect) const;

private:
	const qrinvoice::layout::dimension<double>	page_canvas_;
	const qrinvoice::layout::point<double>	root_lower_left_;
	const qrinvoice::layout::point<double>	payment_part_lower_left_;

	static const double					box_corner_line_width_;
	static const double					box_corner_line_length_;

	const double					quiet_space_pt_;
	const double					additional_print_margin_pt_;
	const double					print_margin_pt_;

	static const double					font_size_title_;
	static const double					font_size_separation_label_;
	static const double					font_size_payment_part_heading_;
	static const double					font_size_payment_part_value_;
	static const double					font_size_payment_part_further_info_;
	static const double					font_size_receipt_heading_;
	static const double					font_size_receipt_value_;
	// TODO: fixed & multiplied_leading maps, if needed

	static constexpr double					boundary_line_width_{ 0.5 };

	const qrinvoice::layout::dimension<double>	receipt_amount_field_;
	const qrinvoice::layout::dimension<double>	receipt_debtor_field_;

	const qrinvoice::layout::dimension<double>	payment_part_amount_field_;
	const qrinvoice::layout::dimension<double>	payment_part_debtor_field_;

	const qrinvoice::layout::dimension<double>		qr_code_;
	const qrinvoice::layout::dimension<double>		swiss_cross_;

	const qrinvoice::layout::rect<double>		receipt_title_section_rect_;
	const qrinvoice::layout::rect<double>		receipt_info_section_rect_;
	const qrinvoice::layout::rect<double>		receipt_amount_section_rect_;
	const qrinvoice::layout::rect<double>		receipt_acceptance_section_rect_;

	const qrinvoice::layout::rect<double>		payment_part_title_section_rect_;
	const qrinvoice::layout::rect<double>		payment_part_qr_section_rect_;
	const qrinvoice::layout::rect<double>		payment_part_info_section_rect_;
	const qrinvoice::layout::rect<double>		payment_part_amount_section_rect_;
	const qrinvoice::layout::rect<double>		payment_part_further_info_section_rect_;

	const system::i18n::translator			tr_;

protected:
	static qrinvoice::layout::dimension<double> mm_to_points(qrinvoice::layout::dimension<unsigned> mm_dim);
	static const double calculate_additional_print_margin(bool additional_margin, unsigned dpi);

private:
	const qrinvoice::layout::dimension<double> init_qr_code() const;
	const qrinvoice::layout::dimension<double> init_swiss_cross() const;

	const qrinvoice::layout::rect<double> init_receipt_title_sect_rect() const;
	const qrinvoice::layout::rect<double> init_receipt_info_sect_rect() const;
	const qrinvoice::layout::rect<double> init_receipt_amount_sect_rect() const;
	const qrinvoice::layout::rect<double> init_receipt_acceptance_sect_rect() const;

	const qrinvoice::layout::rect<double> init_payment_part_title_sect_rect() const;
	const qrinvoice::layout::rect<double> init_payment_part_qr_sect_rect() const;
	const qrinvoice::layout::rect<double> init_payment_part_info_sect_rect() const;
	const qrinvoice::layout::rect<double> init_payment_part_amount_sect_rect() const;
	const qrinvoice::layout::rect<double> init_payment_part_further_info_sect_rect() const;
};

inline const qrinvoice::layout::dimension<double> hummus_writer::init_qr_code() const
{
	return mm_to_points(swiss_qr_code::qr_code_size);
}

inline const qrinvoice::layout::dimension<double> hummus_writer::init_swiss_cross() const
{
	return mm_to_points(swiss_qr_code::qr_code_logo_size);
}

std::string trim_if_required(std::string input, double max_width, PDFUsedFont* font, double font_size);

} // namespace payment_part_receipt
} // namespace qrinvoice

#endif // QR_INVOICE_HUMMUS_WRITER_INTERNAL_HPP

