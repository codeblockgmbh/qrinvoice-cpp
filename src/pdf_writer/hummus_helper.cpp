
#include <string>

#include <pdf_writer/hummus_helper_internal.hpp>
#include <util/text_util_internal.hpp>

namespace qrinvoice {
namespace payment_part_receipt {
namespace hummus_helper {


void info_section_title_writer::write(double x, double& y, const char* txt)
{
	write(x,y,txt, false);
}
void info_section_title_writer::write(double x, double& y, const char* txt, const bool first_heading)
{
	// don't add spacing if it is the first header
	if(!first_heading) {
		y -= line_spacing_;
	}
	y -= opts_.fontSize;
	ctx_.WriteText(x, y, txt, opts_);
}

void info_section_value_writer::write(double x, double& y, const char* txt)
{
	std::string extra = "ab";		// This is a work-around for a subtle issue in the PDFWriter library
	text_util::text_splitter splitter {[this, &extra](const std::string& txt) -> double {
		auto sandwich = "a" + txt + "b";
		return hummus_helper::get_text_width(opts_.font, opts_.fontSize, sandwich.c_str()) -
			hummus_helper::get_text_width(opts_.font, opts_.fontSize, extra.c_str());
	}};

	auto lines = splitter.split_text_by_boundary(txt, break_width_);
	for (const auto& line : lines) {
        y -= opts_.fontSize;
		y -= line_spacing_;

		ctx_.WriteText(x, y, line, opts_);
	}
}


} // namespace hummus_helper
} // namespace payment_part_receipt
} // namespace qrinvoice

