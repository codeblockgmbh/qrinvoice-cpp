
#ifndef QR_INVOICE_HUMMUS_HELPER_INTERNAL_HPP
#define QR_INVOICE_HUMMUS_HELPER_INTERNAL_HPP

#include <string>
#include <PDFWriter/PDFWriter.h>
#include <PDFWriter/PDFUsedFont.h>
#include <PDFWriter/PageContentContext.h>
#include <PDFWriter/AbstractContentContext.h>

#include <qrinvoice/base_exception.hpp>

#include <layout/point_internal.hpp>


namespace qrinvoice {
namespace payment_part_receipt {
namespace hummus_helper {


// RAII enabled class for PDFWriter that ensures proper destruction
class pdf_starter {
public:
	pdf_starter(PDFWriter& writer, IByteWriterWithPosition& stream)
		:writer_{ writer }
	{
		EStatusCode status = writer_.StartPDFForStream(&stream, ePDFVersion13
			,LogConfiguration::DefaultLogConfiguration()
			,PDFCreationSettings(true, true));		// with compression & embedded fonts
		if (status != eSuccess)
			throw qrinvoice::base_exception{"failed to initialize PDFWriter"};
	}

	~pdf_starter() {
		writer_.EndPDFForStream();
	}

private:
	PDFWriter& writer_;
};

inline PDFUsedFont* load_font(PDFWriter& writer, const char* font_file)
{
	PDFUsedFont* font = writer.GetFontForFile(font_file);
	return font ? font : throw qrinvoice::base_exception{std::string{"failed to load font: "} + font_file};
}

inline PageContentContext* start_page_content_context(PDFWriter& writer, PDFPage& page)
{
	PageContentContext* ctx = writer.StartPageContentContext(&page);
	return ctx ? ctx : throw qrinvoice::base_exception{"failed to start PDF page content context"};
}

inline void pause_page_content_context(PDFWriter& pdf_writer, PageContentContext* ctx)
{
	EStatusCode status = pdf_writer.PausePageContentContext(ctx);
	if (status != eSuccess)
		throw qrinvoice::base_exception{"failed to pause page content context"};
}

inline auto get_text_hw(PDFUsedFont* font, double font_size, const char* text) -> std::pair<double, double>
{
	PDFUsedFont::TextMeasures tm = font->CalculateTextDimensions(text, static_cast<long>(font_size));
	return {tm.width, tm.height};
}


inline double get_text_width(PDFUsedFont* font, double font_size, const char* text)
{
	return get_text_hw(font, font_size, text).first;
}

inline double get_text_width(PDFUsedFont* font, double font_size, const std::string& text)
{
	const char* chr_arr = text.c_str();
	return get_text_width(font, font_size, chr_arr);
}


class info_section_writer_base {
public:
	info_section_writer_base(PageContentContext& ctx, const AbstractContentContext::TextOptions& opts, const double line_spacing)
		:ctx_{ ctx }
		,opts_{ opts }
		,line_spacing_ {line_spacing} {}

protected:
	PageContentContext&			ctx_;
	AbstractContentContext::TextOptions	opts_;
	double line_spacing_;
};


class info_section_title_writer : private info_section_writer_base {
public:
	using info_section_writer_base::info_section_writer_base;

	void write(double x, double& y, const char* txt);
	void write(double x, double& y, const char* txt, bool first_heading);
};


class info_section_value_writer : private info_section_writer_base {
public:
	info_section_value_writer(PageContentContext& ctx, const AbstractContentContext::TextOptions& opts, const double line_spacing, const double break_width)
		:info_section_writer_base{ ctx, opts, line_spacing }
		,break_width_{ break_width } {}

public:
	void write(double x, double& y, const char* txt);

private:
	const double				break_width_;
};


} // namespace hummus_helper
} // namespace payment_part_receipt
} // namespace qrinvoice

#endif // QR_INVOICE_HUMMUS_HELPER_INTERNAL_HPP

