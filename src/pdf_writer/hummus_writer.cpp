
#include <string>
#include <memory>
#include <PDFWriter/PDFWriter.h>
#include <PDFWriter/PDFPage.h>
#include <PDFWriter/PageContentContext.h>
#include <PDFWriter/PDFFormXObject.h>
#include <PDFWriter/PDFImageXObject.h>
#include <PDFWriter/AbstractContentContext.h>
#include <PDFWriter/InputByteArrayStream.h>

#include <resource/scissor.hpp>
#include <qrinvoice/output/payment_part_receipt.hpp>
#include <qrinvoice/page_size.hpp>
#include <qrinvoice/boundary_lines.hpp>
#include <qrinvoice/model/qr_invoice.hpp>
#include <qrinvoice/currency.hpp>
#include <qrinvoice/layout/layout_exception.hpp>
#include <qrinvoice/technical_exception.hpp>

#include <pdf_writer/hummus_writer_internal.hpp>
#include <pdf_writer/hummus_helper_internal.hpp>
#include <paymentpart/layout_helper_internal.hpp>
#include <paymentpart/layout_definitions_internal.hpp>
#include <paymentpart/writer_options_internal.hpp>
#include <layout/rect_internal.hpp>
#include <layout/dimension_internal.hpp>
#include <layout/dimension_unit_utils_internal.hpp>
#include <fonts/font_manager_internal.hpp>
#include <model/util/address_utils_internal.hpp>
#include <model/util/alternative_schemes_utils_internal.hpp>
#include <qrinvoice/util/reference_util.hpp>
#include <util/payment_reference_util_internal.hpp>
#include <system/i18n/translator.hpp>
#include <util/iban_util_internal.hpp>
#include <util/amount_util_internal.hpp>
#include <config/env_vars_internal.hpp>
#include <spdlog/spdlog.h>
#include <util/array2d.hpp>
#include <system/stb/stbi_helper.hpp>
#include <qrinvoice/output/qr_code.hpp>
#include <qrcode/swiss_qr_code_writer_internal.hpp>

namespace qrinvoice {
namespace payment_part_receipt {

namespace du = qrinvoice::layout::dimension_unit_utils;


namespace du = qrinvoice::layout::dimension_unit_utils;

const double hummus_writer::font_size_title_{ layout_definitions::font_size_title };
const double hummus_writer::font_size_payment_part_heading_{ layout_definitions::payment_part_font_size_heading_recommended };
const double hummus_writer::font_size_payment_part_value_{ layout_definitions::payment_part_font_size_value_recommended };
const double hummus_writer::font_size_payment_part_further_info_{ layout_definitions::payment_part_font_size_further_info };
const double hummus_writer::font_size_receipt_heading_{ layout_definitions::receipt_font_size_heading_min };
const double hummus_writer::font_size_receipt_value_{ layout_definitions::receipt_font_size_value_min };
const double hummus_writer::font_size_separation_label_{ layout_definitions::font_size_separation_label };
constexpr qrinvoice::layout::dimension<double> hummus_writer::din_lang_landscape_;
const double hummus_writer::box_corner_line_width_{ du::millimeters_to_points(layout_definitions::box_corner_line_width) };
const double hummus_writer::box_corner_line_length_{ du::millimeters_to_points(layout_definitions::box_corner_line_length) };

hummus_writer::hummus_writer(const writer_options& w_options)
	:opts_{ w_options }
	,page_canvas_{ init_page_canvas(w_options.page_size) }
	,root_lower_left_{ page_canvas_.get_width() - get_payment_part_width(), 0 }
    ,payment_part_lower_left_{ page_canvas_.get_width() - mm_to_points(layout_definitions::payment_part).get_width(), 0 }
	,quiet_space_pt_{ du::millimeters_to_points(layout_definitions::quiet_space, 72) } // 72 dpi is fine here
	,additional_print_margin_pt_{ calculate_additional_print_margin(w_options.additional_print_margin, 72) } // 72 dpi is fine here
	,print_margin_pt_{ calculate_additional_print_margin(w_options.additional_print_margin, 72) + du::millimeters_to_points(layout_definitions::quiet_space, 72) } // 72 dpi is fine here

	,receipt_debtor_field_{ mm_to_points(layout_definitions::receipt_debtor_field) }
	,receipt_amount_field_{ mm_to_points(layout_definitions::receipt_amount_field) }
	,payment_part_amount_field_{ mm_to_points(layout_definitions::payment_part_amount_field) }
	,payment_part_debtor_field_{ mm_to_points(layout_definitions::payment_part_debtor_field) }

	,qr_code_{ init_qr_code() }
	,swiss_cross_{ init_swiss_cross() }

	,receipt_title_section_rect_{ init_receipt_title_sect_rect() }
	,receipt_info_section_rect_{ init_receipt_info_sect_rect() }
	,receipt_amount_section_rect_{ init_receipt_amount_sect_rect() }
	,receipt_acceptance_section_rect_{ init_receipt_acceptance_sect_rect() }
	,payment_part_title_section_rect_{ init_payment_part_title_sect_rect() }
	,payment_part_qr_section_rect_{ init_payment_part_qr_sect_rect() }
	,payment_part_info_section_rect_{ init_payment_part_info_sect_rect() }
	,payment_part_amount_section_rect_{ init_payment_part_amount_sect_rect() }
	,payment_part_further_info_section_rect_{ init_payment_part_further_info_sect_rect() }

	,tr_{ w_options.locale } {}

output::payment_part_receipt hummus_writer::write(const model::qr_invoice& qr_invoice, const qrinvoice::qrcode::qr_code_bitmap& qr_code_bitmap)
{
	using namespace hummus_helper;
	OutputStringBufferStream output_stream;

	{
		PDFWriter pdf_writer;
		pdf_starter starter{pdf_writer, output_stream};

		PDFPage page;			// ensure destruction of pdf_writer page & ctx
		setup_page(page);
		PageContentContext* ctx = start_page_content_context(pdf_writer, page);

		PDFUsedFont* font_title = load_font(pdf_writer, font_manager::get_font_file_path(opts_.font_family, qrinvoice::font_style::bold).c_str());
		PDFUsedFont* font_heading = load_font(pdf_writer, font_manager::get_font_file_path(opts_.font_family, qrinvoice::font_style::bold).c_str());
		PDFUsedFont* font_value = load_font(pdf_writer, font_manager::get_font_file_path(opts_.font_family, qrinvoice::font_style::regular).c_str());

		add_receipt_title_section(ctx, font_title);
		add_receipt_info_section(ctx, font_heading, font_value, qr_invoice);
		add_receipt_amount(ctx, font_heading, font_value, qr_invoice.get_payment_amount_info());
		add_receipt_acceptance_section(ctx, font_heading);

		add_payment_part_title_section(ctx, font_title);
		add_payment_part_qr_code_image(pdf_writer, page, ctx, qr_code_bitmap);
		add_payment_part_amount(ctx, font_heading, font_value, qr_invoice.get_payment_amount_info());
		add_payment_part_info_section(ctx, font_heading, font_value, qr_invoice);
		add_payment_part_further_info_section(ctx, font_heading, font_value, qr_invoice);

		draw_boundary_lines(pdf_writer, page, ctx, font_value);

		pdf_writer.EndPageContentContext(ctx);
		pdf_writer.WritePage(&page);
	}

	std::string bytes = output_stream.ToString();
	return {output_format::pdf, reinterpret_cast<const output::payment_part_receipt::byte*>(bytes.data()), bytes.size()};
}

void hummus_writer::debug_layout(PageContentContext* ctx, qrinvoice::layout::rect<double> rect) const
{

	if (env::is_true(env::debug_layout))
	{
		const auto x = rect.get_lower_left_x();
		const auto y = rect.get_lower_left_y();
		const auto width = rect.get_width();
		const auto height = rect.get_height();

		DoubleAndDoublePairList path_points;
		path_points.push_back(DoubleAndDoublePair{x, y});
		path_points.push_back(DoubleAndDoublePair{x, y + height});
		path_points.push_back(DoubleAndDoublePair{x + width, y + height});
		path_points.push_back(DoubleAndDoublePair{x + width, y});

		AbstractContentContext::GraphicOptions go {
				AbstractContentContext::eStroke, AbstractContentContext::eGray, 0, 0.1, true
		};
		ctx->DrawPath(path_points, go);
	}
}


const qrinvoice::layout::dimension<double> hummus_writer::init_page_canvas(qrinvoice::page_size page_sz)
{
	switch (page_sz) {
	case page_size::a4:
		return { 595, 842 };
	case page_size::a5:
		return { 595, 420 };
	case page_size::din_lang:
		return { 595, 297 };
	case page_size::din_lang_cropped:
		throw technical_exception("page size din_lang_cropped unsupported for PDF output");
	default:
		throw technical_exception("unsupported page size");
	}
}

void hummus_writer::add_receipt_title_section(PageContentContext* ctx, PDFUsedFont* font_title)
{
	debug_layout(ctx, receipt_title_section_rect_);

	const auto x = receipt_title_section_rect_.get_left_x() + additional_print_margin_pt_;

	AbstractContentContext::TextOptions title_options {font_title, font_size_title_};
	ctx->WriteText(x, receipt_title_section_rect_.get_top_y() - font_size_title_, tr_.translate("TitleReceipt"), title_options);
}
void hummus_writer::add_receipt_info_section(PageContentContext* ctx, PDFUsedFont* font_heading, PDFUsedFont* font_value, const model::qr_invoice &qr_invoice)
{
	debug_layout(ctx, receipt_info_section_rect_);
	// According to the spec 3.6.2 - Information section - v2.0
	// "Because of the limited space, it is permitted to omit the street name and building number from the addresses of the creditor (Payable to) and the debtor (Payable by)"
	const bool omitStreetNameHouseNumberOrAddressLine1 = opts_.layout ==  payment_part_receipt::layout::compressed;

	using namespace hummus_helper;

	const AbstractContentContext::TextOptions title_opts {font_heading, font_size_receipt_heading_};
	info_section_title_writer title_writer{ *ctx, title_opts,
										 uncompressed_layout(opts_.layout) ? static_cast<double>(layout_definitions::receipt_heading_line_spacing_pts) : 0};
	info_section_value_writer value_writer{ *ctx, {font_value, font_size_receipt_value_},
										 uncompressed_layout(opts_.layout) ? static_cast<double>(layout_definitions::receipt_value_line_spacing_pts) : 0,
										 receipt_info_section_rect_.get_width() - additional_print_margin_pt_ };

	const double x = receipt_info_section_rect_.get_left_x() + additional_print_margin_pt_;
	double y = receipt_info_section_rect_.get_top_y();

	// Account
	title_writer.write(x, y, tr_.translate("CdtrInf.IBANCreditor"), true);
	value_writer.write(x, y, iban_util::format_iban(qr_invoice.get_creditor_info().get_iban()).c_str());

	const auto addr = qr_invoice.get_creditor_info().get_creditor().get_address();
	for (const auto& addr_line : model::address_utils::to_address_lines(addr, omitStreetNameHouseNumberOrAddressLine1))
		value_writer.write(x, y, addr_line.c_str());

    const model::payment_reference &pr = qr_invoice.get_payment_reference();
	if (!pr.empty()) {
		switch (pr.get_reference_type().get()) {
			case model::reference_type::qr_reference:
			case model::reference_type::creditor_reference:
			    if(uncompressed_layout(opts_.layout)) {
				    y -= layout_definitions::receipt_paragraph_spacing_pts;
			    }
				title_writer.write(x, y, tr_.translate("RmtInf.Ref"));
				value_writer.write(x, y, payment_reference_util::format_reference(pr).c_str());
				break;
			case model::reference_type::without_reference:
			default:
				break;
		}
	}

    if(uncompressed_layout(opts_.layout)) {
	    y -= layout_definitions::receipt_paragraph_spacing_pts;
    }
	// Debtor
	if (qr_invoice.get_ultimate_debtor().empty()) {
		title_writer.write(x, y, tr_.translate("UltmtDbtr.Empty"));
		y -= receipt_debtor_field_.get_height();
		y -= du::millimeters_to_points(1.5);
		const auto left_x = receipt_info_section_rect_.get_left_x() + additional_print_margin_pt_;
		const double lower_y = y;
		const qrinvoice::layout::rect<double> debtor_field {left_x, lower_y, receipt_debtor_field_};
		write_free_text_box(ctx, debtor_field);
	} else {
		title_writer.write(x, y, tr_.translate("UltmtDbtr"));
		for (const auto& addr_line : model::address_utils::to_address_lines(qr_invoice.get_ultimate_debtor().get_address()))
			value_writer.write(x, y, addr_line.c_str());
	}

	if (y < receipt_info_section_rect_.get_bottom_y() && env::is_true(env::dont_ignore_layout_errors))
		throw qrinvoice::layout::layout_exception{"Not all content could be printed to the receipt information section"};
}
void hummus_writer::add_receipt_amount(PageContentContext* ctx, PDFUsedFont* font_heading, PDFUsedFont* font_value, const model::payment_amount_info &amount_info)
{
	debug_layout(ctx, receipt_amount_section_rect_);

	const auto upper_y = receipt_amount_section_rect_.get_upper_right_y();

	// header
	namespace du = qrinvoice::layout::dimension_unit_utils;
	const auto current_x = receipt_amount_section_rect_.get_left_x() + additional_print_margin_pt_;
	const auto amount_x = current_x + du::millimeters_to_points_rounded(12, 72);
	const auto header_y = upper_y - font_size_receipt_heading_ ;
	AbstractContentContext::TextOptions header_options{font_heading, font_size_receipt_heading_};
	ctx->WriteText(current_x, header_y, tr_.translate("Currency"), header_options);
	ctx->WriteText(amount_x, header_y, tr_.translate("Amount"), header_options);

	// value
	const auto values_y = header_y - font_size_receipt_value_ - layout_definitions::receipt_amount_line_spacing_pts;
	AbstractContentContext::TextOptions value_options{font_value, font_size_receipt_value_};
	ctx->WriteText(current_x, values_y, currency_ops::get_currency_code(amount_info.get_currency()), value_options);
	if (amount_info.is_amount_set()) {
		std::string amount{ amount_util::format_amount_for_print(amount_info.get_amount()) };
		ctx->WriteText(amount_x, values_y, amount, value_options);
	} else {
		double amount_field_x = receipt_amount_section_rect_.get_right_x() - receipt_amount_field_.get_width() + additional_print_margin_pt_;
		double amount_field_y = upper_y - receipt_amount_field_.get_height() - du::millimeters_to_points_rounded(0.75, 72);
		qrinvoice::layout::rect<double> box{amount_field_x, amount_field_y, receipt_amount_field_};
		write_free_text_box(ctx, box);
	}
}
void hummus_writer::add_receipt_acceptance_section(PageContentContext* ctx, PDFUsedFont* font_heading)
{
	using namespace hummus_helper;
	namespace du = qrinvoice::layout::dimension_unit_utils;
	debug_layout(ctx, receipt_acceptance_section_rect_);

	const char *label = tr_.translate("AcceptancePoint");
	const auto x = receipt_acceptance_section_rect_.get_right_x() - hummus_helper::get_text_width(font_heading, font_size_receipt_heading_, label);
	AbstractContentContext::TextOptions title_options {font_heading, font_size_receipt_heading_};
	double y_title = receipt_acceptance_section_rect_.get_top_y() - font_size_receipt_heading_;
	ctx->WriteText(x, y_title , label, title_options);
}


std::string trim_if_required(std::string input, double max_width, PDFUsedFont* font, double font_size)
{
	using namespace hummus_helper;
	auto width = get_text_width(font, font_size, input);
	if(width <= max_width) {
		return input;
	}

	std::string newString = input;
	while (width > max_width) {
		int len = newString.length();
		double ratio_too_wide = width / max_width;
		// make sure string gets shorter in any circumstances
		int new_len = std::min((int) floor(len / ratio_too_wide), (int) newString.length() - 1);

		newString = newString.substr(0, new_len);
		width = get_text_width(font, font_size, newString + layout_definitions::more_text_indicator);
	}

	return newString + layout_definitions::more_text_indicator;
}

void hummus_writer::add_payment_part_title_section(PageContentContext* ctx, PDFUsedFont* font_title)
{
	debug_layout(ctx, payment_part_title_section_rect_);
	const auto x = payment_part_title_section_rect_.get_left_x();

	AbstractContentContext::TextOptions title_options {font_title, font_size_title_};
	ctx->WriteText(x, payment_part_title_section_rect_.get_top_y() - font_size_title_, tr_.translate("Title"), title_options);
}

void hummus_writer::add_payment_part_qr_code_image(PDFWriter& pdf_writer, PDFPage& page, PageContentContext* ctx, const qrinvoice::qrcode::qr_code_bitmap& qr_code_bitmap)
{
	debug_layout(ctx, payment_part_qr_section_rect_);

	namespace du = qrinvoice::layout::dimension_unit_utils;
	auto qr_img_width = qr_code_bitmap.get_modules();
	auto qr_img_height = qr_code_bitmap.get_modules();

	const auto x = payment_part_qr_section_rect_.get_lower_left_x();
	const auto y = payment_part_qr_section_rect_.get_lower_left_y() + quiet_space_pt_;
	qrinvoice::qrcode::swiss_qr_code_writer qr_code_writer;
	// write qr code
	{
		std::vector<unsigned char> qrcode_tmp = {};
		auto qrcode_unscaled = qr_code_writer.render_int_scaled_qr_code(qr_code_bitmap, 1);
		if(!stbi_helper::saveImageToVector(&qrcode_tmp, qrcode_unscaled, output_format::png)) {
			throw technical_exception("Error while trying to write image into memory");
		}

		// add qr code to PDF
		hummus_helper::pause_page_content_context(pdf_writer, ctx);
		InputByteArrayStream qr_code_img_stream{qrcode_tmp.data(), static_cast<IOBasicTypes::LongFilePositionType>(qrcode_tmp.size())};
		std::unique_ptr<PDFFormXObject> qr_code_img { pdf_writer.CreateFormXObjectFromPNGStream(&qr_code_img_stream) };
		if (!qr_code_img) throw qrinvoice::base_exception{"failed to embed qr code image"};


		namespace du = qrinvoice::layout::dimension_unit_utils;

		auto scale_x = static_cast<double>(du::points_to_pixels(qr_code_.get_width(), 72)) / qr_img_width;
		auto scale_y = static_cast<double>(du::points_to_pixels(qr_code_.get_height(), 72)) / qr_img_height;

		ctx->q();
		ctx->cm(scale_x, 0, 0, scale_y, x, y);
		ctx->Do(page.GetResourcesDictionary().AddFormXObjectMapping(qr_code_img->GetObjectID()));
		ctx->Q();
	}

	// overlay with swiss cross
	{
		std::vector<unsigned char> swiss_cross_tmp = {};
		auto swiss_cross = qr_code_writer.swiss_cross();
		if(!stbi_helper::saveImageToVector(&swiss_cross_tmp, swiss_cross, output_format::png)) {
			throw technical_exception("Error while trying to write image into memory");
		}

		hummus_helper::pause_page_content_context(pdf_writer, ctx);
		InputByteArrayStream swiss_cross_img_stream{swiss_cross_tmp.data(), static_cast<IOBasicTypes::LongFilePositionType>(swiss_cross_tmp.size())};
		std::unique_ptr<PDFFormXObject> swiss_cross_img { pdf_writer.CreateFormXObjectFromPNGStream(&swiss_cross_img_stream) };
		if (!swiss_cross_img) throw qrinvoice::base_exception{"failed to embed qr code image"};

		const auto swiss_cross_scale = static_cast<double>(du::points_to_pixels(swiss_cross_.get_width(), 72)) / swiss_cross.get_width();

		const auto position_offset = (qr_code_.get_width() / 2.0) - (swiss_cross_.get_width() / 2.0);

		ctx->q();
		ctx->cm(swiss_cross_scale, 0, 0, swiss_cross_scale, x + position_offset, y+position_offset);
		ctx->Do(page.GetResourcesDictionary().AddFormXObjectMapping(swiss_cross_img->GetObjectID()));
		ctx->Q();
	}

	hummus_helper::pause_page_content_context(pdf_writer, ctx);
}

void hummus_writer::add_payment_part_amount(PageContentContext* ctx, PDFUsedFont* font_heading, PDFUsedFont* font_value,
											const model::payment_amount_info& amount_info)
{
	debug_layout(ctx, payment_part_amount_section_rect_);

	const auto lower_y = payment_part_amount_section_rect_.get_lower_left_y();
	const auto upper_y = payment_part_amount_section_rect_.get_upper_right_y();

	// header
	namespace du = qrinvoice::layout::dimension_unit_utils;
	const auto current_x = payment_part_amount_section_rect_.get_left_x();
	const auto amount_x = current_x + du::millimeters_to_points_rounded(14, 72);
	const auto header_y = upper_y - font_size_payment_part_heading_;
	AbstractContentContext::TextOptions header_options{ font_heading, font_size_payment_part_heading_ };
	ctx->WriteText(current_x, header_y, tr_.translate("Currency"), header_options);
	ctx->WriteText(amount_x, header_y, tr_.translate("Amount"), header_options);

	// value
	const auto values_y = header_y - font_size_payment_part_value_ - layout_definitions::payment_part_amount_line_spacing_pts;
	AbstractContentContext::TextOptions value_options{ font_value, font_size_payment_part_value_ };
	ctx->WriteText(current_x, values_y, currency_ops::get_currency_code(amount_info.get_currency()), value_options);
	if (amount_info.is_amount_set()) {
		std::string amount{ amount_util::format_amount_for_print(amount_info.get_amount()) };
		ctx->WriteText(amount_x, values_y, amount, value_options);
	} else {
        double amount_field_x = payment_part_amount_section_rect_.get_right_x() - payment_part_amount_field_.get_width() - du::millimeters_to_points(1);
        double amount_field_y = lower_y + du::millimeters_to_points(2);
		qrinvoice::layout::rect<double> box{amount_field_x, amount_field_y, payment_part_amount_field_};
		write_free_text_box(ctx, box);
	}
}

void hummus_writer::add_payment_part_info_section(PageContentContext* ctx, PDFUsedFont* font_heading, PDFUsedFont* font_value, const model::qr_invoice& qr_invoice)
{
	using namespace hummus_helper;
	debug_layout(ctx, payment_part_info_section_rect_);

	const AbstractContentContext::TextOptions title_opts {font_heading, font_size_payment_part_heading_};
	info_section_title_writer title_writer{ *ctx, title_opts,
										 uncompressed_layout(opts_.layout) ? static_cast<double>(layout_definitions::payment_part_heading_line_spacing_pts) : 0};
	info_section_value_writer value_writer{ *ctx, {font_value, font_size_payment_part_value_},
                                            uncompressed_layout(opts_.layout) ? static_cast<double>(layout_definitions::payment_part_value_line_spacing_pts) : 0,
                                         payment_part_info_section_rect_.get_width() - additional_print_margin_pt_};

	const double x = payment_part_info_section_rect_.get_left_x();
	double y = payment_part_info_section_rect_.get_top_y() - 1; /* minus 1 point in order to align with title from payment part */

	// Account
	title_writer.write(x, y, tr_.translate("CdtrInf.IBANCreditor"), true);
	value_writer.write(x, y, iban_util::format_iban(qr_invoice.get_creditor_info().get_iban()).c_str());

	const auto addr = qr_invoice.get_creditor_info().get_creditor().get_address();
	for (const auto& addr_line : model::address_utils::to_address_lines(addr))
		value_writer.write(x, y, addr_line.c_str());

	model::payment_reference pr = qr_invoice.get_payment_reference();
	if (!pr.empty()) {
		switch (pr.get_reference_type().get()) {
			case model::reference_type::qr_reference:
			case model::reference_type::creditor_reference:
                if(uncompressed_layout(opts_.layout)) {
                    y -= layout_definitions::payment_part_paragraph_spacing_pts;
                }
				title_writer.write(x, y, tr_.translate("RmtInf.Ref"));
				value_writer.write(x, y, payment_reference_util::format_reference(pr).c_str());
				break;
			case model::reference_type::without_reference:
			default:
				break;
		}

		// Additional information
		const std::string &unstructured_message = pr.get_additional_information().get_unstructured_message();
		const bool unstructured_msg_do_not_use_for_payment = unstructured_message == "NICHT ZUR ZAHLUNG VERWENDEN"
															 || unstructured_message == "NE PAS UTILISER POUR LE PAIEMENT"
															 || unstructured_message == "DO NOT USE FOR PAYMENT"
															 || unstructured_message == "NON UTILIZZARE PER IL PAGAMENTO";

		const std::string final_unstructured_msg = unstructured_msg_do_not_use_for_payment ? tr_.translate("DoNotUseForPayment") : unstructured_message;

		const std::string &bill_information = pr.get_additional_information().get_bill_information();
		if (!final_unstructured_msg.empty() || !bill_information.empty()) {
            if(uncompressed_layout(opts_.layout)) {
                y -= layout_definitions::payment_part_paragraph_spacing_pts;
            }
			title_writer.write(x, y, tr_.translate("RmtInf.Ustrd"));

			const auto unstructured_msg_length = string_util::utf8_length(final_unstructured_msg);
			const auto bill_information_length = string_util::utf8_length(bill_information);
			const auto total_length = unstructured_msg_length + bill_information_length;
			if (!final_unstructured_msg.empty() && !bill_information.empty() && opts_.layout == payment_part_receipt::layout::compressed && total_length > (2 * layout_definitions::approx_max_line_length_payment_part_info_section)) {
				// according to the spec version 2.0 - 3.5.4:
				// If both elements are filled in, then a line break can be introduced after the information in the first element "Ustrd" (Unstructured message).
				// If there is insufficient space, the line break can be omitted (but this makes it more difficult to read).
				// If not all the details contained in the QR code can be displayed, the shortened content must be marked with "..." at the end. It must be ensured
				// that all personal data is displayed.
				auto joined = final_unstructured_msg + " " + bill_information;
				value_writer.write(x, y, joined.c_str());
			} else {
				if (!final_unstructured_msg.empty()) {
					value_writer.write(x, y, final_unstructured_msg.c_str());
				}
				if (!bill_information.empty()) {
					value_writer.write(x, y, bill_information.c_str());
				}
			}
		}
	}

    if(uncompressed_layout(opts_.layout)) {
        y -= layout_definitions::payment_part_paragraph_spacing_pts;
    }
	// Debtor
	if (qr_invoice.get_ultimate_debtor().empty()) {
		title_writer.write(x, y, tr_.translate("UltmtDbtr.Empty"));

		y -= payment_part_debtor_field_.get_height();
		y -= du::millimeters_to_points(1.5);
		const auto left_x = payment_part_info_section_rect_.get_left_x();
		const double lower_y = y;
		const qrinvoice::layout::rect<double> debtor_field {left_x, lower_y, payment_part_debtor_field_};
		write_free_text_box(ctx, debtor_field);
	} else {
		title_writer.write(x, y, tr_.translate("UltmtDbtr"));
		for (const auto& addr_line : model::address_utils::to_address_lines(qr_invoice.get_ultimate_debtor().get_address()))
			value_writer.write(x, y, addr_line.c_str());
	}

	if (y < payment_part_info_section_rect_.get_bottom_y() && env::is_true(env::dont_ignore_layout_errors))
		throw qrinvoice::layout::layout_exception{"Not all content could be printed to the payment part information section"};
}

void hummus_writer::add_payment_part_further_info_section(PageContentContext* ctx, PDFUsedFont* font_heading, PDFUsedFont* font_value, const model::qr_invoice &qr_invoice)
{
	using namespace hummus_helper;
	debug_layout(ctx, payment_part_further_info_section_rect_);

	auto x = payment_part_further_info_section_rect_.get_left_x();
	auto lowerY = payment_part_further_info_section_rect_.get_bottom_y() + additional_print_margin_pt_;

	AbstractContentContext::TextOptions name_options{ font_heading, font_size_payment_part_further_info_ };
	AbstractContentContext::TextOptions value_options{ font_value, font_size_payment_part_further_info_ };
	
	auto params = qr_invoice.get_alternative_schemes().get_alternative_scheme_params();
	for (unsigned i = params.size(); i-- > 0; )
	{
		auto alternative_scheme = params[i];
        lowerY += layout_definitions::payment_part_further_info_line_spacing_pts;

		const model::alternative_schemes_utils::alternative_scheme_pair &pair = model::alternative_schemes_utils::parse_for_output(alternative_scheme);
		auto remaining_space_value = payment_part_further_info_section_rect_.get_width();
		auto value_x = x;
		if(!pair.name.empty()) {
			ctx->WriteText(x, lowerY, pair.name, name_options);

			double name_width = get_text_width(font_heading, font_size_payment_part_further_info_, pair.name);
			remaining_space_value -= name_width;

			ctx->WriteText(x, lowerY, pair.name, name_options);
			value_x += name_width;
		}

		if(!pair.value.empty()) {
			const std::string &value = trim_if_required(pair.value, remaining_space_value, font_value, font_size_payment_part_further_info_);
			ctx->WriteText(value_x, lowerY, value, value_options);
		}

		lowerY += font_size_payment_part_further_info_;
	}

	// Ultimate Creditor
	if (!qr_invoice.get_ultimate_creditor().empty()) {
        lowerY += layout_definitions::payment_part_further_info_line_spacing_pts;
		const std::string label = std::string(tr_.translate("UltmtCdtr"));
		ctx->WriteText(x, lowerY, label, name_options);

		auto label_width = get_text_width(font_heading, font_size_payment_part_further_info_, label + "-"); // use "-" instead of space because space is not considered in width calculation
		auto remaining_space_value = payment_part_further_info_section_rect_.get_width() - label_width;

		auto address_line = model::address_utils::to_single_line_address(qr_invoice.get_ultimate_creditor().get_address());
		auto address_line_trimmed = trim_if_required(address_line, remaining_space_value, font_value, font_size_payment_part_further_info_);

		ctx->WriteText(x + label_width, lowerY, address_line_trimmed, value_options);
	}
}

void hummus_writer::write_free_text_box(PageContentContext* ctx, qrinvoice::layout::rect<double> rect)
{
	const auto corner_len = box_corner_line_length_;
	const auto left_x = rect.get_lower_left_x();
	const auto lower_y = rect.get_lower_left_y();
	const auto right_x = rect.get_upper_right_x();
	const auto upper_y = rect.get_upper_right_y();

	DoubleAndDoublePairList left_bottom;
	left_bottom.push_back(DoubleAndDoublePair{left_x + corner_len, lower_y});
	left_bottom.push_back(DoubleAndDoublePair{left_x, lower_y});
	left_bottom.push_back(DoubleAndDoublePair{left_x, lower_y + corner_len});

	DoubleAndDoublePairList left_upper;
	left_upper.push_back(DoubleAndDoublePair{left_x + corner_len, upper_y});
	left_upper.push_back(DoubleAndDoublePair{left_x, upper_y});
	left_upper.push_back(DoubleAndDoublePair{left_x, upper_y - corner_len});

	DoubleAndDoublePairList right_upper;
	right_upper.push_back(DoubleAndDoublePair{right_x - corner_len, upper_y});
	right_upper.push_back(DoubleAndDoublePair{right_x, upper_y});
	right_upper.push_back(DoubleAndDoublePair{right_x, upper_y - corner_len});

	DoubleAndDoublePairList right_bottom;
	right_bottom.push_back(DoubleAndDoublePair{right_x - corner_len, lower_y});
	right_bottom.push_back(DoubleAndDoublePair{right_x, lower_y});
	right_bottom.push_back(DoubleAndDoublePair{right_x, lower_y + corner_len});

	AbstractContentContext::GraphicOptions go {
		AbstractContentContext::eStroke, AbstractContentContext::eGray, 0, box_corner_line_width_
	};

	ctx->DrawPath(left_bottom, go);
	ctx->DrawPath(left_upper, go);
	ctx->DrawPath(right_upper, go);
	ctx->DrawPath(right_bottom, go);
}

void hummus_writer::draw_boundary_lines(PDFWriter& pdf_writer, PDFPage& page, PageContentContext* ctx, PDFUsedFont* font)
{
	if (opts_.boundary_lines == boundary_lines::none)
		return;

	write_separation_label(ctx, font);

	ObjectIDType objectIDType;
	{
		hummus_helper::pause_page_content_context(pdf_writer, ctx);

		array2d scissor(35,58);
		for (int row = 0; row < 35; row++) {
			unsigned char *const rowArr = scissor[row];
			for (int col = 0; col < 58; col++) {
				const unsigned char i = qrinvoice::resource::scissor_58x35_horizontal[row][col];
				rowArr[col] = i;
			}
		}

		std::vector<unsigned char> scissor_tmp = {};
		if(!stbi_helper::saveImageToVector(&scissor_tmp, scissor, output_format::png)) {
			throw technical_exception("Error while trying to write image into memory");
		}
		
		InputByteArrayStream scissor_img_stream{scissor_tmp.data(), static_cast<IOBasicTypes::LongFilePositionType>(scissor_tmp.size())};
		std::unique_ptr<PDFFormXObject> scissor_img { pdf_writer.CreateFormXObjectFromPNGStream(&scissor_img_stream) };
		if (!scissor_img) throw qrinvoice::base_exception{"failed to embed scissor image"};

		objectIDType = scissor_img->GetObjectID();
	}

	draw_horizontal_boundary_line(pdf_writer, page, ctx, objectIDType);
	draw_vertical_boundary_line(pdf_writer, page, ctx, objectIDType);
}

void hummus_writer::write_separation_label(PageContentContext* ctx, PDFUsedFont* font)
{
	if(opts_.boundary_line_separation_text && opts_.page_size != page_size::din_lang) {
		AbstractContentContext::TextOptions options{ font, font_size_separation_label_ };
		using namespace qrinvoice::layout::dimension_unit_utils;
		double x = payment_part_lower_left_.get_x() + quiet_space_pt_;
		double y = payment_part_lower_left_.get_y() + get_payment_part_height() + font_size_separation_label_ / 2;
		ctx->WriteText(x, y, tr_.translate_label("SeparationLabel"), options);
	} else if(opts_.boundary_line_separation_text) {
		spdlog::info("Separation label above payment part was not printed as PageSize is too small.");
	}
}

void hummus_writer::draw_horizontal_boundary_line(PDFWriter& pdf_writer, PDFPage& page, PageContentContext* ctx, ObjectIDType& scissor_object_id) {

	const double half_line_width = boundary_line_width_ / 2;
    const auto line_start_x = opts_.boundary_lines == qrinvoice::boundary_lines::enabled_with_margins ? root_lower_left_.get_x() + print_margin_pt_ : root_lower_left_.get_x();
    const auto line_end_x = line_start_x + get_payment_part_width() + (opts_.boundary_lines == qrinvoice::boundary_lines::enabled_with_margins ? - (2 * print_margin_pt_) : 0);
	const auto y = root_lower_left_.get_y() + get_payment_part_height() - half_line_width;

	DoubleAndDoublePairList path_points;
	const AbstractContentContext::GraphicOptions go_line {AbstractContentContext::eStroke, AbstractContentContext::eGray, 0, boundary_line_width_};
	if(opts_.boundary_line_scissors && opts_.page_size != page_size::din_lang) {
		const double scissor_length = du::millimeters_to_points(static_cast<double>(layout_definitions::scissor_length));
		const double scissor_height = du::millimeters_to_points(static_cast<double>(layout_definitions::scissor_length) / 58.0 * 35.0);
		const double scissor_left_x = receipt_title_section_rect_.get_left_x() + (opts_.boundary_lines == qrinvoice::boundary_lines::enabled_with_margins ? print_margin_pt_ :  additional_print_margin_pt_);
		const double scissor_right_x = scissor_left_x + scissor_length;
		const auto scale_scissor =  scissor_length / du::inches_to_points((58.0 / 72));

		// scissors
		ctx->q();
		ctx->cm(scale_scissor, 0, 0, scale_scissor, scissor_left_x, y - (scissor_height / 2));
		ctx->Do(page.GetResourcesDictionary().AddFormXObjectMapping(scissor_object_id));
		ctx->Q();

		path_points.push_back(DoubleAndDoublePair{line_start_x, y});
		path_points.push_back(DoubleAndDoublePair{scissor_left_x, y});
		ctx->DrawPath(path_points, go_line);

		path_points.clear();
		path_points.push_back(DoubleAndDoublePair{scissor_right_x, y});
		path_points.push_back(DoubleAndDoublePair{line_end_x, y});
		ctx->DrawPath(path_points, go_line);
	} else {
		if(opts_.boundary_line_scissors) {
			spdlog::info("Scissor on horizontal line is only printed when using PageSize A4 or A5");
		}

		path_points.push_back(DoubleAndDoublePair{line_start_x, y});
		path_points.push_back(DoubleAndDoublePair{line_end_x, y});
		ctx->DrawPath(path_points, go_line);
	};

}

void hummus_writer::draw_vertical_boundary_line(PDFWriter& pdf_writer, PDFPage& page, PageContentContext* ctx, ObjectIDType& scissor_object_id) {
	const double half_line_width = boundary_line_width_ / 2;
	const auto x = payment_part_lower_left_.get_x() - half_line_width;
	const auto line_start_y = payment_part_lower_left_.get_y() + get_payment_part_height() - half_line_width;
	const auto line_end_y = payment_part_lower_left_.get_y() + (opts_.boundary_lines == qrinvoice::boundary_lines::enabled_with_margins ?  print_margin_pt_ : 0) ;

	DoubleAndDoublePairList path_points;
	const AbstractContentContext::GraphicOptions go_line {AbstractContentContext::eStroke, AbstractContentContext::eGray, 0, boundary_line_width_};
	if(opts_.boundary_line_scissors) {
		const double scissor_length = du::millimeters_to_points(static_cast<double>(layout_definitions::scissor_length));
		const double scissor_height = du::millimeters_to_points(static_cast<double>(layout_definitions::scissor_length) / 58.0 * 35.0);
		const double scissor_top_y = payment_part_title_section_rect_.get_top_y();
		const double scissor_bottom_y = scissor_top_y - scissor_length;
		const auto scale_scissor =  scissor_length / du::inches_to_points((58.0 / 72));

		// scissors
		ctx->q();
		ctx->cm(scale_scissor, 0, 0, scale_scissor, x - (scissor_height / 2), scissor_top_y); // this does scale and position
		ctx->cm(0, -1, 1, 0, 0, scissor_height); // and this does rotate the scissor by 90/270°
		ctx->Do(page.GetResourcesDictionary().AddFormXObjectMapping(scissor_object_id));
		ctx->Q();

		path_points.push_back(DoubleAndDoublePair{x, scissor_bottom_y});
		path_points.push_back(DoubleAndDoublePair{x, line_end_y});
		ctx->DrawPath(path_points, go_line);

		path_points.clear();
		path_points.push_back(DoubleAndDoublePair{x, line_start_y});
		path_points.push_back(DoubleAndDoublePair{x, scissor_top_y});
		ctx->DrawPath(path_points, go_line);
	} else {
		path_points.push_back(DoubleAndDoublePair{x, line_start_y});
		path_points.push_back(DoubleAndDoublePair{x, line_end_y});

		ctx->DrawPath(path_points, go_line);
	}

}

void hummus_writer::setup_page(PDFPage& page)
{
	page.SetMediaBox(PDFRectangle{0, 0, page_canvas_.get_width(), page_canvas_.get_height() });
}

// the following functions assume that
// quiet_space_pt_ and potentially some others are already initialized

 const qrinvoice::layout::rect<double> hummus_writer::init_receipt_title_sect_rect() const
{
	const double info_sect_x = quiet_space_pt_;
	const double info_sect_y = get_receipt_height() - quiet_space_pt_ - mm_to_points(layout_definitions::receipt_title_section).get_height();
	return qrinvoice::layout::rect<double> {info_sect_x, info_sect_y, mm_to_points(layout_definitions::receipt_title_section)}.translate(root_lower_left_);
}

const qrinvoice::layout::rect<double> hummus_writer::init_receipt_info_sect_rect() const
{
	const double info_sect_x = quiet_space_pt_;
	const double info_sect_y = get_receipt_height() - quiet_space_pt_ - mm_to_points(layout_definitions::receipt_title_section).get_height() - mm_to_points(layout_definitions::receipt_information_section).get_height();
	return qrinvoice::layout::rect<double> {info_sect_x, info_sect_y, mm_to_points(layout_definitions::receipt_information_section)}.translate(root_lower_left_);
}

const qrinvoice::layout::rect<double> hummus_writer::init_receipt_amount_sect_rect() const
{
	const double info_sect_x = quiet_space_pt_;
	const double info_sect_y = quiet_space_pt_ + mm_to_points(layout_definitions::receipt_acceptance_section).get_height();
	return qrinvoice::layout::rect<double> {info_sect_x, info_sect_y, mm_to_points(layout_definitions::receipt_amount_section)}.translate(root_lower_left_);
}

const qrinvoice::layout::rect<double> hummus_writer::init_receipt_acceptance_sect_rect() const
{
	const double info_sect_x = quiet_space_pt_;
	const double info_sect_y = quiet_space_pt_;
	return qrinvoice::layout::rect<double> {info_sect_x, info_sect_y, mm_to_points(layout_definitions::receipt_acceptance_section)}.translate(root_lower_left_);
}

const qrinvoice::layout::rect<double> hummus_writer::init_payment_part_title_sect_rect() const
{
	const double info_sect_x = quiet_space_pt_;
	const double info_sect_y = get_payment_part_height() - quiet_space_pt_ - mm_to_points(layout_definitions::payment_part_title_section).get_height();
	return qrinvoice::layout::rect<double> {info_sect_x, info_sect_y, mm_to_points(layout_definitions::payment_part_title_section)}.translate(payment_part_lower_left_);
}

const qrinvoice::layout::rect<double> hummus_writer::init_payment_part_qr_sect_rect() const
{
	const double info_sect_x = quiet_space_pt_;
	const double info_sect_y = get_payment_part_height() - quiet_space_pt_ - mm_to_points(layout_definitions::payment_part_title_section).get_height() - mm_to_points(layout_definitions::payment_part_qr_section).get_height();
	return qrinvoice::layout::rect<double> {info_sect_x, info_sect_y, mm_to_points(layout_definitions::payment_part_qr_section)}.translate(payment_part_lower_left_);
}

const qrinvoice::layout::rect<double> hummus_writer::init_payment_part_info_sect_rect() const
{
	const double info_sect_x = quiet_space_pt_ + mm_to_points(layout_definitions::payment_part_qr_section).get_width();
	const double info_sect_y = get_payment_part_height() - quiet_space_pt_ - mm_to_points(layout_definitions::payment_part_information_section).get_height();
	return qrinvoice::layout::rect<double> {info_sect_x, info_sect_y, mm_to_points(layout_definitions::payment_part_information_section)}.translate(payment_part_lower_left_);
}

const qrinvoice::layout::rect<double> hummus_writer::init_payment_part_amount_sect_rect() const
{
	const double info_sect_x = quiet_space_pt_;
	const double info_sect_y = quiet_space_pt_ + mm_to_points(layout_definitions::payment_part_further_info_section).get_height();
	return qrinvoice::layout::rect<double> {info_sect_x, info_sect_y, mm_to_points(layout_definitions::payment_part_amount_section)}.translate(payment_part_lower_left_);
}

const qrinvoice::layout::rect<double> hummus_writer::init_payment_part_further_info_sect_rect() const
{
	const double info_sect_x = quiet_space_pt_;
	const double info_sect_y = quiet_space_pt_;
	return qrinvoice::layout::rect<double> {info_sect_x, info_sect_y, mm_to_points(layout_definitions::payment_part_further_info_section)}.translate(payment_part_lower_left_);
}

inline qrinvoice::layout::dimension<double> hummus_writer::mm_to_points(const qrinvoice::layout::dimension<unsigned> mm_dim)
{
	using namespace qrinvoice::layout::dimension_unit_utils;
	return {
			millimeters_to_points(static_cast<double>(mm_dim.get_width())),
			millimeters_to_points(static_cast<double>(mm_dim.get_height()))
	};
}

inline const double hummus_writer::calculate_additional_print_margin(bool additional_margin, unsigned dpi)
{
	return additional_margin ? du::millimeters_to_points(layout_definitions::additional_print_margin, dpi) : 0;
}

} // namespace payment_part_receipt
} // namespace qrinvoice

