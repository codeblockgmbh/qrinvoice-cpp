
#include <qrinvoice/model/address_type.hpp>


namespace qrinvoice {
namespace model {

address_type address_type::parse(const std::string& addr_type_code) {
	if (addr_type_code == "K")
		return qrinvoice::model::address_type(address_type::combined);
	if (addr_type_code == "S")
		return qrinvoice::model::address_type(address_type::structured);

	throw parse_exception(std::string{"Invalid address type '"} + addr_type_code + "' given");
}


} // namespace model
} // namespace qrinvoice

