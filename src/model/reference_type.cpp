
#include <qrinvoice/model/reference_type.hpp>


namespace qrinvoice {
namespace model {

reference_type reference_type::parse(const std::string& ref_type_code) {
	if (ref_type_code == "QRR")
		return qrinvoice::model::reference_type(reference_type::qr_reference);
	if (ref_type_code == "SCOR")
		return qrinvoice::model::reference_type(reference_type::creditor_reference);
	if (ref_type_code == "NON")
		return qrinvoice::model::reference_type(reference_type::without_reference);

	throw parse_exception(std::string{"Invalid reference type '"} + ref_type_code + "' given");
}


} // namespace model
} // namespace qrinvoice

