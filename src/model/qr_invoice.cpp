
#include <qrinvoice/model/swiss_payments_code.hpp>
#include <qrinvoice/model/qr_invoice.hpp>
#include <qrinvoice/model/header.hpp>
#include <qrinvoice/model/creditor_info.hpp>
#include <qrinvoice/model/ultimate_creditor.hpp>
#include <qrinvoice/model/payment_amount_info.hpp>
#include <qrinvoice/model/ultimate_debtor.hpp>
#include <qrinvoice/model/payment_reference.hpp>
#include <qrinvoice/model/alternative_schemes.hpp>
#include <qrinvoice/model/validation.hpp>
#include <qrinvoice/util/string_util.hpp>

#include <system/i18n/translator.hpp>

namespace qrinvoice {
namespace model {


const system::i18n::translator tr_ {locale::german};

qr_invoice::qr_invoice(const header& header, const creditor_info& cred_info,
	const ultimate_creditor& u_creditor, const payment_amount_info& payment_info,
	const ultimate_debtor& u_debtor, const payment_reference& payment_ref,
	const alternative_schemes& alt_schemes)
	:header_{header}
	,creditor_info_{cred_info}
	,ultimate_creditor_{u_creditor}
	,payment_amount_info_{payment_info}
	,ultimate_debtor_{u_debtor}
	,payment_reference_{payment_ref}
	,alternative_schemes_{alt_schemes} {}


qr_invoice qr_invoice::builder::build()
{
	const header hdr				= build_header();
	const creditor_info cred_info			= build_creditor_info();
	const class ultimate_creditor u_creditor	= build_ultimate_creditor();
	const class payment_amount_info payment_info	= build_payment_amount_info();
	const class ultimate_debtor u_debtor		= build_ultimate_debtor();
	class payment_reference payment_ref	= build_payment_reference();
	const class alternative_schemes alt_schemes	= build_alternative_schemes();

	// Do not use for payment / notification
	if (payment_info.is_amount_set()) {
		if(payment_info.get_amount() == 0.00) {
			if(payment_ref.get_additional_information().get_unstructured_message().empty() ) {
				payment_ref.get_additional_information().set_unstructured_message(tr_.translate("DoNotUseForPayment"));
			}
		}
	}

	qr_invoice qi {
			hdr, cred_info, u_creditor, payment_info, u_debtor, payment_ref, alt_schemes
	};

	// perform validation
	model::validation::validate(qi).throw_exception_on_errors();

	return qi;
}

header qr_invoice::builder::build_header() const
{
	return header::builder{}
		.qr_type(qrinvoice::model::spc::qr_type)
		.version(string_util::details::parse<short>(qrinvoice::model::spc::version))
		.coding_type(qrinvoice::model::spc::coding_type)
		.build();
}

creditor_info qr_invoice::builder::build_creditor_info()
{
	return creditor_info_builder_
		.iban(iban_)
		.creditor(creditor::builder{}
				.address(creditor_addr_builder_.build())
				.build())
		.build();
}

ultimate_creditor qr_invoice::builder::build_ultimate_creditor()
{
	return ultimate_creditor::builder{}
		.address(ultimate_creditor_addr_builder_.build())
		.build();
}

ultimate_debtor qr_invoice::builder::build_ultimate_debtor()
{
	return ultimate_debtor::builder{}
		.address(ultimate_debtor_addr_builder_.build())
		.build();
}


} // namespace model
} // namespace qrinvoice

