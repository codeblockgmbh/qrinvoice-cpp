
#include <vector>
#include <string>
#include <iterator>
#include <algorithm>
#include <qrinvoice/util/string_util.hpp>
#include <qrinvoice/model/swiss_payments_code.hpp>
#include <qrinvoice/model/validation/validation_exception.hpp>

#include <qr_invoice_swiss_qr_code_internal.hpp>

namespace qrinvoice {
namespace model {

void swiss_payments_code::assert_no_whitespaces(const std::string &str, const std::string &field)
{
    if(qrinvoice::string_util::details::contains_white_space(str)) {
		throw model::validation::validation_exception {
				"SwissPaymentsCode: " + field + " did unexpectedly contain whitespace(s)"
		};
	}
}

std::string swiss_payments_code::to_swiss_payments_code_string() const
{
	// only safety guards to make sure no unexpected whitespaces are printed to swiss payments code
	assert_no_whitespaces(qr_type, "Header/QRType");
	assert_no_whitespaces(version, "Header/Version");
	assert_no_whitespaces(coding, "Header/Coding");
	assert_no_whitespaces(iban, "CdtrInf/IBAN");
	assert_no_whitespaces(cr_adr_tp, "CdtrInf/Cdtr/AdrTp");
	assert_no_whitespaces(ucr_adr_tp, "UltmtCdtr/AdrTp");
	assert_no_whitespaces(ud_adr_tp, "UltmtDbtr/AdrTp");
	assert_no_whitespaces(amt, "CcyAmt/Amt");
	assert_no_whitespaces(ccy, "CcyAmt/Ccy");
	assert_no_whitespaces(ref, "RmtInf/Ref");
	assert_no_whitespaces(tp, "RmtInf/Tp");
	assert_no_whitespaces(trailer, "RmtInf/AddInf/Trailer");

	// According to the spec 4.2.4 - Delivery of the data elements - v2.0:
	// All data elements must be present. If the information of the data element is not available, then at lease one carriage return (CR + LF) must take place.
	std::string str;
	append_field(str, qr_type);
	append_field(str, version);
	append_field(str, coding);
	append_field(str, iban);
	append_field(str, cr_adr_tp);
	append_field(str, cr_name);
	append_field(str, cr_strt_nm_or_adr_line1);
	append_field(str, cr_bldg_nb_or_adr_line2);
	append_field(str, cr_pstd_cd);
	append_field(str, cr_twn_nm);
	append_field(str, cr_ctry);
	append_field(str, ucr_adr_tp);
	append_field(str, ucr_name);
	append_field(str, ucr_strt_nm_or_adr_line1);
	append_field(str, ucr_bldg_nb_or_adr_line2);
	append_field(str, ucr_pstd_cd);
	append_field(str, ucr_twn_nm);
	append_field(str, ucr_ctry);
	append_field(str, amt);
	append_field(str, ccy);
	append_field(str, ud_adr_tp);
	append_field(str, ud_name);
	append_field(str, ud_strt_nm_or_adr_line1);
	append_field(str, ud_bldg_nb_or_adr_line2);
	append_field(str, ud_pstd_cd);
	append_field(str, ud_twn_nm);
	append_field(str, ud_ctry);
	append_field(str, tp);
	append_field(str, ref);
	append_field(str, ustrd);

    std::vector<std::string> alt_pmts_without_blank_str;
    alt_pmts_without_blank_str.reserve(alt_pmts.size());

    std::copy_if(alt_pmts.begin(), alt_pmts.end(), std::back_inserter(alt_pmts_without_blank_str),
                 [](const std::string& str) {
                     return !string_util::details::is_blank(str);
                 }
    );

    const bool has_strd_bkg_inf = !string_util::details::is_blank(strd_bkg_inf);
    const bool has_alt_pmts = !alt_pmts_without_blank_str.empty();

	append_field(str, trailer, has_strd_bkg_inf || has_alt_pmts);
	append_field(str, strd_bkg_inf, has_alt_pmts);

	// According to the spec 4.2.4 - Delivery of the data elements - v2.0:
	// The sole exceptions to this are additional data elements marked with "A" (alternative scheme). These can be eliminated if not needed.
	append_alt_pmts(str, alt_pmts_without_blank_str);

	return str;
}

inline void swiss_payments_code::append_field(std::string& str, const std::string& field, bool append_separator)
{
	str += field;
	if (append_separator)
		str += qrinvoice::model::spc::element_separator;
}

bool operator == (const swiss_payments_code& lhs, const swiss_payments_code& rhs)
{
	return lhs.qr_type == rhs.qr_type &&
	lhs.version == rhs.version &&
	lhs.coding == rhs.coding &&

	lhs.iban == rhs.iban &&
	lhs.cr_adr_tp == rhs.cr_adr_tp &&
	lhs.cr_name == rhs.cr_name &&
	lhs.cr_strt_nm_or_adr_line1 == rhs.cr_strt_nm_or_adr_line1 &&
	lhs.cr_bldg_nb_or_adr_line2 == rhs.cr_bldg_nb_or_adr_line2 &&
	lhs.cr_pstd_cd == rhs.cr_pstd_cd &&
	lhs.cr_twn_nm == rhs.cr_twn_nm &&
	lhs.cr_ctry == rhs.cr_ctry &&

	lhs.ucr_adr_tp == rhs.ucr_adr_tp &&
	lhs.ucr_name == rhs.ucr_name &&
	lhs.ucr_strt_nm_or_adr_line1 == rhs.ucr_strt_nm_or_adr_line1 &&
	lhs.ucr_bldg_nb_or_adr_line2 == rhs.ucr_bldg_nb_or_adr_line2 &&
	lhs.ucr_pstd_cd == rhs.ucr_pstd_cd &&
	lhs.ucr_twn_nm == rhs.ucr_twn_nm &&
	lhs.ucr_ctry == rhs.ucr_ctry &&

	lhs.amt == rhs.amt &&
	lhs.ccy == rhs.ccy &&

	lhs.ud_adr_tp == rhs.ud_adr_tp &&
	lhs.ud_name == rhs.ud_name &&
	lhs.ud_strt_nm_or_adr_line1 == rhs.ud_strt_nm_or_adr_line1 &&
	lhs.ud_bldg_nb_or_adr_line2 == rhs.ud_bldg_nb_or_adr_line2 &&
	lhs.ud_pstd_cd == rhs.ud_pstd_cd &&
	lhs.ud_twn_nm == rhs.ud_twn_nm &&
	lhs.ud_ctry == rhs.ud_ctry &&

	lhs.tp == rhs.tp &&
	lhs.ref == rhs.ref &&

	lhs.ustrd == rhs.ustrd &&
	lhs.trailer == rhs.trailer &&
	lhs.strd_bkg_inf == rhs.strd_bkg_inf &&

	lhs.alt_pmts == rhs.alt_pmts;
}

bool operator != (const swiss_payments_code& lhs, const swiss_payments_code& rhs)
{
	return !(lhs == rhs);
}

} // namespace model
} // namespace qrinvoice

