
#include <string>
#include <qrinvoice/model/swiss_payments_code.hpp>
#include <qrinvoice/model/header.hpp>
#include <qrinvoice/util/string_util.hpp>

namespace qrinvoice {
namespace model {


header::header()
	:qr_type_{ qrinvoice::model::spc::qr_type }
	,version_{ string_util::details::parse<short>(qrinvoice::model::spc::version) }
	,coding_type_{ qrinvoice::model::spc::coding_type } {};


} // namepace model
} // namespace qrinvoice


