
#include <qrinvoice/model/address.hpp>
#include <qrinvoice/model/swiss_payments_code.hpp>

#include <util/string_util_internal.hpp>
#include <model/util/address_utils_internal.hpp>


namespace qrinvoice {
namespace model {
namespace address_utils {

static const char *const COUNTRY_CODE_SEPARATOR = " - ";

std::vector<std::string> to_address_lines(const address& address)
{
	return to_address_lines(address, false);
}

/**
 * According to the spec 3.6.2 - Information section - v2.0<br>
 * <p>"Because of the limited space, it is permitted to omit the street name and building number from the addresses of the creditor (Payable to) and the debtor (Payable by)".</p>
 *
 * @param address                                 The address to build the address lines
 * @param omitStreetNameHouseNumberOrAddressLine1 if true, streetname / housenumber or addressline1 is not returned as address line (used in some cases for the receipt)
 * @return a list of address lines
 */
std::vector<std::string> to_address_lines(const address& address, const bool omitStreetNameHouseNumberOrAddressLine1)
{
    switch (address.get_address_type().get()) {
        case address_type::structured:
            return to_address_lines(address.get_name(), address.get_street_name(),
                                    address.get_house_number(), address.get_postal_code(), address.get_city(),
									address.get_country(),
									omitStreetNameHouseNumberOrAddressLine1);
        case address_type::combined:
            return to_address_lines(address.get_name(), address.get_address_line1(), address.get_address_line2(),
									address.get_country(),
									omitStreetNameHouseNumberOrAddressLine1);
        case address_type::unset:
            return {};
    }
	return {};
}


std::vector<std::string> to_address_lines(const std::string& name, const std::string& street_name,
                                          const std::string& house_number, const std::string& postal_code,
                                          const std::string& city, const std::string& country,
                                          const bool omitStreetNameHouseNumberOrAddressLine1)
{
	const bool has_name = !name.empty();
	const bool has_street_name = !street_name.empty();
	const bool has_house_number = !house_number.empty();
	const bool has_postal_code = !postal_code.empty();
	const bool has_city = !city.empty();
    const bool print_country_code = !country.empty() && country != qrinvoice::model::spc::country_code_switzerland;

	const bool line1 = has_name;
	const bool line2 = (has_street_name || has_house_number) && !omitStreetNameHouseNumberOrAddressLine1;
	const bool line3 = has_postal_code || has_city || print_country_code ;

	std::vector<std::string> address_lines;
	if (line1)
		address_lines.push_back(name);

	if (line2) {
		std::string str;
		if (has_street_name)
			str += street_name;

		if (has_street_name && has_house_number)
			str += ' ';

		if (has_house_number)
			str += house_number;

		address_lines.push_back(str);
	}

	if (line3) {
		std::string str;

		if (print_country_code)
			str += country;

        if (print_country_code && (has_postal_code || has_city))
            str += COUNTRY_CODE_SEPARATOR;

        if (has_postal_code)
			str += postal_code;

		if (has_postal_code && has_city)
			str += ' ';

		if (has_city)
			str += city;

		address_lines.push_back(str);
	}

	return address_lines;
}


std::vector<std::string> to_address_lines(const std::string& name, const std::string& address_line1, const std::string& address_line2,
         const std::string& country, const bool omitStreetNameHouseNumberOrAddressLine1)
{
    const bool has_name = !name.empty();
    const bool has_address_line1 = !address_line1.empty();
    const bool has_address_line2 = !address_line2.empty();
    const bool print_country_code = !country.empty() &&
                                    !(qrinvoice::model::spc::country_codes_ch_li.find(country) != qrinvoice::model::spc::country_codes_ch_li.end());

    const bool line1 = has_name;
    const bool line2 = has_address_line1 && !omitStreetNameHouseNumberOrAddressLine1;
    const bool line3 = has_address_line2 || print_country_code;

    std::vector<std::string> address_lines;
    if (line1)
        address_lines.push_back(name);

    if (line2) {
        address_lines.push_back(address_line1);
    }

    if (line3) {
        std::string str;

        if (print_country_code)
            str += country;

        if (print_country_code && has_address_line2)
            str += COUNTRY_CODE_SEPARATOR;

        if(has_address_line2)
            str += address_line2;

        address_lines.push_back(str);
    }

    return address_lines;
}

std::string to_single_line_address(const address& address)
{
	std::vector<std::string> address_lines = to_address_lines(address);
	return string_util::join(address_lines, ", ");
}


} // namespace address_utils
} // namespace model
} // namespace qrinvoice

