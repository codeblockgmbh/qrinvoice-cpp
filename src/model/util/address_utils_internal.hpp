
#ifndef QR_INVOICE_ADDRESS_UTILS_INTERNAL_HPP
#define QR_INVOICE_ADDRESS_UTILS_INTERNAL_HPP

#include <vector>
#include <string>
#include <qrinvoice/model/address.hpp>

namespace qrinvoice {
namespace model {
namespace address_utils {


std::vector<std::string> to_address_lines(const address& address);

std::vector<std::string> to_address_lines(const address& address, bool omitStreetNameHouseNumberOrAddressLine1);

std::vector<std::string> to_address_lines(const std::string& name, const std::string& street_name,
                                          const std::string& house_number, const std::string& postal_code, const std::string& city, const std::string& country,
                                          bool omitStreetNameHouseNumberOrAddressLine1);

std::vector<std::string> to_address_lines(const std::string& name, const std::string& address_line1, const std::string& address_line2, const std::string& country,
        bool omitStreetNameHouseNumberOrAddressLine1);

std::string to_single_line_address(const address& address);

} // namespace address_utils
} // namespace model
} // namespace qrinvoice


#endif // QR_INVOICE_ADDRESS_UTILS_INTERNAL_HPP

