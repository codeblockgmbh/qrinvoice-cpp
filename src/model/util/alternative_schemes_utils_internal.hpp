
#ifndef QR_INVOICE_ALTERNATIVE_SCHEMES_UTILS_INTERNAL_HPP
#define QR_INVOICE_ALTERNATIVE_SCHEMES_UTILS_INTERNAL_HPP

#include <vector>
#include <string>
#include <qrinvoice/model/address.hpp>


namespace qrinvoice {
namespace model {
namespace alternative_schemes_utils {

struct alternative_scheme_pair {
    std::string name;
    std::string value;
};

const alternative_scheme_pair parse_for_output(const std::string& alternative_scheme);


} // namespace alternative_schemes_utils
} // namespace model
} // namespace qrinvoice


#endif // QR_INVOICE_ALTERNATIVE_SCHEMES_UTILS_INTERNAL_HPP

