
#include <util/string_util_internal.hpp>
#include <model/util/alternative_schemes_utils_internal.hpp>


namespace qrinvoice {
namespace model {
namespace alternative_schemes_utils {


const alternative_scheme_pair parse_for_output(const std::string& alternative_scheme)
{
    alternative_scheme_pair pair;
    if(alternative_scheme.empty()) {
        return pair;
    }

    const std::size_t found = alternative_scheme.find(':');
    if (found!=std::string::npos) {
        auto index_after_delimiter = found + 1;
        pair.name = alternative_scheme.substr(0, index_after_delimiter);
        pair.value = alternative_scheme.substr(index_after_delimiter);
    } else {
        pair.value = alternative_scheme;
    }

    return pair;
}

} // namespace alternative_schemes_utils
} // namespace model
} // namespace qrinvoice

