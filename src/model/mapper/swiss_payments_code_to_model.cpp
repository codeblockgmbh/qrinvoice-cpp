
#include <qrinvoice/model/mapping_exception.hpp>
#include <qrinvoice/model/header.hpp>
#include <qrinvoice/model/creditor_info.hpp>
#include <qrinvoice/model/ultimate_creditor.hpp>
#include <qrinvoice/model/payment_amount_info.hpp>
#include <qrinvoice/model/ultimate_debtor.hpp>
#include <qrinvoice/model/payment_reference.hpp>
#include <qrinvoice/model/additional_information.hpp>
#include <qrinvoice/model/alternative_schemes.hpp>
#include <qrinvoice/model/qr_invoice.hpp>
#include <qrinvoice/model/swiss_payments_code.hpp>
#include <qrinvoice/model/mapper/swiss_payments_code_to_model.hpp>
#include <qrinvoice/currency.hpp>
#include <qrinvoice/util/string_util.hpp>

#include <model/mapper/swiss_payments_code_to_model_internal.hpp>


namespace qrinvoice {
namespace model {

auto map_swiss_payments_code_to_model(const swiss_payments_code& spc) -> qr_invoice
{
	using namespace swiss_payments_code_to_model_mapper;

	qr_invoice qr_invoice;

	map_header(spc, &qr_invoice);
	map_creditor_info(spc, &qr_invoice);
	map_ultimate_creditor(spc, &qr_invoice);
	map_payment_amount_info(spc, &qr_invoice);
	map_ultimate_debtor(spc, &qr_invoice);
	map_payment_reference(spc, &qr_invoice);
	map_alternative_schemes(spc, &qr_invoice);

	return qr_invoice;
}

namespace swiss_payments_code_to_model_mapper {

void map_header(const swiss_payments_code& spc, qr_invoice* qr_invoice)
{
	if (!qr_invoice)
		return;

	auto& hdr = qr_invoice->get_header();
	hdr.set_qr_type(spc.qr_type);

	try {
		hdr.set_version(string_util::details::parse<short>(spc.version));
	}
	catch (const parse_exception&) {
		throw parse_exception{std::string{"Unable to parse version from '"} + spc.version + "'"};
	}

	try {
		hdr.set_coding_type(string_util::details::parse<short>(spc.coding));
	}
	catch (const parse_exception&) {
		throw parse_exception{std::string{"Unable to parse coding from '"} + spc.coding + "'"};
	}
}

void map_creditor_info(const swiss_payments_code& spc, qr_invoice* qr_invoice)
{
	if (!qr_invoice)
		return;

	qr_invoice->get_creditor_info().set_iban(spc.iban);
	map_creditor(spc, qr_invoice);
}

void map_creditor(const swiss_payments_code& spc, qr_invoice* qr_invoice)
{
	if (!qr_invoice)
		return;

	auto& addr = qr_invoice->get_creditor_info().get_creditor().get_address();
	if (!spc.cr_adr_tp.empty())
		addr.set_address_type(address_type::parse(spc.cr_adr_tp));

	addr.set_name(spc.cr_name);
	addr.set_country(spc.cr_ctry);
	switch (addr.get_address_type().get()) {
		case address_type::structured:
			addr.set_street_name(spc.cr_strt_nm_or_adr_line1);
			addr.set_house_number(spc.cr_bldg_nb_or_adr_line2);
			addr.set_postal_code(spc.cr_pstd_cd);
			addr.set_city(spc.cr_twn_nm);
			break;
		case address_type::combined:
			addr.set_address_line1(spc.cr_strt_nm_or_adr_line1);
			addr.set_address_line2(spc.cr_bldg_nb_or_adr_line2);
			break;
		case address_type::unset:
			break;
	}
}

void map_ultimate_creditor(const swiss_payments_code& spc, qr_invoice* qr_invoice)
{
	if (!qr_invoice)
		return;

	auto& addr = qr_invoice->get_ultimate_creditor().get_address();
	
	if (!spc.ucr_adr_tp.empty())
		addr.set_address_type(address_type::parse(spc.ucr_adr_tp));

	addr.set_name(spc.ucr_name);
	addr.set_country(spc.ucr_ctry);
	switch (addr.get_address_type().get()) {
		case address_type::structured:
			addr.set_street_name(spc.ucr_strt_nm_or_adr_line1);
			addr.set_house_number(spc.ucr_bldg_nb_or_adr_line2);
			addr.set_postal_code(spc.ucr_pstd_cd);
			addr.set_city(spc.ucr_twn_nm);
			break;
		case address_type::combined:
			addr.set_address_line1(spc.ucr_strt_nm_or_adr_line1);
			addr.set_address_line2(spc.ucr_bldg_nb_or_adr_line2);
			break;
		case address_type::unset:
			break;
	}
}

void map_payment_amount_info(const swiss_payments_code& spc, qr_invoice* qr_invoice)
{
	if (!qr_invoice)
		return;

	auto& payment_amt_info = qr_invoice->get_payment_amount_info();
	if (!spc.amt.empty())
		try {
			payment_amt_info.set_amount(string_util::details::parse<double>(spc.amt));
		} catch(const parse_exception&) {
			throw parse_exception{std::string{"Unable to parse amount from '"} + spc.amt + "'"};
		}
	else
		payment_amt_info.unset_amount();

	payment_amt_info.set_currency(currency_ops::parse_currency(spc.ccy));
}

void map_ultimate_debtor(const swiss_payments_code& spc, qr_invoice* qr_invoice)
{
	if (!qr_invoice)
		return;

	auto& addr = qr_invoice->get_ultimate_debtor().get_address();
	if (!spc.ud_adr_tp.empty())
		addr.set_address_type(address_type::parse(spc.ud_adr_tp));

	addr.set_name(spc.ud_name);
	addr.set_country(spc.ud_ctry);
	switch (addr.get_address_type().get()) {
		case address_type::structured:
			addr.set_street_name(spc.ud_strt_nm_or_adr_line1);
			addr.set_house_number(spc.ud_bldg_nb_or_adr_line2);
			addr.set_postal_code(spc.ud_pstd_cd);
			addr.set_city(spc.ud_twn_nm);
			break;
		case address_type::combined:
			addr.set_address_line1(spc.ud_strt_nm_or_adr_line1);
			addr.set_address_line2(spc.ud_bldg_nb_or_adr_line2);
			break;
		case address_type::unset:
			break;
	}
}

void map_payment_reference(const swiss_payments_code& spc, qr_invoice* qr_invoice)
{
	if (!qr_invoice)
		return;

	if (!spc.tp.empty())
		qr_invoice->get_payment_reference().set_reference_type(reference_type::parse(spc.tp));

	qr_invoice->get_payment_reference().set_reference(spc.ref);

	map_additional_information(spc, qr_invoice);
}

void map_additional_information(const swiss_payments_code& spc, qr_invoice* qr_invoice)
{
	if (!qr_invoice)
		return;

	auto& additional_information = qr_invoice->get_payment_reference().get_additional_information();

	additional_information.set_unstructured_message(spc.ustrd);
	additional_information.set_trailer(spc.trailer);
	additional_information.set_bill_information(spc.strd_bkg_inf);
}

void map_alternative_schemes(const swiss_payments_code& spc, qr_invoice* qr_invoice)
{
	if (!qr_invoice)
		return;

	qr_invoice->set_alternative_schemes(alternative_schemes{ spc.alt_pmts });
}

} // namespace swiss_payments_code_to_model_mapper


} // namespace model
} // namespace qrinvoice

