
#include <string>
#include <sstream>
#include <iomanip>
#include <qrinvoice/model/mapping_exception.hpp>
#include <qrinvoice/model/header.hpp>
#include <qrinvoice/model/creditor_info.hpp>
#include <qrinvoice/model/ultimate_creditor.hpp>
#include <qrinvoice/model/payment_amount_info.hpp>
#include <qrinvoice/model/ultimate_debtor.hpp>
#include <qrinvoice/model/payment_reference.hpp>
#include <qrinvoice/model/alternative_schemes.hpp>
#include <qrinvoice/model/qr_invoice.hpp>
#include <qrinvoice/model/swiss_payments_code.hpp>
#include <qrinvoice/model/mapper/model_to_swiss_payments_code.hpp>
#include <qrinvoice/currency.hpp>

#include <qr_invoice_swiss_qr_code_internal.hpp>
#include <model/mapper/model_to_swiss_payments_code_internal.hpp>
#include <util/amount_util_internal.hpp>


namespace qrinvoice {
namespace model {


auto map_model_to_swiss_payments_code(const qr_invoice& qr_invoice) -> swiss_payments_code
{
	using namespace model_to_swiss_payments_code_mapper;

	swiss_payments_code spc;

	map_header(qr_invoice.get_header(), &spc);
	map_creditor_info(qr_invoice.get_creditor_info(), &spc);
	map_ultimate_creditor(qr_invoice.get_ultimate_creditor(), &spc);
	map_payment_amount_info(qr_invoice.get_payment_amount_info(), &spc);
	map_ultimate_debtor(qr_invoice.get_ultimate_debtor(), &spc);
	map_payment_reference(qr_invoice.get_payment_reference(), &spc);
	map_alternative_schemes(qr_invoice.get_alternative_schemes(), &spc);

	return spc;
}


namespace model_to_swiss_payments_code_mapper {

void map_header(const header& header, swiss_payments_code* spc)
{
	if (!spc)
		return;

	if (header.empty())
		throw mapping_exception{"header must be given"};

	spc->qr_type = header.get_qr_type();

	std::stringstream os;
	os << std::setfill('0') << std::setw(4) << header.get_version();
	spc->version = os.str();

	spc->coding = std::to_string(header.get_coding_type());
}

void map_creditor_info(const creditor_info& creditor_info, swiss_payments_code* spc)
{
	if (!spc)
		return;

	if (creditor_info.empty())
		throw mapping_exception{"creditor_info must be given"};

	spc->iban = creditor_info.get_iban();
	map_creditor(creditor_info.get_creditor(), spc);
}

void map_creditor(const creditor& creditor, swiss_payments_code* spc)
{
	if (!spc)
		return;

	if (creditor.empty())
		throw mapping_exception{"creditor must be given"};

	spc->cr_adr_tp = creditor.get_address().get_address_type().get_address_type_code();
	spc->cr_name = creditor.get_address().get_name();
	spc->cr_ctry = creditor.get_address().get_country();

	switch (creditor.get_address().get_address_type().get()) {
		case address_type::structured:
			spc->cr_strt_nm_or_adr_line1 = creditor.get_address().get_street_name();
			spc->cr_bldg_nb_or_adr_line2 = creditor.get_address().get_house_number();
			spc->cr_pstd_cd = creditor.get_address().get_postal_code();
			spc->cr_twn_nm = creditor.get_address().get_city();
			break;
		case address_type::combined:
			spc->cr_strt_nm_or_adr_line1 = creditor.get_address().get_address_line1();
			spc->cr_bldg_nb_or_adr_line2 = creditor.get_address().get_address_line2();
			break;
		case address_type::unset:
			break;
	}
}

void map_ultimate_creditor(const ultimate_creditor& u_creditor, swiss_payments_code* spc)
{
	if (!spc)
		return;

	if (u_creditor.empty())
		return;			// optional
	
	spc->ucr_adr_tp = u_creditor.get_address().get_address_type().get_address_type_code();
	spc->ucr_name = u_creditor.get_address().get_name();
	spc->ucr_ctry = u_creditor.get_address().get_country();

	switch (u_creditor.get_address().get_address_type().get()) {
		case address_type::structured:
			spc->ucr_strt_nm_or_adr_line1 = u_creditor.get_address().get_street_name();
			spc->ucr_bldg_nb_or_adr_line2 = u_creditor.get_address().get_house_number();
			spc->ucr_pstd_cd = u_creditor.get_address().get_postal_code();
			spc->ucr_twn_nm = u_creditor.get_address().get_city();
			break;
		case address_type::combined:
			spc->ucr_strt_nm_or_adr_line1 = u_creditor.get_address().get_address_line1();
			spc->ucr_bldg_nb_or_adr_line2 = u_creditor.get_address().get_address_line2();
			break;
		case address_type::unset:
			break;
	}
}

void map_payment_amount_info(const payment_amount_info& payment_amount_info, swiss_payments_code* spc)
{
	if (!spc)
		return;

	if (payment_amount_info.empty())
		throw mapping_exception{"payment_amount_info must be given"};

	if (payment_amount_info.is_amount_set())
		spc->amt = amount_util::format_amount_for_qrcode(payment_amount_info.get_amount());
	if (payment_amount_info.is_currency_set())
		spc->ccy = currency_ops::get_currency_code(payment_amount_info.get_currency());
}

void map_ultimate_debtor(const ultimate_debtor& u_debtor, swiss_payments_code* spc)
{
	if (!spc)
		return;

	if (u_debtor.empty())
		return;			// optional

	spc->ud_adr_tp = u_debtor.get_address().get_address_type().get_address_type_code();
	spc->ud_name = u_debtor.get_address().get_name();
	spc->ud_ctry = u_debtor.get_address().get_country();

	switch (u_debtor.get_address().get_address_type().get()) {
		case address_type::structured:
			spc->ud_strt_nm_or_adr_line1 = u_debtor.get_address().get_street_name();
			spc->ud_bldg_nb_or_adr_line2 = u_debtor.get_address().get_house_number();
			spc->ud_pstd_cd = u_debtor.get_address().get_postal_code();
			spc->ud_twn_nm = u_debtor.get_address().get_city();
			break;
		case address_type::combined:
			spc->ud_strt_nm_or_adr_line1 = u_debtor.get_address().get_address_line1();
			spc->ud_bldg_nb_or_adr_line2 = u_debtor.get_address().get_address_line2();
			break;
		case address_type::unset:
			break;
	}
}

void map_payment_reference(const payment_reference& payment_reference, swiss_payments_code* spc)
{
	if (!spc)
		return;

	if (payment_reference.empty())
		throw mapping_exception{"payment_reference must be given"};

	spc->tp = payment_reference.get_reference_type().get_reference_type_code();
	spc->ref = payment_reference.get_reference();

	map_additional_information(payment_reference.get_additional_information(), spc);
}

void map_additional_information(const additional_information& additional_information, swiss_payments_code* spc)
{
	if (!spc)
		return;

	spc->trailer = qrinvoice::model::spc::end_payment_data_trailer;

	if (!additional_information.empty()) {

		spc->ustrd = additional_information.get_unstructured_message();
	
		spc->strd_bkg_inf = additional_information.get_bill_information();
	}
}

void map_alternative_schemes(const alternative_schemes& alt_schemes, swiss_payments_code* spc)
{
	if (!spc)
		return;

	if (alt_schemes.empty())
		return;			// optional

	spc->alt_pmts = alt_schemes.get_alternative_scheme_params();
}

} // namespace model_to_swiss_payments_code_mapper

} // namespace model
} // namespace qrinvoice

