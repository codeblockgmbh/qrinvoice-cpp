
#ifndef QR_INVOICE_SWISS_PAYMENTS_CODE_TO_MODEL_INTERNAL_HPP
#define QR_INVOICE_SWISS_PAYMENTS_CODE_TO_MODEL_INTERNAL_HPP

#include <qrinvoice/model/qr_invoice.hpp>
#include <qrinvoice/model/swiss_payments_code.hpp>


namespace qrinvoice {
namespace model {
namespace swiss_payments_code_to_model_mapper {


void map_header(const swiss_payments_code& spc, qr_invoice* qr_invoice);
void map_creditor_info(const swiss_payments_code& spc, qr_invoice* qr_invoice);
void map_creditor(const swiss_payments_code& spc, qr_invoice* qr_invoice);
void map_ultimate_creditor(const swiss_payments_code& spc, qr_invoice* qr_invoice);
void map_payment_amount_info(const swiss_payments_code& spc, qr_invoice* qr_invoice);
void map_ultimate_debtor(const swiss_payments_code& spc, qr_invoice* qr_invoice);
void map_payment_reference(const swiss_payments_code& spc, qr_invoice* qr_invoice);
void map_additional_information(const swiss_payments_code& spc, qr_invoice* qr_invoice);
void map_alternative_schemes(const swiss_payments_code& spc, qr_invoice* qr_invoice);


} // namespace swiss_payments_code_to_model_mapper
} // namespace model
} // namespace qrinvoice


#endif // QR_INVOICE_SWISS_PAYMENTS_CODE_TO_MODEL_INTERNAL_HPP

