
#ifndef QR_INVOICE_MODEL_TO_SWISS_PAYMENTS_CODE_INTERNAL_HPP
#define QR_INVOICE_MODEL_TO_SWISS_PAYMENTS_CODE_INTERNAL_HPP

#include <qrinvoice/model/qr_invoice.hpp>
#include <qrinvoice/model/header.hpp>
#include <qrinvoice/model/creditor_info.hpp>
#include <qrinvoice/model/ultimate_creditor.hpp>
#include <qrinvoice/model/payment_amount_info.hpp>
#include <qrinvoice/model/ultimate_debtor.hpp>
#include <qrinvoice/model/payment_reference.hpp>
#include <qrinvoice/model/additional_information.hpp>
#include <qrinvoice/model/alternative_schemes.hpp>
#include <qrinvoice/model/swiss_payments_code.hpp>


namespace qrinvoice {
namespace model {
namespace model_to_swiss_payments_code_mapper {

void map_header(const header& header, swiss_payments_code* spc);
void map_creditor_info(const creditor_info& creditor_info, swiss_payments_code* spc);
void map_creditor(const creditor& creditor, swiss_payments_code* spc);
void map_ultimate_creditor(const ultimate_creditor& u_creditor, swiss_payments_code* spc);
void map_payment_amount_info(const payment_amount_info& payment_amount_info, swiss_payments_code* spc);
void map_ultimate_debtor(const ultimate_debtor& u_debtor, swiss_payments_code* spc);
void map_payment_reference(const payment_reference& payment_reference, swiss_payments_code* spc);
void map_additional_information(const additional_information& additional_information, swiss_payments_code* spc);
void map_alternative_schemes(const alternative_schemes& alt_schemes, swiss_payments_code* spc);


} // namespace model_to_swiss_payments_code_mapper
} // namespace model
} // namespace qrinvoice

#endif // QR_INVOICE_MODEL_TO_SWISS_PAYMENTS_CODE_INTERNAL_HPP

