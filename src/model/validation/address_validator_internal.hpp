
#ifndef QR_INVOICE_ADDRESS_VALIDATOR_INTERNAL_HPP
#define QR_INVOICE_ADDRESS_VALIDATOR_INTERNAL_HPP

#include <qrinvoice/model/creditor.hpp>
#include <qrinvoice/model/ultimate_creditor.hpp>
#include <qrinvoice/model/ultimate_debtor.hpp>
#include <qrinvoice/model/address.hpp>
#include <qrinvoice/model/validation/validation_result.hpp>


namespace qrinvoice {
namespace model {
namespace validation {

void validate_address(const std::string& base_data_path, const address& address, bool mandatory_group, validation_result& result);

inline void validate(const creditor& creditor, validation_result& result)
{
	validate_address("creditor_info.creditor", creditor.get_address(), true, result);
}

inline void validate(const ultimate_creditor& u_creditor, validation_result& result)
{
	validate_address("ultimate_creditor", u_creditor.get_address(), false, result);
}

inline void validate(const ultimate_debtor& u_debtor, validation_result& result)
{
	validate_address("ultimate_debtor", u_debtor.get_address(), false, result);
}

void validate_structured_address(const std::string& base_data_path, const address& address, validation_result& result);

void validate_combined_address(const std::string& base_data_path, const address& address, validation_result& result);


} // namespace validation
} // namespace model
} // namespace qrinvoice


#endif // QR_INVOICE_ADDRESS_VALIDATOR_INTERNAL_HPP

