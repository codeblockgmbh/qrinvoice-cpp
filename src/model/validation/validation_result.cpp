
#include <string>
#include <sstream>

#include <qrinvoice/model/validation/validation_result.hpp>

#include <util/string_util_internal.hpp>
#include <model/validation/validation_error_messages_internal.hpp>
#include <system/common.hpp>
#include <boost/regex.hpp>


namespace qrinvoice {
namespace model {
namespace validation {

const boost::regex line_break_pattern_{R"(\r\n|\n)"};

std::string validation_result::get_validation_error_summary() const
{
	if (!has_errors())
		return "No validation errors";

	std::ostringstream os{"qr_invoice has validation errors:"};
	os << system::new_line;

	// only english error messages
	const auto &error_map = validation::get_error_map();
	int error_nr = 1;
	for (const validation_error& error : errors_) {
		if (error_nr > 1)
			os << system::new_line;

		os << error_nr++ << ". '" << error.get_data_path() << '\''
			<< " has invalid value '" << error.get_value() << '\'';

		for (const auto& err_msg_key : error.get_error_msg_keys()) {
			auto it = error_map.find(err_msg_key);
			if (it == error_map.end())
				continue;

			for (const auto& s : string_util::split_tokens(it->second, line_break_pattern_))
				os << system::new_line << "    => " << s;
		}
	}

	return os.str();
}


} // namespace validation
} // namespace model
} // namespace qrinvoice


