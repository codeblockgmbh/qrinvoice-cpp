
#include <string>
#include <unordered_map>

#include <model/validation/validation_error_messages_internal.hpp>


namespace qrinvoice {
namespace model {
namespace validation {


//
// Mapping of error-message-keys to validation error messages
//
auto initialize_error_map() noexcept -> const std::unordered_map<std::string, std::string>
{
	return {
	{
		"validation.error.invalid_characters",

		u8"Invalid characters found, only latin characters are permitted\n"
		u8"According to the Swiss standard, in the Swiss QR Code, for reasons of compatibility with the Swiss Implementation Guidelines for Credit Transfers for the ISO 20022 \"Customer Credit Transfer Initiation\" message (pain.001), only the Latin character set is permitted.\n"
		u8"For a list of all valid characters, please see qrinvoice::model::spc::valid_characters"
	},

	{
		"validation.error.untrimmed_input",

		u8"The given string starts or ends with one or more whitespaces. Unnecessary whitespaces should be ommitted."
	},

	{
		"validation.error.coding_type",

		u8"One-digit, numeric\n"
		u8"Character set code. Fixed value 1 (indicates Latin character set)"
	},

	{
		"validation.error.version",

		u8"Fixed length: four-digit, numeric\n"
		u8"Contains version of the specifications (Implementation Guidelines) in use on the date on which the Swiss QR Code was created.\n"
		u8"The first two positions indicate the main version, the following two positions the sub-version. Fixed value of \"0200\" for Version 2.0\n"
		u8"QR Invoice Library in use supports version 2.xy (= 02xy)"
	},

	{
		"validation.error.qr_type",

		u8"Fixed length: three-digit, alphanumeric\n"
		u8"Unambiguous indicator for the Swiss QR Code. Fixed value \"SPC\" (Swiss Payments Code)"
	},

	{
		"validation.error.iban",

		u8"Fixed length: 21 alphanumeric characters\n"
		u8"only IBANs with CH or LI country code permitted."
	},

	{
		"validation.error.amount",

		u8"Maximum 12-digits permitted, including decimal separators\n"
		u8"Optional amount must be 0.00 <= amount <= 999999999.99"
	},

	{
		"validation.error.currency",

		u8"Only CHF and EUR are permitted."
	},

	{
		"validation.error.address.group",

		u8"Mandatory data group"
	},

	{
		"validation.error.address.address_type",

		u8"Address type must be set"
	},

    {
        "validation.error.address.addressType.combined",

        u8"Combined address type cannot be used with the specifications (Implementation Guidelines) version 2.3 or later"
    },

	{
		"validation.error.address.name",

		u8"First name (optional, if available) + last name or company name\n"
		u8"Maximum 70 characters permitted"
	},

	{
		"validation.error.address.street_name",

		u8"Maximum 70 characters permitted\n"
		u8"may not include any house or building number"
	},

	{
		"validation.error.address.house_number",

		u8"Maximum 16 characters permitted"
	},

	{
		"validation.error.address.postal_code",

		u8"Maximum 16 characters permitted\n"
		u8"The postal code is always to be entered without a country code prefix"
	},

	{
		"validation.error.address.city",

		u8"Maximum 35 characters permitted"
	},

	{
		"validation.error.address.address_line1",

		u8"Maximum 70 characters permitted"
	},

	{
		"validation.error.address.address_line2",

		u8"Maximum 70 characters permitted"
	},

	{
		"validation.error.address.country",

		u8"Two-digit country code according to ISO 3166-1"
	},
	
	{
		"validation.error.address.structured.addresse_lines",

		u8"Must not be provided when address type \"structured\" is used"
	},
	
	{
		"validation.error.address.combined.city",

		u8"Must not be provided when address type \"combined\" is used"
	},
	
	{
		"validation.error.address.combined.postal_code",

		u8"Must not be provided when address type \"combined\" is used"
	},
	{
		"validation.error.address.combined.house_number",

		u8"Must not be provided when address type \"combined\" is used"
	},
	{
		"validation.error.address.combined.street_name",

		u8"Must not be provided when address type \"combined\" is used"
	},

	{
		"validation.error.reference_type",

		u8"Maximum four characters, alphanumeric\n"
		u8"The following codes are permitted: QRR, SCO, NON"
	},

	{
		"validation.error.reference_type.qr_iban",

		u8"with the use of a QR-IBAN the corresponding reference type must be QRR (QR-Reference)"
	},

	{
		"validation.error.reference_type.iban",

		u8"with the use of a regular IBAN (not a QR-IBAN) the corresponding reference type must not be QRR (QR-Reference)"
	},

	{
		"validation.error.reference",

		u8"Maximum 27 characters, alphanumeric"
	},

	{
		"validation.error.reference.qrr",

		u8"Must be filled if a QR-IBAN is used.\n"
		u8"QR reference: 27 characters, numeric, check sum calculation according to Modulo 10 recursive (27th position of the reference)"
	},

	{
		"validation.error.reference.scor",

		u8"Creditor Reference (ISO 11649): up to 25 characters, alphanumeric.\n"
		u8"Must start with 'RF' followed by two check digits"
	},

	{
		"validation.error.reference.non",

		u8"The element may not be filled for the NON reference type."
	},

	{
		"validation.error.payment_reference.additional_information.unstructured_message",

		u8"Maximum 140 characters"
	},

	{
		"validation.error.payment_reference.additional_information.bill_information",

		u8"Maximum 140 characters"
	},

	{
		"validation.error.payment_reference.additional_information.bill_information.start_pattern",

		u8"Billing information must start with \"//\" after the slashes, a two-char abbreviation has to be used"
	},

	{
		"validation.error.payment_reference.additional_information.common_total",

		u8"Unstructured message and Booking instructions (bill information) may contain a common total of up to 140 characters"
	},

	{
		"validation.error.payment_reference.additional_information.trailer",

		u8"Fixed length: three-digit, alphanumeric\n"
  		u8"Unambiguous indicator for the end of payment data. Fixed value 'EPD' (End Payment Data)."
	},

	{
		"validation.error.alternative_schemes.alternative_scheme_parameter.length",

		u8"Maximum 100 character"
	},

	{
		"validation.error.alternative_schemes.alternative_scheme_parameters.size",

		u8"Alternative scheme parameters - Can be currently delivered a maximum of two times."
	},
	
	{
		"validation.error.ultimate_creditor.must_not_be_used",

		u8"This group must not be filled in at present, because it is intended for future use."
	},


	// Notification - DO NOT USE FOR PAYMENT
	{
			"validation.error.do_not_use_for_payment.non_zero_amount",

			u8"For notifications an amount of zero (0.00) has to be set. Encountered a non-zero amount"
	},

	{
			"validation.error.do_not_use_for_payment.non_empty_unstructured_message",

			u8"For notifications with amount zero (0.00) no other unstructured messages than \"DO NOT USE FOR PAYMENT\" (or its german, french or italian equivalent) must be used"
	}

	};	// map initializer ends here
}

auto get_error_map() noexcept -> const std::unordered_map<std::string, std::string>&
{
	static const auto error_map = initialize_error_map();
	return error_map;
}


} // namespace validation
} // namespace model
} // namespace qrinvoice

