
#ifndef QR_INVOICE_VALIDATION_UTIL_INTERNAL_HPP
#define QR_INVOICE_VALIDATION_UTIL_INTERNAL_HPP

#include <string>
#include <functional>
#include <qrinvoice/model/swiss_payments_code.hpp>
#include <qrinvoice/model/validation/validation_util.hpp>
#include <qrinvoice/util/string_util.hpp>
#include <util/string_util_internal.hpp>
#include <boost/regex.hpp>

namespace qrinvoice {
namespace model {
namespace validation {
namespace util {

// Char sequence

inline bool validate_length(const std::string& str, int min, int max, bool optional)
{
	// empty string is considered as 'not provided'. also check if min > 0
	if (str.empty() && min > 0)
		return optional;

	auto sz = string_util::utf8_length(str);
	return min <= sz && sz <= max;
}

inline void validate_length(const std::string& str, int min, int max, bool optional,
							const std::function<void(const std::string&)> &validation_error_callback)
{
	if (!util::validate_length(str, min, max, optional))
		validation_error_callback(str);
}

inline void validate_length(const std::string& str, int min, int max,
							const std::function<void(const std::string&)> &validation_error_callback)
{
	validate_length(str, min, max, false, validation_error_callback);
}

inline void validate_optional_length(const std::string& str, int min, int max,
									 const std::function<void(const std::string&)> &validation_error_callback)
{
	validate_length(str, min, max, true, validation_error_callback);
}

inline void validate_empty(const std::string& value, const std::function<void(const std::string&)> &validation_error_callback)
{
	if (!value.empty())
		validation_error_callback(value);
}

void validate_trimmed(const std::string& value,
					  const std::function<void(const std::string&, const std::string&)> &validation_error_callback);

inline void validate_characters(const std::string& value,
								const std::function<void(const std::string&, const std::string&)> &validation_error_callback)
{
	if (!value.empty() && !is_valid_string(value))
		validation_error_callback(value, "validation.error.invalid_characters");
}

inline void validate_string(const std::string& value,
							const std::function<void(const std::string&, const std::string&)> &validation_error_callback)
{
	validate_trimmed(value, validation_error_callback);
	validate_characters(value, validation_error_callback);
}

// Number range

inline bool validate_range(double number, double min, double max)
{
	return min <= number && number <= max;
}

inline void validate_range(double number, double min, double max,
						   const std::function<void(double)> &validation_error_callback)
{
	if (!util::validate_range(number, min, max))
			validation_error_callback(number);
}

static const boost::regex valid_characters_pattern {R"(([a-zA-Z0-9\.,;:'\+\-/\(\)?\*\[\]\{\}\\`´~ ]|[!\"#%&<>÷=@_$£€| ¡¢£¤¥¦§¨©ª«¬­®¯°±²³´µ¶·¸¹º»¼½¾¿^]|[ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõö÷øùúûüýþÿĀāĂăĄąĆćĈĉĊċČčĎďĐđĒēĔĕĖėĘęĚěĜĝĞğĠġĢģĤĥĦħĨĩĪīĬĭĮįİıĲĳĴĵĶķĸĹĺĻļĽľĿŀŁłŃńŅņŇňŉŊŋŌōŎŏŐőŒœŔŕŖŗŘřŚśŜŝŞşŠšŢţŤťŦŧŨũŪūŬŭŮůŰűŲųŴŵŶŷŸŹźŻżŽžȘșȚț])*)"};

inline bool is_valid_string(const std::string& str)
{
    return boost::regex_match(str, valid_characters_pattern);
}

// TODO:: improve validate_true's callback argument if possible
// std::function taking template type as argument, doesn't work with lambdas

//template <typename T> inline void validate_true(const T& value, bool valid,
//				const std::function<void(const T&)> &validation_error_callback)
//{
//	if (!valid)
//		validation_error_callback(value);
//}

template <typename T, typename F>
	// requires F of type:  `void(const T&)`
inline void validate_true(const T& value, bool valid, F validation_error_callback)
{
	if (!valid)
		validation_error_callback(value);
}

} // namespace util
} // namespace validation
} // namespace model
} // namespace qrinvoice

#endif // QR_INVOICE_VALIDATION_UTIL_INTERNAL_HPP

