
#include <string>
#include <functional>

#include <qrinvoice/util/string_util.hpp>

#include <model/validation/validation_util_internal.hpp>
#include <config/env_vars_internal.hpp>

#include <spdlog/spdlog.h>
#include <system/i18n/translator.hpp>

namespace qrinvoice {
namespace model {
namespace validation {
namespace util {

const system::i18n::translator tr_ {locale::english};

void validate_trimmed(const std::string& value,
		const std::function<void(const std::string&, const std::string&)> &validation_error_callback)
{
	if (!value.empty() && string_util::details::is_trimmable(value)) {
		if (env::is_true(env::strict_validation)) {
			validation_error_callback(value, "validation.error.untrimmed_input");
		} else {
			spdlog::warn("Input '{}' should be trimmed. {}", value, tr_.translate("validation.error.untrimmed_input"));
		}
	}
}

} // namespace util
} // namespace validation
} // namespace model
} // namespace qrinvoice

