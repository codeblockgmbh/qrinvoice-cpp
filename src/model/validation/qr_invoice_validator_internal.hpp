
#ifndef QR_INVOICE_QR_INVOICE_VALIDATOR_INTERNAL_HPP
#define QR_INVOICE_QR_INVOICE_VALIDATOR_INTERNAL_HPP

#include <qrinvoice/model/header.hpp>
#include <qrinvoice/model/creditor_info.hpp>
#include <qrinvoice/model/payment_amount_info.hpp>
#include <qrinvoice/model/payment_reference.hpp>
#include <qrinvoice/model/additional_information.hpp>
#include <qrinvoice/model/alternative_schemes.hpp>
#include <qrinvoice/model/validation/validation_result.hpp>


namespace qrinvoice {
namespace model {
namespace validation {


void validate(const header& header, validation_result& result);
void validate(const creditor_info& creditor_info, validation_result& result);
void validate(const payment_amount_info& payment_amount_info, validation_result& result);
void validate(const payment_reference& payment_reference, validation_result& result);
void validate(const additional_information& additional_information, validation_result& result);
void validate(const alternative_schemes& alt_schemes, validation_result& result);


} // namespace validation
} // namespace model
} // namespace qrinvoice


#endif // QR_INVOICE_QR_INVOICE_VALIDATOR_INTERNAL_HPP

