
#ifndef QR_INVOICE_VALIDATION_ERROR_MESSAGES_INTERNAL_HPP
#define QR_INVOICE_VALIDATION_ERROR_MESSAGES_INTERNAL_HPP

#include <string>
#include <unordered_map>


namespace qrinvoice {
namespace model {
namespace validation {

auto get_error_map() noexcept -> const std::unordered_map<std::string, std::string>&;


} // namespace validation
} // namespace model
} // namespace qrinvoice

#endif // QR_INVOICE_VALIDATION_ERROR_MESSAGES_INTERNAL_HPP

