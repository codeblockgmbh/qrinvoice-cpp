
#include <string>
#include <boost/regex.hpp>
#include <qrinvoice/currency.hpp>
#include <qrinvoice/model/validation/qr_invoice_validator.hpp>
#include <qrinvoice/model/validation/validation_result.hpp>
#include <qrinvoice/model/qr_invoice.hpp>
#include <qrinvoice/util/string_util.hpp>
#include <qrinvoice/not_yet_implemented_exception.hpp>

#include <qr_invoice_swiss_qr_code_internal.hpp>
#include <config/env_vars_internal.hpp>
#include <model/validation/qr_invoice_validator_internal.hpp>
#include <model/validation/address_validator_internal.hpp>
#include <model/validation/validation_util_internal.hpp>
#include <qrinvoice/util/creditor_reference_util.hpp>
#include <util/iban_util_internal.hpp>
#include <qrinvoice/util/qr_reference_util.hpp>
#include <spdlog/spdlog.h>

namespace qrinvoice {
namespace model {
namespace validation {

static const boost::regex bill_information_pattern{"^//[^/]{2,2}(/.*)?$"};

void validate_iban_reference(const qr_invoice& qr_invoice, validation_result& result);
void validate_do_not_use_for_payment(const qr_invoice& qr_invoice, validation_result& result);

validation_result validate(const qr_invoice& qr_invoice)
{
	spdlog::trace("validate - start");
	validation_result validation_result;

	validate(qr_invoice.get_header(), validation_result);
	validate(qr_invoice.get_creditor_info(), validation_result);
	validate(qr_invoice.get_ultimate_creditor(), validation_result);
	if (!qr_invoice.get_ultimate_creditor().get_address().empty() && !env::is_true(env::unlock_ultimate_creditor)) {
		validation_result.add_error("ultimate_creditor", "", "", {"validation.error.ultimate_creditor.must_not_be_used"});
	}

	validate(qr_invoice.get_ultimate_debtor(), validation_result);
	validate(qr_invoice.get_payment_amount_info(), validation_result);
	validate(qr_invoice.get_payment_reference(), validation_result);
	validate(qr_invoice.get_alternative_schemes(), validation_result);

	validate_iban_reference(qr_invoice, validation_result);
	validate_do_not_use_for_payment(qr_invoice, validation_result);

	spdlog::trace("validate - end");
	return validation_result;
}

void validate(const header& header, validation_result& result)
{
	using namespace util;

	const std::string& qr_type = header.get_qr_type();
	validate_length(qr_type, 3, 3, [&](const std::string& value) {
		result.add_error("header", "qr_type", qr_type, {"validation.error.qr_type"});
	});

	validate_true(qr_type, qr_type == qrinvoice::model::spc::qr_type, [&](const std::string& value) {
		result.add_error("header", "qr_type", qr_type, {"validation.error.qr_type"});
	});

	const short coding_type = header.get_coding_type();
	validate_range(static_cast<int>(coding_type), 1, 9, [&](double value) {
		result.add_error("header", "coding_type", coding_type, {"validation.error.coding_type"});
	});

	validate_true(coding_type, coding_type == qrinvoice::model::spc::coding_type, [&](const short& value) {
		result.add_error("header", "coding_type", coding_type, {"validation.error.coding_type"});
	});

	const short version = header.get_version();
	// length is 4, but can have leading zeros ("0200" equals 2.00)
	validate_range(static_cast<int>(version), 100, 9999, [&](const double& value) {
		result.add_error("header", "version", version, {"validation.error.version"});
	});

	// QRIN-84: only validate major version (e.g. 2.xy -> 02xy)
    const auto knownMajorVersion = string_util::details::parse<short>(qrinvoice::model::spc::version);
    // e.g. 0200 <= version < 0300 (0200 + 100)
    validate_true(version, version >= knownMajorVersion && version < knownMajorVersion + 100, [&](const short& value) {
		result.add_error("header", "version", version, {"validation.error.version"});
	});
}

void validate(const creditor_info& creditor_info, validation_result& result)
{
	using namespace validation::util;

	validate(creditor_info.get_creditor(), result);

	const std::string& iban = creditor_info.get_iban();
	validate_length(iban, 18, 21, [&](const std::string& value) {
		result.add_error("creditor_info", "iban", value, {"validation.error.iban"});
	});

	validate_true(iban, iban_util::is_valid_iban(iban, true), [&](const std::string& value) {
		result.add_error("creditor_info", "iban", value, {"validation.error.iban"});
	});
}

void validate(const payment_amount_info& payment_amount_info, validation_result& result)
{
	using namespace validation::util;

	if (payment_amount_info.is_amount_set()) {
		const double amount = payment_amount_info.get_amount();
		validate_range(amount, qrinvoice::model::spc::amount_min, qrinvoice::model::spc::amount_max, [&](double value) {
			result.add_error("payment_amount_info", "amount", amount, {"validation.error.amount"});
		});
	}

	const currency curr = payment_amount_info.get_currency();
	bool supported = std::find(qrinvoice::model::spc::supported_currencies.begin(), qrinvoice::model::spc::supported_currencies.end(), curr)
		!= qrinvoice::model::spc::supported_currencies.end();
	validate_true(curr, supported, [&](const currency& value) {
		result.add_error("payment_amount_info", "currency", value, {"validation.error.currency"});
	});
}

void validate(const payment_reference& payment_reference, validation_result& result)
{
	using namespace validation::util;

	const std::string& reference = payment_reference.get_reference();
	validate_string(reference, [&](const std::string value, const std::string& msg_key) {
		result.add_error("payment_reference", "reference", value, {msg_key});
	});

	const auto reference_type = payment_reference.get_reference_type();
	switch (reference_type.get()) {
	case reference_type::qr_reference:
		// QR reference: 27 characters, numeric, check sum calculation
		// according to Modulo 10 recursive (27th position of the reference)
		validate_true(reference, qr_reference_util::is_valid_qr_reference(reference), [&](const std::string& value) {
			result.add_error("payment_reference", "reference", value,
					{"validation.error.reference", "validation.error.reference.qrr"});
		});
		break;
	case reference_type::creditor_reference:
		// Creditor Reference (ISO 11649): up to 25 characters, alphanumeric
		validate_true(reference, creditor_reference_util::is_valid_creditor_reference(reference), [&](const std::string& value) {
			result.add_error("payment_reference", "reference", value,
					{"validation.error.reference", "validation.error.reference.scor"});
		});
		break;

	case reference_type::without_reference:
		// the element type may not be filled for the NON reference type
		validate_empty(reference, [&](const std::string& value) {
			result.add_error("payment_reference", "reference", value,
					{"validation.error.reference", "validation.error.reference.non"});
		});
		break;

	default:
		throw not_yet_implemented_exception{"reference_type '" + reference + "' is not yet implemented"};
	}

	validate(payment_reference.get_additional_information(), result);
}

void validate(const additional_information& additional_information, validation_result& result)
{
	using namespace validation::util;

	const auto& unstructured_msg = additional_information.get_unstructured_message();
	validate_optional_length(unstructured_msg, 0, 140, [&](const std::string& value) {
		result.add_error("payment_reference.additional_information", "unstructured_message", value,
				{"validation.error.payment_reference.additional_information.unstructured_message"});
	});

	validate_string(unstructured_msg, [&](const std::string& value, const std::string& msg_key) {
		result.add_error("payment_reference.additional_information", "unstructured_message", value, {msg_key});
	});

	const auto& trailer = additional_information.get_trailer();
	validate_length(trailer, 3, 3, [&](const std::string& value) {
		result.add_error("payment_reference.additional_information", "trailer", value, {"validation.error.paymentReference.additional_information.trailer"});
	});
	validate_true(trailer, trailer == "EPD", [&](const std::string& value) {
		result.add_error("payment_reference.additional_information", "trailer", value,
				{"validation.error.paymentReference.additional_information.trailer"});
	});

	const auto& bill_information = additional_information.get_bill_information();
	validate_optional_length(bill_information, 0, 140, [&](const std::string& value) {
		result.add_error("payment_reference.additional_information", "bill_information", value,
				{"validation.error.payment_reference.additional_information.bill_information"});
	});

	if (!env::is_true(env::disable_bill_information_validation))
	{
		if (!bill_information.empty() && !boost::regex_match(bill_information,bill_information_pattern))
		{
			result.add_error("payment_reference.additional_information", "bill_information", bill_information,
							 {"validation.error.payment_reference.additional_information.bill_information.start_pattern"});
		}
	}

	validate_string(bill_information, [&](const std::string& value, const std::string& msg_key) {
		result.add_error("payment_reference.additional_information", "bill_information", value, {msg_key});
	});

	const auto unstructured_msg_length = string_util::utf8_length(unstructured_msg);
	const auto bill_information_length = string_util::utf8_length(bill_information);
	const auto additional_information_total_length = unstructured_msg_length + bill_information_length;
	if(additional_information_total_length > 140) {
		result.add_error("payment_reference.additional_information", "bill_information", additional_information_total_length, {"validation.error.payment_reference.additional_information.common_total"});
	}
}

void validate(const alternative_schemes& alt_schemes, validation_result& result)
{
	using namespace validation::util;

	const auto size = alt_schemes.get_alternative_scheme_params().size();
	if (size > qrinvoice::model::spc::max_alt_pmt)
		result.add_error("alternative_schemes", "alternative_scheme_params", size,
				{"validation.error.alternative_schemes.alternative_scheme_params.size"});

	for (const auto& param : alt_schemes.get_alternative_scheme_params()) {
		validate_optional_length(param, 0, 100, [&](const std::string& value) {
			result.add_error("alternative_schemes", "alternative_scheme_params", value,
					{"validation.error.alternative_schemes.alternative_scheme_params.length"});
		});

		validate_string(param, [&](const std::string& value, const std::string& msg_key) {
			result.add_error("alternative_schemes", "alternative_scheme_params", value, {msg_key});
		});
	}
}

void validate_iban_reference(const qr_invoice& qr_invoice, validation_result& result)
{
	const std::string& iban = qr_invoice.get_creditor_info().get_iban();
	const reference_type reference_type = qr_invoice.get_payment_reference().get_reference_type();

	if (iban_util::is_qr_iban(iban)) {
		// with the use of a QR-IBAN a QR-Invoice must contain a QR-Reference
		if(reference_type.get() != reference_type::qr_reference) {
			result.add_error("payment_reference", "reference_type", reference_type, {"validation.error.reference_type.qr_iban"});
		}
	} else {
		if(reference_type.get() == reference_type::qr_reference) {
			// QRR must not be used with non QR-IBAN
			result.add_error("payment_reference", "reference_type", reference_type, {"validation.error.reference_type.iban"});
		}
	}
}

void validate_do_not_use_for_payment(const qr_invoice& qr_invoice, validation_result& result)
{
	if (!env::is_true(env::disable_do_not_use_for_payment_validation))
	{
		const bool amount_zero = qr_invoice.get_payment_amount_info().is_amount_set() && qr_invoice.get_payment_amount_info().get_amount() == 0.00;
		const auto& unstructured_msg = qr_invoice.get_payment_reference().get_additional_information().get_unstructured_message();

		const bool unstructured_msg_do_not_use_for_payment = unstructured_msg == "NICHT ZUR ZAHLUNG VERWENDEN"
															 || unstructured_msg == "NE PAS UTILISER POUR LE PAIEMENT"
															 || unstructured_msg == "DO NOT USE FOR PAYMENT"
															 || unstructured_msg == "NON UTILIZZARE PER IL PAGAMENTO";

		if(amount_zero && !unstructured_msg_do_not_use_for_payment) {
			result.add_error("payment_reference.additional_information", "unstructured_message", unstructured_msg, {"validation.error.do_not_use_for_payment.non_empty_unstructured_message"});
		} else if(!amount_zero && unstructured_msg_do_not_use_for_payment) {
			if(qr_invoice.get_payment_amount_info().is_amount_set()) {
				result.add_error("payment_amount_info", "amount", qr_invoice.get_payment_amount_info().get_amount(), {"validation.error.do_not_use_for_payment.non_zero_amount"});
			} else {
				result.add_error("payment_amount_info", "amount", "", {"validation.error.do_not_use_for_payment.non_zero_amount"});
			}
		}
	}
}

} // namespace validation
} // namespace model
} // namespace qrinvoice


