
#include <string>
#include <qrinvoice/model/address.hpp>
#include <qrinvoice/model/address_type.hpp>
#include <qrinvoice/model/validation/validation_result.hpp>

#include <model/validation/address_validator_internal.hpp>
#include <model/validation/validation_util_internal.hpp>
#include <qrinvoice/util/country_util.hpp>


namespace qrinvoice {
namespace model {
namespace validation {


void validate_address(const std::string& base_data_path, const address& address, bool mandatory_group, validation_result& result)
{
	if (address.empty()) {
		if (mandatory_group)
			result.add_error(base_data_path, "", "", {"validation.error.address.group"});
		return;
	}

	const qrinvoice::model::address_type& address_type = address.get_address_type();
	if(address_type.get() == address_type::unset) {
		result.add_error(base_data_path, "", "", {"validation.error.address.address_type"});
		return;
	}

	switch (address_type.get()) {
		case address_type::structured:
			validate_structured_address(base_data_path, address, result);
			return;
		case address_type::combined:
            result.add_error(base_data_path, "address_type", "combined", {"validation.error.address.addressType.combined"});
			return;
		case address_type::unset:
			result.add_error(base_data_path, "", "", {"validation.error.address.address_type"});
			return;
	}
}

void validate_structured_address(const std::string& base_data_path, const address& address, validation_result& result)
{
	using namespace util;
	// length
	validate_length(address.get_name(), 1, 70, [&](const std::string& value) {
		result.add_error(base_data_path, "name", value, {"validation.error.address.name"});
	});

	validate_optional_length(address.get_street_name(), 1, 70, [&](const std::string& value) {
		result.add_error(base_data_path, "street_name", value, {"validation.error.address.street_name"});
	});

	validate_optional_length(address.get_house_number(), 1, 16, [&](const std::string& value) {
		result.add_error(base_data_path, "house_number", value, {"validation.error.address.house_number"});
	});

	validate_length(address.get_postal_code(), 1, 16, [&](const std::string& value) {
		result.add_error(base_data_path, "postal_code", value, {"validation.error.address.postal_code"});
	});

	validate_length(address.get_city(), 1, 35, [&](const std::string& value) {
		result.add_error(base_data_path, "city", value, {"validation.error.address.city"});
	});

	validate_true(address.get_country(), country_util::is_valid_iso_code(address.get_country()),
		[&](const std::string& value) {
			result.add_error(base_data_path, "country", value, {"validation.error.address.country"});
		}
	);

	// characters
	validate_string(address.get_name(), [&](const std::string& value, const std::string& msg_key) {
		result.add_error(base_data_path, "name", value, {msg_key});
	});

	validate_string(address.get_street_name(), [&](const std::string& value, const std::string& msg_key) {
		result.add_error(base_data_path, "street_name", value, {msg_key});
	});

	validate_string(address.get_house_number(), [&](const std::string& value, const std::string& msg_key) {
		result.add_error(base_data_path, "house_number", value, {msg_key});
	});

	validate_string(address.get_postal_code(), [&](const std::string& value, const std::string& msg_key) {
		result.add_error(base_data_path, "postal_code", value, {msg_key});
	});

	validate_string(address.get_city(), [&](const std::string& value, const std::string& msg_key) {
		result.add_error(base_data_path, "city", value, {msg_key});
	});

	// check that addressLine 1 + 2 are not set
	validate_empty(address.get_address_line1(), [&](const std::string& value) {
		result.add_error(base_data_path, "address_line1", value, {"{{validation.error.address.structured.addresse_lines}}"});
	});
	validate_empty(address.get_address_line2(), [&](const std::string& value) {
		result.add_error(base_data_path, "address_line2", value, {"{{validation.error.address.structured.addresse_lines}}"});
	});
	
}

} // namespace validation
} // namespace model
} // namespace qrinvoice



