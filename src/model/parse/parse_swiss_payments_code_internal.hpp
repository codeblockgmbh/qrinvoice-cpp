
#ifndef QR_INVOICE_PARSE_SWISS_PAYMENTS_CODE_INTERNAL_HPP
#define QR_INVOICE_PARSE_SWISS_PAYMENTS_CODE_INTERNAL_HPP

#include <string>
#include <boost/regex.hpp>
#include <qrinvoice/model/swiss_payments_code.hpp>


namespace qrinvoice {
namespace model {


inline void read_token(boost::sregex_token_iterator& it, std::string& token)
{
	if (it != boost::sregex_token_iterator{})
		token = *it++;
}

void read_alt_pmts(boost::sregex_token_iterator& it, std::vector<std::string>& alt_pmts);


} // namespace model
} // namespac qrinvoice


#endif // QR_INVOICE_PARSE_SWISS_PAYMENTS_CODE_INTERNAL_HPP

