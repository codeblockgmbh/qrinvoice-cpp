
#include <vector>
#include <string>
#include <boost/regex.hpp>
#include <qrinvoice/model/parse_exception.hpp>
#include <qrinvoice/model/parse/parse_swiss_payments_code.hpp>

#include <qr_invoice_swiss_qr_code_internal.hpp>
#include <model/parse/parse_swiss_payments_code_internal.hpp>


namespace qrinvoice {
namespace model {

swiss_payments_code parse_swiss_payments_code(const std::string& str) try
{
	using namespace std;
	const boost::regex pattern{qrinvoice::model::spc::element_separator_pattern};

	boost::sregex_token_iterator it{str.begin(), str.end(), pattern, -1};

	swiss_payments_code spc;
	read_token(it, spc.qr_type);
	read_token(it, spc.version);
	read_token(it, spc.coding);

	read_token(it, spc.iban);

	read_token(it, spc.cr_adr_tp);
	read_token(it, spc.cr_name);
	read_token(it, spc.cr_strt_nm_or_adr_line1);
	read_token(it, spc.cr_bldg_nb_or_adr_line2);
	read_token(it, spc.cr_pstd_cd);
	read_token(it, spc.cr_twn_nm);
	read_token(it, spc.cr_ctry);

	read_token(it, spc.ucr_adr_tp);
	read_token(it, spc.ucr_name);
	read_token(it, spc.ucr_strt_nm_or_adr_line1);
	read_token(it, spc.ucr_bldg_nb_or_adr_line2);
	read_token(it, spc.ucr_pstd_cd);
	read_token(it, spc.ucr_twn_nm);
	read_token(it, spc.ucr_ctry);

	read_token(it, spc.amt);
	read_token(it, spc.ccy);

	read_token(it, spc.ud_adr_tp);
	read_token(it, spc.ud_name);
	read_token(it, spc.ud_strt_nm_or_adr_line1);
	read_token(it, spc.ud_bldg_nb_or_adr_line2);
	read_token(it, spc.ud_pstd_cd);
	read_token(it, spc.ud_twn_nm);
	read_token(it, spc.ud_ctry);

	read_token(it, spc.tp);
	read_token(it, spc.ref);

	read_token(it, spc.ustrd);
	read_token(it, spc.trailer);
	read_token(it, spc.strd_bkg_inf);

	read_alt_pmts(it, spc.alt_pmts);

	return spc;
} catch (const base_exception&) {
	throw parse_exception{"Unexpected exception occurred during Swiss Payments Code parsing"};
}

void read_alt_pmts(boost::sregex_token_iterator& it, std::vector<std::string>& alt_pmts)
{
	for (int cnt = 0; it != boost::sregex_token_iterator{}; ++it, ++cnt) {
		// for now, limit is 2 times alt_pmt, we read a bit more and then validate
		// but in order to be more robust in case a long input is
		// provided, we give a hard limit of 10 for the additional input
		if (cnt >= 10)
			break;

		alt_pmts.push_back(*it);
	}
}


} // namespace model
} // namespac qrinvoice

