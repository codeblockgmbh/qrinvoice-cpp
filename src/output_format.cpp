
#include <string>
#include <qrinvoice/output_format.hpp>
#include <qrinvoice/technical_exception.hpp>


namespace qrinvoice {
namespace output_format_ops {


output_format get_format_by_mime_type(const std::string& mime_type)
{
	if (mime_type == mime_pdf)
		return output_format::pdf;

	if (mime_type == mime_png)
		return output_format::png;

	if (mime_type == mime_jpg)
		return output_format::jpg;

	if (mime_type == mime_bmp)
		return output_format::bmp;

	throw technical_exception("invalid mime_type encountered");
}


} // namespace output_format_ops
} // namespace qrinvoice

