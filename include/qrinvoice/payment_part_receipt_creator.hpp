#include <utility>

#ifndef QR_INVOICE_PAYMENT_PART_RECEIPT_CREATOR_HPP
#define QR_INVOICE_PAYMENT_PART_RECEIPT_CREATOR_HPP

#include <qrinvoice/qrinvoicedefs.hpp>
#include <qrinvoice/output/payment_part_receipt.hpp>
#include <qrinvoice/model/qr_invoice.hpp>
#include <qrinvoice/output_format.hpp>
#include <qrinvoice/output_resolution.hpp>
#include <qrinvoice/page_size.hpp>
#include <qrinvoice/font_family.hpp>
#include <qrinvoice/locale.hpp>
#include <qrinvoice/boundary_lines.hpp>


namespace qrinvoice {


class payment_part_receipt_creator {
public:
	payment_part_receipt_creator() = default;
	payment_part_receipt_creator(model::qr_invoice qr_invoice, page_size pg_sz, font_family font_family,
			output_format fmt, output_resolution res, qrinvoice::locale lcl, boundary_lines boundary_lines, bool scissors, bool separation_text, bool additional_print_margin)
		:qr_invoice_{std::move(qr_invoice)}
		,page_size_{ pg_sz }
		,font_family_{ font_family }
		,output_format_{ fmt }
		,output_resolution_{ res }
		,locale_{ lcl }
		,boundary_lines_{ boundary_lines }
		,boundary_line_scissors_ { scissors }
		,boundary_line_separation_text_{ separation_text }
		,additional_print_margin_{ additional_print_margin }
		{}

public:
	payment_part_receipt_creator& qr_invoice(const model::qr_invoice&  qr_invoice) {
		qr_invoice_ = qr_invoice;
		return *this;
	}

	payment_part_receipt_creator& page_size(const page_size& page_size) {
		page_size_ = page_size;
		return *this;
	}

	payment_part_receipt_creator& font_family(const font_family& font_family) {
		font_family_ = font_family;
		return *this;
	}

	payment_part_receipt_creator& output_format(const output_format output_format) {
		output_format_ = output_format;
		return *this;
	}

	payment_part_receipt_creator& output_resolution(const output_resolution output_resolution) {
		output_resolution_ = output_resolution;
		return *this;
	}

	payment_part_receipt_creator& locale(qrinvoice::locale locale) {
		locale_ = locale;
		return *this;
	}

	payment_part_receipt_creator& boundary_lines(boundary_lines boundary_lines) {
		boundary_lines_ = boundary_lines;
		return *this;
	}

	payment_part_receipt_creator& with_boundary_lines() {
		return boundary_lines(boundary_lines::enabled);
	}

	payment_part_receipt_creator& with_boundary_lines_with_margins() {
		return boundary_lines(boundary_lines::enabled_with_margins);
	}

	payment_part_receipt_creator& without_boundary_lines() {
		return boundary_lines(boundary_lines::none);
	}

	payment_part_receipt_creator& scissors(bool boundary_line_scissors) {
		boundary_line_scissors_ = boundary_line_scissors;
		return *this;
	}
	payment_part_receipt_creator& with_scissors() {
		return scissors(true);
	}

	payment_part_receipt_creator& without_scissors() {
		return scissors(true);
	}

	payment_part_receipt_creator& separation_text(bool boundary_line_separation_text) {
		boundary_line_separation_text_ = boundary_line_separation_text;
		return *this;
	}

	payment_part_receipt_creator& with_separation_text() {
		return separation_text(true);
	}

	payment_part_receipt_creator& without_separation_text() {
		return separation_text(true);
	}

	payment_part_receipt_creator& additional_print_margin(bool additional_print_margin) {
		additional_print_margin_ = additional_print_margin;
		return *this;
	}

	payment_part_receipt_creator& with_additional_print_margin() {
		return additional_print_margin(true);
	}

	payment_part_receipt_creator& without_additional_print_margin() {
		return additional_print_margin(true);
	}

	payment_part_receipt_creator& in_german() {
		locale_ = qrinvoice::locale::german;
		return *this;
	}

	payment_part_receipt_creator& in_french() {
		locale_ = qrinvoice::locale::french;
		return *this;
	}

	payment_part_receipt_creator& in_english() {
		locale_ = qrinvoice::locale::english;
		return *this;
	}

	payment_part_receipt_creator& in_italian() {
		locale_ = qrinvoice::locale::italian;
		return *this;
	}

	QRINVOICE_API output::payment_part_receipt create_payment_part_receipt() const;

private:
	model::qr_invoice qr_invoice_;
	qrinvoice::page_size page_size_ = qrinvoice::page_size::a4;
	qrinvoice::font_family font_family_ = qrinvoice::font_family::liberation_sans;
	qrinvoice::output_format output_format_ = qrinvoice::output_format::pdf;
	qrinvoice::output_resolution output_resolution_ = qrinvoice::output_resolution::medium_300_dpi;
	qrinvoice::locale locale_ = qrinvoice::locale::german;
	qrinvoice::boundary_lines boundary_lines_ = qrinvoice::boundary_lines::enabled;
	bool boundary_line_scissors_ = true;
	bool boundary_line_separation_text_ = true;
	bool additional_print_margin_ = false;
};

} // namespace qrinvoice


#endif // QR_INVOICE_PAYMENT_PART_RECEIPT_CREATOR_HPP

