
#ifndef QR_INVOICE_FONT_MANAGER_HPP
#define QR_INVOICE_FONT_MANAGER_HPP

#include <string>
#include <qrinvoice/qrinvoicedefs.hpp>

namespace qrinvoice {
namespace font_manager {

    QRINVOICE_API void set_font_path(const std::string& font_path);

} // namespace font
} // namespace qrinvoice


#endif // QR_INVOICE_FONT_MANAGER_HPP

