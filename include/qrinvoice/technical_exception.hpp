
#ifndef QR_INVOICE_TECHNICAL_EXCEPTION_HPP
#define QR_INVOICE_TECHNICAL_EXCEPTION_HPP

#include <qrinvoice/base_exception.hpp>


namespace qrinvoice {

class technical_exception : public base_exception {
public:
	explicit technical_exception(const std::string& msg) : base_exception(msg) {}
};

} // namespace qrinvoice


#endif // QR_INVOICE_TECHNICAL_EXCEPTION_HPP

