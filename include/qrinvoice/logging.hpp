#ifndef QR_INVOICE_LOGGING_HPP
#define QR_INVOICE_LOGGING_HPP

#include <string>
#include <qrinvoice/qrinvoicedefs.hpp>

namespace qrinvoice {
namespace logging {
	QRINVOICE_API const void set_log_level(const char* log_level);
    QRINVOICE_API const void set_log_file(const char* log_file);
    QRINVOICE_API const void set_lazy_log_file(const char* log_file);
    QRINVOICE_API const void set_flush_level(const char* flush_level);
    QRINVOICE_API const void flush();
    QRINVOICE_API const void shutdown();
} // namespace logging
} // namespace qrinvoice


#endif // QR_INVOICE_LOGGING_HPP

