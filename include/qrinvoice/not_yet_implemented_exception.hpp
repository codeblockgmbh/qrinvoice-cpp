
#ifndef QR_INVOICE_NOT_YET_IMPLEMENTED_EXCEPTION_HPP
#define QR_INVOICE_NOT_YET_IMPLEMENTED_EXCEPTION_HPP

#include <qrinvoice/base_exception.hpp>


namespace qrinvoice {

class not_yet_implemented_exception : public base_exception {
public:
	explicit not_yet_implemented_exception(const std::string& msg) : base_exception(msg) {}
};

} // namespace qrinvoice


#endif // QR_INVOICE_NOT_YET_IMPLEMENTED_EXCEPTION_HPP

