
#ifndef QR_INVOICE_OUTPUT_FORMAT_HPP
#define QR_INVOICE_OUTPUT_FORMAT_HPP

#include <string>

#include <qrinvoice/qrinvoicedefs.hpp>


namespace qrinvoice {

enum class output_format {
	pdf,
	png,
	jpg,
	bmp
};


namespace output_format_ops {

constexpr const char* mime_pdf = "application/pdf";
constexpr const char* mime_jpg = "image/jpg";
constexpr const char* mime_png = "image/png";
constexpr const char* mime_bmp = "image/bmp";

constexpr const char* get_mime_type(output_format op_fmt)
{
	return op_fmt == output_format::pdf ? mime_pdf
		:op_fmt == output_format::png ? mime_png
		:op_fmt == output_format::jpg ? mime_jpg
		:op_fmt == output_format::bmp ? mime_bmp
		:"";
}

constexpr const char* get_file_extension(output_format op_fmt)
{
	return op_fmt == output_format::pdf ? "pdf"
		:op_fmt == output_format::png ? "png"
		:op_fmt == output_format::jpg ? "jpg"
		:op_fmt == output_format::bmp ? "bmp"
		:"";
}

QRINVOICE_API output_format get_format_by_mime_type(const std::string& mime_type);

} // namespace output_format_ops
} // namespace qrinvoice


#endif // QR_INVOICE_OUTPUT_FORMAT_HPP

