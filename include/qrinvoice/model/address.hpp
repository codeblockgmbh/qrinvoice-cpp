#include <utility>

#include <utility>

#include <utility>


#ifndef QR_INVOICE_ADDRESS_HPP
#define QR_INVOICE_ADDRESS_HPP

#include <string>
#include <qrinvoice/model/address_type.hpp>

namespace qrinvoice {
namespace model {

class address {
public:
	address() = default;
	address(const address_type& address_type,
			std::string name,
			std::string street_name, std::string house_number, std::string postal_code, std::string city,  // structured
			std::string address_line1, std::string address_line2, // combined
			std::string country)
		:address_type_{ address_type }
		,name_{std::move(name)}
		,street_name_{std::move(street_name)}
		,house_number_{std::move(house_number)}
		,postal_code_{std::move(postal_code)}
		,city_{std::move(city)}
		,address_line1_ {std::move(address_line1)}
		,address_line2_ {std::move(address_line2)}
		,country_{std::move(country)} {}

	class builder;

public:
	const address_type& get_address_type() const {
		return address_type_;
	}

	void set_address_type(const address_type& at) {
		address_type_ = at;
	}

	const std::string& get_name() const {
		return name_;
	}

	void set_name(const std::string& name) {
		name_ = name;
	}

	const std::string& get_street_name() const {
		return street_name_;
	}

	void set_street_name(const std::string& street_name) {
		street_name_ = street_name;
	}

	const std::string& get_house_number() const {
		return house_number_;
	}

	void set_house_number(const std::string& house_number) {
		house_number_ = house_number;
	}

	const std::string& get_postal_code() const {
		return postal_code_;
	}

	void set_postal_code(const std::string& postal_code) {
		postal_code_ = postal_code;
	}

	void set_postal_code(int postal_code) {
		postal_code_ = std::to_string(postal_code);
	}

	const std::string& get_city() const {
		return city_;
	}

	void set_city(const std::string& city) {
		city_ = city;
	}

	const std::string& get_address_line1() const {
		return address_line1_;
	}

	void set_address_line1(const std::string& address_line1) {
		address_line1_ = address_line1;
	}

	const std::string& get_address_line2() const {
		return address_line2_;
	}

	void set_address_line2(const std::string& address_line2) {
		address_line2_ = address_line2;
	}

	const std::string& get_country() const {
		return country_;
	}

	void set_country(const std::string& country) {
		country_ = country;
	}

	bool empty() const {
		return (address_type_.get_address_type_code() == nullptr || address_type_.get_address_type_code() == "") &&
			name_.empty() &&
			street_name_.empty() && 
			house_number_.empty() &&
			postal_code_.empty() &&
			city_.empty() &&
			address_line1_.empty() &&
			address_line2_.empty() &&
			country_.empty();
	}

private:
	address_type address_type_;
	std::string name_,
				street_name_, house_number_, postal_code_, city_,  // structured
				address_line1_, address_line2_, // combined
				country_;
};

class address::builder {
public:
	builder& address_type(address_type address_type) {
		address_type_ = address_type;
		return *this;
	}

	builder& address_type(address_type::addr_type addr_type) {
		return address_type(qrinvoice::model::address_type(addr_type));
	}

	builder& structured_address() {
		return address_type(address_type::structured);
	}

	builder& combined_address() {
		return address_type(address_type::combined);
	}

	builder& name(const std::string& name) {
		name_ = name;
		return *this;
	}

	builder& country(const std::string& country) {
		country_ = country;
		return *this;
	}

	// structured
	builder& street_name(const std::string& street_name) {
		street_name_ = street_name;
		return *this;
	}

	builder& house_number(const std::string& house_number) {
		house_number_ = house_number;
		return *this;
	}

	builder& postal_code(const std::string& postal_code) {
		postal_code_ = postal_code;
		return *this;
	}

	builder& city(const std::string& city) {
		city_ = city;
		return *this;
	}

	// combined

	builder& address_line1(const std::string& address_line1) {
		address_line1_ = address_line1;
		return *this;
	}

	builder& address_line2(const std::string& address_line2) {
		address_line2_ = address_line2;
		return *this;
	}

	address build() const {
		return address {
			address_type_,
			name_, 
			street_name_, house_number_, postal_code_, city_,  // structured
			address_line1_, address_line2_, // combined
			country_
		};
	}

private:
	class address_type address_type_;
	std::string	name_, 
				street_name_, house_number_, postal_code_, city_,  // structured
				address_line1_, address_line2_, // combined
				country_;
};

} // namespace model
} // namespace qrinvoice


#endif	// QR_INVOICE_ADDRESS_HPP

