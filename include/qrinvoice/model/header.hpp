#include <utility>


#ifndef QR_INVOICE_HEADER_HPP
#define QR_INVOICE_HEADER_HPP

#include <string>

#include <qrinvoice/qrinvoicedefs.hpp>


namespace qrinvoice {
namespace model {

/**
 * <p>From the specification v2.0</p>
 * <table border="1" summary="Excerpt from the specification">
 * <tr><th>Language</th><th>General Definition</th><th>Field Definition</th></tr>
 * <tr><td>EN</td><td>Header<br>Header data. Contains basic information about the Swiss QR Code</td><td>Mandatory data group</td></tr>
 * <tr><td>DE</td><td>Header<br>Header-Daten. Enthält grundlegende Informationen über den QR-Code</td><td>Obligatorische Datengruppe</td></tr>
 * <tr><td>FR</td><td>Header<br>Données d'en-tête. Contient des informations fondamentales sur le code QR</td><td>Groupe de données obligatoire</td></tr>
 * </table>
 * <p>Data Structure Element</p>
 * <pre>
 * QRCH
 * +Header
 * </pre>
 */
class header {
public:
	QRINVOICE_API header();
	header(std::string qr_type, short version, short coding_type)
		:qr_type_{std::move(qr_type)}
		,version_{ version }
		,coding_type_{ coding_type } {}

	class builder;

public:
	/**
     * <p>From the specification v2.0</p>
     * <table border="1" summary="Excerpt from the specification">
     * <tr><th>Language</th><th>General Definition</th><th>Field Definition</th></tr>
     * <tr><td>EN</td><td>QR Type<br>Unambiguous indicator for the Swiss QR Code. Fixed value “SPC” (Swiss Payments Code)</td><td>Fixed length: three-digit, alphanumeric</td></tr>
     * <tr><td>DE</td><td>QRType<br>Eindeutiges Kennzeichen für den Swiss QR Code. Fixer Wert «SPC» (Swiss Payments Code)</td><td>Feste Länge: dreistellig alphanumerisch</td></tr>
     * <tr><td>FR</td><td>QRType<br>Indicateur distinct pour le Swiss QR Code. Valeur fixe «SPC» (Swiss Payments Code)</td><td>Longueur fixe: trois positions alphanumériques</td></tr>
     * </table>
     * <p>Status: {@link Mandatory}</p>
     * <p>Data Structure Element</p>
     * <pre>
     * QRCH
     * +Header
     * ++QRType
     * </pre>
     */
	std::string get_qr_type() const {
		return qr_type_;
	}

	void set_qr_type(const std::string& qr_type) {
		qr_type_ = qr_type;
	}

	/**
     * <p>From the specification v2.0</p>
     * <table border="1" summary="Excerpt from the specification">
     * <tr><th>Language</th><th>General Definition</th><th>Field Definition</th></tr>
     * <tr><td>EN</td><td>Version<br>Contains version of the specifications (Implementation Guidelines) in use on the date on which the Swiss QR Code was created. The first two positions indicate the main version, the following two positions the sub-version. Fixed value of "0200" for Version 2.0</td><td>Fixed length: four-digit, numeric</td></tr>
     * <tr><td>DE</td><td>Version<br>Beinhaltet die zum Zeitpunkt der Swiss-QR-Code-Erstellung verwendete Version der Spezifikation (Implementation Guidelines). Die ersten beiden Stellen bezeichnen die Hauptversion, die folgenden beiden Stellen die Unterversion. Fester Wert «0200» für Version 2.0.</td><td>Feste Länge: vierstellig numerisch</td></tr>
     * <tr><td>FR</td><td>Version<br>Contient la version de la spécification (Implementation Guidelines) utilisée au moment de la création du Swiss QR Code. Les deux premières positions indi- quent la version principale, les deux positions suivantes la sous-version. Valeur fixe «0200» pour la version 2.0</td><td>Longueur fixe: quatre positions numériques</td></tr>
     * </table>
     * <p>Status: {@link Mandatory}</p>
     * <p>Data Structure Element</p>
     * <pre>
     * QRCH
     * +Header
     * ++Version
     * </pre>
     */
	short get_version() const {
		return version_;
	}

	void set_version(short version) {
		version_ = version;
	}

	/**
     * <p>From the specification v2.0</p>
     * <table border="1" summary="Excerpt from the specification">
     * <tr><th>Language</th><th>General Definition</th><th>Field Definition</th></tr>
     * <tr><td>EN</td><td>Coding type<br>Character set code. Fixed value 1 (indicates Latin character set)</td><td>One-digit, numeric</td></tr>
     * <tr><td>DE</td><td>Coding Type<br>Zeichensatz-Code. Fixer Wert 1 (kennzeichnet Latin Character Set)</td><td>Einstellig numerisch</td></tr>
     * <tr><td>FR</td><td>Coding Type<br>Code de jeu de caractères. Valeur fixe 1 (désigne le «Latin Character Set»)</td><td>Une position numérique</td></tr>
     * </table>
     * <p>Status: {@link Mandatory}</p>
     * <p>Data Structure Element</p>
     * <pre>
     * QRCH
     * +Header
     * ++Coding
     * </pre>
     */
	short get_coding_type() const {
		return coding_type_;
	}

	void set_coding_type(short coding_type) {
		coding_type_ = coding_type;
	}

	bool empty() const {
		return qr_type_.empty();
	}

private:
	std::string	qr_type_;
	short		version_;
	short		coding_type_;
};

class header::builder {
public:
	builder& qr_type(const std::string& qr_type) {
		qr_type_ = qr_type;
		return *this;
	}

	builder& version(short version) {
		version_ = version;
		return *this;
	}

	builder& coding_type(short coding_type) {
		coding_type_ = coding_type;
		return *this;
	}

	header build() const {
		return header {qr_type_, version_, coding_type_};
	}

private:
	std::string	qr_type_;
	short		version_, coding_type_;
};

} // namepace model
} // namespace qrinvoice


#endif // QR_INVOICE_HEADER_HPP

