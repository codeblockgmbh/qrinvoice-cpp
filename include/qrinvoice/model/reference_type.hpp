
#ifndef QR_INVOICE_REFERENCE_TYPE_HPP
#define QR_INVOICE_REFERENCE_TYPE_HPP

#include <iostream>
#include <string>
#include <cassert>

#include <qrinvoice/qrinvoicedefs.hpp>
#include <qrinvoice/model/parse_exception.hpp>


namespace qrinvoice {
namespace model {


class reference_type {
public:
	enum ref_type {
		/**
         * <p>QRR – QR reference</p>
         * <p>From the specification v2.0</p>
         * <table border="1" summary="Excerpt from the specification">
         * <tr><th>Language</th><th>Description</th></tr>
         * <tr><td>EN</td><td>The biller's structured reference corresponds to the former ISR reference number.</td></tr>
         * <tr><td>DE</td><td>Die strukturierte Referenz des Rechnungsstellers entspricht der ehemaligen ESR Referenznummer.</td></tr>
         * <tr><td>FR</td><td>Référence structurée de l'émetteur de factures, correspondant à l'ancien numéro de référence BVR.</td></tr>
         * </table>
         *
         * @see PaymentReference#getReferenceType()
         */
		qr_reference,		// "QRR"

		/**
         * <p>SCOR – Creditor Reference (ISO 11649)</p>
         * <p>From the specification v2.0</p>
         * <table border="1" summary="Excerpt from the specification">
         * <tr><th>Language</th><th>Description</th></tr>
         * <tr><td>EN</td><td>Creditor Reference according to the ISO 11649 standard.</td></tr>
         * <tr><td>DE</td><td>Creditor Reference gemäss ISO-11649-Standard.</td></tr>
         * <tr><td>FR</td><td>Creditor Reference selon la norme ISO 11649.</td></tr>
         * </table>
         *
         * @see PaymentReference#getReferenceType()
         */
		creditor_reference,	// "SCOR"

		/**
         * <p>NON – without reference</p>
         * <p>From the specification v2.0</p>
         * <table border="1" summary="Excerpt from the specification">
         * <tr><th>Language</th><th>Description</th></tr>
         * <tr><td>EN</td><td></td></tr>
         * <tr><td>DE</td><td></td></tr>
         * <tr><td>FR</td><td></td></tr>
         * </table>
         *
         * @see PaymentReference#getReferenceType()
         */
		without_reference	// "NON"
	};

	explicit reference_type(reference_type::ref_type rt = without_reference)
		:ref_type_(rt) {}

public:
	ref_type get() const {
		return ref_type_;
	}

	void set(ref_type rt) {
		ref_type_ = rt;
	}

	const char* get_reference_type_code() const {
		switch (ref_type_) {
		case qr_reference:
			return "QRR";
		case creditor_reference:
			return "SCOR";
		case without_reference:
			return "NON";
		}

		assert(false);		// Invalid reference type
		return nullptr;
	}

	QRINVOICE_API static reference_type parse(const std::string& ref_type_code);

private:
	ref_type	ref_type_;
};

inline std::ostream& operator << (std::ostream& os, const reference_type& reference_type)
{
	return os << reference_type.get_reference_type_code();
}


} // namespace model
} // namespace qrinvoice

#endif	// QR_INVOICE_REFERENCE_TYPE_HPP

