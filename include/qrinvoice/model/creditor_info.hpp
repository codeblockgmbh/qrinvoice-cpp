#include <utility>

#include <utility>


#ifndef QR_INVOICE_CREDITOR_INFO_HPP
#define QR_INVOICE_CREDITOR_INFO_HPP

#include <string>

#include <qrinvoice/model/creditor.hpp>
#include <qrinvoice/util/iban_util.hpp>


namespace qrinvoice {
namespace model {

/**
 * <p>From the specification v2.0</p>
 * <table border="1" summary="Excerpt from the specification">
 * <tr><th>Language</th><th>General Definition</th><th>Field Definition</th></tr>
 * <tr><td>EN</td><td>Creditor information</td><td>Mandatory data group</td></tr>
 * <tr><td>DE</td><td>Zahlungsempfänger Informationen</td><td>Obligatorische Datengruppe</td></tr>
 * <tr><td>FR</td><td>Informations sur le bénéficiaire</td><td>Groupe de données obligatoire</td></tr>
 * </table>
 * <p>Data Structure Element</p>
 * <pre>
 * QRCH
 * +CdtrInf
 * </pre>
 */
class creditor_info {
public:
	creditor_info() = default;
	creditor_info(std::string iban, creditor creditor)
		:iban_{std::move(iban)}
		,creditor_{std::move(creditor)} {}

	class builder;

public:
	/**
     * <p>From the specification v2.0</p>
     * <table border="1" summary="Excerpt from the specification">
     * <tr><th>Language</th><th>General Definition</th><th>Field Definition</th></tr>
     * <tr><td>EN</td><td>IBAN<br>Account<br>IBAN or QR-IBAN of the creditor.</td><td>Fixed length: 21 alphanumeric characters, only IBANs with CH or LI country code permitted.</td></tr>
     * <tr><td>DE</td><td>IBAN<br>Konto<br>IBAN bzw. QR-IBAN des Zahlungsempfängers</td><td>Feste Länge: 21 alphanumerische Zeichen, nur IBANs mit CH- oder LI-Landescode zulässig.</td></tr>
     * <tr><td>FR</td><td>IBAN<br>Compte<br>IBAN ou QR-IBAN du bénéficiare</td><td>Longueur fixe: 21 caractères alphanumériques, IBAN seulement admis avec code de pays CH ou LI.</td></tr>
     * </table>
     * <p>Status: {@link Mandatory}</p>
     * <p>Data Structure Element</p>
     * <pre>
     * QRCH
     * +CdtrInf
     * ++IBAN
     * </pre>
     */
	const std::string& get_iban() const {
		return iban_;
	}

	void set_iban(const std::string& iban) {
		iban_ = iban;
	}

	const creditor& get_creditor() const {
		return creditor_;
	}

	creditor& get_creditor() {
		return creditor_;
	}

	void set_creditor(const creditor& creditor) {
		creditor_ = creditor;
	}

	bool empty() const {
		return iban_.empty() && creditor_.empty();
	}

private:
	std::string	iban_;
	creditor	creditor_;
};

class creditor_info::builder {
public:
	builder& iban(const std::string& iban) {
		iban_ = iban;
		return *this;
	}

	builder& creditor(const creditor& creditor) {
		creditor_ = creditor;
		return *this;
	}

	creditor_info build() const {
		return { iban_util::normalize_iban(iban_), creditor_ };
	}

private:
	std::string	iban_;
	class creditor	creditor_;
};

} // namespace model
} // namespace qrinvoice


#endif	// QR_INVOICE_CREDITOR_INFO_HPP

