
#ifndef QR_INVOICE_QR_INVOICE_HPP
#define QR_INVOICE_QR_INVOICE_HPP

#include <qrinvoice/qrinvoicedefs.hpp>
#include <qrinvoice/model/swiss_payments_code.hpp>
#include <qrinvoice/model/header.hpp>
#include <qrinvoice/model/creditor_info.hpp>
#include <qrinvoice/model/ultimate_creditor.hpp>
#include <qrinvoice/model/payment_amount_info.hpp>
#include <qrinvoice/model/ultimate_debtor.hpp>
#include <qrinvoice/model/payment_reference.hpp>
#include <qrinvoice/model/alternative_schemes.hpp>

namespace qrinvoice {
namespace model {

/**
 * <p>Swiss QR Invoice root element</p>
 * <p>Data Structure Element</p>
 * <pre>
 *     QRCH
 * </pre>
 */
class qr_invoice {
public:
	QRINVOICE_API qr_invoice() = default;
	qr_invoice(const header& header, const creditor_info& cred_info, const ultimate_creditor& u_creditor,
		const payment_amount_info& payment_info, const ultimate_debtor& u_debtor,
		const payment_reference& payment_ref, const alternative_schemes& alt_schemes);

	class builder;

public:
	const header& get_header() const {
		return header_;
	}

	header& get_header() {
		return header_;
	}

	void set_header(const header& header) {
		header_ = header;
	}

	const creditor_info& get_creditor_info() const {
		return creditor_info_;
	}

	creditor_info& get_creditor_info() {
		return creditor_info_;
	}

	void set_creditor_info(const creditor_info& creditor_info) {
		creditor_info_ = creditor_info;
	}

	const ultimate_creditor& get_ultimate_creditor() const {
		return ultimate_creditor_;
	}

	ultimate_creditor& get_ultimate_creditor() {
		return ultimate_creditor_;
	}

	void set_ultimate_creditor(const ultimate_creditor& ultimate_creditor) {
		ultimate_creditor_ = ultimate_creditor;
	}

	const payment_amount_info& get_payment_amount_info() const {
		return payment_amount_info_;
	}

	payment_amount_info& get_payment_amount_info() {
		return payment_amount_info_;
	}

	void set_payment_amount_info(const payment_amount_info& payment_amount_info) {
		payment_amount_info_ = payment_amount_info;
	}

	const ultimate_debtor& get_ultimate_debtor() const {
		return ultimate_debtor_;
	}

	ultimate_debtor& get_ultimate_debtor() {
		return ultimate_debtor_;
	}

	void set_ultimate_debtor(const ultimate_debtor& ultimate_debtor) {
		ultimate_debtor_ = ultimate_debtor;
	}

	const payment_reference& get_payment_reference() const {
		return payment_reference_;
	}

	payment_reference& get_payment_reference() {
		return payment_reference_;
	}

	void set_payment_reference(const payment_reference& payment_reference) {
		payment_reference_ = payment_reference;
	}

	const alternative_schemes& get_alternative_schemes() const {
		return alternative_schemes_;
	}

	alternative_schemes& get_alternative_schemes() {
		return alternative_schemes_;
	}

	void set_alternative_schemes(const alternative_schemes& alternative_schemes) {
		alternative_schemes_ = alternative_schemes;
	}

private:
	header			header_;
	creditor_info		creditor_info_;
	ultimate_creditor	ultimate_creditor_;
	payment_amount_info	payment_amount_info_;
	ultimate_debtor		ultimate_debtor_;
	payment_reference	payment_reference_;
	additional_information additional_information_;
	alternative_schemes	alternative_schemes_;
};

class qr_invoice::builder {
public:
	address::builder& creditor() {
		return creditor_addr_builder_;
	}

	address::builder& ultimate_creditor() {
		return ultimate_creditor_addr_builder_;
	}

	address::builder& ultimate_debtor() {
		return ultimate_debtor_addr_builder_;
	}

	payment_amount_info::builder& payment_amount_info() {
		return payment_amount_info_builder_;
	}

	payment_reference::builder& payment_reference() {
		return payment_reference_builder_;
	}

	additional_information::builder& additional_information() {
		return additional_information_builder_;
	}

	alternative_schemes::builder& alternative_schemes() {
		return alternative_schemes_builder_;
	}

	builder& creditor_iban(const std::string& iban) {
		iban_ = iban;
		return *this;
	}

	builder& alternative_scheme_params(const std::vector<std::string>& alternative_scheme_params) {
		alternative_scheme_params_ = alternative_scheme_params;
		return *this;
	}

	QRINVOICE_API qr_invoice build();

private:
	QRINVOICE_API header build_header() const;
	QRINVOICE_API creditor_info build_creditor_info();
	QRINVOICE_API class ultimate_creditor build_ultimate_creditor();
	QRINVOICE_API class ultimate_debtor build_ultimate_debtor();

	class payment_amount_info build_payment_amount_info() {
		return payment_amount_info_builder_.build();
	}

	class payment_reference build_payment_reference() {
		return payment_reference_builder_
			.additional_information(additional_information_builder_.trailer(qrinvoice::model::spc::end_payment_data_trailer).build())
			.build();
	}

	class alternative_schemes build_alternative_schemes() {
		return alternative_schemes_builder_
			.alternative_scheme_params(alternative_scheme_params_).build();
	}

private:
	// creditor_info
	std::string			iban_;

	// builder delegates
	creditor_info::builder		creditor_info_builder_;
	address::builder		creditor_addr_builder_;
	address::builder		ultimate_creditor_addr_builder_;
	address::builder		ultimate_debtor_addr_builder_;
	payment_amount_info::builder	payment_amount_info_builder_;
	payment_reference::builder	payment_reference_builder_;
	additional_information::builder additional_information_builder_;
	alternative_schemes::builder	alternative_schemes_builder_;

	// alternative_scheme_params
	std::vector<std::string>	alternative_scheme_params_;
};

} // namespace model
} // namespace qrinvoice


#endif // QR_INVOICE_QR_INVOICE_HPP

