#include <utility>


#ifndef QR_INVOICE_ALTERNATIVE_SCHEMES_HPP
#define QR_INVOICE_ALTERNATIVE_SCHEMES_HPP

#include <vector>
#include <string>


namespace qrinvoice {
namespace model {

/**
 * <p>From the specification v2.0</p>
 * <table border="1" summary="Excerpt from the specification">
 * <tr><th>Language</th><th>General Definition</th><th>Field Definition</th></tr>
 * <tr><td>EN</td><td>Alternative schemes<br>Parameters and data of other supported schemes</td><td>Optional data group with a variable number of elements</td></tr>
 * <tr><td>DE</td><td>Alternative Verfahren<br>Parameter und Daten weiterer unterstützter Verfahren</td><td>Optionale Datengruppe mit variabler Anzahl von Elementen</td></tr>
 * <tr><td>FR</td><td>Procédures alternatives<br>Paramètres et données d'autres procédures supportées</td><td>Groupe de données optionnel avec un nombre variable d'éléments</td></tr>
 * </table>
 * <p>Data Structure Element</p>
 * <pre>
 * QRCH
 * +AltPmtInf
 * </pre>
 */
class alternative_schemes {
public:
	alternative_schemes() = default;
	explicit alternative_schemes(std::vector<std::string> alternative_scheme_params)
		:alternative_scheme_params_{std::move(alternative_scheme_params)} {}

	class builder;

public:
    /**
     * <p>From the specification v2.0</p>
     * <table border="1" summary="Excerpt from the specification">
     * <tr><th>Language</th><th>General Definition</th><th>Field Definition</th></tr>
     * <tr><td>EN</td><td>Alternative scheme parameters<br>Parameter character chain of the alternative scheme according to the syntax definition in the “Alternative scheme” section</td><td>Can be currently delivered a maximum of two times.<br>Maximum 100 characters</td></tr>
     * <tr><td>DE</td><td>Parameter alternatives Verfahren<br>Parameter-Zeichenkette des alternativen Verfahrens gemäss Syntaxdefinition in Kapitel «Alternative Verfahren»</td><td>Kann aktuell maximal zweimal geliefert werden.<br>Maximal 100 Zeichen</td></tr>
     * <tr><td>FR</td><td>Paramètres de procédure alternative<br>Chaîne de caractères de paramètres de la procédure alternative selon définition de syntaxe dans le chapitre «Procédures alternatives»</td><td>Peuvent actuellement être livrés deux fois au maximum. 100 caractères au maximum </td></tr>
     * </table>
     * <p>Status: {@link Additional}</p>
     * <p>Data Structure Element</p>
     * <pre>
     * QRCH
     * +AltPmtInf
     * ++AltPmt
     * </pre>
     */
	const std::vector<std::string>& get_alternative_scheme_params() const {
		return alternative_scheme_params_;
	}

	void set_alternative_scheme_params(const std::vector<std::string>& asp) {
		alternative_scheme_params_ = asp;
	}

	bool empty() const {
		for (const auto& str : alternative_scheme_params_)
			if (!str.empty())
				return false;
		return true;
	}

private:
	std::vector<std::string> alternative_scheme_params_;
};

class alternative_schemes::builder {
public:
	builder& alternative_scheme_params(const std::vector<std::string>& alternative_scheme_params) {
		alternative_scheme_params_ = alternative_scheme_params;
		return *this;
	}

	alternative_schemes build() const {
		return alternative_schemes { alternative_scheme_params_ };
	}

private:
	std::vector<std::string> alternative_scheme_params_;
};

} // namespace model
} // namespace qrinvoice


#endif	// QR_INVOICE_ALTERNATIVE_SCHEMES_HPP

