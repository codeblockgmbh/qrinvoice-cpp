#ifndef QR_INVOICE_ADDITIONAL_INFORMATION_HPP
#define QR_INVOICE_ADDITIONAL_INFORMATION_HPP

#include <string>
#include <qrinvoice/model/reference_type.hpp>

namespace qrinvoice {
namespace model {

/**
 * <p>From the specification v2.0</p>
 * <table border="1" summary="Excerpt from the specification">
 * <tr><th>Language</th><th>General Definition</th><th>Field Definition</th></tr>
 * <tr><td>EN</td><td>Additional information<br>Additional information can be used for the scheme with message and for the scheme with structured reference.</td><td>Mandatory data group</td></tr>
 * <tr><td>DE</td><td>Zusätzliche Informationen<br>Zusätzliche Informationen können beim Verfahren mit Mitteilung und beim Verfahren mit strukturierter Referenz verwendet werden.</td><td>Obligatorische Datengruppe</td></tr>
 * <tr><td>FR</td><td>Informations supplémentaires<br>Des informations supplémentaires peuvent être utilisées pour la procédure avec communication et pour la procédure avec référence structurée.</td><td>Groupe de données obligatoire</td></tr>
 * </table>
 * <p>Data Structure Element</p>
 * <pre>
 * QRCH
 * +RmtInf
 * ++AddInf
 * </pre>
 */
class additional_information {
public:
	additional_information() = default;
	additional_information(std::string unstructured_msg, std::string trailer, std::string bill_information)
		:unstructured_message_{std::move(unstructured_msg)}
		,trailer_{std::move(trailer)}
		,bill_information_{std::move(bill_information)} {}

	class builder;

public:
	/**
     * <p>From the specification v2.0</p>
     * <table border="1" summary="Excerpt from the specification">
     * <tr><th>Language</th><th>General Definition</th><th>Field Definition</th></tr>
     * <tr><td>EN</td><td>Unstructured message<br>Unstructured information can be used to indicate the payment purpose or for additional textual information about payments with a structured reference.</td><td>Maximum 140 characters</td></tr>
     * <tr><td>DE</td><td>Unstrukturierte Mitteilung<br>Unstrukturierte Informationen können zur Angabe eines Zahlungszwecks oder für ergänzende textuelle Informationen zu Zahlungen mit strukturierter Referenz verwendet werden.</td><td>Maximal 140 Zeichen</td></tr>
     * <tr><td>FR</td><td>Communication non structurée<br>Les informations instructurées peuvent être utilisées pour l'indication d'un motif de paiement ou pour des informations textuelles complémentaires au sujet de paiements avec référence structurée.</td><td>140 caractères au maximum admis</td></tr>
     * </table>
     * <p>Status: {@link Optional}</p>
     * <p>Data Structure Element</p>
     * <pre>
     * QRCH
     * +RmtInf
     * ++AddInf
     * +++Ustrd
     * </pre>
     */
	const std::string& get_unstructured_message() const {
		return unstructured_message_;
	}

	void set_unstructured_message(const std::string& unstructured_msg) {
		unstructured_message_ = unstructured_msg;
	}

	/**
     * <p>From the specification v2.0</p>
     * <table border="1" summary="Excerpt from the specification">
     * <tr><th>Language</th><th>General Definition</th><th>Field Definition</th></tr>
     * <tr><td>EN</td><td>Trailer<br>Unambiguous indicator for the end of payment data. Fixed value "EPD" (End Payment Data).</td><td>Fixed length: three-digit, alphanumeric</td></tr>
     * <tr><td>DE</td><td>Trailer<br>Eindeutiges Kennzeichen für Ende der Zahlungsdaten. Fixer Wert "EPD" (End Payment Data).</td><td>Feste Länge: 3-stellig, alphanumerisch</td></tr>
     * <tr><td>FR</td><td>Trailer<br>Identifiant univoque pour la fin du code QR. Valeur fixe "EPD" (End Payment Data).</td><td>Longueur fixe: 3 positions alphanumériques</td></tr>
     * </table>
     * <p>Status: {@link Optional}</p>
     * <p>Data Structure Element</p>
     * <pre>
     * QRCH
     * +RmtInf
     * ++AddInf
     * +++Trailer
     * </pre>
     */
	const std::string& get_trailer() const {
		return trailer_;
	}

	void set_trailer(const std::string& trailer) {
		trailer_ = trailer;
	}

	/**
     * <p>From the specification v2.0</p>
     * <table border="1" summary="Excerpt from the specification">
     * <tr><th>Language</th><th>General Definition</th><th>Field Definition</th></tr>
     * <tr><td>EN</td><td>Bill information<br>Bill information contain coded information for automated booking of the payment. The data is not forwarded with the payment.</td><td>Maximum 140 characters permitted<br>Use of the information is not part of the standardization.</td></tr>
     * <tr><td>DE</td><td>Rechnungsinformationen<br>Rechnungsinformationen enthalten codierte Informationen für die automatisierte Verbuchung der Zahlung. Die Daten werden nicht mit der Zahlung weitergeleitet.</td><td>Maximal 140 Zeichen zulässig</td></tr>
     * <tr><td>FR</td><td>Informations de facture<br>Les informations structurelles de l'émetteur de factures contiennent des informations codées pour la comptabilisation automatisée du paiement. Les données ne sont pas transmises avec le paiement.</td><td>140 caractères au maximum.</td></tr>
     * </table>
     * <p>Status: {@link Optional}</p>
     * <p>Data Structure Element</p>
     * <pre>
     * QRCH
     * +RmtInf
     * ++AddInf
     * +++StrdBkgInf
     * </pre>
     */
	const std::string& get_bill_information() const {
		return bill_information_;
	}

	void set_bill_information(const std::string& bill_information) {
		bill_information_ = bill_information;
	}

	bool empty() const {
		return unstructured_message_.empty() &&
			trailer_.empty() &&
			bill_information_.empty();
	}

private:
	std::string		unstructured_message_;
	std::string		trailer_;
	std::string		bill_information_;
};

class additional_information::builder {
public:
	builder& unstructured_message(const std::string& unstructured_message) {
		unstructured_message_ = unstructured_message;
		return *this;
	}

	builder& trailer(const std::string& trailer) {
		trailer_ = trailer;
		return *this;
	}

	builder& bill_information(const std::string& bill_information) {
		bill_information_ = bill_information;
		return *this;
	}

	additional_information build() const {
		return additional_information {
			unstructured_message_, trailer_, bill_information_
		};
	}

private:
	std::string		unstructured_message_;
	std::string		trailer_;
	std::string		bill_information_;
};

} // namespace model
} // namespace qrinvoice


#endif	// QR_INVOICE_ADDITIONAL_INFORMATION_HPP

