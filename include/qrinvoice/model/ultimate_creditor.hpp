#include <utility>


#ifndef QR_INVOICE_ULTIMATE_CREDITOR_HPP
#define QR_INVOICE_ULTIMATE_CREDITOR_HPP

#include <qrinvoice/model/address.hpp>


namespace qrinvoice {
namespace model {

/**
 * <p>From the specification v2.0</p>
 * <table border="1" summary="Excerpt from the specification">
 * <tr><th>Language</th><th>General Definition</th><th>Field Definition</th></tr>
 * <tr><td>EN</td><td>Ultimate creditor<br>Ultimate creditor<br>Information about the ultimate creditor</td><td>Optional data group; may only be used in agreement with the creditor’s financial institution</td></tr>
 * <tr><td>DE</td><td>Endgültiger Zahlungsempfänger<br>Endgültiger Zahlungsempfänger<br>Informationen zum endgültigen Zahlungsempfänger</td><td>Optionale Datengruppe; darf nur in Absprache mit dem Finanzinstitut des Zahlungsempfängers verwendet werden</td></tr>
 * <tr><td>FR</td><td>Bénéficiaire final<br>Bénéficiaire final<br>Informations sur le bénéficiaire final</td><td>Groupe de données optionnel; ne peut être utilisé que d'entente avec l'établissement financier du bénéficiaire</td></tr>
 * </table>
 * <p>Data Structure Element</p>
 * <pre>
 * QRCH
 * +UltmtCdtr
 * </pre>
 */
class ultimate_creditor {
public:
	ultimate_creditor() = default;
	explicit ultimate_creditor(address address)
		:address_{std::move(address)} {}

	class builder;

public:
	address& get_address() {
		return address_;
	}

	const address& get_address() const {
		return address_;
	}

	void set_address(const address& addr) {
		address_ = addr;
	}

	bool empty() const {
		return address_.empty();
	}

private:
	address address_;
};

class ultimate_creditor::builder {
public:
	builder& address(const address& address) {
		address_ = address;
		return *this;
	}

	ultimate_creditor build() const {
		return ultimate_creditor{ address_ };
	}

private:
	class address address_;
};

} // namespace model
} // namespace qrinvoice


#endif	// QR_INVOICE_ULTIMATE_CREDITOR_HPP

