
#ifndef QR_INVOICE_ADDRESS_TYPE_HPP
#define QR_INVOICE_ADDRESS_TYPE_HPP

#include <iostream>
#include <string>
#include <cassert>

#include <qrinvoice/qrinvoicedefs.hpp>
#include <qrinvoice/model/parse_exception.hpp>
#include <qrinvoice/technical_exception.hpp>


namespace qrinvoice {
namespace model {


class address_type {
public:
	enum addr_type {
		/**
		 * <p>Structured Address</p>
		 * <p>From the specification v2.0</p>
		 * <table border="1" summary="Excerpt from the specification">
		 * <tr><th>Language</th><th>Description</th></tr>
		 * <tr><td>EN</td><td>"S" - structured address</td></tr>
		 * <tr><td>DE</td><td>"S" - Strukturierte Adresse</td></tr>
		 * <tr><td>FR</td><td>"S" - Adresse structurée</td></tr>
		 * </table>
		 */
		structured, // S
		/**
		 * <p>Combined Address</p>
		 * <p>From the specification v2.0</p>
		 * <table border="1" summary="Excerpt from the specification">
		 * <tr><th>Language</th><th>Description</th></tr>
		 * <tr><td>EN</td><td>"K" - combined address elements (2 lines)</td></tr>
		 * <tr><td>DE</td><td>"K" - Kombinierte Adressfelder (2 Zeilen)</td></tr>
		 * <tr><td>FR</td><td>"K" - Champs d'adresse combinés (2 lignes)</td></tr>
		 * </table>
		 */
		combined, // K
		// in order to ease things, have an "unset" enum value here
		unset
	};

	explicit address_type(address_type::addr_type at = unset)
		:addr_type_(at) {}

public:
	addr_type get() const {
		return addr_type_;
	}

	void set(addr_type at) {
		addr_type_ = at;
	}

	const char* get_address_type_code() const {
		switch (addr_type_) {
		case structured:
			return "S";
		case combined:
			return "K";
		case unset:
			return "";
		}

		throw technical_exception("invalid addr_type_ encountered");
	}

	QRINVOICE_API static address_type parse(const std::string& addr_type_code);

private:
	addr_type	addr_type_;
};

inline std::ostream& operator << (std::ostream& os, const address_type& address_type)
{
	return os << address_type.get_address_type_code();
}


} // namespace model
} // namespace qrinvoice

#endif	// QR_INVOICE_ADDRESS_TYPE_HPP

