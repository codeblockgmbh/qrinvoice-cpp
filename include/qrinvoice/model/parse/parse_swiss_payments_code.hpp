
#ifndef QR_INVOICE_PARSE_SWISS_PAYMENTS_CODE_HPP
#define QR_INVOICE_PARSE_SWISS_PAYMENTS_CODE_HPP

#include <string>

#include <qrinvoice/model/swiss_payments_code.hpp>


namespace qrinvoice {
namespace model {


/**
 * @param str The Swiss Payments Code String
 * @return the parsed swiss_payments_code instance
 * @throws parse_exception in case of any parsing exceptions or unexpected problems
 */
QRINVOICE_API swiss_payments_code parse_swiss_payments_code(const std::string& str);


} // namespace model
} // namespace qrinvoice


#endif // QR_INVOICE_PARSE_SWISS_PAYMENTS_CODE_HPP

