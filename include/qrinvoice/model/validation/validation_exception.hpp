#include <utility>


#ifndef QR_INVOICE_VALIDATION_EXCEPTION_HPP
#define QR_INVOICE_VALIDATION_EXCEPTION_HPP

#include <memory>
#include <qrinvoice/base_exception.hpp>


namespace qrinvoice {
namespace model {
namespace validation {

class validation_result;		// can't include validation_result.hpp as it includes this file

class validation_exception : public base_exception {
public:
	using base_exception::base_exception;

	validation_exception( const std::string& error_summary, std::shared_ptr<validation_result> vr)
		:base_exception{ error_summary }
		,result_{std::move(vr)} {}

private:
	//
	// result_ can be removed if the only concern of the user is with
	// error_summary, which is already stored in the base class.
	//
	std::shared_ptr<validation_result>	result_;

public:

	const std::shared_ptr<validation_result> get_result() const {
		return result_;
	}
};


} // namespace validation
} // namespace model
} // namespace qrinvoice


#endif // QR_INVOICE_VALIDATION_EXCEPTION_HPP

