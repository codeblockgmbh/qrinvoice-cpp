
#ifndef QR_INVOICE_QR_INVOICE_VALIDATOR_HPP
#define QR_INVOICE_QR_INVOICE_VALIDATOR_HPP

#include <qrinvoice/qrinvoicedefs.hpp>
#include <qrinvoice/model/qr_invoice.hpp>
#include <qrinvoice/model/validation/validation_result.hpp>


namespace qrinvoice {
namespace model {
namespace validation {

QRINVOICE_API validation_result validate(const qr_invoice& qr_invoice);


} // namespace validation
} // namespace model
} // namespace qrinvoice


#endif // QR_INVOICE_QR_INVOICE_VALIDATOR_HPP

