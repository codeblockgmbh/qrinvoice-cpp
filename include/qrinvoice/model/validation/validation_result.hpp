#include <utility>

#include <utility>


#ifndef QR_INVOICE_VALIDATION_RESULT_HPP
#define QR_INVOICE_VALIDATION_RESULT_HPP

#include <vector>
#include <string>
#include <algorithm>
#include <memory>

#include <qrinvoice/util/string_util.hpp>
#include <qrinvoice/model/validation/validation_exception.hpp>
#include <qrinvoice/qrinvoicedefs.hpp>


namespace qrinvoice {
namespace model {
namespace validation {


class validation_result {
public:
	class validation_error;

	template <typename T>
	void add_error(const std::string& parent_data_path, const std::string& data_path,
			const T& val, const std::vector<std::string>& error_msg_keys) {
		const std::string complete_data_path = (!data_path.empty())
			? parent_data_path + '.' + data_path
			: parent_data_path;

		add_error(validation_error{complete_data_path, val, error_msg_keys});
	}

	bool empty() const {
		return errors_.empty();
	}

	bool is_valid() const {
		return empty();
	}

	bool has_errors() const {
		return !empty();
	}

	/**
	 * Checks that there are no errors for a given data_path
	 *
	 * @param data_path the data_path to check if it is valid (= has no errors)
	 * @return true, if there is no error present for the data path
	 */
	bool is_valid(const std::string& data_path) const {
		return !has_errors(data_path);
	}

	/**
	 * Checks if there are errors for the passed data_path
	 *
	 * @param data_path the data_path to check for errors, must not be null
	 * @return true, if at least one error is present for the data path
	 */
	bool has_errors(const std::string& data_path) const {
		return std::find_if(errors_.begin(), errors_.end(), [&](const validation_error& ve) {
			return data_path == ve.get_data_path();
		}) != errors_.end();
	}

	/**
	 * if there are {@link #get_errors()}, throw a {@link validation_exception}, otherwise it is a silent operation
	 *
	 * @throws validation_exception in case there are validation errors present
	 */
	void throw_exception_on_errors() const {
		if (has_errors())
			throw validation_exception{get_validation_error_summary(), std::make_shared<validation_result>(*this)};
	}

	const std::vector<validation_error> get_errors() const {
		return errors_;
	}

	std::vector<validation_error> get_errors() {
		return errors_;
	}

	QRINVOICE_API std::string get_validation_error_summary() const;

public:
	class validation_error {
	public:
		validation_error() = default;

		template <typename T>
		validation_error(const std::string& data_path, const T& val, const std::vector<std::string>& error_msg_keys)
			:data_path_{ data_path }
			,value_{ string_util::details::to_string(val) }
			,error_msg_keys_{ error_msg_keys } {}

		// special overload for std::string -- a bit of optimization to
		// avoid call to to_string()
		validation_error(std::string data_path, std::string val,
						 std::vector<std::string> error_msg_keys)
			:data_path_{std::move(data_path)}
			,value_{std::move(val)}
			,error_msg_keys_{std::move(error_msg_keys)} {}

		friend bool operator == (const validation_error& lhs, const validation_error& rhs);

		const std::string& get_data_path() const {
			return data_path_;
		}

		const std::string& get_value() const {
			return value_;
		}

		const std::vector<std::string>& get_error_msg_keys() const {
			return error_msg_keys_;
		}

	private:
		const std::string		data_path_;
		const std::string		value_;
		const std::vector<std::string>	error_msg_keys_;
	};

private:
	void add_error(const validation_error& error) {
		if (std::find(errors_.begin(), errors_.end(), error) == errors_.end())
			errors_.push_back(error);
	}

private:
	std::vector<validation_error>		errors_;
};

inline bool operator == (const validation_result::validation_error& lhs, const validation_result::validation_error& rhs)
{
	return lhs.data_path_ == rhs.data_path_ &&
		lhs.value_ == rhs.value_ &&
		lhs.error_msg_keys_ == rhs.error_msg_keys_;
}


} // namespace validation
} // namespace model
} // namespace qrinvoice


#endif // QR_INVOICE_VALIDATION_RESULT_HPP

