
#ifndef QR_INVOICE_VALIDATION_UTIL_HPP
#define QR_INVOICE_VALIDATION_UTIL_HPP

#include <string>
#include <qrinvoice/model/swiss_payments_code.hpp>

namespace qrinvoice {
namespace model {
namespace validation {
namespace util {

QRINVOICE_API bool is_valid_string(const std::string& str);

} // namespace util
} // namespace validation
} // namespace model
} // namespace qrinvoice

#endif // QR_INVOICE_VALIDATION_UTIL_HPP

