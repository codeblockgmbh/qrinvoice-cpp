
#ifndef QR_INVOICE_MAPPING_EXCEPTION_HPP
#define QR_INVOICE_MAPPING_EXCEPTION_HPP

#include <qrinvoice/base_exception.hpp>


namespace qrinvoice {
namespace model {

class mapping_exception : public base_exception {
public:
	explicit mapping_exception(const std::string& msg) : base_exception(msg) {}
};

} // namespace model
} // namespace qrinvoice


#endif // QR_INVOICE_MAPPING_EXCEPTION_HPP

