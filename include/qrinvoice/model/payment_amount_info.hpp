#include <utility>


#ifndef QR_INVOICE_PAYMENT_AMOUNT_INFO_HPP
#define QR_INVOICE_PAYMENT_AMOUNT_INFO_HPP

#include <string>

#include <qrinvoice/currency.hpp>
#include <qrinvoice/base_exception.hpp>


namespace qrinvoice {
namespace model {

class optional_amount {
public:
	optional_amount(): amount_{ 0.0 } ,empty_{ true } {}

	explicit optional_amount(double amt)
		:amount_{ amt }
		,empty_{ false } {}

    optional_amount(const optional_amount& that)
		:amount_{ that.amount_ }
		,empty_{ that.empty_ } {}

	optional_amount& operator = (const optional_amount& that) {
		empty_ = that.empty_;
		if (!empty_)
			amount_ = that.amount_;
		return *this;
	}

	optional_amount(optional_amount&& that) noexcept
		:amount_{ that.amount_ }
		,empty_{ that.empty_ }
	{
		that.empty_ = true;
	}

	optional_amount& operator = (optional_amount&& that) noexcept {
		empty_ = that.empty_;
		if (!empty_)
			amount_ = that.amount_;
		that.empty_ = true;
		return *this;
	}

	bool is_set() const {
		return !empty_;
	}

	void set(double amt) {
		amount_ = amt;
		empty_ = false;
	}

	void unset() {
		empty_ = true;
	}

	double get() const {
		return (!empty_) ? amount_ : throw base_exception{ "amount not set" };
	}

private:
	double		amount_;
	bool		empty_;
};



/**
 * <p>From the specification v2.0</p>
 * <table border="1" summary="Excerpt from the specification">
 * <tr><th>Language</th><th>General Definition</th><th>Field Definition</th></tr>
 * <tr><td>EN</td><td>Payment amount information</td><td>Mandatory data group</td></tr>
 * <tr><td>DE</td><td>Zahlbetragsinformation</td><td>Obligatorische Datengruppe</td></tr>
 * <tr><td>FR</td><td>Information sur le montant du paiement</td><td>Groupe de données obligatoire</td></tr>
 * </table>
 * <p>Data Structure Element</p>
 * <pre>
 * QRCH
 * +CcyAmt
 * </pre>
 */
class payment_amount_info {
public:
	payment_amount_info() = default;
	payment_amount_info(optional_amount amount, currency cur)
		:amount_{std::move(amount)}
		,currency_{ cur } {}

	class builder;

public:
	/**
     * <p>From the specification v2.0</p>
     * <table border="1" summary="Excerpt from the specification">
     * <tr><th>Language</th><th>General Definition</th><th>Field Definition</th></tr>
     * <tr><td>EN</td><td>Amount<br>The payment amount</td><td>The amount element is to be entered without leading zeroes, including decimal separators and two decimal places.<br><br>Decimal, maximum 12-digits permitted, including decimal separators. Only decimal points (".") are permitted as decimal separators.</td></tr>
     * <tr><td>DE</td><td>Betrag<br>Betrag der Zahlung</td><td>Das Element ist ohne führende Nullen, inklusive Dezimaltrenn zeichen und zwei Nachkommastellen, anzugeben.<br>Dezimal, max. 12 Stellen zulässig, inklusive Dezimaltrennzeichen.<br>Als Dezimaltrennzeichen ist nur das Punktzeichen («.») zulässig.</td></tr>
     * <tr><td>FR</td><td>Montant<br>Montant du paiement</td><td>L'élément est à indiquer sans zéros de tête y compris séparateur décimal et deux décimales.<br>Décimal, 12 positions au maximum admises, y compris séparateur décimal. Seul le point («.») est admis comme séparateur décimal.</td></tr>
     * </table>
     * <p>Status: {@link Optional}</p>
     * <p>Data Structure Element</p>
     * <pre>
     * QRCH
     * +CcyAmt
     * ++Amt
     * </pre>
     */
	double get_amount() const {
		return amount_.get();
	}

	bool is_amount_set() const {
		return amount_.is_set();
	}

	void set_amount(double amount) {
		amount_.set(amount);
	}

	void unset_amount() {
		amount_.unset();
	}

	bool is_currency_set() const {
		return currency_ != currency::invalid;
	}

	/**
     * <p>From the specification v2.0</p>
     * <table border="1" summary="Excerpt from the specification">
     * <tr><th>Language</th><th>General Definition</th><th>Field Definition</th></tr>
     * <tr><td>EN</td><td>Currency<br>The payment currency, 3-digit alphanumeric currency code according to ISO 4217</td><td>Only CHF and EUR are permitted.</td></tr>
     * <tr><td>DE</td><td>Währung<br>Währung der Zahlung, dreistelliger alphabetischer Währungscode gemäss ISO 4217</td><td>Nur CHF und EUR zugelassen.</td></tr>
     * <tr><td>FR</td><td>Monnaie<br>Monnaie du paiement, code monétaire alphabétique à trois positions selon ISO 4217</td><td>Seuls CHF et EUR sont admis.</td></tr>
     * </table>
     * <p>Status: {@link Mandatory}</p>
     * <p>Data Structure Element</p>
     * <pre>
     * QRCH
     * +CcyAmt
     * ++Ccy
     * </pre>
     */
	currency get_currency() const {
		return currency_;
	}

	void set_currency(currency currency) {
		currency_ = currency;
	}

	bool empty() const {
		return !amount_.is_set() && !is_currency_set();
	}

private:
	optional_amount		amount_;
	qrinvoice::currency	currency_{ currency::invalid };
};

class payment_amount_info::builder {
public:
	builder& amount(double amt) {
		amount_.set(amt);
		return *this;
	}

	builder& currency(qrinvoice::currency cur) {
		currency_ = cur;
		return *this;
	}

	builder& chf() {
		return currency(qrinvoice::currency::chf);
	}

	builder& chf(double amount) {
		return currency(qrinvoice::currency::chf).amount(amount);
	}

	builder& eur() {
		return currency(qrinvoice::currency::eur);
	}

	builder& eur(double amount) {
		return currency(qrinvoice::currency::eur).amount(amount);
	}

	payment_amount_info build() const {
		return payment_amount_info { amount_, currency_ };
	}

private:
	optional_amount		amount_;
	qrinvoice::currency	currency_{ qrinvoice::currency::chf };
};

} // namespace model
} // namespace qrinvoice


#endif // QR_INVOICE_PAYMENT_AMOUNT_INFO_HPP

