
#ifndef QR_INVOICE_SWISS_PAYMENTS_CODE_HPP
#define QR_INVOICE_SWISS_PAYMENTS_CODE_HPP

#include <vector>
#include <array>
#include <set>
#include <string>
#include <qrinvoice/currency.hpp>

#include <qrinvoice/qrinvoicedefs.hpp>

namespace qrinvoice {
namespace model {

namespace spc {
	/**
	 * Swiss Payments Code
	 */
	constexpr const char* qr_type = "SPC";

	/**
	 * Version 2.00
	 */
	constexpr const char* version = "0200";

	/**
	 * Coding t = Latin Character Set
	 */
	constexpr short coding_type{1};

	/**
	 * Unambiguous indicator for the end of payment data. Fixed value "EPD" (End Payment Data). - according to the spec 4.3.3 - Data elements in the QR-bill - v2.0
	 */
	constexpr const char* end_payment_data_trailer = "EPD";


	/**
	 * Acording to the spec 4.2.3 - v2.0
	 */
	constexpr const char* element_separator = "\n";
	constexpr const char* element_separator_pattern = R"(\r\n|\n|\r)";


	/**
	 * Only CHF and EUR are supported according to the spec 4.3.2 - v2.0
	 */
	constexpr std::array<currency, 2> supported_currencies = {{ currency::chf, currency::eur }};

	/**
	 * Only the latin character set is supported, UTF-8 should be used for encoding - according to the spec 4.2.1 - Character set - v2.0
	 */
	// TODO check
	constexpr const char* encoding = "UTF-8";

	constexpr const char* valid_characters = R"( !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~ ¡¢£¤¥¦§¨©ª«¬\u00AD®¯°±²³´µ¶·¸¹º»¼½¾¿ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõö÷øùúûüýþÿĀāĂăĄąĆćĈĉĊċČčĎďĐđĒēĔĕĖėĘęĚěĜĝĞğĠġĢģĤĥĦħĨĩĪīĬĭĮįİıĲĳĴĵĶķĸĹĺĻļĽľĿŀŁłŃńŅņŇňŉŊŋŌōŎŏŐőŒœŔŕŖŗŘřŚśŜŝŞşŠšŢţŤťŦŧŨũŪūŬŭŮůŰűŲųŴŵŶŷŸŹźŻżŽžȘșȚț€)";

	constexpr double amount_min = 0.0;
	constexpr double amount_max = 999999999.99;

	constexpr const char* country_code_switzerland = "CH";
	constexpr const char* country_code_liechtenstein = "LI";
	const std::set<std::string> country_codes_ch_li {country_code_switzerland, country_code_liechtenstein};
	const std::set<std::string> supported_iban_countries = country_codes_ch_li;


	/**
	 * Alternative scheme parameters - Can be currently delivered a maximum of two times according to the spec 4.3.1 - Depiction conventions - v2.0
	 */
	constexpr int max_alt_pmt = 2;

	/**
	 * The maximum Swiss QR Code data content permitted is 997 characters (including the element separators) according to the spec 5.2 - Maximum data range and QR code version - v2.0
	 */
	constexpr int swiss_payments_code_max_length = 997;

}

struct swiss_payments_code {
	// Header
	std::string qr_type;
	std::string version;
	std::string coding;

	// CdtrInf
	std::string iban;

	std::string cr_adr_tp;
	std::string cr_name;
	std::string cr_strt_nm_or_adr_line1;
	std::string cr_bldg_nb_or_adr_line2;
	std::string cr_pstd_cd;
	std::string cr_twn_nm;
	std::string cr_ctry;

	// UltmtCdtr
	std::string ucr_adr_tp;
	std::string ucr_name;
	std::string ucr_strt_nm_or_adr_line1;
	std::string ucr_bldg_nb_or_adr_line2;
	std::string ucr_pstd_cd;
	std::string ucr_twn_nm;
	std::string ucr_ctry;

	// CcyAmd
	std::string amt;
	std::string ccy;

	// UltmtDbtr
	std::string ud_adr_tp;
	std::string ud_name;
	std::string ud_strt_nm_or_adr_line1;
	std::string ud_bldg_nb_or_adr_line2;
	std::string ud_pstd_cd;
	std::string ud_twn_nm;
	std::string ud_ctry;

	// RmtInf
	std::string tp;
	std::string ref;

	// RmtInf/AddInf
	std::string ustrd;
	std::string trailer;
	std::string strd_bkg_inf;

	// AltPmtInf
	std::vector<std::string> alt_pmts;

	QRINVOICE_API std::string to_swiss_payments_code_string() const;

private:
	static void append_field(std::string& str, const std::string& field, bool append_separator = true);
	static void append_alt_pmts(std::string& str, const std::vector<std::string>& alt_pmts);
	static void assert_no_whitespaces(const std::string& str, const std::string& field);
};

bool operator == (const swiss_payments_code& lhs, const swiss_payments_code& rhs);
bool operator != (const swiss_payments_code& lhs, const swiss_payments_code& rhs);

// According to the spec 4.2.4 - Delivery of the data elements - v2.0:
// The last data element delivered may not be completed with a concluding carriage return (CR + LF or LF).
inline void swiss_payments_code::append_alt_pmts(std::string& str, const std::vector<std::string>& alt_pmts)
{
	for (auto it = alt_pmts.begin(); it != alt_pmts.end();) {
		append_field(str, *it, it + 1 != alt_pmts.end());
		++it;
	}
}


} // model
} // qrinvoice


#endif // QR_INVOICE_SWISS_PAYMENTS_CODE_HPP

