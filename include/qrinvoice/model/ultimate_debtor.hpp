#include <utility>


#ifndef QR_INVOICE_ULTIMATE_DEBTOR_HPP
#define QR_INVOICE_ULTIMATE_DEBTOR_HPP

#include <qrinvoice/model/address.hpp>


namespace qrinvoice {
namespace model {

/**
 * <p>From the specification v2.0</p>
 * <table border="1" summary="Excerpt from the specification">
 * <tr><th>Language</th><th>General Definition</th><th>Field Definition</th></tr>
 * <tr><td>EN</td><td>Ultimate debtor<br>Debtor</td><td>Optional data group</td></tr>
 * <tr><td>DE</td><td>Endgültiger Zahlungspflichtiger<br>Zahlungspflichtiger</td><td>Optionale Datengruppe</td></tr>
 * <tr><td>FR</td><td>Débiteur final<br>Débiteur</td><td>Groupe de données optionnel</td></tr>
 * </table>
 * <p>Data Structure Element</p>
 * <pre>
 * QRCH
 * +UltmtDbtr
 * </pre>
 */
class ultimate_debtor {
public:
	ultimate_debtor() = default;
	explicit ultimate_debtor(address address)
		:address_{std::move(address)} {}

	class builder;

public:
	address& get_address() {
		return address_;
	}

	const address& get_address() const {
		return address_;
	}

	void set_address(const address& addr) {
		address_ = addr;
	}

	bool empty() const {
		return address_.empty();
	}

private:
	address address_;
};

class ultimate_debtor::builder {
public:
	builder& address(const address& address) {
		address_ = address;
		return *this;
	}

	ultimate_debtor build() const {
		return ultimate_debtor{ address_ };
	}

private:
	class address address_;
};

} // namespace model
} // namespace qrinvoice


#endif	// QR_INVOICE_ULTIMATE_DEBTOR_HPP

