
#ifndef QR_INVOICE_PARSE_EXCEPTION_HPP
#define QR_INVOICE_PARSE_EXCEPTION_HPP

#include <qrinvoice/base_exception.hpp>


namespace qrinvoice {
namespace model {

class parse_exception : public qrinvoice::base_exception {
public:
	explicit parse_exception(const std::string& msg) : base_exception(msg) {}
};

} // namespace model
} // namespace qrinvoice


#endif // QR_INVOICE_PARSE_EXCEPTION_HPP

