#include <utility>


#ifndef QR_INVOICE_CREDITOR_HPP
#define QR_INVOICE_CREDITOR_HPP

#include <qrinvoice/model/address.hpp>


namespace qrinvoice {
namespace model {

/**
 * <p>From the specification v2.0</p>
 * <table border="1" summary="Excerpt from the specification">
 * <tr><th>Language</th><th>General Definition</th><th>Field Definition</th></tr>
 * <tr><td>EN</td><td>Creditor<br>Creditor</td><td>Mandatory data group</td></tr>
 * <tr><td>DE</td><td>Zahlungsempfänger<br>Zahlungsempfänger</td><td>Obligatorische Datengruppe</td></tr>
 * <tr><td>FR</td><td>Bénéficiaire<br>Bénéficiaire</td><td>Groupe de données obligatoire</td></tr>
 * </table>
 * <p>Data Structure Element</p>
 * <pre>
 * QRCH
 * +CdtrInf
 * ++Cdtr
 * </pre>
 */
class creditor {
public:
	creditor() = default;
	explicit creditor(address address)
		:address_{std::move(address)} {}

	class builder;

public:
	const address& get_address() const {
		return address_;
	}

	address& get_address() {
		return address_;
	}

	void set_address(const address& addr) {
		address_ = addr;
	}

	bool empty() const {
		return address_.empty();
	}

private:
	address address_;
};

class creditor::builder {
public:
	builder& address(const address& address) {
		address_ = address;
		return *this;
	}

	creditor build() const {
		return creditor{ address_ };
	}

private:
	class address address_;
};

} // namespace model
} // namespace qrinvoice


#endif	// QR_INVOICE_CREDITOR_HPP

