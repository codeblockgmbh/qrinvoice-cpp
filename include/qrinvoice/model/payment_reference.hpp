#include <utility>

#include <utility>


#ifndef QR_INVOICE_PAYMENT_REFERENCE_HPP
#define QR_INVOICE_PAYMENT_REFERENCE_HPP

#include <string>
#include <qrinvoice/model/reference_type.hpp>
#include <qrinvoice/model/additional_information.hpp>

#include <qrinvoice/util/reference_util.hpp>

namespace qrinvoice {
namespace model {

/**
 * <p>From the specification v2.0</p>
 * <table border="1" summary="Excerpt from the specification">
 * <tr><th>Language</th><th>General Definition</th><th>Field Definition</th></tr>
 * <tr><td>EN</td><td>Payment reference</td><td>Mandatory data group</td></tr>
 * <tr><td>DE</td><td>Zahlungsreferenz</td><td>Obligatorische Datengruppe</td></tr>
 * <tr><td>FR</td><td>Référence de paiement</td><td>Groupe de données obligatoire</td></tr>
 * </table>
 * <p>Data Structure Element</p>
 * <pre>
 * QRCH
 * +RmtInf
 * </pre>
 */
class payment_reference {
public:
	payment_reference() = default;
	payment_reference(reference_type ref_type, std::string ref, additional_information additional_information)
		:ref_type_{ref_type}
		,reference_{std::move(ref)}
		,additional_information_{std::move(additional_information)} {}

	class builder;

public:
	/**
      * <p>From the specification v2.0</p>
      * <table border="1" summary="Excerpt from the specification">
      * <tr><th>Language</th><th>General Definition</th><th>Field Definition</th></tr>
      * <tr><td>EN</td><td>Reference type<br>Reference type (QR, ISO)<br>The following codes are permitted:<br>QRR – QR reference<br>SCOR – Creditor Reference (ISO 11649)<br>NON – without reference</td><td>Maximum four characters, alphanumeric<br>Must contain the code QRR where a QR-IBAN is used;<br>where the IBAN is used, either the SCOR or NON code can be entered</td></tr>
      * <tr><td>DE</td><td>Referenztyp<br>Referenztyp (QR, ISO)<br>Die folgenden Codes sind zugelassen:<br>QRR – QR-Referenz<br>SCOR – Creditor Reference (ISO 11649)<br>NON – ohne Referenz</td><td>Maximal vier Zeichen, alphanumerisch;<br>Muss bei Verwendung einer QR-IBAN den Code QRR enthalten;<br>bei Verwendung der IBAN kann entweder der Code SCOR oder NON angegeben werden</td></tr>
      * <tr><td>FR</td><td>Type de référence<br>Type de référence (QR, ISO)<br>Les codes suivants sont admis:<br>QRR – Référence QR<br>SCOR – Creditor Reference (ISO 11649) NON – sans référence</td><td>Quatre caractères au maximum; En cas d'utilisation d'un QR-IBAN, doit contenir le code QRR;<br>en cas d'utilisation de l'IBAN, il est possible d'indiquer soit le code SCOR, soit le code NON.</td></tr>
      * </table>
      * <p>Status: {@link Mandatory}</p>
      * <p>Data Structure Element</p>
      * <pre>
      * QRCH
      * +RmtInf
      * ++Tp
      * </pre>
      *
      * @see ReferenceType#QR_REFERENCE
      * @see ReferenceType#CREDITOR_REFERENCE
      * @see ReferenceType#WITHOUT_REFERENCE
      */
	reference_type get_reference_type() const {
		return ref_type_;
	}

	void set_reference_type(reference_type::ref_type ref_type) {
		set_reference_type(reference_type(ref_type));
	}

	void set_reference_type(reference_type rt) {
		ref_type_ = rt;
	}

	/**
     * <p>From the specification v2.0</p>
     * <table border="1" summary="Excerpt from the specification">
     * <tr><th>Language</th><th>General Definition</th><th>Field Definition</th></tr>
     * <tr><td>EN</td><td>Reference<br>Reference number<br>Structured payment reference<br>Note: The reference is either a QR reference or a Creditor Reference (ISO 11649)</td><td>Maximum 27 characters, alphanumeric;<br>must be filled if a QR-IBAN is used.<br>QR reference: 27 characters, numeric, check sum calculation according to Modulo 10 recursive (27th position of the reference)<br>Creditor Reference (ISO 11649): max 25 characters, alphanumeric<br>The element may not be filled for the NON reference type</td></tr>
     * <tr><td>DE</td><td>Referenz<br>Referenznummer<br>Strukturierte Zahlungsreferenz<br>Anmerkung: Die Referenz ist entweder eine QR-Referenz oder Creditor Reference (ISO 11649)</td><td>Maximal 27 Zeichen, alphanumerisch;<br>Muss bei Verwendung einer QR-IBAN befüllt werden.<br>QR-Referenz: 27 Zeichen, numerisch, Prüfzifferberechnung nach Modulo 10 rekursiv (27. Stelle der Referenz)<br>Creditor Reference (ISO 11649): bis 25 Zeichen, alphanumerisch.<br>Für den Referenztyp NON darf das Element nicht befüllt werden.</td></tr>
     * <tr><td>FR</td><td>Référence<br>Numéro de référence<br>Référence de paiement structurée<br>Remarque: La référence est soit une référence QR, soit une Creditor Reference (ISO 11649)</td><td>27 caractères alphanumériques au maximum; doit être rempli en cas d'utilisation d'un QR-IBAN.<br>Référence QR: 27 caractères numériques, calcul du chiffre de contrôle selon modulo 10 récursif (27e position de la référence).<br>Creditor Reference (ISO 11649): jusqu'à 25 caractères alphanumériques.<br>L'élément ne doit pas être rempli pour le type de référence NON.</td></tr>
     * </table>
     * <p>Status: {@link Optional}</p>
     * <p>Data Structure Element</p>
     * <pre>
     * QRCH
     * +RmtInf
     * ++Ref
     * </pre>
     */
	const std::string& get_reference() const {
		return reference_;
	}

	void set_reference(const std::string& ref) {
		reference_ = ref;
	}

	const additional_information& get_additional_information() const {
		return additional_information_;
	}

	additional_information& get_additional_information() {
		return additional_information_;
	}

	void set_additional_information(const additional_information& additional_information) {
		additional_information_ = additional_information;
	}

	bool empty() const {
		return ref_type_.get_reference_type_code() == nullptr &&
			reference_.empty() &&
			additional_information_.empty();
	}

private:
	reference_type		ref_type_;
	std::string		reference_;
	additional_information		additional_information_;
};

class payment_reference::builder {
public:
	builder& reference_type(reference_type reference_type) {
		reference_type_ = reference_type;
		return *this;
	}

	builder& reference(const std::string& reference) {
		reference_ = reference;
		return *this;
	}

	builder& qr_reference(const std::string& reference) {
		return reference_type(qrinvoice::model::reference_type(reference_type::qr_reference)).reference(reference);
	}

	builder& creditor_reference(const std::string& reference) {
		return reference_type(qrinvoice::model::reference_type(reference_type::creditor_reference)).reference(reference);
	}

	builder& without_reference() {
		return reference_type(qrinvoice::model::reference_type(reference_type::without_reference));
	}

	builder& additional_information(const additional_information& additional_information) {
		additional_information_ = additional_information;
		return *this;
	}

	payment_reference build() const {
		return payment_reference {
			reference_type_, qrinvoice::reference_util::normalize_reference(reference_type_.get(), reference_), additional_information_
		};
	}

private:
	class reference_type	reference_type_;
	std::string		reference_;
	class additional_information	additional_information_;
};

} // namespace model
} // namespace qrinvoice


#endif	// QR_INVOICE_PAYMENT_REFERENCE_HPP

