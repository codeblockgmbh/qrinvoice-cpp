
#ifndef QR_INVOICE_SWISS_PAYMENTS_CODE_TO_MODEL_HPP
#define QR_INVOICE_SWISS_PAYMENTS_CODE_TO_MODEL_HPP

#include <qrinvoice/model/qr_invoice.hpp>
#include <qrinvoice/model/swiss_payments_code.hpp>


namespace qrinvoice {
namespace model {


QRINVOICE_API auto map_swiss_payments_code_to_model(const swiss_payments_code& spc) -> qr_invoice;


} // namespace model
} // namespace qrinvoice


#endif	// QR_INVOICE_SWISS_PAYMENTS_CODE_TO_MODEL_HPP

