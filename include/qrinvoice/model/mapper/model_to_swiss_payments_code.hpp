
#ifndef QR_INVOICE_MODEL_TO_SWISS_PAYMENTS_CODE_HPP
#define QR_INVOICE_MODEL_TO_SWISS_PAYMENTS_CODE_HPP

#include <qrinvoice/model/qr_invoice.hpp>
#include <qrinvoice/model/swiss_payments_code.hpp>


namespace qrinvoice {
namespace model {


QRINVOICE_API auto map_model_to_swiss_payments_code(const qr_invoice& qr_invoice) -> swiss_payments_code;


} // namespace model
} // namespace qrinvoice


#endif	// QR_INVOICE_MODEL_TO_SWISS_PAYMENTS_CODE_HPP


