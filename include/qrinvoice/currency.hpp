
#ifndef QR_INVOICE_CURRENCY_HPP
#define QR_INVOICE_CURRENCY_HPP

#include <iostream>

#include <qrinvoice/qrinvoicedefs.hpp>


namespace qrinvoice {


enum class currency {
	chf, eur, invalid
};


namespace currency_ops {

QRINVOICE_API std::string get_currency_code(currency curr);
QRINVOICE_API currency parse_currency(const std::string& str);

} // namespace currency_ops


inline std::ostream& operator << (std::ostream& os, const currency& currency)
{
	return os << currency_ops::get_currency_code(currency);
}


} // namespace qrinvoice


#endif // QR_INVOICE_CURRENCY_HPP

