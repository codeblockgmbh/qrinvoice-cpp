#include <utility>


#ifndef QR_INVOICE_CODE_CREATOR_HPP
#define QR_INVOICE_CODE_CREATOR_HPP

#include <string>
#include <qrinvoice/qrinvoicedefs.hpp>
#include <qrinvoice/model/qr_invoice.hpp>
#include <qrinvoice/output_format.hpp>
#include <qrinvoice/output/qr_code.hpp>


namespace qrinvoice {


class code_creator {
public:
	code_creator() = default;
	code_creator(model::qr_invoice qr_invoice, unsigned desired_size, output_format fmt)
		:qr_invoice_{std::move(qr_invoice)}
		,desired_qr_code_size_{ desired_size }
		,output_format_{ fmt } {}

public:
	code_creator& qr_invoice(const model::qr_invoice& qr_invoice) {
		qr_invoice_ = qr_invoice;
		return *this;
	}

	code_creator& output_format(const output_format output_format) {
		output_format_ = output_format;
		return *this;
	}

	/**
	 * @param desired_qr_code_size in pixels
	 * @return The {@link code_creator} instance
	 */
	code_creator& desired_qr_code_size(unsigned desired_qr_code_size) {
		desired_qr_code_size_ = desired_qr_code_size;
		return *this;
	}

	QRINVOICE_API std::string create_swiss_payments_code() const;
	QRINVOICE_API qrinvoice::output::qr_code create_qr_code() const;

private:
	model::qr_invoice		qr_invoice_;
	unsigned			desired_qr_code_size_ = 0;
        qrinvoice::output_format	output_format_ = qrinvoice::output_format::png;
};


} // namespace qrinvoice


#endif // QR_INVOICE_CODE_CREATOR_HPP

