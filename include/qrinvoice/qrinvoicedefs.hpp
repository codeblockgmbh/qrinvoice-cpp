
#ifndef QR_INVOICE_QRINVOICEDEFS_HPP
#define QR_INVOICE_QRINVOICEDEFS_HPP


#if defined _WIN32 || defined __CYGWIN__	// Windows environment
	#define QRINVOICE_SYMBOL_IMPORT	__declspec(dllimport)
	#define QRINVOICE_SYMBOL_EXPORT	__declspec(dllexport)
	#define QRINVOICE_SYMBOL_LOCAL
#else
	#if defined __clang__ || __GNUC__ >= 4
		#define QRINVOICE_SYMBOL_IMPORT	__attribute__((visibility("default")))
		#define QRINVOICE_SYMBOL_EXPORT	__attribute__((visibility("default")))
		#define QRINVOICE_SYMBOL_LOCAL	__attribute__((visibility("hidden")))
	#else
		#define QRINVOICE_SYMBOL_IMPORT
		#define QRINVOICE_SYMBOL_EXPORT
		#define QRINVOICE_SYMBOL_LOCAL
	#endif
#endif


#ifdef QRINVOICE_DYNAMIC_LINKING		// dynamically linked shared library
	#ifdef QRINVOICE_EXPORTS		// building the library(not using)
		#define QRINVOICE_API	QRINVOICE_SYMBOL_EXPORT
	#else					// using the library(not building)
		#define QRINVOICE_API	QRINVOICE_SYMBOL_IMPORT
	#endif
	#define QRINVOICE_LOCAL	QRINVOICE_SYMBOL_LOCAL
#else						// statically linked library
	#define QRINVOICE_API
	#define QRINVOICE_LOCAL
#endif


#endif // QR_INVOICE_QRINVOICEDEFS_HPP

