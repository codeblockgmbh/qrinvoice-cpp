
#ifndef QR_INVOICE_LOCALE_HPP
#define QR_INVOICE_LOCALE_HPP

namespace qrinvoice {


enum class locale {
	german, french, english, italian
};


namespace locale_ops {

constexpr const char* get_language(locale loc)
{
	return loc == locale::german ? "de"
		:loc == locale::french ? "fr"
		:loc == locale::english ? "en"
		:loc == locale::italian ? "it"
		:"";
}

} // namespace locale_ops
} // namespace qrinvoice


#endif // QR_INVOICE_LOCALE_HPP

