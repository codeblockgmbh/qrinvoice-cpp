
#ifndef QR_INVOICE_BOUNDARY_LINES_HPP
#define QR_INVOICE_BOUNDARY_LINES_HPP

namespace qrinvoice {

enum class boundary_lines {
	none,
	enabled,
	enabled_with_margins
};

} // namespace qrinvoice


#endif // QR_INVOICE_BOUNDARY_LINES_HPP

