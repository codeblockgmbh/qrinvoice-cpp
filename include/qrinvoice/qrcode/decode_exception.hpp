
#ifndef QR_INVOICE_DECODE_EXCEPTION_HPP
#define QR_INVOICE_DECODE_EXCEPTION_HPP

#include <qrinvoice/base_exception.hpp>


namespace qrinvoice {
namespace qrcode {

class decode_exception : public base_exception {
public:
	using base_exception::base_exception;
};

} // namespace qrcode
} // namespace qrinvoice


#endif // QR_INVOICE_DECODE_EXCEPTION_HPP

