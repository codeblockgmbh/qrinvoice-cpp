
#ifndef QR_INVOICE_FONT_STYLE_HPP
#define QR_INVOICE_FONT_STYLE_HPP


namespace qrinvoice {

enum class font_style {
	regular,
	bold
};

} // namespace qrinvoice


#endif // QR_INVOICE_FONT_STYLE_HPP

