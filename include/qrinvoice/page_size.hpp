
#ifndef QR_INVOICE_PAGE_SIZE_HPP
#define QR_INVOICE_PAGE_SIZE_HPP


namespace qrinvoice {

enum class page_size {
	/** A4 portrait (210 x 297 mm) */
	a4,
	/** A5 landscape (210 x 148 mm) */
	a5,
	/** DIN_LANG landscape (210 x 105 mm) */
	din_lang,
	/** DIN_LANG_CROPPED landscape (200 x 100 mm) OR (198 x 99 mm) in case "additional print margin" (6mm instead of 5mm) is requested */
	din_lang_cropped
};

} // namespace qrinvoice


#endif // QR_INVOICE_PAGE_SIZE_HPP

