
#ifndef QR_INVOICE_FONT_FAMILY_HPP
#define QR_INVOICE_FONT_FAMILY_HPP


namespace qrinvoice {

enum class font_family {
	helvetica,
	arial,
	liberation_sans
};

} // namespace qrinvoice


#endif // QR_INVOICE_FONT_FAMILY_HPP

