
#ifndef QR_INVOICE_BASE_EXCEPTION_HPP
#define QR_INVOICE_BASE_EXCEPTION_HPP

#include <stdexcept>


namespace qrinvoice {

class base_exception : public std::runtime_error {
public:
    explicit base_exception(const std::string& msg) : std::runtime_error(msg) {}
};

} // namespace qrinvoice


#endif	// QR_INVOICE_BASE_EXCEPTION_HPP

