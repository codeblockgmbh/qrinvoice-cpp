
#ifndef QR_INVOICE_QR_CODE_HPP
#define QR_INVOICE_QR_CODE_HPP

#include <qrinvoice/output/output.hpp>


namespace qrinvoice {
namespace output {

class qr_code : public output {
public:
	using output::output;
};

} // namespace output
} // namespace qrinvoice


#endif // QR_INVOICE_QR_CODE_HPP

