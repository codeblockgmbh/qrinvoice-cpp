
#ifndef QR_INVOICE_PAYMENT_PART_HPP
#define QR_INVOICE_PAYMENT_PART_HPP

#include <qrinvoice/output/output.hpp>


namespace qrinvoice {
namespace output {

class payment_part_receipt : public output {
public:
	using output::output;
};

} // namespace output
} // namespace qrinvoice


#endif // QR_INVOICE_PAYMENT_PART_HPP

