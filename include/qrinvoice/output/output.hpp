#include <utility>


#ifndef QR_INVOICE_OUTPUT_HPP
#define QR_INVOICE_OUTPUT_HPP

#include <vector>
#include <qrinvoice/output_format.hpp>


namespace qrinvoice {
namespace output {

class output {
public:
	using byte = unsigned char;

public:
	output(output_format format, std::vector<byte> data)
		:format_{ format }
		,data_{std::move(data)} {}

	output(output_format format, const byte* data, size_t size)
		:format_{ format }
		,data_{ data, data + size } {}

	output(const output&) = default;
	output& operator = (const output&) = default;

	output(output&&) = default;
	output& operator = (output&&) = default;

public:
	output_format get_output_format() const {
		return format_;
	}

	size_t get_size() const {
		return data_.size();
	}

	std::vector<byte> get_data() const {
		return data_;
	}

private:
	output_format		format_;
	std::vector<byte>	data_;
};

} // namespace output
} // namespace qrinvoice


#endif // QR_INVOICE_OUTPUT_HPP

