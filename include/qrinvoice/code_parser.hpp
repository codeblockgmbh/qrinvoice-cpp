
#ifndef QR_INVOICE_CODE_PARSER_HPP
#define QR_INVOICE_CODE_PARSER_HPP

#include <string>

#include <qrinvoice/model/qr_invoice.hpp>


namespace qrinvoice {
namespace code_parser {


QRINVOICE_API model::qr_invoice parse(const std::string& spc);
QRINVOICE_API model::qr_invoice parse_and_validate(const std::string& spc);


} // namespace code_parser
} // namespace qrinvoice


#endif //QR_INVOICE_CODE_PARSER_HPP

