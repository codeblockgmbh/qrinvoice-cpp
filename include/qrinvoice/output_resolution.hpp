
#ifndef QR_INVOICE_OUTPUT_RESOLUTION_HPP
#define QR_INVOICE_OUTPUT_RESOLUTION_HPP

#include <type_traits>


namespace qrinvoice {

enum class output_resolution : unsigned {
	low_150_dpi = 150,
	medium_300_dpi = 300,
	high_600_dpi = 600
};

constexpr auto get_resolution_dpi(output_resolution op_res)
	-> std::underlying_type<output_resolution>::type
{
	return static_cast<std::underlying_type<output_resolution>::type>(op_res);
}


} // namespace qrinvoice


#endif // QR_INVOICE_OUTPUT_RESOLUTION_HPP

