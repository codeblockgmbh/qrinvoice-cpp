
#ifndef QR_INVOICE_LAYOUT_EXCEPTION_HPP
#define QR_INVOICE_LAYOUT_EXCEPTION_HPP

#include <qrinvoice/base_exception.hpp>


namespace qrinvoice {
namespace layout {

class layout_exception : public base_exception {
public:
	explicit layout_exception(const std::string& msg) : base_exception(msg) {}
};

} // namespace layout
} // namespace qrinvoice


#endif // QR_INVOICE_LAYOUT_EXCEPTION_HPP

