
#ifndef QR_INVOICE_STRING_UTIL_HPP
#define QR_INVOICE_STRING_UTIL_HPP

#include <sstream>
#include <string>
#include <iomanip>
#include <cctype>
#include <algorithm>
#include <qrinvoice/model/parse_exception.hpp>

#include <qrinvoice/qrinvoicedefs.hpp>

namespace qrinvoice {
namespace string_util {
namespace details {


template <typename T> 
T parse(const std::string& str)
{
	std::istringstream iss{str};
	T t;
	iss >> t;
	if (!iss)
		throw model::parse_exception{"parse error"};
	return t;
}

template <typename T>
std::string to_string(const T& t)
{
	std::ostringstream oss;
	oss << std::setprecision(12) << t;
	return oss.str();
}

QRINVOICE_API std::string remove_white_spaces(const std::string& str);

QRINVOICE_API bool contains_white_space(const std::string& str);

//
// UTF-8 characters can occupy more than a byte, following call to isspace()
// checks only a byte, but it actually works as all the spaces characters in the
// valid-character-set are within ASCII part of UTF-8
//
inline bool is_trimmable(const std::string& str)
{
	return !str.empty() && (std::isspace(str.front()) || std::isspace(str.back()));
}

inline bool is_blank(const std::string& str)
{
    return str.empty() || std::all_of(str.begin(), str.end(), [](unsigned char c){ return std::isspace(c); });
}

inline void to_upper(std::string& str)
{
	std::transform(str.begin(), str.end(), str.begin(), ::toupper);
}

inline void to_lower(std::string& str)
{
	std::transform(str.begin(), str.end(), str.begin(), ::tolower);
}

inline std::string left_pad(const std::string& str, unsigned int size, const char padChar)
{

    if(size < 0) {
        size = 0;
    }

    if(str.length() >= size)
        return str;


    return std::string( size - str.length(), padChar).append(str);
}

} // namespace details
} // namespace string_util
} // namespace qrinvoice


#endif // QR_INVOICE_STRING_UTIL_HPP

