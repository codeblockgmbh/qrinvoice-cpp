
#ifndef QR_INVOICE_CREDITOR_REFERENCE_UTIL_HPP
#define QR_INVOICE_CREDITOR_REFERENCE_UTIL_HPP

#include <qrinvoice/qrinvoicedefs.hpp>

#include <string>
#include <qrinvoice/util/string_util.hpp>


namespace qrinvoice {

/**
 * Util for Creditor Reference according to ISO 11649
 */
namespace creditor_reference_util {

inline std::string normalize_creditor_reference(const std::string& str)
{
    std::string normalized = string_util::details::remove_white_spaces(str);
    string_util::details::to_upper(normalized);
    return normalized;
}

/**
 * @param str a string containing alphanumeric characters, e.g. 1234512345RF45
 * @return the string with numeric characters only, alphanumeric characters were mapped to their ascii code representation e.g. 1234512345271545
 */
std::string convert_to_numeric_representation(std::string str);

std::string calculate_check_digits_for_reference_number(const std::string& input);

std::string calculate_check_digits(const std::string& input);

/**
* @param creditor_reference The Creditor Reference string
* @return true, if the given string is a valid creditor reference
* @see <a href="https://en.wikipedia.org/wiki/Creditor_Reference">Creditor_Reference</a>
*/
QRINVOICE_API bool is_valid_creditor_reference(const std::string& creditor_reference);

/**
 * Formats the Creditor Reference number for output
 * @param creditorReferenceInput e.g. RF451234512345
 * @return formatted for print or display output e.g. RF45 1234 5123 45
 */
QRINVOICE_API std::string format_creditor_reference(const std::string& creditor_reference_input);

/**
 * Creates a normalized Creditor Reference
 *
 * @param creditorReferenceInput A string that may already be a valid Creditor Reference number or just any number (like invoice nr) that needs to be converted
 * @return The normalized Creditor Reference (e.g. RF451234512345)
 */
QRINVOICE_API std::string create_creditor_reference(const std::string& creditor_reference_input);


} // namespace creditor_reference_util
} // namespace qrinvoice


#endif // QR_INVOICE_CREDITOR_REFERENCE_UTIL_HPP

