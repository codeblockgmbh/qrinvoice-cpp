
#ifndef QR_INVOICE_CHARSET_ENCODING_UTIL_HPP
#define QR_INVOICE_CHARSET_ENCODING_UTIL_HPP

#include <string>
#include <qrinvoice/qrinvoicedefs.hpp>

namespace qrinvoice {
namespace charset_encoding_util {

QRINVOICE_API std::string to_utf8(const std::string &input, const std::string &input_charset);

} // namespace charset_encoding_util
} // namespace qrinvoice


#endif // QR_INVOICE_CHARSET_ENCODING_UTIL_HPPlo

