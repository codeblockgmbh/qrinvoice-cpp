
#ifndef QR_INVOICE_COUNTRY_UTIL_HPP
#define QR_INVOICE_COUNTRY_UTIL_HPP

#include <qrinvoice/qrinvoicedefs.hpp>
#include <string>
#include <unordered_map>


namespace qrinvoice {
namespace country_util {

QRINVOICE_API bool is_valid_iso_code(const std::string&);
const std::unordered_map<std::string, std::string> init_country_map() noexcept;

} // namespace country_util
} // namespace qrinvoice


#endif // QR_INVOICE_COUNTRY_UTIL_HPP

