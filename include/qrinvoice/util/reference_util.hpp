
#ifndef QR_INVOICE_REFERENCE_UTIL_HPP
#define QR_INVOICE_REFERENCE_UTIL_HPP

#include <string>

#include <qrinvoice/model/reference_type.hpp>
#include <qrinvoice/util/string_util.hpp>

namespace qrinvoice {

namespace reference_util {

std::string format_reference(const model::reference_type::ref_type& reference_type , const std::string& reference);

std::string normalize_reference(const model::reference_type::ref_type& reference_type , const std::string& reference);

} // namespace reference_util
} // namespace qrinvoice


#endif // QR_INVOICE_REFERENCE_UTIL_HPP

