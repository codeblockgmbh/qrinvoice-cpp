
#ifndef QR_INVOICE_QR_REFERENCE_UTIL_HPP
#define QR_INVOICE_QR_REFERENCE_UTIL_HPP

#include <qrinvoice/qrinvoicedefs.hpp>
#include <string>

#include <qrinvoice/util/string_util.hpp>

namespace qrinvoice {

/**
 * Util for QR-reference (former ISR reference number)
 */
namespace qr_reference_util {

inline std::string normalize_qr_reference(const std::string& str)
{
    return string_util::details::remove_white_spaces(str);
}

/**
* QR reference: 27 characters, numeric, check sum calculation according to Modulo 10 recursive (27th position of the reference)
*
* @param qr_reference The QR Reference string
* @return true, if the given string is a valid QR Reference Number
* @see <a href="https://www.iso-20022.ch/lexikon/strukturierte-referenz/">QR Reference</a>
*/

QRINVOICE_API bool is_valid_qr_reference(const std::string& qr_reference);

/**
 * A string that may already be a valid QR Reference number or just any number (like invoice nr) that needs to be converted
 *
 * @param qr_reference_input A string that may already be a valid QR Reference number or just any number (like invoice nr) that needs to be converted
 * @return A string that may already be a valid QR Reference number or just any number (like invoice nr) that needs to be converted
 */
QRINVOICE_API std::string create_qr_reference(const std::string& qr_reference_input);

/**
 * Creates a normalized QR Reference from customer Id and reference number
 *
 * @param customer_id A string representing the banks customer id number
 * @param qr_reference_input Any number (like invoice nr)
 * @return The normalized QR Reference (e.g. 110001234560000000000813457)
 */
QRINVOICE_API std::string create_qr_reference(const std::string& customer_id, const std::string& qr_reference_input);

int modulo_10_recursive(const std::string& number);

/**
 * Formats the QR Reference number for output
 * @param qr_reference_input e.g. "110001234560000000000813457" or "11 00012 34560 00000 00008 13457"
 * @return formatted for print or display output, eg 11 00012 34560 00000 00008 13457
 */
QRINVOICE_API std::string format_qr_reference(const std::string& qr_reference_input);

} // namespace qr_reference_util
} // namespace qrinvoice


#endif // QR_INVOICE_QR_REFERENCE_UTIL_HPP

