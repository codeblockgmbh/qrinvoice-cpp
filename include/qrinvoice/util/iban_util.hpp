
#ifndef QR_INVOICE_IBAN_UTIL_HPP
#define QR_INVOICE_IBAN_UTIL_HPP

#include <string>
#include <qrinvoice/qrinvoicedefs.hpp>
#include <qrinvoice/util/string_util.hpp>


namespace qrinvoice {
namespace iban_util {

inline std::string normalize_iban(const std::string& str)
{
	return string_util::details::remove_white_spaces(str);
}

QRINVOICE_API std::string format_iban(const std::string& iban_input);

/**
* @param iban           The IBAN to validate
* @param validate_country_code true, if the country code should be validated. If true, it checks that only supported country codes are considered valid
* @return true, if the given IBAN number is valid, false otherwise
* @see <a href="https://en.wikipedia.org/wiki/International_Bank_Account_Number#Validating_the_IBAN">Validating_the_IBAN</a>
*/
QRINVOICE_API bool is_valid_iban(const std::string& iban, bool validate_country_code);

// currently this can lead to validate the iban twice, maybe this should be refactory to parse the iban and work on an IBAN object to avoid double validation and parsing
QRINVOICE_API bool is_qr_iban(const std::string& iban);

} // namespace iban_util
} // namespace qrinvoice


#endif // QR_INVOICE_IBAN_UTIL_HPP

