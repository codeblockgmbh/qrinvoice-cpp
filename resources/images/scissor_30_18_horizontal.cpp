static const unsigned char scissor_30x18_horizontal[18][30] = {
	{0xff, 0xff, 0xe9, 0xc6, 0xc7, 0xee, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff},
	{0xff, 0xb9, 0x55, 0xa9, 0x9f, 0x3d, 0xca, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff},
	{0xe0, 0x5c, 0xf6, 0xff, 0xff, 0xe2, 0x22, 0xf0, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff},
	{0xc5, 0xa5, 0xff, 0xff, 0xff, 0xff, 0x78, 0xdb, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfb, 0xd2, 0x98, 0x53, 0x22, 0x73, 0xc1, 0xfe},
	{0xd8, 0x7a, 0xff, 0xff, 0xff, 0xff, 0x69, 0xeb, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xe4, 0xaa, 0x45, 0x0, 0x0, 0x0, 0x0, 0x3d, 0xa7, 0xf3},
	{0xfe, 0x7d, 0x9e, 0xee, 0xf7, 0xb6, 0x5c, 0xf8, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xf3, 0xc2, 0x6e, 0x0, 0x0, 0x0, 0x0, 0x0, 0x7f, 0xcb, 0xf9, 0xff, 0xff},
	{0xff, 0xf8, 0xab, 0x60, 0x49, 0x49, 0x49, 0x3b, 0x6c, 0xa1, 0xc7, 0xef, 0xff, 0xff, 0xfc, 0xd6, 0x94, 0x1c, 0x0, 0x0, 0x0, 0x0, 0x49, 0xaf, 0xea, 0xff, 0xff, 0xff, 0xff, 0xff},
	{0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xed, 0xad, 0xd, 0x0, 0x8f, 0xad, 0x4d, 0x0, 0x0, 0x0, 0x0, 0xd, 0x89, 0xd2, 0xfb, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff},
	{0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xc0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x56, 0xb6, 0xee, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff},
	{0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xc6, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x60, 0xb9, 0xee, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff},
	{0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfa, 0xbe, 0x1c, 0x0, 0x83, 0x8f, 0x1c, 0x0, 0x0, 0x0, 0x0, 0xd, 0x85, 0xcd, 0xf9, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff},
	{0xff, 0xff, 0xe7, 0xaf, 0x8d, 0x7e, 0x8b, 0x70, 0x40, 0x85, 0xbb, 0xee, 0xff, 0xff, 0xf5, 0xc8, 0x83, 0xd, 0x0, 0x0, 0x0, 0x0, 0x38, 0xa2, 0xde, 0xfe, 0xff, 0xff, 0xff, 0xff},
	{0xff, 0xc5, 0x5d, 0xcb, 0xd9, 0x94, 0x2a, 0xe4, 0xfd, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xf1, 0xc0, 0x76, 0x0, 0x0, 0x0, 0x0, 0x0, 0x60, 0xb9, 0xee, 0xff, 0xff},
	{0xf4, 0x3b, 0xee, 0xff, 0xff, 0xfd, 0x6c, 0xee, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xeb, 0xb9, 0x68, 0x0, 0x0, 0x0, 0x0, 0xd, 0x85, 0xeb},
	{0xdb, 0x84, 0xff, 0xff, 0xff, 0xff, 0x97, 0xd0, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xe5, 0xb0, 0x79, 0x5d, 0x81, 0xcd, 0xff},
	{0xec, 0x5d, 0xfc, 0xff, 0xff, 0xfb, 0x56, 0xdb, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff},
	{0xff, 0x9d, 0x7f, 0xcf, 0xce, 0x82, 0x89, 0xfe, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff},
	{0xff, 0xfc, 0xc5, 0x9d, 0x9b, 0xc8, 0xfb, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff}
};
