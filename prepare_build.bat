mkdir build
cd build
cmake .. -G "Visual Studio 15 2017" -DSPDLOG_ROOT=C:/dev/git/spdlog -DSTB_ROOT=C:/dev/git/stb -DBOOST_ROOT=C:/boost/boost_1_68_0 -DBOOST_INCLUDEDIR=C:/boost/boost_1_68_0 -DBOOST_LIBRARYDIR=C:/boost/boost_1_68_0/lib -DCPPCODED_ROOT=C:\dev\git\cppcodec

cd ..