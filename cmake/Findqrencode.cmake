
# CMake find module for libqrencode

# Following variables are set
# qrencode_FOUND		# TRUE if all components are found
# qrencode_INCLUDE_DIRS		# include directory paths
# qrencode_LIBRARIES		# library paths

set( LIB_QRENCODE_SRC_DIR "${PROJECT_SOURCE_DIR}/../libqrencode/" )

if( CMAKE_SIZEOF_VOID_P EQUAL 8 )
	#set( bit "64" )
	set( LIB_QRENCODE_RELEASE_DIR "${LIB_QRENCODE_SRC_DIR}build64/Release/" )
else( CMAKE_SIZEOF_VOID_P EQUAL 8 )
	#set( bit "32" )
	set( LIB_QRENCODE_RELEASE_DIR "${LIB_QRENCODE_SRC_DIR}build/Release/" )
endif( CMAKE_SIZEOF_VOID_P EQUAL 8 )

# finding header files
find_path(qrencode_INCLUDE_DIR NAMES qrencode.h
	HINTS
		"${LIB_QRENCODE_SRC_DIR}")

# finding the library
find_library(qrencode_LIBRARY NAMES qrencode
	HINTS
		"${LIB_QRENCODE_RELEASE_DIR}")

include(FindPackageHandleStandardArgs)

find_package_handle_standard_args(qrencode
	DEFAULT_MSG qrencode_LIBRARY qrencode_INCLUDE_DIR)


if (qrencode_FOUND)
	set(qrencode_INCLUDE_DIRS	${qrencode_INCLUDE_DIR})
	set(qrencode_LIBRARIES		${qrencode_LIBRARY})
endif()

mark_as_advanced(
	${qrencode_FOUND}
	${qrencode_INCLUDE_DIR}
	${qrencode_LIBRARY})

