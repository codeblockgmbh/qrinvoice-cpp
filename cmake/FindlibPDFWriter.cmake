
# CMake find module for libPDFWriter

# Following variables are set
# libPDFWriter_FOUND		# TRUE if all components are found
# libPDFWriter_INCLUDE_DIRS	# include directory paths
# libPDFWriter_LIBRARIES	# library paths

set( PDF_Writer_DIR "deps/PDF-Writer/" )

if( CMAKE_SIZEOF_VOID_P EQUAL 8 )
	#set( bit "64" )
	set( PDF_Writer_BUILD_DIR "${PDF_Writer_DIR}/build64/" )
else( CMAKE_SIZEOF_VOID_P EQUAL 8 )
	#set( bit "32" )
	set( PDF_Writer_BUILD_DIR "${PDF_Writer_DIR}/build/" )
endif( CMAKE_SIZEOF_VOID_P EQUAL 8 )

# find header files
find_path(libPDFWriter_INCLUDE_DIR NAMES PDFWriter/PDFWriter.h
	HINTS
		"${PDF_Writer_DIR}")

# find library path
find_library(libPDFWriter_LIBRARY NAMES PDFWriter libPDFWriter
	HINTS
	"${PDF_Writer_BUILD_DIR}/PDFWriter/Release/"
	"${PDF_Writer_BUILD_DIR}/PDFWriter/Debug/"
	"${PDF_Writer_BUILD_DIR}/PDFWriter/")

find_package_handle_standard_args(libPDFWriter
		DEFAULT_MSG libPDFWriter_LIBRARY libPDFWriter_INCLUDE_DIR)

# find library path
find_library(FreeType_LIBRARY NAMES FreeType
		HINTS
		"${PDF_Writer_BUILD_DIR}/FreeType/Release/"
		"${PDF_Writer_BUILD_DIR}/FreeType/Debug/"
		"${PDF_Writer_BUILD_DIR}/FreeType/")

if (libPDFWriter_FOUND)
	set(libPDFWriter_INCLUDE_DIRS	${libPDFWriter_INCLUDE_DIR})
	set(libPDFWriter_LIBRARIES	${libPDFWriter_LIBRARY})

	set(libPDFWriter_freetype_INCLUDE_DIRS	"${PDF_Writer_DIR}/FreeType/include")
	set(libPDFWriter_freetype_LIBRARIES	${FreeType_LIBRARY})
endif()

mark_as_advanced(
	${libPDFWriter_FOUND}
	${libPDFWriter_INCLUDE_DIR}
	${libPDFWriter_LIBRARY})

