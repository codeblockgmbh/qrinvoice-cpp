
//#define _X86_
#define _AMD64_


#include <string>
#include <iostream>
#include <fstream>
#include <processthreadsapi.h>
#include <handleapi.h>

#include <windows.h>

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd)
try {
    int argc = __argc;
    char **argv = __argv;

    const bool pWait = true;

    //const auto pAppName = argv[1];
    const auto pAppName = "qrinvoice_ppr.exe -b beispiel_daten.json -i DIN_LANG -l 300 -f JPG -e qrinvoice.jpg -y qrinvoice.log -x DEBUG";
    //if (FindExecutable((LPCTSTR)sTmpName, "", (LPTSTR)sFName) >= (HINSTANCE)32) {

        DWORD                sExitCode;
        STARTUPINFO          sProcInfo;
        PROCESS_INFORMATION  sDosProcess;

        sExitCode = 0;
        memset(&sProcInfo, 0, sizeof(sProcInfo));
        memset(&sDosProcess, 0, sizeof(sDosProcess));
        sProcInfo.cb          = sizeof(sProcInfo);

        if (CreateProcess(NULL, (LPTSTR)pAppName, NULL, NULL, TRUE, NORMAL_PRIORITY_CLASS,
                          NULL, NULL, &sProcInfo, &sDosProcess)) {
            
            if (pWait) {
                WaitForSingleObject(sDosProcess.hProcess,INFINITE);
                GetExitCodeProcess(sDosProcess.hProcess,&sExitCode);
            };
            CloseHandle(sDosProcess.hThread);
            CloseHandle(sDosProcess.hProcess);
            return sExitCode == 0;
        };
    //}
    return -1;
}
catch (const std::exception &x) {
    std::cerr << x.what() << std::endl;
    MessageBox( NULL, x.what(), "QR Invoice", MB_OK | MB_ICONINFORMATION);
    return 2;
}
catch (...) {
    MessageBox( NULL, "error!", "QR Invoice", MB_OK | MB_ICONINFORMATION);
    return 1;
}

