
#include <vector>
#include <qrinvoice/util/creditor_reference_util.hpp>
#include <qrinvoice/util/qr_reference_util.hpp>
#include <qrinvoice/util/iban_util.hpp>

#include <iostream>
#include <fstream>

#include <qrinvoice/model/qr_invoice.hpp>
#include <qrinvoice/model/validation.hpp>
#include <qrinvoice/layout/layout_exception.hpp>

#include <qrinvoice/output/output.hpp>
#include <qrinvoice/output/payment_part_receipt.hpp>
#include <qrinvoice/code_creator.hpp>
#include <qrinvoice/output_resolution.hpp>
#include <qrinvoice/payment_part_receipt_creator.hpp>

#include <qrinvoice/code_parser.hpp>
#include <qrinvoice/model/parse_exception.hpp>
#include <spdlog/spdlog.h>

#include <qrinvoice/util/country_util.hpp>
#include <qrinvoice/logging.hpp>

using namespace std;
qrinvoice::model::qr_invoice buildQrInvoice();

void create_qrcode() {
    spdlog::info("qr code start");
    qrinvoice::code_creator cc;
    const qrinvoice::output::qr_code qrcode = cc
            .qr_invoice(buildQrInvoice())
            .desired_qr_code_size(500)
            .output_format(qrinvoice::output_format::png)
            .create_qr_code();

    spdlog::info("size: {}", qrcode.get_size());
    spdlog::info("qr code built");
    spdlog::info("---");

    ofstream qrcodefile{"qr_code.png", ofstream::_Iosb::binary};
    if (qrcodefile.fail()) {
        spdlog::error("failed to write file");
    }
    qrinvoice::output::output::byte *const data = qrcode.get_data().data();
    const vector<qrinvoice::output::output::byte> &v_data = qrcode.get_data();
    const unsigned char *const x = qrcode.get_data().data();
    const size_t i = qrcode.get_size();
    qrcodefile.write(reinterpret_cast<const char *>(data), i);
    qrcodefile.close();
}

void create_ppr_png() {
    spdlog::info("payment part png start");
    qrinvoice::logging::set_log_level("trace");
    qrinvoice::payment_part_receipt_creator pp_creator;
    pp_creator.qr_invoice(buildQrInvoice());
    pp_creator.page_size(qrinvoice::page_size::din_lang);
    pp_creator.in_german();
    pp_creator.output_format(qrinvoice::output_format::png);
    pp_creator.output_resolution(qrinvoice::output_resolution::high_600_dpi);

    auto ppr = pp_creator
            .with_boundary_lines()
            .with_scissors()
            .with_separation_text()
            .create_payment_part_receipt();
    spdlog::info("size: {}", ppr.get_size());
    spdlog::info("payment part png built");

    // img
    ofstream file{"payment_part_receipt.png", ofstream::_Iosb::binary};
    file.write(reinterpret_cast<const char *>(ppr.get_data().data()), ppr.get_size());
    if (file.fail()) {
        spdlog::error("failed to write file");
    }
    file.close();
}

void create_ppr_jpeg_din_lang_cropped() {
    spdlog::info("payment part jpeg start");
    qrinvoice::logging::set_log_level("trace");
    qrinvoice::payment_part_receipt_creator pp_creator;
    pp_creator.qr_invoice(buildQrInvoice());
    pp_creator.page_size(qrinvoice::page_size::din_lang_cropped);
    pp_creator.in_german();
    pp_creator.output_format(qrinvoice::output_format::jpg);
    pp_creator.output_resolution(qrinvoice::output_resolution::medium_300_dpi);

    auto ppr = pp_creator
            .with_boundary_lines()
            .with_scissors()
            .with_separation_text()
            .create_payment_part_receipt();
    spdlog::info("size: {}", ppr.get_size());
    spdlog::info("payment part jpeg built");

    // img
    ofstream file{"payment_part_receipt_cropped.jpeg", ofstream::_Iosb::binary};
    file.write(reinterpret_cast<const char *>(ppr.get_data().data()), ppr.get_size());
    if (file.fail()) {
        spdlog::error("failed to write file");
    }
    file.close();
}

void create_ppr_pdf() {
    spdlog::info("payment part pdf start");
    qrinvoice::payment_part_receipt_creator ppr_creator;
    const qrinvoice::output::payment_part_receipt ppr = ppr_creator.qr_invoice(buildQrInvoice())
            .page_size(qrinvoice::page_size::a5)
            .in_german()
            .output_format(qrinvoice::output_format::pdf)
            .create_payment_part_receipt();
    spdlog::info("size: {}", ppr.get_size());
    spdlog::info("payment part pdf built");

    // pdf
    ofstream pdffile{"payment_part_receipt.pdf", ofstream::_Iosb::binary};
    pdffile.write(reinterpret_cast<const char *>(ppr.get_data().data()), ppr.get_size());
    if (pdffile.fail()) {
        spdlog::error("failed to write file");
    }
    pdffile.close();
}

void validate_test() {
    qrinvoice::model::qr_invoice qr_invoice = buildQrInvoice();
    qr_invoice.get_header().set_qr_type("foo");
    qr_invoice.get_creditor_info().get_creditor().get_address().set_name("");
    const qrinvoice::model::validation::validation_result vr = qrinvoice::model::validation::validate(qr_invoice);

    spdlog::info("expect validation error...");
    if(vr.has_errors()) {
        const string &validation_error_summary = vr.get_validation_error_summary();
        spdlog::info(validation_error_summary);

        const vector<qrinvoice::model::validation::validation_result::validation_error> &vector = vr.get_errors();
        for(const qrinvoice::model::validation::validation_result::validation_error &error : vector) {
            spdlog::info(error.get_data_path());
            spdlog::info(error.get_value());
            for(const string &error_msg_key : error.get_error_msg_keys()) {
                spdlog::info(error_msg_key);
            }
        }
    }
}

qrinvoice::model::qr_invoice buildQrInvoice() {
    qrinvoice::model::qr_invoice::builder qr_invoice_builder;

    qr_invoice_builder.creditor_iban("CH4431999123000889012");

    qr_invoice_builder.payment_amount_info();
            //.chf(1949.75);

    qr_invoice_builder.creditor()
            .structured_address()
            .name("Robert Schneider AG")
            .street_name("Rue du Lac")
            .house_number("1268")
            .postal_code("2501")
            .city("Biel")
            .country("CH");
/*
	qr_invoice_builder.ultimate_debtor()
		.structured_address()
		//.name("Pia-Maria Rustschmann-Schnyder ж ")
		.name("Pia-Maria Rustschmann-Schnyder")
		.street_name("Grosse Marktgasse")
		.house_number("28")
		.postal_code("9400")
		.city("Rorschach")
		.country("CH");*/

    qr_invoice_builder.ultimate_debtor()
            .combined_address()
            .name("Pia-Maria Rutschmann-Schnyder")
            .address_line1("Grosse Marktgasse 28")
            .address_line2("9400 Rorschach")
            .country("CH");

    qr_invoice_builder.payment_reference()
            .qr_reference("210000000003139471430009017");

    qr_invoice_builder.additional_information()
            .unstructured_message("Instruction of 03.04.2019")
            .bill_information("//S1/10/10201409/11/190512/20/1400.000-53/30/106017086/31/180508/32/7.7/40/2:10;0:30");

    qr_invoice_builder.alternative_scheme_params({
                                                         "Name AV1: UV;UltraPay005;12345",
                                                         "Name AV2: XY;XYService;54321"
                                                 });

    qrinvoice::model::qr_invoice qr_invoice = qr_invoice_builder.build();
    return qr_invoice;
}

void parse_spc() {
    string existing_spc = "SPC\n"
                          "0200\n"
                          "1\n"
                          "CH4431999123000889012\n"
                          "S\n"
                          "Robert Schneider AG\n"
                          "Rue du Lac\n"
                          "1268\n"
                          "2501\n"
                          "Biel\n"
                          "CH\n"
                          "\n"
                          "\n"
                          "\n"
                          "\n"
                          "\n"
                          "\n"
                          "\n"
                          "1949.75\n"
                          "CHF\n"
                          "K\n"
                          "Pia-Maria Rutschmann-Schnyder\n"
                          "Grosse Marktgasse 28\n"
                          "9400 Rorschach\n"
                          "\n"
                          "\n"
                          "CH\n"
                          "QRR\n"
                          "210000000003139471430009017\n"
                          "Instruction of 03.04.2019\n"
                          "EPD\n"
                          "//S1/10/10201409/11/190512/20/1400.000-53/30/106017086/31/180508/32/7.7/40/2:10;0:30\n"
                          "Name AV1: UV;UltraPay005;12345\n"
                          "Name AV2: XY;XYService;54321";

    qrinvoice::model::qr_invoice qrinvoice1 = qrinvoice::code_parser::parse(existing_spc);
    const double amount = qrinvoice1.get_payment_amount_info().get_amount();
    spdlog::info(amount);
    qrinvoice::model::qr_invoice qrinvoice2 = qrinvoice::code_parser::parse_and_validate(existing_spc);
    spdlog::info(qrinvoice2.get_payment_amount_info().get_amount());
}

void country_util() {
    spdlog::info("iso country code valid:" + std::to_string(qrinvoice::country_util::is_valid_iso_code("ch")));
    spdlog::info("iso country code valid:" + std::to_string(qrinvoice::country_util::is_valid_iso_code("LI")));
    spdlog::info("iso country code valid:" + std::to_string(qrinvoice::country_util::is_valid_iso_code("DE")));
}

void string_util() {
    spdlog::info("string valid: " + std::to_string(qrinvoice::model::validation::util::is_valid_string("Robert Schneider AG")));
    spdlog::info("string valid: " + std::to_string(qrinvoice::model::validation::util::is_valid_string("#€")));
    spdlog::info("string valid: " + std::to_string(qrinvoice::model::validation::util::is_valid_string("gexc")));
}

void reference_utils() {
    spdlog::info("qr_reference_valid: " + std::to_string(qrinvoice::qr_reference_util::is_valid_qr_reference("11 00012 34560 00000 00008 13457")));
    spdlog::info("qr_reference_valid: " + std::to_string(qrinvoice::qr_reference_util::is_valid_qr_reference("11 00012 34560 00000 00008 13457")));
    spdlog::info("creditor_reference_valid: " + std::to_string(qrinvoice::creditor_reference_util::is_valid_creditor_reference("RF45 1234 5123 45")));
}

void iban_util() {
    spdlog::info(qrinvoice::iban_util::format_iban("CH3908704016075473007"));
    spdlog::info(qrinvoice::iban_util::format_iban("CH39 0870 4016 0754 7300 7"));
    spdlog::info(qrinvoice::iban_util::normalize_iban("CH39 0870 4016 0754 7300 7"));

    spdlog::info(qrinvoice::iban_util::format_iban("CH4431999123000889012"));
    spdlog::info(qrinvoice::iban_util::format_iban("CH39 0870 4016 0754 7300 7"));

    spdlog::info(qrinvoice::iban_util::normalize_iban("CH39 0870 4016 0754 7300 7"));

    spdlog::info("valid iban: " + std::to_string(qrinvoice::iban_util::is_valid_iban("CH39 0870 4016 0754 7300 7", true)));
    spdlog::info("valid qr iban: " + std::to_string(qrinvoice::iban_util::is_qr_iban("CH39 0870 4016 0754 7300 7")));
}

void create_spc() {
    qrinvoice::code_creator code_creator;
    string spc = code_creator
            .qr_invoice(buildQrInvoice())
            .create_swiss_payments_code();
    spdlog::info(spc);
}

int main() try {
    _putenv_s("QRINVOICE_UNLOCK_ULTIMATE_CREDITOR", "true");
    _putenv_s("QRINVOICE_DEBUG_LAYOUT", "true");

    spdlog::info("---");
    reference_utils();
    spdlog::info("---");
    string_util();
    spdlog::info("---");
    country_util();
    spdlog::info("---");
    parse_spc();
    spdlog::info("---");
    iban_util();
    spdlog::info("---");
    validate_test();
    spdlog::info("---");
    create_ppr_pdf();
    spdlog::info("---");
    create_ppr_png();
    spdlog::info("---");
    create_ppr_jpeg_din_lang_cropped();
    spdlog::info("---");
    create_qrcode();
    spdlog::info("---");
    create_spc();
    spdlog::info("---");
    spdlog::info("done ");
}
catch (const qrinvoice::model::parse_exception &exception) {

    spdlog::error("error: " + std::string(exception.what()));
    return 5;
}
catch (const qrinvoice::layout::layout_exception &exception) {
    spdlog::error("error: " + std::string(exception.what()));
    return 4;
}
catch (const qrinvoice::model::validation::validation_exception &exception) {
    std::vector<qrinvoice::model::validation::validation_result::validation_error> errors = exception.get_result().get()->get_errors();

    spdlog::error("Errors: ");
    for (const qrinvoice::model::validation::validation_result::validation_error &err : errors) {
        spdlog::error("> " + err.get_value());
    }

    return 3;
}
catch (const std::exception &x) {
    spdlog::error("error: " + std::string(x.what()));
    return 2;
}
catch (...) {
    spdlog::error("error!");
    return 1;
}
