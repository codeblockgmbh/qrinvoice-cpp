
#include <vector>

#include <iostream>

#define STB_IMAGE_IMPLEMENTATION

#include <stb_image.h>
#include <sstream>
#include <fstream>

// images have to be in 8bit (1 channel only) - use gimp to do it
int main(int argc, char **argv) {

    std::ofstream file;
    file.open(argv[2]);

    int width, height, channels;
    unsigned char *image = stbi_load(argv[1],
                                     &width,
                                     &height,
                                     &channels,
                                     1);

    if (channels > 1)
        throw std::exception("not grayscale / 8bit");

    std::cout << "width: " << width << std::endl;
    std::cout << "height: " << height << std::endl;
    std::cout << "channels: " << channels << std::endl;

    file << "const unsigned char " << argv[3] << "[" << height << "][" << width << "] = {" << std::endl;
    for (int row = 0; row < height; row++) {
        file << "\t{";
        for (int col = 0; col < width; col++) {
            if (col > 0) {
                file << ", ";
            }
            const unsigned char *p = image + (channels * (row * width + col));
            unsigned char i = p[0];
            file << "0x" << std::hex << (0xFF & i);
        }
        if (row < height - 1) {
            file << "}," << std::endl;
        } else {
            file << "}" << std::endl;
        }
    }

    file << "};" << std::endl;

    file.close();


}