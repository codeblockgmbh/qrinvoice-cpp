
#include <vector>
#include <string>
#include <boost/test/unit_test.hpp>
#include <boost/test/data/test_case.hpp>

#include <qrinvoice/locale.hpp>
#include <qrinvoice/output_format.hpp>
#include <qrinvoice/output/output.hpp>
#include <qrinvoice/payment_part_receipt_creator.hpp>
#include <qrinvoice/model.hpp>

#include <system/combination.hpp>
#include <system/file_util.hpp>
#include <system/test_data_file_registry.hpp>
#include <system/target_test_file_helper.hpp>

using namespace qrinvoice;
using namespace qrinvoice::model;
using namespace qrinvoice::test;
using namespace qrinvoice::test::system;


namespace {

std::vector<output_format> fmts {output_format::pdf, output_format::png};
std::vector<locale> locales { locale::german, locale::french, locale::english, locale::italian };
auto files = test::system::test_data_file_registry{}.data();

// combinations' type: std::vector<std::tuple<output_format, locale, std::string>>
const auto combinations = combination_ops::combination_vector(fmts, locales, files);

} // namespace


BOOST_AUTO_TEST_CASE(a6_on_a5_payment_part_tests)
{
	for (const auto& tpl : combinations) {
		const auto& fmt = std::get<0>(tpl);
		const auto& loc = std::get<1>(tpl);
		const auto& file_name = std::get<2>(tpl);

		const std::string spc_str = test_data_file_registry::get_file_content(file_name);
		const swiss_payments_code spc_original = parse_swiss_payments_code(spc_str);
		const qr_invoice qi = map_swiss_payments_code_to_model(spc_original);

		const qrinvoice::output::payment_part_receipt pp = qrinvoice::payment_part_receipt_creator{}
                .qr_invoice(qi)
                .output_format(fmt)
                .page_size(qrinvoice::page_size::a5)
                .without_boundary_lines()
                .locale(loc)
                .create_payment_part_receipt();

		const auto lang = locale_ops::get_language(loc);
		std::string out_file_name = test_data_file_registry::file_name(file_name);
		out_file_name += '_';
		out_file_name += lang;
		out_file_name += '.';
		out_file_name += output_format_ops::get_file_extension(fmt);
		const auto file_path = target_test_file_helper::compose_output_file_path({
			"payment_part_receipt", "a6_on_a5", lang, out_file_name
		});
		file_util::write_to_file(file_path, pp);
	}
}

