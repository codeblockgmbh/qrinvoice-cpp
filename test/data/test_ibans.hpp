
#ifndef QR_INVOICE_TEST_TEST_IBANS_HPP
#define QR_INVOICE_TEST_TEST_IBANS_HPP


namespace qrinvoice {
namespace test {
namespace data {
namespace ibans {


constexpr const char* const iban_ch3908704016075473007 = "CH3908704016075473007";
constexpr const char* const iban_ch3709000000304442225 = "CH3709000000304442225";
constexpr const char* const qr_iban_ch4431999123000889012 = "CH44 3199 9123 0008 8901 2";


} // namespace ibans
} // namespace data
} // namespace test
} // namespace qrinvoice


#endif // QR_INVOICE_TEST_TEST_IBANS_HPP

