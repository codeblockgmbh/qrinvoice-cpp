spc_IG2.0_page*orig_*.txt are the examples from the specification
spc_IG2.0_page*variation_*.txt are the examples from the specification, but modified regarding one or the other aspect

spc_QRIN_*.txst are self made up examples

Note about max lengths:
* element max length are tested using the max permitted string lengths
* overall max length has to be tested against the qr code version 25, which can contain up to 997 bytes.
But as some header is written to the qr code and UTF-8 is used as an encoding, this does not mean that there may be 997 chars.