
#include <vector>
#include <string>
#include <unordered_map>
#include <stdexcept>

#include <qrinvoice/output_format.hpp>

#include <system/target_test_file_helper.hpp>
#include <system/env_vars.hpp>
#include <system/file_util.hpp>

namespace qrinvoice {
namespace test {
namespace system {
namespace target_test_file_helper {


constexpr const char* const test_target_dir = "test-output";

//
// std::ofstream fails to create a new file if we pass it a path like
// "abc/def/ghi.ext" and the intermediate directory "def" doesn't exist.
// So it's a hack to put '-' instead of the real path separator '/' or '\'.
// The resulting file will look like this: "abc/def-ghi.ext" where, "abc/"
// already exists and "def-ghi.ext" is actuall the file name with '-' acting as
// a dummy path separator.
//
// This will be resolved whtn a file system library will be incorporated.
//
constexpr char fake_path_separator = '-';

inline void ensure_fake_trailing_path_separator(std::string& dir_path)
{
	if (dir_path.back() != file_util::path_separator && dir_path.back() != fake_path_separator)
		dir_path += fake_path_separator;
}


std::string get_test_target_directory()
{
	const char* const env_path = std::getenv(env::test_target_output_path);
	if (!env_path)
		throw std::runtime_error{ std::string{"environment variable "} + env::test_target_output_path + " must be set" };

	std::string path{env_path};
	file_util::ensure_trailing_path_separator(path);
	path += test_target_dir;
	path += file_util::path_separator;
	return path;
}

inline std::string compose_format_output_dir(const std::string& output_path, qrinvoice::output_format fmt)
{
	auto path = output_path;
	file_util::ensure_trailing_path_separator(path);
	return path + output_format_ops::get_file_extension(fmt);
}

std::unordered_map<qrinvoice::output_format, std::string>
make_output_format_directory_map(const std::string& output_path)
{
	std::unordered_map<qrinvoice::output_format, std::string> map;
	map[qrinvoice::output_format::pdf] = compose_format_output_dir(output_path, qrinvoice::output_format::pdf);
	map[qrinvoice::output_format::png] = compose_format_output_dir(output_path, qrinvoice::output_format::png);
	map[qrinvoice::output_format::jpg] = compose_format_output_dir(output_path, qrinvoice::output_format::jpg);
	map[qrinvoice::output_format::bmp] = compose_format_output_dir(output_path, qrinvoice::output_format::bmp);

	return map;
}

inline const std::string& get_output_path()
{
	static const std::string output_path{ get_test_target_directory() };
	return output_path;
}

inline const std::string& get_text_output_path()
{
	static const std::string text_output_path{ get_output_path() + "txt" };
	return text_output_path;
}

inline auto get_output_format_directory_map()
	-> const std::unordered_map<qrinvoice::output_format, std::string>&
{
	static const std::unordered_map<qrinvoice::output_format, std::string>
		fmt_map { make_output_format_directory_map(get_output_path()) };

	return fmt_map;
}

std::string compose_output_file_path_helper(const std::vector<std::string>& path_components)
{
	if (path_components.empty())
		return {};

	if (path_components.size() == 1)
		return path_components.front();

	std::string path;
	for (unsigned int i = 0; i < path_components.size() - 1 ; ++i) {
		path += path_components[i];
		ensure_fake_trailing_path_separator(path);
	}

	path += path_components.back();
	return path;
}

std::string compose_output_file_path(const std::vector<std::string>& path_components)
{
	std::vector<std::string> path{ get_output_path() };
	path.insert(path.end(), path_components.begin(), path_components.end());
	return compose_output_file_path_helper(path);
}

std::string compose_output_file_path(qrinvoice::output_format fmt,
		const std::vector<std::string>& path_components)
{
	std::vector<std::string> path_cmps;
	path_cmps.reserve(path_components.size() + 1);
	path_cmps.push_back(get_output_format_directory_map().find(fmt)->second);
	path_cmps.insert(path_cmps.end(), path_components.begin(), path_components.end());
	return compose_output_file_path_helper(path_cmps);
}

} // namespace target_test_file_helper
} // namespace system
} // namespace test
} // namespace qrinvoice

