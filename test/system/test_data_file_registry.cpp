
#include <vector>
#include <string>
#include <iterator>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <stdexcept>

#include <system/test_data_file_registry.hpp>
#include <system/env_vars.hpp>
#include <system/file_util.hpp>

namespace qrinvoice {
namespace test {
namespace system {


std::vector<std::string> test_data_file_registry::data() const
{
	return {
		test_data_path_ + "esr+.txt",
		test_data_path_ + "Example.txt",
		test_data_path_ + "spc_IG2.0_page39_orig_qrr_qriban_avs_billinformation.txt",
		test_data_path_ + "spc_IG2.0_page39_variation_qrr_avs_billinfo_plus_ultimatecreditor.txt",
		test_data_path_ + "spc_IG2.0_page41_orig_non_nodebtor_unstructuredmessage.txt",
		test_data_path_ + "spc_IG2.0_page41_variation_non_unstructuredmessage_plus_debtor.txt",
		test_data_path_ + "spc_IG2.0_page41_variation_scor_unstructuredmessage_plus_reference.txt",
		test_data_path_ + "spc_IG2.0_page43_orig_scor.txt",
		test_data_path_ + "spc_IG2.0_page43_variation_scor_round_amount.txt", // test with a round amount
		test_data_path_ + "spc_QRIN_qrr_syntheticdata_max_nodebtor.txt",// test with synthetic data and without debtor in order to get the free text field which might consume more space
		test_data_path_ + "spc_QRIN_syntheticdata_max_overall.txt", // test with synthetic data in order to see edge cases regarding text wrapping
		test_data_path_ + "spc_QRIN_syntheticdata_max_per_element_without_ultimate_creditor.txt",// test with synthetic data and max per element in order to see edge cases regarding text wrapping
		test_data_path_ + "spc_QRIN_syntheticdata_max_per_element.txt"// test with synthetic data and max per element in order to see edge cases regarding text wrapping
   	};
}

std::string test_data_file_registry::get_file_content(const std::string& file_name)
{
	std::ifstream file{ file_name };
	if (!file)
		throw std::runtime_error{ std::string{"failed to open file \""} + file_name + "\"" };

	std::stringstream buffer;
	buffer << file.rdbuf();
	return buffer.str();
}

std::string test_data_file_registry::get_test_data_directory()
{
	std::string path{ get_test_resource_path() };
	path += test_data_dir;
	path += file_util::path_separator;
	return path;
}

std::string test_data_file_registry::get_test_resource_path()
{
	const char* const env_path = std::getenv(env::test_resource_path);
	if (!env_path)
		throw std::runtime_error{ std::string{"environment variable "} + env::test_resource_path + " must be set" };

	std::string path{ env_path };
	file_util::ensure_trailing_path_separator(path);
	return path;
}


} // namespace system
} // namespace test
} // namespace qrinvoice

