
#ifndef QR_INVOICE_TEST_SYSTEM_FILE_UTIL_HPP
#define QR_INVOICE_TEST_SYSTEM_FILE_UTIL_HPP

#include <string>
#include <fstream>

#include <qrinvoice/output/output.hpp>

//
// We need some file related utility functions which can be implemented by the
// application. We could have #included the header "util/file_util_internal.hpp"
// But non-internal tests should never use internal components of the library.
// So instead of including the header, following functions are copied from the
// file.
//

namespace qrinvoice {
namespace test {
namespace file_util {

constexpr char path_separator = PATH_SEPARATOR;

inline void ensure_trailing_path_separator(std::string& dir_path)
{
	if (dir_path.back() != file_util::path_separator)
		dir_path += file_util::path_separator;
}

/*
 * This function actually checks for accesibility of the file rather than its existance
 */
inline bool file_exists(const std::string& file_name)
{
	std::ifstream file{ file_name };
	return file.good();
}

inline void write_to_file(const std::string& file_name, const qrinvoice::output::output& op)
{
	std::ofstream file{file_name, std::ios_base::binary};
	if (!file)
		throw std::runtime_error{"failed to open file " + file_name};
	file.write(reinterpret_cast<const char*>(op.get_data().data()), op.get_size());
}

inline void write_to_file(const std::string& file_name, const std::string& str)
{
	std::ofstream file{file_name, std::ios_base::binary};
	if (!file)
		throw std::runtime_error{"failed to open file " + file_name};
	file.write(str.data(), str.size());
}

//
// example input: "/abc/def/xyz.txt"
// output: "xyz.txt"
//
inline std::string get_file_name(const std::string& file_path)
{
	auto pos = file_path.rfind(file_util::path_separator);
	if (pos == std::string::npos)
		return file_path;

	return { file_path.begin() + pos + 1, file_path.end() };
}

//
// example input: "xyz.txt"
// output: "xyz"
//
inline std::string get_file_stem(const std::string& file_path)
{
	std::string file_name = get_file_name(file_path);
	auto pos = file_name.rfind('.');
	if (pos == std::string::npos)
		return file_name;

	return { file_name.begin(), file_name.begin() + pos };
}


} // namespace file_util
} // namespace test
} // namespace qrinvoice



#endif	// QR_INVOICE_TEST_SYSTEM_FILE_UTIL_HPP

