
#ifndef QR_INVOICE_TEST_SYSTEM_COMBINATION_HPP
#define QR_INVOICE_TEST_SYSTEM_COMBINATION_HPP

#include <vector>
#include <tuple>


namespace qrinvoice {
namespace test {
namespace combination_ops {


template <typename T1, typename T2, typename T3>
auto combination_vector(const std::vector<T1>& v1, const std::vector<T2>& v2, const std::vector<T3>& v3)
	-> std::vector<std::tuple<T1, T2, T3>>
{
	std::vector<std::tuple<T1, T2, T3>> result;
	for (const T1& e1 : v1)
		for (const T2& e2 : v2)
			for (const T3& e3 : v3)
				result.push_back(std::make_tuple(e1, e2, e3));

	return result;
}

template <typename T1, typename T2, typename T3, typename T4>
auto combination_vector(const std::vector<T1>& v1, const std::vector<T2>& v2, const std::vector<T3>& v3, const std::vector<T4>& v4)
	-> std::vector<std::tuple<T1, T2, T3, T4>>
{
	std::vector<std::tuple<T1, T2, T3, T4>> result;
	for (const T1& e1 : v1)
		for (const T2& e2 : v2)
			for (const T3& e3 : v3)
				for (const T4& e4 : v4)
					result.push_back(std::make_tuple(e1, e2, e3, e4));

	return result;
}


} // namespace combination_ops
} // namespace test
} // namespace qrinvoice


#endif // QR_INVOICE_TEST_SYSTEM_COMBINATION_HPP

