
#include <vector>
#include <string>
#include <unordered_map>

#include <qrinvoice/output_format.hpp>

#include <system/file_util.hpp>


namespace qrinvoice {
namespace test {
namespace system {
namespace target_test_file_helper {


std::string compose_output_file_path(const std::vector<std::string>& path_components);
std::string compose_output_file_path(qrinvoice::output_format fmt, const std::vector<std::string>& path_components);


} // namespace target_test_file_helper
} // namespace system
} // namespace test
} // namespace qrinvoice

