
#ifndef QR_INVOICE_TEST_SYSTEM_TEST_DATA_FILE_REGISTRY_HPP
#define QR_INVOICE_TEST_SYSTEM_TEST_DATA_FILE_REGISTRY_HPP

#include <vector>
#include <string>

#include <system/file_util.hpp>


namespace qrinvoice {
namespace test {
namespace system {

class test_data_file_registry {
public:
	static constexpr const char* const test_data_dir = "testdata";

	test_data_file_registry()
		:test_data_path_{ get_test_data_directory() } {}

public:
	static std::string get_test_resource_path();

	std::vector<std::string> data() const;
	static std::string get_file_content(const std::string& file_name);

	static std::string file_name(const std::string& file_path) {
		return file_util::get_file_stem(file_util::get_file_name(file_path));
	}

	static std::string file_name(const std::string& file_path, const std::string& postfix) {
		return file_util::get_file_stem(file_util::get_file_name(file_path)) + postfix;
	}

private:
	static std::string get_test_data_directory();

private:
	const std::string		test_data_path_;
};


} // namespace system
} // namespace test
} // namespace qrinvoice


#endif // QR_INVOICE_TEST_SYSTEM_TEST_DATA_FILE_REGISTRY_HPP

