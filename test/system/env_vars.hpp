
#ifndef QR_INVOICE_TEST_SYSTEM_ENV_VARS_HPP
#define QR_INVOICE_TEST_SYSTEM_ENV_VARS_HPP


namespace qrinvoice {
namespace test {
namespace system {
namespace env {


/*
 * User should set this environment variable to directory path where test
 * resources reside.
 */
constexpr const char* const test_resource_path = "QR_INVOICE_TEST_RESOURCE_PATH";

/*
 * User should set this environment variable to directory path where test
 * output will be generated.
 */
constexpr const char* const test_target_output_path = "QR_INVOICE_TEST_TARGET_OUTPUT_PATH";

} // namespace env
} // namespace system
} // namespace test
} // namespace qrinvoice


#endif	// QR_INVOICE_TEST_SYSTEM_ENV_VARS_HPP

