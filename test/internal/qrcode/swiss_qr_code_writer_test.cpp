
#include <iostream>
#include <vector>
#include <string>
#include <boost/test/unit_test.hpp>
#include <boost/test/data/test_case.hpp>

#include <qrcode/swiss_qr_code_writer_internal.hpp>
#include <qrinvoice/output_format.hpp>

using namespace qrinvoice;
using namespace qrinvoice::qrcode;
namespace bdata = boost::unit_test::data;


namespace qrinvoice
{
	// required by the data-test-case
	inline std::ostream& operator << (std::ostream& os, output_format fmt)
	{
		switch (fmt) {
		case output_format::pdf:
			return os << "output_format::pdf";
		case output_format::png:
			return os << "output_format::png";
		case output_format::jpg:
			return os << "output_format::jpg";
		case output_format::bmp:
			return os << "output_format::bmp";
		default:
			return os;
		}
	}
}

namespace {
	const std::array<output_format, 3> data_set = { output_format::png, output_format::jpg, output_format::bmp };
} // namespace


BOOST_AUTO_TEST_SUITE(qr_code_writer)


BOOST_DATA_TEST_CASE(write, data_set)
{
	const std::string qr_code_string{ "test string to encode" };
	output::qr_code code = swiss_qr_code_writer{}.write(sample, qr_code_string, 500);
	std::vector<output::qr_code::byte> img = code.get_data();

	BOOST_TEST(!img.empty());
	BOOST_TEST(code.get_size() > 0);
}


BOOST_AUTO_TEST_CASE(write_image)
{
	const std::string qr_code_string{ "test string to encode" };
	const swiss_qr_code_writer &writer = swiss_qr_code_writer{};
	auto img = writer.render_int_scaled_qr_code(writer.write_bitmap(qr_code_string), 500);

	BOOST_TEST(static_cast<unsigned int>(img.get_width()) > 15);
	BOOST_TEST(static_cast<unsigned int>(img.get_height()) > 15);
}


BOOST_AUTO_TEST_SUITE_END()

