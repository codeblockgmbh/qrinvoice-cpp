
#include <vector>
#include <boost/test/unit_test.hpp>

#include <qrinvoice/output_format.hpp>
#include <qrinvoice/output/output.hpp>

using namespace qrinvoice;


BOOST_AUTO_TEST_SUITE(output)

BOOST_AUTO_TEST_CASE(output_from_array_test)
{
	auto fmt = output_format::pdf;
	const qrinvoice::output::output::byte bytes[] = {1, 2, 3, 4, 5, 6};
	const qrinvoice::output::output op{ fmt, bytes, sizeof(bytes) };

	BOOST_TEST(!op.get_data().empty());
	BOOST_CHECK(op.get_output_format() == output_format::pdf);
	BOOST_TEST(op.get_size() == sizeof(bytes));
}

BOOST_AUTO_TEST_CASE(output_from_vector_test)
{
	auto fmt = output_format::pdf;
	std::vector<qrinvoice::output::output::byte> bytes{1, 2, 3, 4, 5, 6};
	const qrinvoice::output::output op{ fmt, bytes };

	BOOST_TEST(!op.get_data().empty());
	BOOST_CHECK(op.get_output_format() == output_format::pdf);
	BOOST_TEST(op.get_size() == bytes.size());
}


BOOST_AUTO_TEST_SUITE_END()

