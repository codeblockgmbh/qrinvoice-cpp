
#include <boost/test/unit_test.hpp>

#include <config/env_vars_internal.hpp>
#include <qrinvoice/model.hpp>
#include <qrinvoice/currency.hpp>

#include <util/iban_util_internal.hpp>
#include <data/test_ibans.hpp>


using namespace qrinvoice;
using namespace qrinvoice::model;


namespace {

void validate(const qr_invoice& qi)
{
	const auto header = qi.get_header();
	BOOST_TEST(header.get_qr_type() == "SPC");
	BOOST_TEST(header.get_version() == 200);
	BOOST_TEST(header.get_coding_type() == 1);

	const auto pai = qi.get_payment_amount_info();
	BOOST_TEST(pai.get_amount() == 1949.75);
	BOOST_TEST(currency_ops::get_currency_code(pai.get_currency()) == "CHF");

	const auto creditor_info = qi.get_creditor_info();
	BOOST_TEST(creditor_info.get_iban() == "CH3709000000304442225");

        const auto creditor = creditor_info.get_creditor();
        BOOST_TEST(creditor.get_address().get_name() == "Robert Schneider AG");
        BOOST_TEST(creditor.get_address().get_street_name() == "Rue du Lac");
        BOOST_TEST(creditor.get_address().get_house_number() == "1268");
        BOOST_TEST(creditor.get_address().get_postal_code() == "2501");
        BOOST_TEST(creditor.get_address().get_city() == "Biel");
        BOOST_TEST(creditor.get_address().get_country() == "CH");

        const auto ultimate_creditor = qi.get_ultimate_creditor();
        BOOST_TEST(ultimate_creditor.get_address().get_name() == "Robert Schneider Services Switzerland AG");
        BOOST_TEST(ultimate_creditor.get_address().get_street_name() == "Rue du Lac");
        BOOST_TEST(ultimate_creditor.get_address().get_house_number() == "1268/3/1");
        BOOST_TEST(ultimate_creditor.get_address().get_postal_code() == "2501");
        BOOST_TEST(ultimate_creditor.get_address().get_city() == "Biel");
        BOOST_TEST(ultimate_creditor.get_address().get_country() == "CH");

        const auto ultimate_debtor = qi.get_ultimate_debtor();
        BOOST_TEST(ultimate_debtor.get_address().get_name() == "Pia-Maria Rutschmann-Schnyder");
        BOOST_TEST(ultimate_debtor.get_address().get_street_name() == "Grosse Marktgasse");
        BOOST_TEST(ultimate_debtor.get_address().get_house_number() == "28");
        BOOST_TEST(ultimate_debtor.get_address().get_postal_code() == "9400");
        BOOST_TEST(ultimate_debtor.get_address().get_city() == "Rorschach");
        BOOST_TEST(ultimate_debtor.get_address().get_country() == "CH");

        const auto payment_reference = qi.get_payment_reference();
        BOOST_TEST(payment_reference.get_reference_type().get_reference_type_code() == "SCOR");
        BOOST_TEST(payment_reference.get_reference() == "RF18539007547034");
        
		const auto additional_information = payment_reference.get_additional_information();
		BOOST_TEST(additional_information.get_unstructured_message() == "Instruction of 03.04.2019");
		BOOST_TEST(additional_information.get_bill_information() == "//S1/10/10201409/11/190512/20/1400.000-53/30/106017086/31/180508/32/7.7/40/2:10;0:30");
		BOOST_TEST(additional_information.get_trailer() == "EPD");
		// TODO Test bill information

        BOOST_TEST(qi.get_alternative_schemes().empty());
}

qr_invoice create_qr_invoice()
{
	qrinvoice::model::qr_invoice::builder qr_invoice_builder;

	qr_invoice_builder.creditor_iban(test::data::ibans::iban_ch3709000000304442225);

	qr_invoice_builder.payment_amount_info()
		.chf(1949.75);

	qr_invoice_builder.creditor()
		.structured_address()
		.name("Robert Schneider AG")
		.street_name("Rue du Lac")
		.house_number("1268")
		.postal_code("2501")
		.city("Biel")
		.country("CH");

	qr_invoice_builder.ultimate_creditor()
		.structured_address()
		.name("Robert Schneider Services Switzerland AG")
		.street_name("Rue du Lac")
		.house_number("1268/3/1")
		.postal_code("2501")
		.city("Biel")
		.country("CH");

	qr_invoice_builder.ultimate_debtor()
		.structured_address()
		.name("Pia-Maria Rutschmann-Schnyder")
		.street_name("Grosse Marktgasse")
		.house_number("28")
		.postal_code("9400")
		.city("Rorschach")
		.country("CH");

	qr_invoice_builder.payment_reference()
		.creditor_reference("RF18539007547034");

	qr_invoice_builder.additional_information()
		.unstructured_message("Instruction of 03.04.2019")
		.bill_information("//S1/10/10201409/11/190512/20/1400.000-53/30/106017086/31/180508/32/7.7/40/2:10;0:30");

	qr_invoice_builder.alternative_scheme_params({});

	return qr_invoice_builder.build();
}

} // namespace


BOOST_AUTO_TEST_SUITE(model)


BOOST_AUTO_TEST_CASE(validate_qr_invoice_builder)
{
	env::set_env(env::unlock_ultimate_creditor);
	validate(create_qr_invoice());
}


BOOST_AUTO_TEST_SUITE_END()

