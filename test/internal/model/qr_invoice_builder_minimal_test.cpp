
#include <boost/test/unit_test.hpp>

#include <qrinvoice/model.hpp>

#include <data/test_ibans.hpp>

using namespace qrinvoice;
using namespace qrinvoice::model;


namespace {

void validate(const qr_invoice& qi)
{
	const auto header = qi.get_header();
	BOOST_TEST(header.get_qr_type() == "SPC");
	BOOST_TEST(header.get_version() == 200);
	BOOST_TEST(header.get_coding_type() == 1);

	const auto pai = qi.get_payment_amount_info();
	BOOST_TEST(!pai.is_amount_set());
	BOOST_TEST(currency_ops::get_currency_code(pai.get_currency()) == "EUR");

	const auto creditor_info = qi.get_creditor_info();
	BOOST_TEST(creditor_info.get_iban() == "CH3709000000304442225");

	const auto creditor = creditor_info.get_creditor();
	BOOST_TEST(creditor.get_address().get_name() == "Salvation Army Foundation Switzerland");
	BOOST_TEST(creditor.get_address().get_street_name() == "");
	BOOST_TEST(creditor.get_address().get_house_number() == "");
	BOOST_TEST(creditor.get_address().get_postal_code() == "3000");
	BOOST_TEST(creditor.get_address().get_city() == "Bern");
	BOOST_TEST(creditor.get_address().get_country() == "CH");

	const auto ultimate_creditor = qi.get_ultimate_creditor();
	BOOST_TEST(ultimate_creditor.empty());

	const auto ultimate_debtor = qi.get_ultimate_debtor();
	BOOST_TEST(ultimate_debtor.empty());

	const auto payment_reference = qi.get_payment_reference();
	BOOST_TEST(payment_reference.get_reference_type().get_reference_type_code() == "NON");
	BOOST_TEST(payment_reference.get_reference() == "");
			
	const auto additional_information = payment_reference.get_additional_information();
	BOOST_TEST(additional_information.get_unstructured_message() == "");
	BOOST_TEST(additional_information.get_trailer() == "EPD");
	// TODO Test bill information

	BOOST_TEST(qi.get_alternative_schemes().empty());
}

qr_invoice create_qr_invoice()
{
	qrinvoice::model::qr_invoice::builder qr_invoice_builder;

	qr_invoice_builder.creditor_iban(test::data::ibans::iban_ch3709000000304442225);

	qr_invoice_builder.payment_amount_info()
		.currency(currency::eur);

	qr_invoice_builder.creditor()
		.structured_address()
		.name("Salvation Army Foundation Switzerland")
		.postal_code("3000")
		.city("Bern")
		.country("CH");

	return qr_invoice_builder.build();
}

} // namespace


BOOST_AUTO_TEST_SUITE(model)


BOOST_AUTO_TEST_CASE(qr_invoice_builder_minimal_test)
{
	validate(create_qr_invoice());
}


BOOST_AUTO_TEST_SUITE_END()



