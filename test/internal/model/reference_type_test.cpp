
#include <boost/test/unit_test.hpp>

#include <qrinvoice/model.hpp>

using namespace qrinvoice;
using namespace qrinvoice::model;


BOOST_AUTO_TEST_SUITE(model)


BOOST_AUTO_TEST_CASE(test_valid_reference_type_codes)
{
	BOOST_TEST(reference_type::parse("QRR").get() == reference_type::qr_reference);
	BOOST_TEST(reference_type::parse("SCOR").get() == reference_type::creditor_reference);
	BOOST_TEST(reference_type::parse("NON").get() == reference_type::without_reference);

	BOOST_TEST(reference_type{reference_type::qr_reference}.get_reference_type_code() == "QRR");
	BOOST_TEST(reference_type{reference_type::creditor_reference}.get_reference_type_code() == "SCOR");
	BOOST_TEST(reference_type{}.get_reference_type_code() == "NON");
}

BOOST_AUTO_TEST_CASE(test_invalid_reference_type_codes)
{
	BOOST_CHECK_THROW(reference_type::parse("qrr"), parse_exception);
	BOOST_CHECK_THROW(reference_type::parse(""), parse_exception);

	BOOST_TEST(reference_type{static_cast<reference_type::ref_type>(3)}.get_reference_type_code() == nullptr);
}


BOOST_AUTO_TEST_SUITE_END()

