
#include <boost/test/unit_test.hpp>

#include <string>

#include <qrinvoice/model.hpp>
#include <qrinvoice/model/parse/parse_swiss_payments_code.hpp>
#include <qrinvoice/model/mapper/swiss_payments_code_to_model.hpp>

#include <model/mapper/model_to_swiss_payments_code_internal.hpp>
#include <system/test_data_file_registry.hpp>

using namespace qrinvoice;
using namespace qrinvoice::model;



BOOST_AUTO_TEST_SUITE(model)


BOOST_AUTO_TEST_CASE(test_mapping_back_and_forth)
{
	using namespace test::system;
	test_data_file_registry file_registry;
	auto files = file_registry.data();
	for (const auto& file_name : files) {
		const std::string spc_str = test_data_file_registry::get_file_content(file_name);
		const swiss_payments_code spc_original = parse_swiss_payments_code(spc_str);

		const qr_invoice qi = map_swiss_payments_code_to_model(spc_original);
		const swiss_payments_code spc_result = map_model_to_swiss_payments_code(qi);

		BOOST_CHECK(spc_original == spc_result);
	}
}


BOOST_AUTO_TEST_SUITE_END()

