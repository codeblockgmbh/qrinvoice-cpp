
#include <boost/test/unit_test.hpp>

#include <qrinvoice/model.hpp>
#include <qrinvoice/model/parse/parse_swiss_payments_code.hpp>

#include <qr_invoice_swiss_qr_code_internal.hpp>
#include <system/test_data_file_registry.hpp>

using namespace qrinvoice;
using namespace qrinvoice::model;


BOOST_AUTO_TEST_SUITE(model)


BOOST_AUTO_TEST_CASE(test_parse)
{
	using namespace test::system;
	test_data_file_registry file_registry;
	auto files = file_registry.data();
	for (const auto& file_name : files) {
		const auto original_spc = parse_swiss_payments_code(test_data_file_registry::get_file_content(file_name));
		BOOST_TEST(original_spc.qr_type == "SPC");
		BOOST_TEST(original_spc.version == "0200");
	}
}


BOOST_AUTO_TEST_SUITE_END()

