
#include <limits>
#include <algorithm>
#include <boost/test/unit_test.hpp>
#include <boost/regex.hpp>

#include <qrinvoice/model/validation.hpp>

#include <model/validation/validation_util_internal.hpp>
#include <qr_invoice_swiss_qr_code_internal.hpp>
#include <model/validation/validation_util_test.hpp>

using namespace qrinvoice;
using namespace qrinvoice::model;
using namespace qrinvoice::model::validation;
using namespace qrinvoice::model::validation::util;



namespace {

std::string generate_all_allowed_characters()
{
    return qrinvoice::model::spc::valid_characters;
}

} // namespace


namespace qrinvoice {
namespace test {
namespace validation {
namespace util {


const std::string get_all_allowed_characters()
{
	static std::string s = generate_all_allowed_characters();
	return s;
}


} // namespace util
} // namespace validation
} // namespace test
} // namespace qrinvoice



BOOST_AUTO_TEST_SUITE(model_validation)

BOOST_AUTO_TEST_CASE(test_validate_length)
{
        BOOST_TEST(validate_length("", 0, 10, false));
        BOOST_TEST(!validate_length("", 1, 10, false));
        BOOST_TEST(validate_length("a", 1, 10, false));
        BOOST_TEST(validate_length("abc", 1, 10, false));
        BOOST_TEST(validate_length("abcdefghij", 1, 10, false));
        BOOST_TEST(!validate_length("abcdefghijk", 1, 10, false));
}

BOOST_AUTO_TEST_CASE(test_validate_length_optional)
{
        BOOST_TEST(validate_length("", 0, 10, true));
        BOOST_TEST(validate_length("", 1, 10, true));
        BOOST_TEST(validate_length("a", 1, 10, true));
        BOOST_TEST(!validate_length("a", 2, 10, true));
        BOOST_TEST(validate_length("abc", 1, 10, true));
        BOOST_TEST(validate_length("abcdefghij", 1, 10, true));
        BOOST_TEST(!validate_length("abcdefghijk", 1, 10, true));
}

BOOST_AUTO_TEST_CASE(test_validate_range)
{
        BOOST_TEST(validate_range(0, 0, 10));
        BOOST_TEST(validate_range(1, 0, 10));
        BOOST_TEST(validate_range(10, 0, 10));
        BOOST_TEST(!validate_range(-1, 0, 10));
        BOOST_TEST(!validate_range(11, 0, 10));
}

BOOST_AUTO_TEST_CASE(test_valid_latin_characters)
{
        BOOST_TEST(is_valid_string(" - abcdefghijklmnopqerstufvwxyzälüç123456780;éàèÀÉÈÜÄÖ, !"), "Only latin characters are valid");
        BOOST_TEST(is_valid_string(test::validation::util::get_all_allowed_characters()), "All latin characters are valid");
        BOOST_TEST(is_valid_string(" !\\\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\\\]^_`abcdefghijklmnopqrstuvwxyz{|}~ ¡¢£¤¥¦§¨©ª«¬\\u00AD®¯°±²³´µ¶·¸¹º»¼½¾¿ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõö÷øùúûüýþÿĀāĂăĄąĆćĈĉĊċČčĎďĐđĒēĔĕĖėĘęĚěĜĝĞğĠġĢģĤĥĦħĨĩĪīĬĭĮįİıĲĳĴĵĶķĸĹĺĻļĽľĿŀŁłŃńŅņŇňŉŊŋŌōŎŏŐőŒœŔŕŖŗŘřŚśŜŝŞşŠšŢţŤťŦŧŨũŪūŬŭŮůŰűŲųŴŵŶŷŸŹźŻżŽžȘșȚț€"), "Fix string with all allowed characters");
        BOOST_TEST(!is_valid_string("abcdefg - ж - hijkl"), "Even a single encoding stranger character is not permitted");
        BOOST_TEST(!is_valid_string("жф"), "cyrillic characters are not valid");
        BOOST_TEST(!is_valid_string("あま"), "japanese characters are not valid");
        BOOST_TEST(!is_valid_string("诶比"), "chinese characters are not valid");
        BOOST_TEST(!is_valid_string("ㅂㅁ"), "korean characters are not valid");
        BOOST_TEST(!is_valid_string("ﺐﺶ"), "arabic characters are not valid");
}


BOOST_AUTO_TEST_CASE(test_allowed_characters)
{
	using namespace std;
	std::string valid_chars{ qrinvoice::model::spc::valid_characters };
	for (const char c : test::validation::util::get_all_allowed_characters())
		BOOST_TEST(
			(find(valid_chars.begin(), valid_chars.end(), c) != valid_chars.end()),
			string{"Characters '"} + c + "' is not in fix list of valid characters"
		);
}

BOOST_AUTO_TEST_CASE(test_line_break_validation)
{
	BOOST_TEST(!is_valid_string("\n"), "Line breaks are note permitted as characters in elements");
	BOOST_TEST(!is_valid_string("\r"), "Line breaks are note permitted as characters in elements");
	BOOST_TEST(!is_valid_string("\r\n"), "Line breaks are note permitted as characters in elements");
}


BOOST_AUTO_TEST_SUITE_END()

