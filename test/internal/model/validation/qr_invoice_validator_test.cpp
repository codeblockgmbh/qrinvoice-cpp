
#include <boost/test/unit_test.hpp>

#include <qrinvoice/model/validation.hpp>

#include <config/env_vars_internal.hpp>
#include <qr_invoice_swiss_qr_code_internal.hpp>
#include <model/validation/address_validator_internal.hpp>
#include <data/test_ibans.hpp>

#include <spdlog/spdlog.h>

using namespace qrinvoice;
using namespace qrinvoice::model;
using namespace qrinvoice::model::validation;


namespace {


qr_invoice create_valid_creditor_reference()
{
	qrinvoice::model::qr_invoice::builder qr_invoice_builder;

	qr_invoice_builder.creditor_iban(test::data::ibans::iban_ch3709000000304442225);

	qr_invoice_builder.payment_amount_info()
		.chf(1949.75);

	qr_invoice_builder.creditor()
		.address_type(address_type::structured)
		.name("Robert Schneider AG")
		.street_name("Rue du Lac")
		.house_number("1268")
		.postal_code("2501")
		.city("Biel")
		.country("CH");

	qr_invoice_builder.ultimate_creditor()
		.address_type(address_type::structured)
		.name("Robert Schneider Services Switzerland AG")
		.street_name("Rue du Lac")
		.house_number("1268/3/1")
		.postal_code("2501")
		.city("Biel")
		.country("CH");

	qr_invoice_builder.ultimate_debtor()
		.address_type(address_type::structured)
		.name("Pia-Maria Rutschmann-Schnyder")
		.street_name("Grosse Marktgasse")
		.house_number("28")
		.postal_code("9400")
		.city("Rorschach")
		.country("CH");

	qr_invoice_builder.payment_reference()
		.creditor_reference("RF18539007547034");

	qr_invoice_builder.additional_information()
		.unstructured_message("Instruction of 03.04.2019")
		.bill_information("//S1/10/10201409/11/190512/20/1400.000-53/30/106017086/31/180508/32/7.7/40/2:10;0:30");

	qr_invoice_builder.alternative_scheme_params({});

	return qr_invoice_builder.build();
}

qr_invoice create_valid_qr_reference()
{
	qrinvoice::model::qr_invoice::builder qr_invoice_builder;

	qr_invoice_builder.creditor_iban(test::data::ibans::qr_iban_ch4431999123000889012);

	qr_invoice_builder.payment_amount_info()
		.chf(1949.75);

	qr_invoice_builder.creditor()
		.address_type(address_type::structured)
		.name("Robert Schneider AG")
		.street_name("Rue du Lac")
		.house_number("1268")
		.postal_code("2501")
		.city("Biel")
		.country("CH");

	qr_invoice_builder.ultimate_creditor()
		.address_type(address_type::structured)
		.name("Robert Schneider Services Switzerland AG")
		.street_name("Rue du Lac")
		.house_number("1268/3/1")
		.postal_code("2501")
		.city("Biel")
		.country("CH");

	qr_invoice_builder.ultimate_debtor()
		.address_type(address_type::structured)
		.name("Pia-Maria Rutschmann-Schnyder")
		.street_name("Grosse Marktgasse")
		.house_number("28")
		.postal_code("9400")
		.city("Rorschach")
		.country("CH");

	qr_invoice_builder.payment_reference()
		.qr_reference("110001234560000000000813457");

	qr_invoice_builder.additional_information()
		.unstructured_message("Instruction of 03.04.2019")
		.bill_information("//S1/10/10201409/11/190512/20/1400.000-53/30/106017086/31/180508/32/7.7/40/2:10;0:30");

	qr_invoice_builder.alternative_scheme_params({});

	return qr_invoice_builder.build();
}

qr_invoice create_valid_without_reference()
{	
	qrinvoice::model::qr_invoice::builder qr_invoice_builder;

	qr_invoice_builder.creditor_iban(test::data::ibans::iban_ch3709000000304442225);

	qr_invoice_builder.payment_amount_info()
		.chf(1949.75);

	qr_invoice_builder.creditor()
		.address_type(address_type::structured)
		.name("Robert Schneider AG")
		.street_name("Rue du Lac")
		.house_number("1268")
		.postal_code("2501")
		.city("Biel")
		.country("CH");

	qr_invoice_builder.ultimate_creditor()
		.address_type(address_type::structured)
		.name("Robert Schneider Services Switzerland AG")
		.street_name("Rue du Lac")
		.house_number("1268/3/1")
		.postal_code("2501")
		.city("Biel")
		.country("CH");

	qr_invoice_builder.ultimate_debtor()
		.address_type(address_type::structured)
		.name("Pia-Maria Rutschmann-Schnyder")
		.street_name("Grosse Marktgasse")
		.house_number("28")
		.postal_code("9400")
		.city("Rorschach")
		.country("CH");

	qr_invoice_builder.payment_reference()
		.without_reference();

	qr_invoice_builder.additional_information()
		.unstructured_message("Instruction of 03.04.2019")
		.bill_information("//S1/10/10201409/11/190512/20/1400.000-53/30/106017086/31/180508/32/7.7/40/2:10;0:30");

	qr_invoice_builder.alternative_scheme_params({});

	return qr_invoice_builder.build();
}


} // namespace



BOOST_AUTO_TEST_SUITE(model_validation)

// ----------
// creditor_reference - SCOR
// ----------

BOOST_AUTO_TEST_CASE(test_valid_creditor_reference)
{
	env::set_env(env::unlock_ultimate_creditor);
	const auto qi = create_valid_creditor_reference();
	validation_result vr = validate(qi);
	spdlog::info(vr.get_validation_error_summary());
	BOOST_TEST(vr.is_valid());
}

BOOST_AUTO_TEST_CASE(test_invalid_creditor_name)
{
	env::set_env(env::unlock_ultimate_creditor);
	auto qi = create_valid_creditor_reference();
        BOOST_TEST(validate(qi).is_valid());

        qi.get_creditor_info().get_creditor().get_address().set_name("this is invalid as it is just a too long string with more than 70 chars");

	validation_result vr = validate(qi);
	spdlog::info(vr.get_validation_error_summary());
        BOOST_TEST(vr.has_errors("creditor_info.creditor.name"));
        BOOST_TEST(vr.get_errors().size() == 1);
}

BOOST_AUTO_TEST_CASE(test_invalid_creditor_iban)
{
	env::set_env(env::unlock_ultimate_creditor);
	// check valid before modifying QrInvoice
	auto qi = create_valid_creditor_reference();
        BOOST_TEST(validate(qi).is_valid());

        qi.get_creditor_info().set_iban("non-valid-iban");

	validation_result vr = validate(qi);
	spdlog::info(vr.get_validation_error_summary());
        BOOST_TEST(vr.has_errors("creditor_info.iban"));
        BOOST_TEST(vr.get_errors().size() == 1);
}

BOOST_AUTO_TEST_CASE(test_unsupported_currency)
{
	env::set_env(env::unlock_ultimate_creditor);
	// check valid before modifying QrInvoice
	auto qi = create_valid_creditor_reference();
        BOOST_TEST(validate(qi).is_valid());

        qi.get_payment_amount_info().set_currency(static_cast<currency>(2));

	validation_result vr = validate(qi);
	spdlog::info(vr.get_validation_error_summary());
        BOOST_TEST(vr.has_errors("payment_amount_info.currency"));
        BOOST_TEST(vr.get_errors().size() == 1);
}

BOOST_AUTO_TEST_CASE(test_creditor_reference_with_wrong_reference_number)
{
	env::set_env(env::unlock_ultimate_creditor);
	// check valid before modifying QrInvoice
	auto qi = create_valid_creditor_reference();
        BOOST_TEST(validate(qi).is_valid());

        qi.get_payment_reference().set_reference_type(reference_type::qr_reference);
	validation_result vr = validate(qi);
	spdlog::info(vr.get_validation_error_summary());
        BOOST_TEST(vr.has_errors("payment_reference.reference"));
        BOOST_TEST(vr.get_errors().size() >= 1);
}


// ----------
// qr_reference - QRR
// ----------

BOOST_AUTO_TEST_CASE(test_validate_qr_reference)
{
	env::set_env(env::unlock_ultimate_creditor);
	auto qi = create_valid_qr_reference();
	validation_result vr = validate(qi);
	spdlog::info(vr.get_validation_error_summary());
        BOOST_TEST(vr.is_valid());
}

BOOST_AUTO_TEST_CASE(test_qr_reference_with_wrong_reference_number)
{
	env::set_env(env::unlock_ultimate_creditor);
	// check valid before modifying QrInvoice
	auto qi = create_valid_qr_reference();
        BOOST_TEST(validate(qi).is_valid());

        // when setting a creditor-reference reference number for type QRR, a validation error must occur
        qi.get_payment_reference().set_reference("RF18539007547034");
	validation_result vr = validate(qi);
	spdlog::info(vr.get_validation_error_summary());
        BOOST_TEST(vr.has_errors("payment_reference.reference"));
        BOOST_TEST(vr.get_errors().size() == 1);
}


// ----------
// WITHOUT_REFERENCE - NON
// ----------

BOOST_AUTO_TEST_CASE(test_validate_without_reference)
{
	env::set_env(env::unlock_ultimate_creditor);
	auto qi = create_valid_without_reference();
	validation_result vr = validate(qi);
	spdlog::info(vr.get_validation_error_summary());
        BOOST_TEST(vr.is_valid());
}

BOOST_AUTO_TEST_CASE(test_without_reference_with_reference_number)
{
	env::set_env(env::unlock_ultimate_creditor);
	// check valid before modifying QrInvoice
	auto qi = create_valid_creditor_reference();
        BOOST_TEST(validate(qi).is_valid());

        // when setting reference number for type NON (without reference), a validation error must occur
        qi.get_payment_reference().set_reference("110001234560000000000813457");
	validation_result vr = validate(qi);
	spdlog::info(vr.get_validation_error_summary());
        BOOST_TEST(vr.has_errors("payment_reference.reference"));
        BOOST_TEST(vr.get_errors().size() == 1);
}

// ----------
// BILL INFORMATION
// ----------

BOOST_AUTO_TEST_CASE(test_invalid_prefix)
{
	env::set_env(env::unlock_ultimate_creditor);
	// check valid before modifying QrInvoice
	auto qi = create_valid_creditor_reference();
	BOOST_TEST(validate(qi).is_valid());

	// when setting bill_information without // prefix, a validation error must occur
	qi.get_payment_reference().get_additional_information().set_bill_information("S1/10/10201409/11/190512/20/1400.000-53/30/106017086/31/180508/32/7.7/40/2:10;0:30");
	validation_result vr = validate(qi);
	spdlog::info(vr.get_validation_error_summary());
	BOOST_TEST(vr.has_errors("payment_reference.additional_information.bill_information"));
	BOOST_TEST(vr.get_errors().size() == 1);
}


BOOST_AUTO_TEST_CASE(test_invalid_prefix_code_length)
{
	env::set_env(env::unlock_ultimate_creditor);
	// check valid before modifying QrInvoice
	auto qi = create_valid_creditor_reference();
	BOOST_TEST(validate(qi).is_valid());

	// when setting bill_information with a three char code after the // prefix, a validation error must occur
	qi.get_payment_reference().get_additional_information().set_bill_information("//ABC/10/10201409");
	validation_result vr = validate(qi);
	spdlog::info(vr.get_validation_error_summary());
	BOOST_TEST(vr.has_errors("payment_reference.additional_information.bill_information"));
	BOOST_TEST(vr.get_errors().size() == 1);
}


BOOST_AUTO_TEST_CASE(test_valid_prefix_code)
{
	env::set_env(env::unlock_ultimate_creditor);
	// check valid before modifying QrInvoice
	auto qi = create_valid_creditor_reference();
	BOOST_TEST(validate(qi).is_valid());

	// when setting bill_information with a two char code after the // prefix, no validation must occur
	qi.get_payment_reference().get_additional_information().set_bill_information("//CB/abcdefghijklmnopqrestuvwxyz");
	BOOST_TEST(validate(qi).is_valid());
	BOOST_TEST(validate(qi).is_valid());
}


BOOST_AUTO_TEST_SUITE_END()

