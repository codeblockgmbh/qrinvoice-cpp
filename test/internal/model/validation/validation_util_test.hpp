
#ifndef QR_INVOICE_TEST_VALIDATION_UTIL_TEST_HPP
#define QR_INVOICE_TEST_VALIDATION_UTIL_TEST_HPP

#include <string>


namespace qrinvoice {
namespace test {
namespace validation {
namespace util {


const std::string get_all_allowed_characters();


} // namespace util
} // namespace validation
} // namespace test
} // namespace qrinvoice


#endif // QR_INVOICE_TEST_VALIDATION_UTIL_TEST_HPP

