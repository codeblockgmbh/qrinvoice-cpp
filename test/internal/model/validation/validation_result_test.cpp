
#include <boost/test/unit_test.hpp>

#include <qrinvoice/model/validation.hpp>

#include <qr_invoice_swiss_qr_code_internal.hpp>

using namespace qrinvoice;
using namespace qrinvoice::model;
using namespace qrinvoice::model::validation;


namespace {

constexpr const char* const test_path = "test_path";
constexpr const char* const non_error_data_path = "non_error_data_path";

} // namespace


BOOST_AUTO_TEST_SUITE(model_validation)


BOOST_AUTO_TEST_CASE(test_empty_validation_result)
{
	const validation_result vr{};

	BOOST_TEST(vr.empty());
	BOOST_TEST(vr.get_errors().empty());
	BOOST_TEST(vr.is_valid());
	BOOST_TEST(vr.is_valid(test_path));
	BOOST_TEST(vr.is_valid(non_error_data_path));

	BOOST_TEST(!vr.has_errors());
	BOOST_TEST(!vr.has_errors(test_path));
	BOOST_TEST(!vr.has_errors(non_error_data_path));
	BOOST_TEST(vr.get_validation_error_summary() == "No validation errors");

        // mustn't throw, but if it should, test will fail
        vr.throw_exception_on_errors();
}

BOOST_AUTO_TEST_CASE(test_one_valid_error)
{
	validation_result vr{};
	vr.add_error(test_path, "", "fake_err_key", {});

	BOOST_TEST(!vr.empty());
	BOOST_TEST(!vr.get_errors().empty());
	BOOST_TEST(vr.get_errors().size() == 1);
	BOOST_TEST(!vr.is_valid());
	BOOST_TEST(!vr.is_valid(test_path));
	BOOST_TEST(vr.is_valid(non_error_data_path));

	BOOST_TEST(vr.has_errors());
	BOOST_TEST(vr.has_errors(test_path));
	BOOST_TEST(!vr.has_errors(non_error_data_path));
	BOOST_TEST(vr.get_validation_error_summary().find(test_path) != std::string::npos);

	try {
		// must throw, but if it shouldn't, test will fail
		vr.throw_exception_on_errors();
		BOOST_FAIL("validation_exception expected but not received");
	} catch (const validation_exception& ex) {
		// expected
	}
}


BOOST_AUTO_TEST_SUITE_END()

