
#include <boost/test/unit_test.hpp>

#include <qrinvoice/model/validation.hpp>

#include <qr_invoice_swiss_qr_code_internal.hpp>
#include <model/validation/address_validator_internal.hpp>

#include <spdlog/spdlog.h>

using namespace qrinvoice;
using namespace qrinvoice::model;
using namespace qrinvoice::model::validation;


BOOST_AUTO_TEST_SUITE(model_validation)


BOOST_AUTO_TEST_CASE(test_validate_creditor)
{
	creditor crdtr {
		address::builder()
			.structured_address()
			.name("Robert Schneider AG")
			.postal_code("2501")
			.city("Biel")
			.country("CH")
			.build()
	};

	validation_result vr;
	validate(crdtr, vr);
	spdlog::info(vr.get_validation_error_summary());
	BOOST_TEST(vr.empty());
}

BOOST_AUTO_TEST_CASE(test_validate_creditor_combined)
{
    creditor crdtr {
            address::builder()
                    .combined_address()
                    .name("Robert Schneider AG")
                    .postal_code("2501")
                    .city("Biel")
                    .country("CH")
                    .build()
    };

    validation_result vr;
    validate(crdtr, vr);
    spdlog::info(vr.get_validation_error_summary());
    BOOST_TEST(vr.has_errors());
}

BOOST_AUTO_TEST_CASE(test_invalid_empty)
{
	creditor crdtr {
		address::builder()
			.address_type(address_type::structured)
			.name("Robert Schneider AG")
			.build()
	};
	
	validation_result vr;
	validate(crdtr, vr);
	spdlog::info(vr.get_validation_error_summary());
	BOOST_TEST(!vr.empty());
}

BOOST_AUTO_TEST_CASE(test_invalid_too_long)
{
	creditor crdtr {
		address::builder()
			.address_type(address_type::structured)
			.name("This is a way too long company name that exceeds 70 characters by a 5 chars")
			.build()
	};

	validation_result vr;
	validate(crdtr, vr);
	spdlog::info(vr.get_validation_error_summary());
	BOOST_TEST(!vr.empty());
}


BOOST_AUTO_TEST_SUITE_END()

