
#include <iostream>
#include <string>
#include <boost/test/unit_test.hpp>
#include <boost/test/output_test_stream.hpp>

#include <config/env_vars_internal.hpp>
#include <qrinvoice/model.hpp>
#include <qrinvoice/currency.hpp>

#include <util/iban_util_internal.hpp>
#include <data/test_ibans.hpp>
#include <model/validation/validation_util_test.hpp>
#include <model/mapper/model_to_swiss_payments_code_internal.hpp>


using namespace qrinvoice;
using namespace qrinvoice::model;


namespace {


std::string generate(int len)
{
	auto allowed_chars = test::validation::util::get_all_allowed_characters();
	std::cout << allowed_chars << '\n';

	std::string chars;
	chars.reserve(len);
	for (int i = 0; i < len; ++i)
		chars += allowed_chars[i % allowed_chars.size()];

	std::cout << chars << " <-> " << chars.size() << '\n';
	return chars;
}

qr_invoice create_qr_invoice()
{
	qrinvoice::model::qr_invoice::builder qr_invoice_builder;

	qr_invoice_builder.creditor_iban(test::data::ibans::qr_iban_ch4431999123000889012);

	qr_invoice_builder.payment_amount_info()
		.chf(654321949.75);

	qr_invoice_builder.creditor()
		.structured_address()
		.name(generate(70))
		.street_name(generate(70))
		.house_number(generate(16))
		.postal_code(generate(16))
		.city(generate(35))
		.country("CH");

	qr_invoice_builder.ultimate_creditor()
		.structured_address()
		.name(generate(70))
		.street_name(generate(70))
		.house_number(generate(16))
		.postal_code(generate(16))
		.city(generate(35))
		.country("CH");

	qr_invoice_builder.ultimate_debtor()
		.structured_address()
		.name(generate(70))
		.street_name(generate(70))
		.house_number(generate(16))
		.postal_code(generate(16))
		.city(generate(35))
		.country("CH");

	qr_invoice_builder.payment_reference()
		// SCOR is the longest type, however QR allows to use a longer reference number
		.qr_reference("110001234560000000000813457");

	qr_invoice_builder.additional_information()
		.unstructured_message(generate(140));

	qr_invoice_builder.alternative_scheme_params({ generate(100), generate(100) });

	return qr_invoice_builder.build();
}

void validate()
{
    const auto qi = create_qr_invoice();
	const auto spc = map_model_to_swiss_payments_code(qi).to_swiss_payments_code_string();
}

} // namespace


BOOST_AUTO_TEST_SUITE(model)


BOOST_AUTO_TEST_CASE(qr_invoice_builder_max_length_test)
{
	env::set_env(env::unlock_ultimate_creditor);
	validate();
}

BOOST_AUTO_TEST_SUITE_END()

