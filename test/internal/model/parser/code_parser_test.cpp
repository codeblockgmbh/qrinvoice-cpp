
#include <iostream>
#include <boost/test/unit_test.hpp>

#include <qrinvoice/model.hpp>
#include <qrinvoice/code_parser.hpp>
#include <qrinvoice/model/validation.hpp>


using namespace qrinvoice;
using namespace qrinvoice::model;


BOOST_AUTO_TEST_SUITE(model)


BOOST_AUTO_TEST_CASE(test_parse_empty)
{
	BOOST_CHECK_THROW(code_parser::parse(""), parse_exception);
}

BOOST_AUTO_TEST_CASE(test_parse_with_only_type)
{
	BOOST_CHECK_THROW(code_parser::parse("SPC"), parse_exception);
}

BOOST_AUTO_TEST_CASE(test_parse_with_type_and_version)
{
	BOOST_CHECK_THROW(code_parser::parse("SPC\n"
		"0200"), parse_exception);
}

BOOST_AUTO_TEST_CASE(test_parse_with_type_version_and_coding_type)
{
	code_parser::parse("SPC\n"
		"0200\n"
		"1");
}

BOOST_AUTO_TEST_CASE(test_parse_with_type_version_and_coding_type_and_manually_validate)
{
	const auto qi = code_parser::parse("SPC\n"
		"0200\n"
		"1");

	const auto vr = validation::validate(qi);
	std::cout << vr.get_validation_error_summary() << '\n';
}

BOOST_AUTO_TEST_CASE(test_parse_and_validate_with_type_version_and_coding_type)
{
	BOOST_CHECK_THROW(code_parser::parse_and_validate("SPC\n"
		"0200\n"
		"1"), validation::validation_exception);
}


BOOST_AUTO_TEST_SUITE_END()

