
#include <iostream>
#include <boost/test/unit_test.hpp>

#include <qrinvoice/model.hpp>
#include <qrinvoice/code_parser.hpp>
#include <qrinvoice/model/validation.hpp>

#include <system/test_data_file_registry.hpp>


using namespace qrinvoice;
using namespace qrinvoice::model;
using namespace qrinvoice::test::system;


BOOST_AUTO_TEST_SUITE(model)


BOOST_AUTO_TEST_CASE(test_parse_and_validate)
{
	test_data_file_registry file_registry;
	auto files = file_registry.data();
	for (const auto& file_name : files) {
		const std::string spc_str = test_data_file_registry::get_file_content(file_name);
		const auto qi = code_parser::parse_and_validate(spc_str);
		BOOST_TEST(qi.get_header().get_qr_type() == "SPC");
	}
}

BOOST_AUTO_TEST_CASE(test_parse_and_manually_validate)
{
	test_data_file_registry file_registry;
	auto files = file_registry.data();
	for (const auto& file_name : files) {
		std::cout << "test data file: " << file_name << std::endl;
		const std::string spc_str = test_data_file_registry::get_file_content(file_name);
		const auto qi = code_parser::parse(spc_str);
		BOOST_TEST(qi.get_header().get_qr_type() == "SPC");

		const validation::validation_result vr = validation::validate(qi);
		BOOST_TEST(vr.is_valid(), vr.get_validation_error_summary());
	}
}



BOOST_AUTO_TEST_SUITE_END()

