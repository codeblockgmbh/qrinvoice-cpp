
#include <boost/test/unit_test.hpp>

#include <qrinvoice/model.hpp>

using namespace qrinvoice;
using namespace qrinvoice::model;


BOOST_AUTO_TEST_SUITE(model)


BOOST_AUTO_TEST_CASE(test_valid_address_type_codes)
{
	BOOST_TEST(address_type::parse("K").get() == address_type::combined);
	BOOST_TEST(address_type::parse("S").get() == address_type::structured);

	BOOST_TEST(address_type{address_type::combined}.get_address_type_code() == "K");
	BOOST_TEST(address_type{address_type::structured}.get_address_type_code() == "S");
	BOOST_TEST(address_type{address_type::unset}.get_address_type_code() == "");
}

BOOST_AUTO_TEST_CASE(test_invalid_address_type_codes)
{
	BOOST_CHECK_THROW(address_type::parse("x"), parse_exception);
	BOOST_CHECK_THROW(address_type::parse(""), parse_exception);

	BOOST_CHECK_THROW(address_type{static_cast<address_type::addr_type>(3)}.get_address_type_code(), technical_exception);
}


BOOST_AUTO_TEST_SUITE_END()

