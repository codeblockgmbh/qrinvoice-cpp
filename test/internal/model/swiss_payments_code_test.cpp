
#include <boost/test/unit_test.hpp>
#include <boost/regex.hpp>

#include <qrinvoice/model.hpp>
#include <qrinvoice/model/parse/parse_swiss_payments_code.hpp>

#include <qr_invoice_swiss_qr_code_internal.hpp>
#include <spdlog/spdlog.h>

using namespace qrinvoice;
using namespace qrinvoice::model;


namespace {

static const boost::regex bill_information_pattern{"^//[^/]{2,2}(/.*)?$"};

void line_feed_test_asserts(const swiss_payments_code& spc)
{
	BOOST_TEST(spc.qr_type == "SPC");
	BOOST_TEST(spc.version == "0200");
	BOOST_TEST(spc.coding == "1");
}

} // namespace


BOOST_AUTO_TEST_SUITE(model)


BOOST_AUTO_TEST_CASE(test_to_qr_code_string)
{
	swiss_payments_code spc;
	spc.qr_type = "SPC";
	spc.amt = "1234.75";
	spdlog::info(spc.to_swiss_payments_code_string());

	const auto spc_str = spc.to_swiss_payments_code_string();
	const auto parsed = parse_swiss_payments_code(spc_str);
	spdlog::info(parsed.to_swiss_payments_code_string());

	const auto parsed_str = parsed.to_swiss_payments_code_string();

	BOOST_CHECK(spc_str == parsed_str);
}

BOOST_AUTO_TEST_CASE(test_parse_spc)
{
	const auto spc = parse_swiss_payments_code(qrinvoice::model::spc::qr_type);
	spdlog::info(spc.to_swiss_payments_code_string());
	BOOST_TEST(spc.qr_type == qrinvoice::model::spc::qr_type);
}

BOOST_AUTO_TEST_CASE(test_parse_cr_lf)
{
        const auto spc = parse_swiss_payments_code("SPC\r\n"
                "0200\r\n"
                "1");
	spdlog::info(spc.to_swiss_payments_code_string());

	BOOST_TEST(spc.qr_type == qrinvoice::model::spc::qr_type);
        line_feed_test_asserts(spc);
}

BOOST_AUTO_TEST_CASE(test_parse_lf)
{
        const auto spc = parse_swiss_payments_code("SPC\n"
                "0200\n"
                "1");
	spdlog::info(spc.to_swiss_payments_code_string());

	BOOST_TEST(spc.qr_type == qrinvoice::model::spc::qr_type);
        line_feed_test_asserts(spc);
}

BOOST_AUTO_TEST_CASE(test_parse_cr)
{
        const auto spc = parse_swiss_payments_code("SPC\r"
                "0200\r"
                "1");
	spdlog::info(spc.to_swiss_payments_code_string());

	BOOST_TEST(spc.qr_type == qrinvoice::model::spc::qr_type);
        line_feed_test_asserts(spc);
}

BOOST_AUTO_TEST_CASE(test_bill_information_pattern)
{
	BOOST_TEST(boost::regex_match("//S1", bill_information_pattern));
	BOOST_TEST(boost::regex_match("//CB",bill_information_pattern));
	BOOST_TEST(boost::regex_match("//XY", bill_information_pattern));

	BOOST_TEST(!boost::regex_match("///S1", bill_information_pattern));
	BOOST_TEST(!boost::regex_match("///XYZ",bill_information_pattern));
	BOOST_TEST(!boost::regex_match("/S1",bill_information_pattern));
	BOOST_TEST(!boost::regex_match("S1", bill_information_pattern));
}

BOOST_AUTO_TEST_SUITE_END()

