
#include <boost/test/unit_test.hpp>

#include <qrinvoice/util/charset_encoding_util.hpp>
#include <cppcodec/base64_rfc4648.hpp>
#include <iostream>
#include <fstream>

BOOST_AUTO_TEST_SUITE(charset_encoding_util)


    BOOST_AUTO_TEST_CASE(test_iso_8859_1)
    {
        /*
        TODO implement test
        BOOST_TEST("a" == qrinvoice::charset_encoding_util::to_utf8("a","ISO-8859-1ISO-8859-1"));

        std::ifstream file(R"(C:\dev\git\qrinvoice-cpp\test\resources\charset\sample_iso8859-1.txt)");
        BOOST_TEST(file.good());
        std::string fooo((std::istreambuf_iterator<char>(file)),
                        std::istreambuf_iterator<char>());
        std::cout << "iso 8859-1: " << fooo << std::endl;

        const std::vector<uint8_t> iso_8859_1 = cppcodec::base64_rfc4648::decode("YWJjWFlaw6TDtsO8w4TDlsOcw6nDicOow4jDoMOAJDEyMw0K");
        std::cout << "iso 8859-1 uint8: " << reinterpret_cast<const char*>(iso_8859_1.data()) << std::endl;

        const std::string &iso_8859_1_str = reinterpret_cast<const char*>(iso_8859_1.data());
        std::cout << "iso 8859-1: " << iso_8859_1_str << std::endl;
        BOOST_TEST("abcXYZäöüÄÖÜéÉèÈàÀ$123" != iso_8859_1_str);
        const std::string &utf8_str = qrinvoice::charset_encoding_util::to_utf8(fooo, "ISO-8859-1");
        std::cout << "utf8: " << utf8_str << std::endl;

        BOOST_TEST("YWJjWFlaw6TDtsO8w4TDlsOcw6nDicOow4jDoMOAJDEyMw0K" == cppcodec::base64_rfc4648::encode(fooo));
        BOOST_TEST("abcXYZäöüÄÖÜéÉèÈàÀ$123" == utf8_str);
        */
    }

BOOST_AUTO_TEST_SUITE_END()

