
#include <boost/test/unit_test.hpp>

#include <util/amount_util_internal.hpp>

using namespace qrinvoice;
using namespace qrinvoice::amount_util;


BOOST_AUTO_TEST_SUITE(util)

BOOST_AUTO_TEST_CASE(test_format_amount)
{
	BOOST_TEST(format_amount_for_print(0) == "0.00");
	BOOST_TEST(format_amount_for_print(1) == "1.00");
	BOOST_TEST(format_amount_for_print(10) == "10.00");
	BOOST_TEST(format_amount_for_print(1.10) == "1.10");
	BOOST_TEST(format_amount_for_print(999.99) == "999.99");
	BOOST_TEST(format_amount_for_print(9999.99) == "9 999.99");
	BOOST_TEST(format_amount_for_print(999999.99) == "999 999.99");
	BOOST_TEST(format_amount_for_print(9999999.99) == "9 999 999.99");
	BOOST_TEST(format_amount_for_print(999999999.99) == "999 999 999.99");
}



BOOST_AUTO_TEST_SUITE_END()

