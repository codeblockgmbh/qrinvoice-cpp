
#include <boost/test/unit_test.hpp>

#include <qrinvoice/util/creditor_reference_util.hpp>
#include <qrinvoice/technical_exception.hpp>

#include <string>

using namespace qrinvoice::creditor_reference_util;


BOOST_AUTO_TEST_SUITE(creditor_reference_util)

BOOST_AUTO_TEST_CASE(is_valid_creditor_reference_tests)
{
	BOOST_TEST(is_valid_creditor_reference("RF45 1234 5123 45"));
	BOOST_TEST(is_valid_creditor_reference("RF451234512345"));
	BOOST_TEST(is_valid_creditor_reference("RF411274"));
	BOOST_TEST(is_valid_creditor_reference("RF941290"));
	BOOST_TEST(is_valid_creditor_reference("RF551410"));
	BOOST_TEST(is_valid_creditor_reference("RF68AB2G5"));
	BOOST_TEST(is_valid_creditor_reference("RF68ab2g5"));

	BOOST_TEST(!is_valid_creditor_reference("AB451234512345"), "wrong prefix");
	BOOST_TEST(!is_valid_creditor_reference("RF461234512345"), "wrong check digits");
	BOOST_TEST(!is_valid_creditor_reference("RF4"), "too short");
	BOOST_TEST(!is_valid_creditor_reference(""), "empty string");
	BOOST_TEST(!is_valid_creditor_reference("RF451234351234523423423423"), "too long");
	BOOST_TEST(!is_valid_creditor_reference("RF35C4"), "wrong check digits");
	BOOST_TEST(!is_valid_creditor_reference("RF214377"), "wrong check digits");
	BOOST_TEST(!is_valid_creditor_reference("ä"), "invalid character");

	// some random creditor references generated using an online service
	BOOST_TEST(is_valid_creditor_reference("RF96DPM2A4WAW0SLLQA6"));
	BOOST_TEST(is_valid_creditor_reference("RF06GZAXTCA"));
	BOOST_TEST(is_valid_creditor_reference("RF23GW3I60LF6RFYPF"));
	BOOST_TEST(is_valid_creditor_reference("RF14QYI17IRZPRZNMC6EJL5"));
	BOOST_TEST(is_valid_creditor_reference("RF65ANEFDWKCMPYFO3I2"));
	BOOST_TEST(is_valid_creditor_reference("RF380FHSFUG34KQUX4"));
	BOOST_TEST(is_valid_creditor_reference("RF4756COLMQZ17E8YMZP6FW"));
	BOOST_TEST(is_valid_creditor_reference("RF9048PR104Z"));
	BOOST_TEST(is_valid_creditor_reference("RF94T"));
	BOOST_TEST(is_valid_creditor_reference("RF815JZCU5S97KAJJMIAC6X"));
	BOOST_TEST(is_valid_creditor_reference("RF10YQ0Q91C91V"));
	BOOST_TEST(is_valid_creditor_reference("RF61PGQRC15X9WXEHLN"));
	BOOST_TEST(is_valid_creditor_reference("RF746C2A0WC"));
	BOOST_TEST(is_valid_creditor_reference("RF17R2UMJ8FAZV8CBFAUC"));
	BOOST_TEST(is_valid_creditor_reference("RF62BJ4OQYRWYTT0NMH4BQDG"));
	BOOST_TEST(is_valid_creditor_reference("RF54C14EFPF0R1DCNRT"));
	BOOST_TEST(is_valid_creditor_reference("RF71KR5F5NSAU10"));
	BOOST_TEST(is_valid_creditor_reference("RF05H3F45OOQ2T8"));
	BOOST_TEST(is_valid_creditor_reference("RF54XUB56UZZN5FBAYBUDCJ4"));
	BOOST_TEST(is_valid_creditor_reference("RF396TWEJ64E3"));
}


BOOST_AUTO_TEST_CASE(test_format_creditor_reference)
{
	BOOST_TEST("RF45 1234 5123 45" == format_creditor_reference("RF45 1234 5123 45"));
	BOOST_TEST("RF45 1234 5123 45" == format_creditor_reference("RF451234512345"));
	BOOST_TEST("RF41 1274" == format_creditor_reference("RF411274"));
	BOOST_TEST("RF94 1290" == format_creditor_reference("RF941290"));
	BOOST_TEST("RF68 AB2G 5" == format_creditor_reference("RF68AB2G5"));
	BOOST_TEST("RF68 AB2G 5" == format_creditor_reference("RF68ab2g5"));
	BOOST_TEST("" == format_creditor_reference(""));
}

BOOST_AUTO_TEST_CASE(test_normalize)
{
	BOOST_TEST("RF451234512345" == normalize_creditor_reference("RF45 1234 5123 45"));
	BOOST_TEST("RF451234512345" == normalize_creditor_reference("RF451234512345"));
	BOOST_TEST("RF411274" == normalize_creditor_reference("RF411274"));
	BOOST_TEST("RF941290" == normalize_creditor_reference("RF941290"));
	BOOST_TEST("RF68AB2G5" == normalize_creditor_reference("RF68 AB2G 5"));
	BOOST_TEST("RF68AB2G5" == normalize_creditor_reference("RF68 ab2g 5"));
	BOOST_TEST("" == normalize_creditor_reference(""));
}

BOOST_AUTO_TEST_CASE(test_create_creditor_reference)
{
	BOOST_TEST("RF741"== create_creditor_reference("1"));
	BOOST_TEST("RF7401"== create_creditor_reference("01"));
	BOOST_TEST("RF74001"== create_creditor_reference("001"));
	BOOST_TEST("RF635"== create_creditor_reference("5"));
	BOOST_TEST("RF6812"== create_creditor_reference("12"));
	BOOST_TEST("RF78123"== create_creditor_reference("123"));
	BOOST_TEST("RF96187"== create_creditor_reference("187"));
	BOOST_TEST("RF462348"== create_creditor_reference("2348"));
	BOOST_TEST("RF25A"== create_creditor_reference("a"));
	BOOST_TEST("RF25A"== create_creditor_reference("A"));
	BOOST_TEST("RF87B2D"== create_creditor_reference("B2D"));
	BOOST_TEST("RF25243DF"== create_creditor_reference("243DF"));
	BOOST_TEST("RF374238791"== create_creditor_reference("4238791"));
	BOOST_TEST("RF02O9JLZKZBX0WKCRVID3K9P"== create_creditor_reference("O9JLZKZBX0WKCRVID3K9P"));
	BOOST_TEST("RF374238791"== create_creditor_reference("RF374238791"));
	BOOST_TEST("RF451234512345"== create_creditor_reference("RF45 1234 5123 45"));
	BOOST_TEST(create_creditor_reference("rf374238791") == create_creditor_reference("RF374238791"));

	BOOST_CHECK_THROW(create_creditor_reference("CT8VI6S9U34ZDIM2Q9L3ZA"), qrinvoice::technical_exception);
	BOOST_CHECK_THROW(create_creditor_reference(""), qrinvoice::technical_exception);
	BOOST_CHECK_THROW(create_creditor_reference(""), qrinvoice::technical_exception);
}


BOOST_AUTO_TEST_CASE(test_normalize_format_roundtrip)
{
	BOOST_TEST("RF45 1234 5123 45", format_creditor_reference(normalize_creditor_reference("RF45 1234 5123 45")));
	BOOST_TEST("RF451234512345", normalize_creditor_reference(format_creditor_reference("RF451234512345")));

	// some random creditor references generated using an online service
	BOOST_TEST("RF96DPM2A4WAW0SLLQA6", normalize_creditor_reference(format_creditor_reference("RF96DPM2A4WAW0SLLQA6")));
	BOOST_TEST("RF06GZAXTCA", normalize_creditor_reference(format_creditor_reference("RF06GZAXTCA")));
	BOOST_TEST("RF23GW3I60LF6RFYPF", normalize_creditor_reference(format_creditor_reference("RF23GW3I60LF6RFYPF")));
	BOOST_TEST("RF14QYI17IRZPRZNMC6EJL5", normalize_creditor_reference(format_creditor_reference("RF14QYI17IRZPRZNMC6EJL5")));
	BOOST_TEST("RF65ANEFDWKCMPYFO3I2", normalize_creditor_reference(format_creditor_reference("RF65ANEFDWKCMPYFO3I2")));
	BOOST_TEST("RF380FHSFUG34KQUX4", normalize_creditor_reference(format_creditor_reference("RF380FHSFUG34KQUX4")));
	BOOST_TEST("RF4756COLMQZ17E8YMZP6FW", normalize_creditor_reference(format_creditor_reference("RF4756COLMQZ17E8YMZP6FW")));
	BOOST_TEST("RF9048PR104Z", normalize_creditor_reference(format_creditor_reference("RF9048PR104Z")));
	BOOST_TEST("RF94T", normalize_creditor_reference(format_creditor_reference("RF94T")));
	BOOST_TEST("RF815JZCU5S97KAJJMIAC6X", normalize_creditor_reference(format_creditor_reference("RF815JZCU5S97KAJJMIAC6X")));
	BOOST_TEST("RF10YQ0Q91C91V", normalize_creditor_reference(format_creditor_reference("RF10YQ0Q91C91V")));
	BOOST_TEST("RF61PGQRC15X9WXEHLN", normalize_creditor_reference(format_creditor_reference("RF61PGQRC15X9WXEHLN")));
	BOOST_TEST("RF746C2A0WC", normalize_creditor_reference(format_creditor_reference("RF746C2A0WC")));
	BOOST_TEST("RF17R2UMJ8FAZV8CBFAUC", normalize_creditor_reference(format_creditor_reference("RF17R2UMJ8FAZV8CBFAUC")));
	BOOST_TEST("RF62BJ4OQYRWYTT0NMH4BQDG", normalize_creditor_reference(format_creditor_reference("RF62BJ4OQYRWYTT0NMH4BQDG")));
	BOOST_TEST("RF54C14EFPF0R1DCNRT", normalize_creditor_reference(format_creditor_reference("RF54C14EFPF0R1DCNRT")));
	BOOST_TEST("RF71KR5F5NSAU10", normalize_creditor_reference(format_creditor_reference("RF71KR5F5NSAU10")));
	BOOST_TEST("RF05H3F45OOQ2T8", normalize_creditor_reference(format_creditor_reference("RF05H3F45OOQ2T8")));
	BOOST_TEST("RF54XUB56UZZN5FBAYBUDCJ4", normalize_creditor_reference(format_creditor_reference("RF54XUB56UZZN5FBAYBUDCJ4")));
	BOOST_TEST("RF396TWEJ64E3", normalize_creditor_reference(format_creditor_reference("RF396TWEJ64E3")));
}
	
BOOST_AUTO_TEST_CASE(test_convert_to_numeric_representation)
{
	BOOST_TEST("10" == convert_to_numeric_representation("a"));
	BOOST_TEST("10" == convert_to_numeric_representation("A"));
	BOOST_TEST("11" == convert_to_numeric_representation("b"));
	BOOST_TEST("11" == convert_to_numeric_representation("B"));
	BOOST_TEST("12" == convert_to_numeric_representation("c"));
	BOOST_TEST("12" == convert_to_numeric_representation("C"));
	BOOST_TEST("13" == convert_to_numeric_representation("d"));
	BOOST_TEST("13" == convert_to_numeric_representation("D"));
	BOOST_TEST("14" == convert_to_numeric_representation("e"));
	BOOST_TEST("14" == convert_to_numeric_representation("E"));
	BOOST_TEST("15" == convert_to_numeric_representation("f"));
	BOOST_TEST("15" == convert_to_numeric_representation("F"));
	BOOST_TEST("16" == convert_to_numeric_representation("g"));
	BOOST_TEST("16" == convert_to_numeric_representation("G"));
	BOOST_TEST("17" == convert_to_numeric_representation("h"));
	BOOST_TEST("17" == convert_to_numeric_representation("H"));
	BOOST_TEST("18" == convert_to_numeric_representation("i"));
	BOOST_TEST("18" == convert_to_numeric_representation("I"));
	BOOST_TEST("19" == convert_to_numeric_representation("j"));
	BOOST_TEST("19" == convert_to_numeric_representation("J"));
	BOOST_TEST("20" == convert_to_numeric_representation("k"));
	BOOST_TEST("20" == convert_to_numeric_representation("K"));
	BOOST_TEST("21" == convert_to_numeric_representation("l"));
	BOOST_TEST("21" == convert_to_numeric_representation("L"));
	BOOST_TEST("22" == convert_to_numeric_representation("m"));
	BOOST_TEST("22" == convert_to_numeric_representation("M"));
	BOOST_TEST("23" == convert_to_numeric_representation("n"));
	BOOST_TEST("23" == convert_to_numeric_representation("N"));
	BOOST_TEST("24" == convert_to_numeric_representation("o"));
	BOOST_TEST("24" == convert_to_numeric_representation("O"));
	BOOST_TEST("25" == convert_to_numeric_representation("p"));
	BOOST_TEST("25" == convert_to_numeric_representation("P"));
	BOOST_TEST("26" == convert_to_numeric_representation("q"));
	BOOST_TEST("26" == convert_to_numeric_representation("Q"));
	BOOST_TEST("27" == convert_to_numeric_representation("r"));
	BOOST_TEST("27" == convert_to_numeric_representation("R"));
	BOOST_TEST("28" == convert_to_numeric_representation("s"));
	BOOST_TEST("28" == convert_to_numeric_representation("S"));
	BOOST_TEST("29" == convert_to_numeric_representation("t"));
	BOOST_TEST("29" == convert_to_numeric_representation("T"));
	BOOST_TEST("30" == convert_to_numeric_representation("u"));
	BOOST_TEST("30" == convert_to_numeric_representation("U"));
	BOOST_TEST("31" == convert_to_numeric_representation("v"));
	BOOST_TEST("31" == convert_to_numeric_representation("V"));
	BOOST_TEST("32" == convert_to_numeric_representation("w"));
	BOOST_TEST("32" == convert_to_numeric_representation("W"));
	BOOST_TEST("33" == convert_to_numeric_representation("x"));
	BOOST_TEST("33" == convert_to_numeric_representation("X"));
	BOOST_TEST("34" == convert_to_numeric_representation("y"));
	BOOST_TEST("34" == convert_to_numeric_representation("Y"));
	BOOST_TEST("35" == convert_to_numeric_representation("z"));
	BOOST_TEST("35" == convert_to_numeric_representation("Z"));
	BOOST_TEST("0" == convert_to_numeric_representation("0"));
	BOOST_TEST("1" == convert_to_numeric_representation("1"));
	BOOST_TEST("2" == convert_to_numeric_representation("2"));
	BOOST_TEST("3" == convert_to_numeric_representation("3"));
	BOOST_TEST("4" == convert_to_numeric_representation("4"));
	BOOST_TEST("5" == convert_to_numeric_representation("5"));
	BOOST_TEST("6" == convert_to_numeric_representation("6"));
	BOOST_TEST("7" == convert_to_numeric_representation("7"));
	BOOST_TEST("8" == convert_to_numeric_representation("8"));
	BOOST_TEST("9" == convert_to_numeric_representation("9"));
	BOOST_TEST("2715" == convert_to_numeric_representation("rf"));
	BOOST_TEST("2715" == convert_to_numeric_representation("RF"));
	BOOST_TEST("24919213520351133032201227311813320925271502" == convert_to_numeric_representation("O9JLZKZBX0WKCRVID3K9PRF02"));
}

BOOST_AUTO_TEST_CASE(test_calculate_check_digits_for_reference_number)
{
	BOOST_TEST("45" == calculate_check_digits_for_reference_number("1672303027"));
	BOOST_TEST("65" == calculate_check_digits_for_reference_number("18205"));
	BOOST_TEST("51" == calculate_check_digits_for_reference_number("124"));
	BOOST_TEST("68" == calculate_check_digits_for_reference_number("4377"));
	BOOST_TEST("02" == calculate_check_digits_for_reference_number("O9JLZKZBX0WKCRVID3K9P"));
}

BOOST_AUTO_TEST_CASE(test_calculate_check_digits)
{
	BOOST_TEST("45" == calculate_check_digits("1672303027271500"));
	BOOST_TEST("65" == calculate_check_digits("18205271500"));
	BOOST_TEST("51" == calculate_check_digits("124271500"));
	BOOST_TEST("68" == calculate_check_digits("4377271500"));
	BOOST_TEST("02" == calculate_check_digits("24919213520351133032201227311813320925271500"));
}

BOOST_AUTO_TEST_SUITE_END()
