
#include <boost/test/unit_test.hpp>

#include <qrinvoice/util/qr_reference_util.hpp>
#include <qrinvoice/technical_exception.hpp>

using namespace qrinvoice::qr_reference_util;


BOOST_AUTO_TEST_SUITE(qr_reference_util)

BOOST_AUTO_TEST_CASE(test_is_valid)
{
	BOOST_TEST(is_valid_qr_reference("00 00000 00000 00000 00000 05989"));
	BOOST_TEST(is_valid_qr_reference("11 27098 76540 00000 55555 22228"));
	BOOST_TEST(is_valid_qr_reference("11 00012 34560 00000 00008 13457"));
	BOOST_TEST(is_valid_qr_reference("110001234560000000000813457"));

	BOOST_TEST(!is_valid_qr_reference("11 00012 34560 00000 00008 13456"), "wrong checksum digit");
	BOOST_TEST(!is_valid_qr_reference("11 27098 76540 00000 55555 22229"), "wrong checksum digit");
	BOOST_TEST(!is_valid_qr_reference("1"), "too short - 27 digits expected");
	BOOST_TEST(!is_valid_qr_reference("1234567890123456789012345678"), "too long - 27 digits expected");
}

BOOST_AUTO_TEST_CASE(test_normalize_qr_reference)
{
	BOOST_TEST("000000000000000000000005989" == normalize_qr_reference("00 00000 00000 00000 00000 05989"));
	BOOST_TEST("110001234560000000000813457" == normalize_qr_reference("110001234560000000000813457"));
	BOOST_TEST("" == normalize_qr_reference(""));
}

BOOST_AUTO_TEST_CASE(test_format)
{
	BOOST_TEST("00 00000 00000 00000 00000 05989" == format_qr_reference("000000000000000000000005989"));
	BOOST_TEST("11 00012 34560 00000 00008 13457" == format_qr_reference("110001234560000000000813457"));
	BOOST_TEST("1" == format_qr_reference("1"));
	BOOST_TEST("" == format_qr_reference(""));
}

BOOST_AUTO_TEST_CASE(test_create_qr_reference)
{
    BOOST_TEST("000000000000000000000005989" == create_qr_reference("00 00000 00000 00000 00000 05989"));
    BOOST_TEST("000000000000000000000005989" == create_qr_reference("000000000000000000000005989"));
    BOOST_TEST("000000000000000000000000011" == create_qr_reference("1"));
    BOOST_TEST("000000000000000000000000420" == create_qr_reference("42"));
    BOOST_TEST("000000000000000000000001273" == create_qr_reference("127"));

    BOOST_CHECK_THROW(create_qr_reference(""), qrinvoice::technical_exception);
}


BOOST_AUTO_TEST_CASE(test_create_qr_reference_with_customer_id)
{
    BOOST_TEST("123000000000000000000000017" == create_qr_reference("123", "1"));
    BOOST_TEST("123456789012300000000000423" == create_qr_reference("123456 7890123", "42"));
    BOOST_TEST("123000000000000000000001279" == create_qr_reference("123", "127"));
    BOOST_TEST("123456123456789012345678904" == create_qr_reference("123 456", "1234567890 1234567890"));
}

BOOST_AUTO_TEST_CASE(test_create_qr_reference_with_empty_customer_id)
{
    BOOST_TEST("000000000000000000000005989" == create_qr_reference("", "00 00000 00000 00000 00000 05989"));
    BOOST_TEST("000000000000000000000005989" == create_qr_reference("", "000000000000000000000005989"));
    BOOST_TEST("000000000000000000000000011" == create_qr_reference("", "1"));
    BOOST_TEST("000000000000000000000000420" == create_qr_reference("", "42"));
    BOOST_TEST("000000000000000000000001273" == create_qr_reference("", "127"));
}

BOOST_AUTO_TEST_CASE(test_create_qr_reference_with_customer_id_too_long)
{
    BOOST_CHECK_THROW(create_qr_reference("123", "000000000000000000000005989"), qrinvoice::technical_exception);
    BOOST_CHECK_THROW(create_qr_reference("123456789012345678901234567890", "1"), qrinvoice::technical_exception);
}

BOOST_AUTO_TEST_CASE(test_create_qr_reference_with_customer_id_input_empty)
{
    BOOST_CHECK_THROW(create_qr_reference("123", ""), qrinvoice::technical_exception);
}

BOOST_AUTO_TEST_SUITE_END()

