
#include <boost/test/unit_test.hpp>

#include <qrinvoice/util/country_util.hpp>

using namespace qrinvoice::country_util;


BOOST_AUTO_TEST_SUITE(util)

BOOST_AUTO_TEST_CASE(valid_iso_code_test)
{
	BOOST_TEST(is_valid_iso_code("CH"));
	BOOST_TEST(is_valid_iso_code("LI"));
	BOOST_TEST(is_valid_iso_code("DE"));
}


BOOST_AUTO_TEST_SUITE_END()

