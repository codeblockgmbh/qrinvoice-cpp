
#include <string>
#include <boost/test/unit_test.hpp>

#include <qrinvoice/util/iban_util.hpp>
#include <util/iban_util_internal.hpp>
#include <data/test_ibans.hpp>

using namespace qrinvoice::iban_util;


BOOST_AUTO_TEST_SUITE(util)

BOOST_AUTO_TEST_CASE(test_format_iban)
{
	BOOST_TEST(format_iban("CH3908704016075473007") == "CH39 0870 4016 0754 7300 7");
}

BOOST_AUTO_TEST_CASE(test_format_iban_empty_string)
{
	BOOST_TEST(format_iban("") == "");
}

BOOST_AUTO_TEST_CASE(test_format_iban_blank_string)
{
	BOOST_TEST(format_iban("    ") == "");
}

BOOST_AUTO_TEST_CASE(test_valid_ch_li_ibans)
{
	BOOST_TEST(is_valid_iban("CH3908704016075473007", true));
	BOOST_TEST(is_valid_iban("CH39 0870 4016 0754 7300 7", true));
	BOOST_TEST(is_valid_iban("LI21 0881 0000 2324 013A A", true));
}

BOOST_AUTO_TEST_CASE(test_valid_foreign_ibans)
{
	BOOST_TEST(is_valid_iban("BE68844010370034", false));
	BOOST_TEST(is_valid_iban("MT98MMEB44093000000009027293051", false));
}

BOOST_AUTO_TEST_CASE(test_invalid_ibans)
{
	BOOST_TEST(!is_valid_iban("CH39", false));
	BOOST_TEST(!is_valid_iban("CH3908704016075473001", false));
	BOOST_TEST(!is_valid_iban("AR3908704016075473007", false));
}

BOOST_AUTO_TEST_CASE(test_empty_string)
{
	BOOST_TEST(!is_valid_iban("", false));
}

BOOST_AUTO_TEST_CASE(test_lengths)
{
	BOOST_TEST(!is_valid_iban("CH390870401607", false));				// 14 < 15
	BOOST_TEST(!is_valid_iban("CH390870401607547300723432423234234", false));	// 35 > 34
}

BOOST_AUTO_TEST_CASE(test_german_iban)
{
	BOOST_TEST(is_valid_iban("DE89 3704 0044 0532 0130 00", false));
}

BOOST_AUTO_TEST_CASE(test_german_iban_but_invalid_due_te_country_code)
{
	BOOST_TEST(!is_valid_iban("DE89 3704 0044 0532 0130 00", true));
}

BOOST_AUTO_TEST_CASE(test_is_qr_iban)
{
	BOOST_TEST(is_qr_iban("CH44 3199 9123 0008 8901 2"));
	BOOST_TEST(is_qr_iban("CH4431999123000889012"));
	BOOST_TEST(!is_qr_iban("CH31 8123 9000 0012 4568 9"));
}

BOOST_AUTO_TEST_CASE(test_iban_test_data)
{
	BOOST_TEST(!is_qr_iban(qrinvoice::test::data::ibans::iban_ch3709000000304442225));
	BOOST_TEST(!is_qr_iban(qrinvoice::test::data::ibans::iban_ch3908704016075473007));
	BOOST_TEST(is_qr_iban(qrinvoice::test::data::ibans::qr_iban_ch4431999123000889012));

	BOOST_TEST(is_valid_iban(qrinvoice::test::data::ibans::iban_ch3709000000304442225, true));
	BOOST_TEST(is_valid_iban(qrinvoice::test::data::ibans::iban_ch3908704016075473007, true));
	BOOST_TEST(is_valid_iban(qrinvoice::test::data::ibans::qr_iban_ch4431999123000889012, true));
}


BOOST_AUTO_TEST_SUITE_END()

