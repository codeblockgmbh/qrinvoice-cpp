
#include <boost/test/unit_test.hpp>

#include <qrinvoice/model/parse_exception.hpp>
#include <qrinvoice/util/string_util.hpp>

#include <util/string_util_internal.hpp>

using namespace qrinvoice;
using namespace qrinvoice::string_util;
using namespace qrinvoice::string_util::details;


BOOST_AUTO_TEST_SUITE(util)


BOOST_AUTO_TEST_CASE(test_is_trimmable)
{
	BOOST_TEST(!is_trimmable(""));
	BOOST_TEST(is_trimmable(" "));
	BOOST_TEST(is_trimmable(" a"));
	BOOST_TEST(is_trimmable(" a "));
	BOOST_TEST(is_trimmable("a "));
	BOOST_TEST(!is_trimmable("a"));
}

BOOST_AUTO_TEST_CASE(test_parse)
{
	BOOST_TEST(parse<int>("12") == 12);
	BOOST_CHECK_THROW(parse<int>("ab"), model::parse_exception);
}

BOOST_AUTO_TEST_CASE(test_remove_white_spaces)
{
	BOOST_TEST(remove_white_spaces("abc") == "abc");
	BOOST_TEST(remove_white_spaces(" abc") == "abc");
	BOOST_TEST(remove_white_spaces("abc ") == "abc");
	BOOST_TEST(remove_white_spaces(" abc ") == "abc");
	BOOST_TEST(remove_white_spaces(" a b c ") == "abc");
	BOOST_TEST(remove_white_spaces(" a\tb\nc ") == "abc");
	BOOST_TEST(remove_white_spaces("  ") == "");
	BOOST_TEST(remove_white_spaces(" \t\n ") == "");
}

BOOST_AUTO_TEST_CASE(test_get_numeric_value)
{
	BOOST_TEST(get_numeric_value('A') == 10);
	BOOST_TEST(get_numeric_value('Z') == 35);
	BOOST_TEST(get_numeric_value('a') == 42);
	BOOST_TEST(get_numeric_value('z') == 67);
}

BOOST_AUTO_TEST_CASE(test_split_tokens_and_words)
{
	std::vector<std::string> vs{ "abc", "def", "ghi" };
	BOOST_TEST(split_tokens("abc def ghi", boost::regex{" "}) == vs);
	BOOST_TEST(split_tokens("abc//def//ghi", boost::regex{"//"}) == vs);
	BOOST_TEST(split_words("abc def\t\nghi") == vs);
}


BOOST_AUTO_TEST_CASE(test_utf8_length)
{
	BOOST_TEST(utf8_length("") == 0);
	// TODO oops, not sure if this is correct!
	//BOOST_TEST(utf8_length(u8"a\u00FCb\u00DCc\u00E4d\u00E9e\u00C0f") == 5);
    BOOST_TEST(utf8_length("abcde") == 5);
    BOOST_TEST(utf8_length(u8"abcde") == 5);
}


BOOST_AUTO_TEST_CASE(test_split_utf8_characters)
{
	std::vector<std::string> vs{ u8"a", u8"\u00FC", u8"b", u8"\u00DC", u8"c", u8"\u00E4", u8"d", u8"\u00E9", u8"e", u8"\u00C0", u8"f" };
	BOOST_TEST(split_utf8_characters(u8"a\u00FCb\u00DCc\u00E4d\u00E9e\u00C0f") == vs);
}

BOOST_AUTO_TEST_CASE(test_join)
{
	std::vector<std::string> vs{ "A","B","C" };
	BOOST_TEST(join(vs, ",") == "A,B,C");

	std::vector<std::string> empty{ "" };
	BOOST_TEST(join(empty, ",") == "");
}

BOOST_AUTO_TEST_CASE(test_to_upper)
{
	std::string str = "abc";
	to_upper(str);
	BOOST_TEST(str == "ABC");

	str = "aBc";
	to_upper(str);
	BOOST_TEST(str == "ABC");
}

BOOST_AUTO_TEST_CASE(test_to_lower)
{
	std::string str = "ABC";
	to_lower(str);
	BOOST_TEST(str == "abc");

	str = "aBc";
	to_lower(str);
	BOOST_TEST(str == "abc");
}

BOOST_AUTO_TEST_CASE(test_contains_white_space)
{
	BOOST_TEST(!contains_white_space(""));
	BOOST_TEST(!contains_white_space("a"));
	BOOST_TEST(!contains_white_space("abc"));

	BOOST_TEST(contains_white_space(" "));
	BOOST_TEST(contains_white_space("a b"));
	BOOST_TEST(contains_white_space("a "));
}

BOOST_AUTO_TEST_CASE(test_left_pad)
{
	BOOST_TEST("00000000000000000000000123" == left_pad("123", 26, '0'));
	BOOST_TEST("0000000000000000000000000x" == left_pad("x", 26, '0'));
	BOOST_TEST("0000000000000000000000123x" == left_pad("123x", 26, '0'));
	BOOST_TEST("000000 " == left_pad(" ", 7, '0'));
	BOOST_TEST("0000000" == left_pad("", 7, '0'));
	BOOST_TEST("001 1 1" == left_pad("1 1 1", 7, '0'));
	BOOST_TEST("123" == left_pad("123", 0, '0'));
	BOOST_TEST("123" == left_pad("123", 1, '0'));
	BOOST_TEST("0123"== left_pad("123", 4, '0'));
}

BOOST_AUTO_TEST_SUITE_END()

