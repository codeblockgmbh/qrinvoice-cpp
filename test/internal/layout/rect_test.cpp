
#include <boost/test/unit_test.hpp>

#include <layout/rect_internal.hpp>

using namespace qrinvoice::layout;


namespace {

constexpr int width = 42;
constexpr int height = 21;
constexpr dimension<int> dim{ width, height };

void rect_dim_ctor_test(int x, int y)
{
	const rect<int> r{ x, y, dim };
	BOOST_TEST(r.get_lower_left_x() == x);
	BOOST_TEST(r.get_upper_right_x() == x + width);
	BOOST_TEST(r.get_lower_left_y() == y);
	BOOST_TEST(r.get_upper_right_y() == y + height);
}

} // namespace


BOOST_AUTO_TEST_SUITE(layout)

BOOST_AUTO_TEST_CASE(rect_ctor)
{
	const int width = 5;
	const int height = 3;

	constexpr rect<int> r{ 1, 2, 6, 5 };

	BOOST_TEST(r.get_width() == width);
	BOOST_TEST(r.get_height() == height);

	BOOST_TEST(r.get_lower_left_x() == 1);
	BOOST_TEST(r.get_lower_left_y() == 2);

	BOOST_TEST(r.get_upper_right_x() == 6);
	BOOST_TEST(r.get_upper_right_y() == 5);
}

BOOST_AUTO_TEST_CASE(test_translation_y_to_left_top)
{
	const int width = 5;
	const int height = 3;

	constexpr rect<int> r{ 0, 0, 5, 3 };
	BOOST_TEST(width == r.get_width());
	BOOST_TEST(height == r.get_height());

	BOOST_TEST(r.get_lower_left_x() == 0);
	BOOST_TEST(r.get_lower_left_y() == 0);

	BOOST_TEST(r.get_upper_right_x() == 5);
	BOOST_TEST(r.get_upper_right_y() == 3);

	// ---

	const rect<int> translated_rect = r.translate_y_to_left_top(7);
	BOOST_TEST(translated_rect.get_width() == width);
	BOOST_TEST(translated_rect.get_height() == height);

	BOOST_TEST(translated_rect.get_lower_left_x() == 0);
	BOOST_TEST(translated_rect.get_lower_left_y() == 7);

	BOOST_TEST(translated_rect.get_upper_right_x() == 5);
	BOOST_TEST(translated_rect.get_upper_right_y() == 4);
}

BOOST_AUTO_TEST_CASE(zero_coordinates_from_dim)
{
	const int x = 0;
	const int y = 0;
	rect_dim_ctor_test(x, y);
}

BOOST_AUTO_TEST_CASE(non_zero_coordinates_from_dim)
{
	const int x = 1;
	const int y = 2;
	rect_dim_ctor_test(x, y);
}

BOOST_AUTO_TEST_SUITE_END()

