
#include <boost/test/unit_test.hpp>

#include <layout/dimension_internal.hpp>

using namespace qrinvoice::layout;


BOOST_AUTO_TEST_SUITE(layout)

BOOST_AUTO_TEST_CASE(dimension_ctor)
{
	constexpr int width = 42;
	constexpr int height = 21;
	constexpr dimension<int> dim{ width, height };

	BOOST_TEST(dim.get_width() == width);
	BOOST_TEST(dim.get_height() == height);
}

BOOST_AUTO_TEST_SUITE_END()

