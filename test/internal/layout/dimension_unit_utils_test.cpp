
#include <boost/test/unit_test.hpp>

#include <layout/dimension_unit_utils_internal.hpp>

using namespace qrinvoice::layout;
namespace tt = boost::test_tools;



BOOST_AUTO_TEST_SUITE(layout)

BOOST_AUTO_TEST_CASE(mm2pts_test)
{
	BOOST_TEST(dimension_unit_utils::millimeters_to_points(14) == 39.685, tt::tolerance(0.001));
}

BOOST_AUTO_TEST_CASE(mm2pts_dip_test)
{
	BOOST_TEST(dimension_unit_utils::millimeters_to_points(14, 72) == 39.685, tt::tolerance(0.001));
	BOOST_TEST(dimension_unit_utils::millimeters_to_points(14, 300) == 165.354, tt::tolerance(0.001));
}

BOOST_AUTO_TEST_CASE(mm2ins_test)
{
	BOOST_TEST(dimension_unit_utils::millimeters_to_inches(254) == 10, tt::tolerance(0.0));
}

BOOST_AUTO_TEST_CASE(ins2pts_test)
{
	BOOST_TEST(dimension_unit_utils::inches_to_points(10) == 720, tt::tolerance(0.0));
}

BOOST_AUTO_TEST_CASE(ins2pts_dpi_test)
{
	BOOST_TEST(dimension_unit_utils::inches_to_points(10, 72) == 720, tt::tolerance(0.0));
	BOOST_TEST(dimension_unit_utils::inches_to_points(10, 300) == 3000, tt::tolerance(0.0));
}


BOOST_AUTO_TEST_SUITE_END()

