
#include <boost/test/unit_test.hpp>
#include <boost/regex.hpp>

#include <qrinvoice/model/swiss_payments_code.hpp>

using namespace qrinvoice;


BOOST_AUTO_TEST_SUITE(qr_invoice_spec)


BOOST_AUTO_TEST_CASE(non_zero_coordinates_from_dim)
{
	boost::regex separator_pattern{ qrinvoice::model::spc::element_separator_pattern };
	BOOST_TEST(boost::regex_match(qrinvoice::model::spc::element_separator, separator_pattern));
	BOOST_TEST(boost::regex_match("\r", separator_pattern));
	BOOST_TEST(boost::regex_match("\n", separator_pattern));
	BOOST_TEST(boost::regex_match("\r\n", separator_pattern));
	BOOST_TEST(!boost::regex_match("x", separator_pattern));
	BOOST_TEST(!boost::regex_match(" ", separator_pattern));
}


BOOST_AUTO_TEST_SUITE_END()

