
#include <string>
#include <stdlib.h>
#include <boost/test/unit_test.hpp>

#include <qrinvoice/output_format.hpp>
#include <qrinvoice/output/output.hpp>
#include <qrinvoice/model.hpp>
#include <qrinvoice/code_creator.hpp>
#include <qrinvoice/technical_exception.hpp>

#include <data/test_ibans.hpp>
#include <system/file_util.hpp>
#include <system/target_test_file_helper.hpp>

using namespace qrinvoice;
using namespace qrinvoice::model;
using namespace qrinvoice::test;
using namespace qrinvoice::test::system;


namespace {


qr_invoice create_qr_invoice()
{
	qrinvoice::model::qr_invoice::builder qr_invoice_builder;

	qr_invoice_builder.creditor_iban(test::data::ibans::iban_ch3709000000304442225);

	qr_invoice_builder.payment_amount_info()
		.chf(1949.75);

	qr_invoice_builder.creditor()
		.structured_address()
		.name("Robert Schneider AG")
		.street_name("Rue du Lac")
		.house_number("1268")
		.postal_code("2501")
		.city("Biel")
		.country("CH");

	qr_invoice_builder.ultimate_creditor()
		.structured_address()
		.name("Robert Schneider Services Switzerland AG")
		.street_name("Rue du Lac")
		.house_number("1268/3/1")
		.postal_code("2501")
		.city("Biel")
		.country("CH");

	qr_invoice_builder.ultimate_debtor()
		.structured_address()
		.name("Pia-Maria Rutschmann-Schnyder")
		.street_name("Grosse Marktgasse")
		.house_number("28")
		.postal_code("9400")
		.city("Rorschach")
		.country("CH");

	qr_invoice_builder.payment_reference()
		.creditor_reference("RF18539007547034");

	qr_invoice_builder.additional_information()
		.unstructured_message("Instruction of 03.04.2019")
            .bill_information("//S1/10/10201409/11/190512/20/1400.000-53/30/106017086/31/180508/32/7.7/40/2:10;0:30");

	qr_invoice_builder.alternative_scheme_params({});

	return qr_invoice_builder.build();
}


} // namespace


BOOST_AUTO_TEST_CASE(test_qr_code)
{
    _putenv_s("QRINVOICE_UNLOCK_ULTIMATE_CREDITOR", "true");
	qrinvoice::code_creator cc { create_qr_invoice(), 500, output_format::png };
	const auto qrcode = cc.create_qr_code();

	BOOST_TEST(qrcode.get_size() != 0);

	std::string file_name = "qrinvoice_code_creator_test.png";
	const auto file_path = target_test_file_helper::compose_output_file_path(output_format::png, {"qr_code", file_name});
	file_util::write_to_file(file_path, qrcode);
    _putenv_s("QRINVOICE_UNLOCK_ULTIMATE_CREDITOR", "false");
}

BOOST_AUTO_TEST_CASE(test_qr_code_spec)
{
    _putenv_s("QRINVOICE_UNLOCK_ULTIMATE_CREDITOR", "true");
	qrinvoice::model::qr_invoice qi = create_qr_invoice();
	qrinvoice::code_creator cc;
	cc.qr_invoice(qi);

	const std::string spc = cc.create_swiss_payments_code();
	BOOST_TEST(!spc.empty());

	const auto file_path = target_test_file_helper::compose_output_file_path({
		"swiss_payments_code", "code_creator_test.txt"
	});

	file_util::write_to_file(file_path, spc);
    _putenv_s("QRINVOICE_UNLOCK_ULTIMATE_CREDITOR", "false");
}

BOOST_AUTO_TEST_CASE(test_qr_code_max_size)
{
    _putenv_s("QRINVOICE_UNLOCK_ULTIMATE_CREDITOR", "true");
	qrinvoice::model::qr_invoice qi = create_qr_invoice();
	qrinvoice::code_creator cc { create_qr_invoice(), 5000, output_format::png };
	const auto qrcode = cc.create_qr_code();
	BOOST_TEST(qrcode.get_size() != 0);
    _putenv_s("QRINVOICE_UNLOCK_ULTIMATE_CREDITOR", "false");
}

BOOST_AUTO_TEST_CASE(test_qr_code_too_big)
{
    _putenv_s("QRINVOICE_UNLOCK_ULTIMATE_CREDITOR", "true");
	qrinvoice::model::qr_invoice qi = create_qr_invoice();
	qrinvoice::code_creator cc { create_qr_invoice(), 5000 + 1, output_format::png };
	BOOST_CHECK_THROW(cc.create_qr_code(), qrinvoice::technical_exception);
    _putenv_s("QRINVOICE_UNLOCK_ULTIMATE_CREDITOR", "false");
}


