
#include <string>
#include <boost/regex.hpp>
#include <boost/test/unit_test.hpp>
#include <boost/test/data/test_case.hpp>

#include <qrinvoice/output_format.hpp>
#include <qrinvoice/output/output.hpp>
#include <qrinvoice/model.hpp>
#include <qrinvoice/code_creator.hpp>
#include <qrinvoice/model/mapper/swiss_payments_code_to_model.hpp>
#include <qrinvoice/technical_exception.hpp>
#include <qrinvoice/not_yet_implemented_exception.hpp>

#include <data/test_ibans.hpp>
#include <system/file_util.hpp>
#include <system/test_data_file_registry.hpp>
#include <system/target_test_file_helper.hpp>

using namespace qrinvoice;
using namespace qrinvoice::model;
using namespace qrinvoice::test;
using namespace qrinvoice::test::system;


namespace {

int count_lines(const std::string& str)
{
	if (str.empty())
		return 0;

	const boost::regex pat{ R"(\r\n|\n|\r)" };
	boost::sregex_iterator beg{ str.begin(), str.end(), pat };
	boost::sregex_iterator end{};
	if (beg == end)
		return 1;

	int cnt = 1;
	for (auto p = beg; p != end; ++p)
		cnt++;

	return cnt;
}

test::system::test_data_file_registry file_registry;
auto files = file_registry.data();

} // namespace


BOOST_DATA_TEST_CASE(test_qr_invoice, files)
{
	const std::string spc_str = test_data_file_registry::get_file_content(sample);
	const swiss_payments_code spc_original = parse_swiss_payments_code(spc_str);
	const qr_invoice qi = map_swiss_payments_code_to_model(spc_original);

	qrinvoice::code_creator cc { qi, 500, output_format::png };
	const auto qrcode = cc.create_qr_code();

	BOOST_TEST(qrcode.get_size() != 0);

	std::string file_name = test_data_file_registry::file_name(sample, ".png");
	const auto file_path = target_test_file_helper::compose_output_file_path(output_format::png, {"qr_code", file_name});
	file_util::write_to_file(file_path, qrcode);

	// TODO: read QR code -- this needs QR code reader library.
}

BOOST_DATA_TEST_CASE(test_swiss_payments_code, files)
{
	const std::string spc_str = test_data_file_registry::get_file_content(sample);
	const swiss_payments_code spc_original = parse_swiss_payments_code(spc_str);
	const qr_invoice qi = map_swiss_payments_code_to_model(spc_original);

	qrinvoice::code_creator cc;
	cc.qr_invoice(qi);
	const auto spc = cc.create_swiss_payments_code();

	std::string file_name = test_data_file_registry::file_name(sample, ".txt");
	const auto file_path = target_test_file_helper::compose_output_file_path({"swiss_payments_code", file_name});
	file_util::write_to_file(file_path, spc);

	BOOST_TEST(!spc.empty());
	const int line_count = count_lines(spc);
    BOOST_TEST((line_count >= 31 && line_count <= 34));
}

BOOST_DATA_TEST_CASE(test_unsupported_output_format, files)
{
	const std::string spc_str = test_data_file_registry::get_file_content(sample);
	const swiss_payments_code spc_original = parse_swiss_payments_code(spc_str);
	const qr_invoice qi = map_swiss_payments_code_to_model(spc_original);

	code_creator cc{ qi, 500, output_format::pdf };
	BOOST_CHECK_THROW(cc.create_qr_code(), qrinvoice::not_yet_implemented_exception);
}

