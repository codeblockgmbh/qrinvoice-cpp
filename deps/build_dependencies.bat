cd PDF-Writer

@rem This library by default compiles as a statically linked library. In order to compile it as a dynamically linked library, we need add few lines in few ﬁles: 4> In `LibPng\CMakeLists.txt` append following line:
@rem target_link_libraries(LibPng PRIVATE ${ZLIB_LDFLAGS}) 5> In `PDFWriter\CMakeLists.txt` append following line:
@rem target_link_libraries(PDFWriter ${LIBPNG_LDFLAGS})

@rem > -DCMAKE_WINDOWS_EXPORT_ALL_SYMBOLS=TRUE This will make the library export all the symbols. This is necessary for a dynamically shared library as the source code does not contain any export macros.
@rem 
@rem > -DBUILD_SHARED_LIBS=TRUE This tells it that we want a dynamically shared library(i.e. a DLL) instead of a statically linked library.
@rem 
@rem PDFWriter also supports TIFF & JPG ﬁles but as we aren't using them, we can skip building the related dependencies:
@rem 
@rem > -DPDFHUMMUS_NO_TIFF=1 This will skip building LibTiﬀ.
@rem 
@rem > -DPDFHUMMUS_NO_DCT=1 This will skip building LibJpeg.

mkdir build
cd build

cmake .. -G "Visual Studio 15 2017" -DCMAKE_WINDOWS_EXPORT_ALL_SYMBOLS=TRUE -DBUILD_SHARED_LIBS=TRUE -DPDFHUMMUS_NO_TIFF=1 -DPDFHUMMUS_NO_DCT=1
cmake --build . --config Release

cd ..

mkdir build64
cd build64
cmake .. -G "Visual Studio 15 2017 Win64" -DCMAKE_WINDOWS_EXPORT_ALL_SYMBOLS=TRUE -DBUILD_SHARED_LIBS=TRUE -DPDFHUMMUS_NO_TIFF=1 -DPDFHUMMUS_NO_DCT=1
cmake --build . --config Release